ifdef CYGWIN
SHELL = bash.exe
CGPREFIX = /opt/ti
else
ifdef WSL #Windows
CGPREFIX = /mnt/c/ti
else #linux
CGPREFIX = /opt/TI/C2000CGT5.2.8
endif
endif
BINPATH = ${CGPREFIX}/bin
HEXGEN = ${BINPATH}/hex2000

HEXFLAGS = --section_name_prefix="data" --load_image -o combined.out
HEXFLAGS2 = -i -image -fill 0xffff -o intelHex_absolute.hex hex2000.rc
HEXFLAGS3 = -i -image -zero -fill 0xffff -o intelHex_offset.hex hex2000.rc
APP := $(firstword $(wildcard app/*.out))
BK := $(firstword $(wildcard bk/*.out))

all:
ifeq ($(APP),)
	${MAKE} -k -C app
endif
ifeq ($(BK),)
	${MAKE} -k -C bk
endif
	${MAKE} -k combine #recursive call to force variable reload

combine:
	@echo Combining Files
	@echo ${HEXGEN} ${APP} ${BK} ${HEXFLAGS}
	@${HEXGEN} ${APP} ${BK} ${HEXFLAGS}
	@echo ${HEXGEN} ${APP} ${BK} ${HEXFLAGS2}
	@${HEXGEN} ${APP} ${BK} ${HEXFLAGS2}
	@echo ${HEXGEN} ${APP} ${BK} ${HEXFLAGS3}
	@${HEXGEN} ${APP} ${BK} ${HEXFLAGS3}

clean:
	rm -f *.out *.log *~
	${MAKE} -k -C app clean
	${MAKE} -k -C bk clean

