 /*!
 * \file    sched.h
 * \author  Zach Haver
 * \brief   Scheduler definitions
 *
 */

/* $Id: $ */
/* $Name: $ */

/* NOTICE: This file is the property of Globe Motors and contains */
/* CONFIDENTIAL information.  Any use, copying or dissemination of the */
/* information contained in this file is strictly prohibited unless */
/* authorized by Globe Motors in writing. */

/* Copyright 2016, Globe Motors, Inc. */

/*
 * Revision History:
 *
 * $Log: $
 *
 */

#ifndef SCHED_H
#define SCHED_H

#include "misra.h"

/*! \enum  SchedState_t
 *  \brief Execution states for the scheduler tasks.
 */
typedef enum {
    SCHED_TASK_READY,       /*!< Task is ready to execute. */
    SCHED_TASK_RUNNING,     /*!< Task is currently executing. */
    SCHED_TASK_SLEEPING     /*!< Task is sleeping. */
}SchedState_t;

/*! \struct task_t
 *  \brief  Structure to hold information about tasks to execute.
 */
typedef struct {
    void (*tp)(void);       /*!< pointer to the task function */
    SchedState_t state;     /*!< state of the task */
    UINT_16 sleepCtr;       /*!< jiffies left to sleep */
    UINT_16 period;         /*!< period of execution, in jiffies */
} task_t;

/*! \def SCHED_TASK_MAX - maximum number of tasks the scheduler can process. */
#define SCHED_TASK_MAX      16U

void schedInit(void);
void schedExec(void);
void schedTick(void);
void schedAddTask(void (*tp)(void), UINT_16 period);

#endif /* SCHED_H */
