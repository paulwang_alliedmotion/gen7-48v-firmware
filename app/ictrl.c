/*!
 * \file      ictrl.c
 * \author    Zach Haver
 * \brief     Current control routines for the CPU to interact with the CLA.
 */

/* $Id: $ */
/* $Name: $ */

/* NOTICE: This file is the property of Globe Motors and contains */
/* CONFIDENTIAL information.  Any use, copying or dissemination of the */
/* information contained in this file is strictly prohibited unless */
/* authorized by Globe Motors in writing. */

/* Copyright 2016, Globe Motors, Inc. */

/*
 * Revision History:
 *
 * $Log: $
 *
 */

#include "adc.h"
#include "claShared.h"
#include "dtc.h"
#include "eeprom.h"
#include "eqep.h"
#include "ictrl.h"
#include "IQmathLib.h"
#include "misra.h"
#include "opstate.h"
#include "utils.h"

/* private variable declarations */

/*!
 * \brief   Q-axis reference current
 */
static _iq ictrlQ = 0L;
/*!
 * \brief   D-axis reference current
 */
static _iq ictrlD = 0L;
/*!
 * \brief   Current multiplication factor to reduce available assist
 *          in faulted situations
 */
static _iq ictrlMultiplier = _IQ(1.0L);
/*!
 * \brief   Maximum allowable current command
 */
static _iq ictrlMax = _IQ(60.0L);
/*!
 * \brief   pointer to transfer Q-axis reference info to the CLA
 */
#pragma DATA_SECTION (q_cpu, "CpuToClaMsgRAM");
static FLOAT_32 q_cpu;
/*!
 * \brief   pointer to transfer D-axis reference info to the CLA
 */
#pragma DATA_SECTION (d_cpu, "CpuToClaMsgRAM");
static FLOAT_32 d_cpu;

/* public functions */

/*!
 * \fn      void ictrlInit (void)
 *
 * \brief   Initialization of the current control variables
 */
void
ictrlInit(void) {
    piQRef_cpu = &q_cpu;
    piDRef_cpu = &d_cpu;
    q_cpu = 0.0;
    d_cpu = 0.0;
}

void
ictrlUpdateILimit(void) {
    UINT_8 data[4] = {0U, 0U, 0U, 0U};

    if (EE_ERR_NONE == eepromReadXBytes(EEADDR_I_LIM, data, 4U)) {
        ictrlMax = bytesToIQ(data);
    }
}

/*!
 * \fn      void ictrlSetRef (_iq qRef, _iq dRef)
 *
 * \param   q_ref - desired Q-axis reference current
 * \param   d_ref - desired D-axis reference current
 * \brief   This function sets the desired reference values
 *          for the Q-axis and D-axis
 */
void
ictrlSetRef(_iq qRef, _iq dRef) {
    _iq max = ictrlMax;
    _iq negMax = _IQmpy (max, _IQ(-1.0L));
    _iq q;
    _iq d;

    q = _IQmpy (qRef, ictrlMultiplier);
    d = _IQmpy (dRef, ictrlMultiplier);

    if (q > max) {
        q = max;
    } else if (q < negMax) {
        q = negMax;
    } else {
        /* in range */
    }

    if (d > _IQ(60.0L)) {
        d = _IQ(60.0L);
    } else if (d < 0L) {
        d = 0L;
    } else {
        /* in range */
    }

    ictrlQ = q;
    ictrlD = d;

    q_cpu = _IQtoF(ictrlQ);
    d_cpu = _IQtoF(ictrlD);
}

/*!
 * \fn      void ictrlSetMultiplier (_iq mult)
 *
 * \param   mult - desired multiplier value
 * \brief   This function sets the current multiplier value
 */
void
ictrlSetMultiplier(_iq mult) {
    ictrlMultiplier = mult;
}

/*!
 * \fn    void ictrlExcessChk(void) {
 *
 * \brief This function checks to make sure we are closing the error
 *        between our commanded and feedback Q-axis current.
 */
void
ictrlIExcessChk(void) {
    _iq lclQRef = ictrlQ;
    _iq lclQFbk = _IQ(parkQs_cla);
    _iq iError = 0L;
    UINT_32 vCount = (UINT_32)adcGetRaw(ADC_BATT_ON);
    UINT_32 vShifted = vCount << 18;
    _iq vbat = (_iq)vShifted;
//    _iq limitSpeed = _IQmpy(vbat, _IQ(0.62637362637L));/* GEN7 VBat * 57RPM */
    _iq limitSpeed = _IQmpy(vbat, _IQ(0.53171641790L));/* GEN7HV */

    static UINT_16 iErrorCtr = 0U;


    /* check for disparity across zero */
    if (0L <= lclQRef) {
        /* command is positive */
        iError = lclQRef - lclQFbk;
    } else {
        /* command is negative */
        iError = _IQmpy(_IQ(-1.0L), (lclQRef - lclQFbk));
    }

    if (0L <= iError) {
        /* less than command */
        if (_IQabs(eqepGetVelocity()) < limitSpeed) {
            if (_IQ(20.0L) < iError) {
                if (512U > iErrorCtr) {
                    iErrorCtr += 2U;
                }
            } else {
                if (3U < iErrorCtr) {
                    iErrorCtr = 0U;
                }
            }
        }
    } else {
        /* greater than command */
        if (_IQ(-20.0L) > iError) {
             if (512U > iErrorCtr) {
                iErrorCtr += 2U;
             }
        } else {
            if (3U < iErrorCtr) {
                iErrorCtr -= 2U;
            }
        }
    }

    if (250U < iErrorCtr) {
        dtcSet(DTCHW_I_EXCESS);
    }
}
