/*!
 * \file      pie.c
 * \author    Zach Haver
 * \brief     Peripheral Interrupt Expansion controller initialization
 *            functions for the TI TMS320C28033/35 Piccolo.
 */

/* $Id: $ */
/* $Name: $ */

/* NOTICE: This file is the property of Globe Motors and contains */
/* CONFIDENTIAL information.  Any use, copying or dissemination of the */
/* information contained in this file is strictly prohibited unless */
/* authorized by Globe Motors in writing. */

/* Copyright 2016, Globe Motors, Inc. */

/*
 * Revision History:
 *
 * $Log: $
 *
 */

#include "piccolo.h"
#include "pie.h"
#include "isr.h"

/* public functions */

/*!
 * \fn     void pieInit (void)
 * \brief  Initialize and enable the PIE interrupts on the F280x
 */
void
pieInit(void) {
    /* Initialize the PIE_RAM */

    /* Enable EALLOW protected register access */
    PICCOLO_EALLOW();

    /* Initialize Vector Maps */
    isrVectorInit();

    /* Set PIECTRL.ENPIE to 1: enable the PIE */
    *PIECTRL = 0x0001U;

    /* Disable all PIE interrupts */
    *PIEIER1 = 0x0000U;
    *PIEIER2 = 0x0000U;
    *PIEIER3 = 0x0000U;
    *PIEIER4 = 0x0000U;
    *PIEIER5 = 0x0000U;
    *PIEIER6 = 0x0000U;
    *PIEIER7 = 0x0000U;
    *PIEIER8 = 0x0000U;
    *PIEIER9 = 0x0000U;
    *PIEIER10 = 0x0000U;
    *PIEIER11 = 0x0000U;
    *PIEIER12 = 0x0000U;

    /* Clear any potentially pending PIEIFR flags */
    *PIEIFR1 = 0x0000U;
    *PIEIFR2 = 0x0000U;
    *PIEIFR3 = 0x0000U;
    *PIEIFR4 = 0x0000U;
    *PIEIFR5 = 0x0000U;
    *PIEIFR6 = 0x0000U;
    *PIEIFR7 = 0x0000U;
    *PIEIFR8 = 0x0000U;
    *PIEIFR9 = 0x0000U;
    *PIEIFR10 = 0x0000U;
    *PIEIFR11 = 0x0000U;
    *PIEIFR12 = 0x0000U;

    /* Acknowlege all PIE interrupt groups */
    *PIEACK = 0xffffU;

    /* enable interrupt groups */
    /* enable ADC and external interrupts */
    *PIEIER1 = 0x001bU;
    /* enable EPWM5 interrupt */
    *PIEIER3 = 0x0010U;
    /* enable ECAP interrupt */
    *PIEIER4 = 0x0001U;
    /* enable SPI interrupt */
    *PIEIER6 = 0x0001U;
    /* enable I2C interrupts */
    *PIEIER8 = 0x0003U;
    /* enable CAN interrupt */
    *PIEIER9 = 0x0020U;
    /* enable CLA1 and CLA2 finished interrupt */
    *PIEIER11 = 0x0003U;
    /* enable external3 interrupt */
    *PIEIER12 = 0x0001U;

    /* Disable EALLOW protected register access */
    PICCOLO_EDIS();
}
