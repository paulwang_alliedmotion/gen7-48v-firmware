/*!
 * \file      sys.c
 * \author    Zach Haver
 * \brief     System initialization functions for the TI TMS320C28035.
 */

/* $Id: $ */
/* $Name: $ */

/* NOTICE: This file is the property of Globe Motors and contains */
/* CONFIDENTIAL information.  Any use, copying or dissemination of the */
/* information contained in this file is strictly prohibited unless */
/* authorized by Globe Motors in writing. */

/* Copyright 2006, Globe Motors, Inc. */

/*
 * Revision History:
 *
 * $Log: $
 *
 */

#include "piccolo.h"
#include "misra.h"
#include "sys.h"

/* private function declarations */

/* private variables */

/* public functions */

void
sysReset(void) {
    PICCOLO_EALLOW ();
    /* Write an illegal value to the WDCR to force a reset */
    *WDCR = 0U;
}
