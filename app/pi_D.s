;//###########################################################################
;//
;// FILE:  pi_D.s
;//
;// TITLE: D-axis pi calculation Assembly Code.
;//
;// This file contains the D-axis pi calculation assembly code.
;//
;//###########################################################################

pi_d_CLA        .macro

_pi_d_CLA_START
        MMOV16  MAR0,@_piDRef_cpu
        MNOP
        MNOP
        MNOP
        MMOV32  MR0,*MAR0
        MMOV32  @_piDRef_cla,MR0 ;ref
        MMOV32  MR1,@_parkDs_cla ;fdbk

        MSUBF32 MR0,MR0,MR1     ;ref - fdbk
        MMOV32  @_piDUP_cla,MR0 ;up = ref - fbdk

        MMOV32  MR1,@_piDOut_cla;out
        MMOV32  MR2,@_piDV1_cla ;v1
        MCMPF32 MR1,MR2         ;out == v1
        MNOP
        MNOP
        MNOP
        MBCNDD  cmpNotEqD,NEQ
        MNOP
        MNOP
        MNOP
        MMOVF32 MR3,#0.000088   ;Ki
        MMPYF32 MR3,MR3,MR0     ;Ki * up
        MMOV32  MR1,@_piDI1_cla
        MADDF32 MR3,MR3,MR1     ;(ki * up) + i1
        MBCNDD  iCompD
        MNOP
        MNOP
        MNOP
cmpNotEqD
        MMOV32  MR3,@_piDI1_cla
iCompD
        MMOV32  @_piDUI_cla,MR3 ;ui
        MMOV32  @_piDI1_cla,MR3 ;i1 = ui

        MADDF32 MR1,MR0,MR3     ;up + ui
        MMOVF32 MR2,#0.0352     ;Kp
        MMPYF32 MR1,MR1,MR2     ;Kp * (up + ui)
        MMOV32  @_piDV1_cla,MR1

        MMOVF32 MR2,#0.65       ;Umax
        MMINF32 MR1,MR2
        MMOVF32 MR2,#-0.65      ;Umin
        MMAXF32 MR1,MR2
        MMOV32  @_piDOut_cla,MR1

_pi_d_CLA_END
        .endm
