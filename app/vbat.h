/*!
 * \file      vbat.h
 * \author    Zach Haver
 * \brief     Battery voltage monitoring definitions.
 */

/* $Id: $ */
/* $Name: $ */

/* NOTICE: This file is the property of Globe Motors and contains */
/* CONFIDENTIAL information.  Any use, copying or dissemination of the */
/* information contained in this file is strictly prohibited unless */
/* authorized by Globe Motors in writing. */

/* Copyright 2016, Globe Motors, Inc. */

/*
 * Revision History:
 *
 * $Log: $
 *
 */

#ifndef VBAT_H
#define VBAT_H

#include "misra.h"

/* temp limit definitions */
/* GEN7 91 counts/volt */
/* GEN7HV 26.8 counts/volt */

/* VBAT LOW SET = 36V */
#define VBAT_LOW_SET                965U
/* VBAT LOW SET = 37V */
#define VBAT_LOW_REC                992U

#define VBAT_LOW_TIMEOUT             20U


/* VBAT LOW SET = 64V */
#define VBAT_HIGH_SET              1715U
/* VBAT LOW SET = 63V */
#define VBAT_HIGH_REC              1688U

#define VBAT_HIGH_TIMEOUT            20U

/* function prototypes */
void vbatChk(void);

#endif /* VBAT_H */
