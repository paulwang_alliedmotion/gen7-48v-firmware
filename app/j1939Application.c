/*!
 * \file      j1939Application.c
 * \author    Zach Haver
 * \brief     Implementation of the J1939 application layer.
 *
 */

/* $Id: $ */
/* $Name: $ */

/* NOTICE: This file is the property of Globe Motors and contains */
/* CONFIDENTIAL information.  Any use, copying or dissemination of the */
/* information contained in this file is strictly prohibited unless */
/* authorized by Globe Motors in writing. */

/* Copyright 2016, Globe Motors, Inc. */

/*
 * Revision History:
 *
 * $Log: $
 *
 */

#include <string.h> /* for NULL */

#include "adc.h"
#include "claShared.h"
#include "config.h"
//#include "drv8305.h"
#include "ecap.h"
#include "eeprom.h"
#include "eqep.h"
#include "IQmathLib.h"
#include "j1939Application.h"
#include "j1939DataLink.h"
#include "j1939Diagnostic.h"
#include "mctrl.h"
#include "misra.h"
#include "opstate.h"
#include "softid.h"
#include "tctrl.h"
#include "tle5012.h"
#include "tmr0.h"
#include "utils.h"

/* private function prototypes */
static void jappCCVS(const J1939msg_t *pmsg);
static void jappEEC1(const J1939msg_t *pmsg);
static void jappPropB00(const J1939msg_t *pmsg);
static void jappPropB40(const J1939msg_t *pmsg);
static void jappPropB50(const J1939msg_t *pmsg);
static void jappPropB60(const J1939msg_t *pmsg);
static void jappTxFF20(void);
static void jappTxFF21(void);
static void jappTxFF22(void);
static void jappTxFF23(void);
static void jappTxFF24(void);
static void jappTxFF25(void);
static void jappTxFF26(void);
static void jappTxFF27(void);
static void jappTxFF28(void);
static void jappTxFF29(void);
static void jappTxFF2A(void);//using FF21 timing right now.
static void jappTxDTHN1(void);
static void jappTxDTHN2(void);
static void jappTxDTHN3(void);
static void jappTxDTHN4(void);
static void jappTxDTHN5(void);
static void jappTxDTHN6(void);

/* private variables */
static JAPPDM13State_t jappDM13State = JAPP_DM13_TRANSMIT;
static UINT_16 jappVS = 0U;
static UINT_16 jappES = 0U;
static UINT_16 jappMsgEn = 0xfc00U;
static UINT_16 jappFdbkTime0 = 100U;
static UINT_16 jappFdbkTime1 = 100U;
static UINT_16 jappFdbkTime2 = 100U;
static UINT_16 jappFdbkTime3 = 100U;
static UINT_16 jappFdbkTime4 = 100U;
static UINT_16 jappFdbkTime5 = 100U;
static UINT_16 jappFdbkTime6 = 100U;
static UINT_16 jappFdbkTime7 = 100U;
static UINT_16 jappFdbkTime8 = 100U;
static UINT_16 jappFdbkTime9 = 100U;
static UINT_16 jappDTHNTime1 = 100U;
static UINT_16 jappDTHNTime2 = 100U;
static UINT_16 jappDTHNTime3 = 100U;
static UINT_16 jappDTHNTime4 = 100U;
static UINT_16 jappDTHNTime5 = 100U;
static UINT_16 jappDTHNTime6 = 100U;
static UINT_16 jappFDBKEn = 0x03ffU;
static UINT_16 jappDTHNEn = 0xfc00U;

static JAPPTxState_t txState = JAPP_DTHN;

static UINT_16 ramTestVar = 1U;

/* public functions */
void
jappInit(void) {
    if (CONFIG_VALID == configGetState()) {
        jappFDBKEn = 0U;
        jappDTHNEn = 0U;
        jappFdbkTime0 = *MAP_U16(CONFIG_FDBK0);
        if (0U != jappFdbkTime0) {
            jappFDBKEn |= 0x1U;
        }
        jappFdbkTime1 = *MAP_U16(CONFIG_FDBK1);
        if (0U != jappFdbkTime1) {
            jappFDBKEn |= 0x2U;
        }
        jappFdbkTime2 = *MAP_U16(CONFIG_FDBK2);
        if (0U != jappFdbkTime2) {
            jappFDBKEn |= 0x4U;
        }
        jappFdbkTime3 = *MAP_U16(CONFIG_FDBK3);
        if (0U != jappFdbkTime3) {
            jappFDBKEn |= 0x8U;
        }
        jappFdbkTime4 = *MAP_U16(CONFIG_FDBK4);
        if (0U != jappFdbkTime4) {
            jappFDBKEn |= 0x10U;
        }
        jappFdbkTime5 = *MAP_U16(CONFIG_FDBK5);
        if (0U != jappFdbkTime5) {
            jappFDBKEn |= 0x20U;
        }
        jappFdbkTime6 = *MAP_U16(CONFIG_FDBK6);
        if (0U != jappFdbkTime6) {
            jappFDBKEn |= 0x40U;
        }
        jappFdbkTime7 = *MAP_U16(CONFIG_FDBK7);
        if (0U != jappFdbkTime7) {
            jappFDBKEn |= 0x80U;
        }
        jappFdbkTime8 = *MAP_U16(CONFIG_FDBK8);
        if (0U != jappFdbkTime8) {
            jappFDBKEn |= 0x100U;
        }
        jappFdbkTime9 = *MAP_U16(CONFIG_FDBK9);
        if (0U != jappFdbkTime9) {
            jappFDBKEn |= 0x200U;
        }
        jappDTHNTime1 = *MAP_U16(CONFIG_DTHN1);
        if (0U != jappDTHNTime1) {
            jappDTHNEn |= 0x400U;
        }
        jappDTHNTime2 = *MAP_U16(CONFIG_DTHN2);
        if (0U != jappDTHNTime2) {
            jappDTHNEn |= 0x800U;
        }
        jappDTHNTime3 = *MAP_U16(CONFIG_DTHN3);
        if (0U != jappDTHNTime3) {
            jappDTHNEn |= 0x1000U;
        }
        jappDTHNTime4 = *MAP_U16(CONFIG_DTHN4);
        if (0U != jappDTHNTime4) {
            jappDTHNEn |= 0x2000U;
        }
        jappDTHNTime5 = *MAP_U16(CONFIG_DTHN5);
        if (0U != jappDTHNTime5) {
            jappDTHNEn |= 0x4000U;
        }
        jappDTHNTime6 = *MAP_U16(CONFIG_DTHN6);
        if (0U != jappDTHNTime6) {
            jappDTHNEn |= 0x8000U;
        }
    }
    jappMsgEn =jappDTHNEn;
}

void
jappMessageProcess(const J1939msg_t *pmsg) {
    switch(pmsg->pgn) {
    case JAPP_PGN_CCVS:
        jappCCVS(pmsg);
        break;
    case JAPP_PGN_EEC1:
        jappEEC1(pmsg);
        break;
    case JAPP_PGN_FF00:
        jappPropB00(pmsg);
        break;
    case JAPP_PGN_FF40:
        jappPropB40(pmsg);
        break;
    case JAPP_PGN_FF50:
        jappPropB50(pmsg);
        break;
    case JAPP_PGN_FF60:
        jappPropB60(pmsg);
        break;
    default:
        /* Unused message discard */
        break;
    }
}

void
jappTimer(void) {
    static UINT_16 fdbk0Ctr =0U;
    static UINT_16 fdbk1Ctr =1U;
    static UINT_16 fdbk2Ctr =2U;
    static UINT_16 fdbk3Ctr =3U;
    static UINT_16 fdbk4Ctr =4U;
    static UINT_16 fdbk5Ctr =5U;
    static UINT_16 fdbk6Ctr =6U;
    static UINT_16 fdbk7Ctr =7U;
    static UINT_16 fdbk8Ctr =8U;
    static UINT_16 fdbk9Ctr =9U;
    static UINT_16 dthn1Ctr = 0U;
    static UINT_16 dthn2Ctr = 1U;
    static UINT_16 dthn3Ctr = 2U;
    static UINT_16 dthn4Ctr = 3U;
    static UINT_16 dthn5Ctr = 4U;
    static UINT_16 dthn6Ctr = 5U;

    if (JAPP_DM13_HOLD != jappDM13State) {
        if ((jappFdbkTime0 <= ++fdbk0Ctr) &&
            (0U != jappFdbkTime0) &&
            (0U != (jappMsgEn & 0x1U))) {
            fdbk0Ctr = 0U;
            jappTxFF20();
        }
        if ((jappFdbkTime1 <= ++fdbk1Ctr) &&
            (0U != jappFdbkTime1) &&
            (0U != (jappMsgEn & 0x2U))) {
            fdbk1Ctr = 0U;
            jappTxFF21();
            jappTxFF2A();
        }
        if ((jappFdbkTime2 <= ++fdbk2Ctr) &&
            (0U != jappFdbkTime2) &&
            (0U != (jappMsgEn & 0x4U))) {
                fdbk2Ctr = 0U;
            jappTxFF22();
        }
        if ((jappFdbkTime3 <= ++fdbk3Ctr) &&
            (0U != jappFdbkTime3) &&
            (0U != (jappMsgEn & 0x8U))) {
            fdbk3Ctr = 0U;
            jappTxFF23();
        }
        if ((jappFdbkTime4 <= ++fdbk4Ctr) &&
            (0U != jappFdbkTime4) &&
            (0U != (jappMsgEn & 0x10U))) {
            fdbk4Ctr = 0U;
            jappTxFF24();
        }
        if ((jappFdbkTime5 <= ++fdbk5Ctr) &&
            (0U != jappFdbkTime5) &&
            (0U != (jappMsgEn & 0x20U))) {
            fdbk5Ctr = 0U;
            jappTxFF25();
        }
        if ((jappFdbkTime6 <= ++fdbk6Ctr) &&
            (0U != jappFdbkTime6) &&
            (0U != (jappMsgEn & 0x40U))) {
            fdbk6Ctr = 0U;
            jappTxFF26();
        }
        if ((jappFdbkTime7 <= ++fdbk7Ctr) &&
            (0U != jappFdbkTime7) &&
            (0U != (jappMsgEn & 0x80U))) {
            fdbk7Ctr = 0U;
            jappTxFF27();
        }
        if ((jappFdbkTime8 <= ++fdbk8Ctr) &&
            (0U != jappFdbkTime8) &&
            (0U != (jappMsgEn & 0x100U))) {
            fdbk8Ctr = 0U;
            jappTxFF28();
        }
        if ((jappFdbkTime9 <= ++fdbk9Ctr) &&
            (0U != jappFdbkTime9) &&
            (0U != (jappMsgEn & 0x200U))) {
            fdbk9Ctr = 0U;
            jappTxFF29();
        }
        if ((jappDTHNTime1 <= ++dthn1Ctr) &&
            (0U != jappDTHNTime1) &&
            (0U != (jappMsgEn & 0x400U))) {
            dthn1Ctr = 0U;
            jappTxDTHN1();
        }
        if ((jappDTHNTime2 <= ++dthn2Ctr) &&
            (0U != jappDTHNTime2) &&
            (0U != (jappMsgEn & 0x800U))) {
            dthn2Ctr = 0U;
            jappTxDTHN2();
        }
        if ((jappDTHNTime3 <= ++dthn3Ctr) &&
            (0U != jappDTHNTime3) &&
            (0U != (jappMsgEn & 0x1000U))) {
            dthn3Ctr = 0U;
            jappTxDTHN3();
        }
        if ((jappDTHNTime4 <= ++dthn4Ctr) &&
            (0U != jappDTHNTime4) &&
            (0U != (jappMsgEn & 0x2000U))) {
            dthn4Ctr = 0U;
            jappTxDTHN4();
        }
        if ((jappDTHNTime5 <= ++dthn5Ctr) &&
            (0U != jappDTHNTime5) &&
            (0U != (jappMsgEn & 0x4000U))) {
            dthn5Ctr = 0U;
            jappTxDTHN5();
        }
        if ((jappDTHNTime6 <= ++dthn6Ctr) &&
            (0U != jappDTHNTime6) &&
            (0U != (jappMsgEn & 0x8000U))) {
            dthn6Ctr = 0U;
            jappTxDTHN6();
        }
    }
}

void
jappSetDM13State(JAPPDM13State_t newState) {
    jappDM13State = newState;
}

void
jappSetTxState(JAPPTxState_t newState) {
    txState = newState;
    switch (txState) {
    case JAPP_ALL:
        jappMsgEn = 0xffffU;
        break;
    case JAPP_FDBK:
        jappMsgEn = jappFDBKEn;
        break;
    case JAPP_DTHN:
    default:
        jappMsgEn = jappDTHNEn;
        break;
    }
}

/*!
 * \fn    void jappFEDA(const J1939RxMsg_t *pmsg)
 *
 * \brief This function creates and sends the SOFT ID.
 */
void
jappFEDA(const J1939msg_t *pmsg) {
    J1939msg_t feda;
    UINT_8 data[SOFTID_MAX_LENGTH];

    feda.fp = NULL;
    feda.pri = pmsg->pri;
    feda.pgn = JAPP_PGN_FEDA;
    feda.dst = pmsg->src;
    feda.src = JDL_OUR_ADDY;
    feda.dlc = softIdLoadData(data);
    feda.dIdx = jdlAddMsgData(data, feda.dlc, TX_DATA);
    jdlJ1939ToCAN(&feda);
}

void
jappChangeRAMTest(void) {
    ramTestVar = 0xffffU;
}

static void
jappCCVS(const J1939msg_t *pmsg) {
    UINT_8 data[8];

    jdlGetMsgData(data, pmsg->dIdx, pmsg->dlc, RX_DATA);

    jappVS = bytesToU16(&data[1], LSB);
}

static void
jappEEC1(const J1939msg_t *pmsg) {
    UINT_8 data[8];

    jdlGetMsgData(data, pmsg->dIdx, pmsg->dlc, RX_DATA);

    jappES = bytesToU16(&data[3], LSB);
}

static void
jappPropB00(const J1939msg_t *pmsg) {
    UINT_8 data[8];
    MCTRLCMD_t cmd;
    UINT_32 var1;
    UINT_32 var2;

    jdlGetMsgData(data, pmsg->dIdx, pmsg->dlc, RX_DATA);

    var1 = bytesToU32(&data[2], LSB);
    var2 = (UINT_32)bytesToU16(&data[6], LSB);
    var2 <<= 10;
    if (0x80U == (data[7] & 0x80U)) {
        var2 |= 0xfc000000UL;
    }
    cmd.v1 = _IQ20toIQ((_iq)var1);
    cmd.v2 = (_iq)var2;
    cmd.mode = data[0];
    mctrlSetCMD(cmd);
}

static void
jappPropB40(const J1939msg_t *pmsg) {
    UINT_8 data[8];
    J1939msg_t ff41;
    UINT_16 reg;
    UINT_16 regData;

    jdlGetMsgData(data, pmsg->dIdx, pmsg->dlc, RX_DATA);

    reg = (UINT_16)data[1];
    regData = bytesToU16(&data[2], LSB);
    data[1] = 0xffU;
    data[2] = 0xffU;
    data[3] = 0xffU;

    if (0U == data[0]) {
        /* read */
//        if (DRV_SUCCESS == drvGetReg(reg, &regData)) {
            data[0] = 0U;
            data[1] = reg;
            data[2] = (regData & 0xffU);
            data[3] = (regData >> 8);
//        } else {
//            data[0] = 0xfcU;
//        }
    } else if (1U == data[0]) {
        /* write */
//        if (DRV_SUCCESS == drvSetReg(reg, regData)) {
            data[0] = 1U;
//        } else {
//            data[0] = 0xfdU;
//        }
    } else {
        /* NACK */
        data[0] = 0xfeU;
    }
    ff41.fp = NULL;
    ff41.pri = 6U;
    ff41.pgn = JAPP_PGN_FF41;
    ff41.dst = JDL_GLOBAL_ADDY;
    ff41.src = JDL_OUR_ADDY;
    ff41.dlc = 4U;
    ff41.dIdx = jdlAddMsgData(data, ff41.dlc, TX_DATA);
    jdlJ1939ToCAN(&ff41);
}

static void
jappPropB50(const J1939msg_t *pmsg) {
    UINT_8 data[8];
    J1939msg_t ff51;
    UINT_16 reg;
    UINT_16 regData;

    jdlGetMsgData(data, pmsg->dIdx, pmsg->dlc, RX_DATA);

    reg = bytesToU16(&data[1], LSB);

    switch (data[0]) {
    case 0:
        /* read TLE */
        if (TLE_SUCCESS == tleGetReg(reg, &regData)) {
            data[0] = 0U;
            u16ToBytes(regData, &data[3], LSB);
        } else {
            data[0] = 0xfcU;
            data[3] = 0xffU;
            data[4] = 0xffU;
        }
        break;
    case 1:
        /* write TLE */
        regData = bytesToU16(&data[3], LSB);
        if (TLE_SUCCESS == tleSetReg(reg, regData)) {
            data[0] = 1U;
        } else {
            data[0] = 0xfdU;
        }
        break;
    default:
        data[0] = 0xfeU;
        break;
    }
    ff51.fp = NULL;
    ff51.pri = 6U;
    ff51.pgn = JAPP_PGN_FF51;
    ff51.dst = JDL_GLOBAL_ADDY;
    ff51.src = JDL_OUR_ADDY;
    ff51.dlc = 5U;
    ff51.dIdx = jdlAddMsgData(data, ff51.dlc, TX_DATA);
    jdlJ1939ToCAN(&ff51);
}

static void
jappPropB60(const J1939msg_t *pmsg) {
    UINT_8 data[8];
    J1939msg_t ff61;
    UINT_16 eeAddy;
    UINT_16 eeData;

    jdlGetMsgData(data, pmsg->dIdx, pmsg->dlc, RX_DATA);

    eeAddy = bytesToU16(&data[1], LSB);

    switch (data[0]) {
    case 0:
        /* read EEPROM */
        if (EE_ERR_NONE == eepromReadXBytes(eeAddy, &data[3], 2)) {
            data[0] = 0U;
        } else {
            data[0] = 0xfcU;
        }
        break;
    case 1:
        /* write EEPROM */
        eeData = bytesToU16(&data[3], LSB);
        if (EE_ERR_NONE == eepromWriteWord(eeAddy, eeData)) {
            data[0] = 1U;
        } else {
            data[0] = 0xfdU;
        }
        break;
    default:
        data[0] = 0xfeU;
        break;
    }
    ff61.fp = NULL;
    ff61.pri = 6U;
    ff61.pgn = JAPP_PGN_FF61;
    ff61.dst = JDL_GLOBAL_ADDY;
    ff61.src = JDL_OUR_ADDY;
    ff61.dlc = 5U;
    ff61.dIdx = jdlAddMsgData(data, ff61.dlc, TX_DATA);
    jdlJ1939ToCAN(&ff61);
}

static void
jappTxFF20(void) {
    J1939msg_t ff20;
    UINT_8 data[8];
    _iq qCmd = _IQ((FLOAT_64)piQRef_cla);
    _iq qFdbk = _IQ((FLOAT_64)parkQs_cla);

    ff20.fp = NULL;
    ff20.pri = 6U;
    ff20.pgn = JAPP_PGN_FF20;
    ff20.dst = JDL_GLOBAL_ADDY;
    ff20.src = JDL_OUR_ADDY;
    ff20.dlc = 8U;
    u32ToBytes((UINT_32)qCmd, data, LSB);
    u32ToBytes((UINT_32)qFdbk, &data[4], LSB);
    ff20.dIdx = jdlAddMsgData(data, ff20.dlc, TX_DATA);
    jdlJ1939ToCAN(&ff20);
}

static void
jappTxFF21(void) {
    J1939msg_t ff21;
    UINT_8 data[8];
    UINT_16 aOn = adcGetFilt(ADC_ON_A);
    UINT_16 bOn = adcGetFilt(ADC_ON_B);
    UINT_16 cOn = adcGetFilt(ADC_ON_C);
    UINT_16 t1 = adcGetFilt(ADC_T1);

    ff21.fp = NULL;
    ff21.pri = 6U;
    ff21.pgn = JAPP_PGN_FF21;
    ff21.dst = JDL_GLOBAL_ADDY;
    ff21.src = JDL_OUR_ADDY;
    ff21.dlc = 8U;
    u16ToBytes(aOn, data, LSB);
    u16ToBytes(bOn, &data[2], LSB);
    u16ToBytes(cOn, &data[4], LSB);
    u16ToBytes(t1, &data[6], LSB);
    ff21.dIdx = jdlAddMsgData(data, ff21.dlc, TX_DATA);
    jdlJ1939ToCAN(&ff21);
}

static void
jappTxFF22(void) {
    J1939msg_t ff22;
    UINT_8 data[8];
    UINT_16 aOff = adcGetFilt(ADC_OFF_A);
    UINT_16 bOff = adcGetFilt(ADC_OFF_B);
    UINT_16 cOff = adcGetFilt(ADC_OFF_C);
    UINT_16 t2 = adcGetFilt(ADC_T2);

    ff22.fp = NULL;
    ff22.pri = 6U;
    ff22.pgn = JAPP_PGN_FF22;
    ff22.dst = JDL_GLOBAL_ADDY;
    ff22.src = JDL_OUR_ADDY;
    ff22.dlc = 8U;
    u16ToBytes(aOff, data, LSB);
    u16ToBytes(bOff, &data[2], LSB);
    u16ToBytes(cOff, &data[4], LSB);
    u16ToBytes(t2, &data[6], LSB);
    ff22.dIdx = jdlAddMsgData(data, ff22.dlc, TX_DATA);
    jdlJ1939ToCAN(&ff22);
}

static void
jappTxFF23(void) {
    J1939msg_t ff23;
    UINT_8 data[8];
    UINT_16 batt = adcGetFilt(ADC_BATT_ON);
    UINT_16 vref = adcGetFilt(ADC_VREF2);
    UINT_16 dtemp = adcGetFilt(ADC_DTEMP);
    UINT_16 tlePos = 0xffffU;

    (void)tleGetPos(&tlePos);

    ff23.fp = NULL;
    ff23.pri = 6U;
    ff23.pgn = JAPP_PGN_FF23;
    ff23.dst = JDL_GLOBAL_ADDY;
    ff23.src = JDL_OUR_ADDY;
    ff23.dlc = 8U;
    u16ToBytes(batt, data, LSB);
    u16ToBytes(vref, &data[2], LSB);
    u16ToBytes(dtemp, &data[4], LSB);
    u16ToBytes(tlePos, &data[6], LSB);
    ff23.dIdx = jdlAddMsgData(data, ff23.dlc, TX_DATA);
    jdlJ1939ToCAN(&ff23);
}

static void
jappTxFF24(void) {
    J1939msg_t ff24;
    UINT_8 data[8];
    _iq tIn = tctrlGetTin();
    _iq tOut = tctrlGetOut();

    ff24.fp = NULL;
    ff24.pri = 6U;
    ff24.pgn = JAPP_PGN_FF24;
    ff24.dst = JDL_GLOBAL_ADDY;
    ff24.src = JDL_OUR_ADDY;
    ff24.dlc = 8U;
    u32ToBytes((UINT_32)tIn, data, LSB);
    u32ToBytes((UINT_32)tOut, &data[4], LSB);
    ff24.dIdx = jdlAddMsgData(data, ff24.dlc, TX_DATA);
    jdlJ1939ToCAN(&ff24);
}

static void
jappTxFF25(void) {
    J1939msg_t ff25;
    UINT_8 data[8];
    _iq vel = eqepGetVelocity();
    UINT_16 opF = opsGetFlags();
    UINT_16 thetaMode = (UINT_16)eqepGetThetaMode();
    UINT_16 discreteState = 0U;

    if (0UL != (*GPADAT & BIT32_24)) {
        discreteState |= 1U;
    }
    if (0UL != (*GPADAT & BIT32_22)) {
        discreteState |= 2U;
    }

    ff25.fp = NULL;
    ff25.pri = 6U;
    ff25.pgn = JAPP_PGN_FF25;
    ff25.dst = JDL_GLOBAL_ADDY;
    ff25.src = JDL_OUR_ADDY;
    ff25.dlc = 8U;
    u32ToBytes((UINT_32)vel, data, LSB);
    u16ToBytes(opF, &data[4], LSB);
    data[6] = (UINT_8)thetaMode;
    data[7] = (UINT_8)discreteState;
    ff25.dIdx = jdlAddMsgData(data, ff25.dlc, TX_DATA);
    jdlJ1939ToCAN(&ff25);
}

static void
jappTxFF26(void) {
    J1939msg_t ff26;
    UINT_8 data[8];
    UINT_16 dVS = ecapGetHz();
    UINT_16 dES = tmr0GetHz();

    ff26.fp = NULL;
    ff26.pri = 6U;
    ff26.pgn = JAPP_PGN_FF26;
    ff26.dst = JDL_GLOBAL_ADDY;
    ff26.src = JDL_OUR_ADDY;
    ff26.dlc = 8U;
    u16ToBytes(dVS, data, LSB);
    u16ToBytes(dES, &data[2], LSB);
    u16ToBytes(jappVS, &data[4], LSB);
    u16ToBytes(jappES, &data[6], LSB);
    ff26.dIdx = jdlAddMsgData(data, ff26.dlc, TX_DATA);
    jdlJ1939ToCAN(&ff26);
}

static void
jappTxFF27(void) {
    J1939msg_t ff27;
    UINT_8 data[8];
    UINT_16 max[3] = {2048U, 2048U, 2048U};

    adcGetMax(max);

    ff27.fp = NULL;
    ff27.pri = 6U;
    ff27.pgn = JAPP_PGN_FF27;
    ff27.dst = JDL_GLOBAL_ADDY;
    ff27.src = JDL_OUR_ADDY;
    ff27.dlc = 8U;
    u16ToBytes(max[0], data, LSB);
    u16ToBytes(max[1], &data[2], LSB);
    u16ToBytes(max[2], &data[4], LSB);
    u16ToBytes(ramTestVar, &data[6], LSB);
    ff27.dIdx = jdlAddMsgData(data, ff27.dlc, TX_DATA);
    jdlJ1939ToCAN(&ff27);
}

static void
jappTxFF28(void) {
    J1939msg_t ff28;
    UINT_8 data[8];
    UINT_16 min[3] = {2048U, 2048U, 2048U};

    adcGetMin(min);

    ff28.fp = NULL;
    ff28.pri = 6U;
    ff28.pgn = JAPP_PGN_FF28;
    ff28.dst = JDL_GLOBAL_ADDY;
    ff28.src = JDL_OUR_ADDY;
    ff28.dlc = 6U;
    u16ToBytes(min[0], data, LSB);
    u16ToBytes(min[1], &data[2], LSB);
    u16ToBytes(min[2], &data[4], LSB);
    ff28.dIdx = jdlAddMsgData(data, ff28.dlc, TX_DATA);
    jdlJ1939ToCAN(&ff28);
}

static void
jappTxFF29(void) {
    J1939msg_t ff29;
    UINT_8 data[8];
    UINT_32 pos = *QPOSCNT;
    UINT_16 theta = thetaCounts_cla;
    UINT_16 stemp = tleGetTemp();

    ff29.fp = NULL;
    ff29.pri = 6U;
    ff29.pgn = JAPP_PGN_FF29;
    ff29.dst = JDL_GLOBAL_ADDY;
    ff29.src = JDL_OUR_ADDY;
    ff29.dlc = 8U;
    data[0] = stemp;
    data[1] = 0U;
    u16ToBytes(theta, &data[2], LSB);
    u32ToBytes(pos, &data[4], LSB);
    ff29.dIdx = jdlAddMsgData(data, ff29.dlc, TX_DATA);
    jdlJ1939ToCAN(&ff29);
}

static void
jappTxFF2A(void) {
    J1939msg_t ff2a;
    UINT_8 data[8];

    ff2a.fp = NULL;
    ff2a.pri = 6U;
    ff2a.pgn = JAPP_PGN_FF2A;
    ff2a.dst = JDL_GLOBAL_ADDY;
    ff2a.src = JDL_OUR_ADDY;
    ff2a.dlc = 8U;
    u16ToBytes(adcGetFilt(ADC_P1), data, LSB);
    u16ToBytes(adcGetFilt(ADC_P2), &data[2], LSB);
    u16ToBytes(adcGetFilt(ADC_P3), &data[4], LSB);
    u16ToBytes(adcGetRaw(ADC_DTEMP), &data[6], LSB);
    ff2a.dIdx = jdlAddMsgData(data, ff2a.dlc, TX_DATA);
    jdlJ1939ToCAN(&ff2a);
}

static void
jappTxDTHN1(void) {
    J1939msg_t ff11;
    UINT_8 data[8];
    _iq20 t = _IQtoIQ20(tctrlGetTin());
    UINT_32 pos = *QPOSCNT;

    ff11.fp = NULL;
    ff11.pri = 6U;
    ff11.pgn = JAPP_PGN_FF11;
    ff11.dst = JDL_GLOBAL_ADDY;
    ff11.src = JDL_OUR_ADDY;
    ff11.dlc = 8U;
    u32ToBytes((UINT_32)t, data, LSB);
    u32ToBytes(pos, &data[4], LSB);
    ff11.dIdx = jdlAddMsgData(data, ff11.dlc, TX_DATA);
    jdlJ1939ToCAN(&ff11);
}

static void
jappTxDTHN2(void) {
    J1939msg_t ff12;
    UINT_8 data[8];
    _iq tIn = tctrlGetTin();
    _iq tOut = tctrlGetOut();
    _iq iOut = _IQ(parkQs_cla);
    UINT_16 stemp = tleGetTemp();
    UINT_32 shifter;
    UINT_16 out;

    ff12.fp = NULL;
    ff12.pri = 6U;
    ff12.pgn = JAPP_PGN_FF12;
    ff12.dst = JDL_GLOBAL_ADDY;
    ff12.src = JDL_OUR_ADDY;
    ff12.dlc = 8U;
    shifter = (UINT_32)_IQmpy(tIn, _IQ(10.0L));
    shifter = shifter >> 18;
    if (tIn < 0L) {
        shifter |= 0xffffc000UL;
    }
    shifter += 32000UL;
    out = (UINT_16)shifter;
    data[0] = out & 0xffU;
    data[1] = (out >> 8) & 0xffU;
    shifter = (UINT_32)_IQmpy(tOut, _IQ(10.0L));
    shifter = shifter >> 18;
    if (tOut < 0L) {
        shifter |= 0xffffc000UL;
    }
    shifter += 32000UL;
    out = (UINT_16)shifter;
    data[2] = out & 0xffU;
    data[3] = (out >> 8) & 0xffU;
    shifter = (UINT_32)_IQmpy(iOut, _IQ(10.0L));
    shifter = shifter >> 18;
    if (iOut < 0L) {
        shifter |= 0xffffc000UL;
    }
    shifter *= 26UL;
    shifter += 32768UL;
    out = (UINT_16)shifter;
    data[4] = out & 0xffU;
    data[5] = (out >> 8) & 0xffU;
    data[6] = 0xffU;
    data[7] = stemp & 0xffU;
    ff12.dIdx = jdlAddMsgData(data, ff12.dlc, TX_DATA);
    jdlJ1939ToCAN(&ff12);
}

static void
jappTxDTHN3(void) {
    J1939msg_t ff13;
    UINT_8 data[8];
    UINT_16 dVS = ecapGetHz();
    UINT_16 dES = tmr0GetHz();
    UINT_32 calc = (UINT_32)dVS;

    calc *= 6UL;
    calc >>= 5;
    dVS = (UINT_16)calc;

    calc = (UINT_32)dES;
    calc *= 21UL;
    calc >>= 1;
    dES = (UINT_16)calc;

    ff13.fp = NULL;
    ff13.pri = 6U;
    ff13.pgn = JAPP_PGN_FF13;
    ff13.dst = JDL_GLOBAL_ADDY;
    ff13.src = JDL_OUR_ADDY;
    ff13.dlc = 8U;
    u16ToBytes(dES, data, LSB);
    u16ToBytes(dVS, &data[2], LSB);
    u16ToBytes(jappES, &data[4], LSB);
    u16ToBytes(jappVS, &data[6], LSB);
    ff13.dIdx = jdlAddMsgData(data, ff13.dlc, TX_DATA);
    jdlJ1939ToCAN(&ff13);
}

static void
jappTxDTHN4(void) {
    J1939msg_t ff14;
    UINT_8 data[8];
    _iq20 q = _IQ20(parkQs_cla);
    _iq20 d = _IQ20(parkDs_cla);

    ff14.fp = NULL;
    ff14.pri = 6U;
    ff14.pgn = JAPP_PGN_FF14;
    ff14.dst = JDL_GLOBAL_ADDY;
    ff14.src = JDL_OUR_ADDY;
    ff14.dlc = 8U;
    u32ToBytes((UINT_32)q, data, LSB);
    u32ToBytes((UINT_32)d, &data[4], LSB);
    ff14.dIdx = jdlAddMsgData(data, ff14.dlc, TX_DATA);
    jdlJ1939ToCAN(&ff14);
}

static void
jappTxDTHN5(void) {
    J1939msg_t ff15;
    UINT_8 data[8];
    UINT_16 t1 = adcGetFilt(ADC_T1);
    UINT_16 t2 = adcGetFilt(ADC_T2);
    UINT_16 b = adcGetFilt(ADC_BATT_ON);
    UINT_16 opF = opsGetFlags();

    ff15.fp = NULL;
    ff15.pri = 6U;
    ff15.pgn = JAPP_PGN_FF15;
    ff15.dst = JDL_GLOBAL_ADDY;
    ff15.src = JDL_OUR_ADDY;
    ff15.dlc = 8U;
    u16ToBytes(t1, data, LSB);
    u16ToBytes(t2, &data[2], LSB);
    u16ToBytes(b, &data[4], LSB);
    u16ToBytes(opF, &data[6], LSB);
    ff15.dIdx = jdlAddMsgData(data, ff15.dlc, TX_DATA);
    jdlJ1939ToCAN(&ff15);
}

static void
jappTxDTHN6(void) {
    J1939msg_t ff16;
    UINT_8 data[8];
    _iq20 offset = _IQtoIQ20(tctrlGetOffset());

    ff16.fp = NULL;
    ff16.pri = 6U;
    ff16.pgn = JAPP_PGN_FF16;
    ff16.dst = JDL_GLOBAL_ADDY;
    ff16.src = JDL_OUR_ADDY;
    ff16.dlc = 4U;
    u32ToBytes((UINT_32)offset, data, LSB);
    ff16.dIdx = jdlAddMsgData(data, ff16.dlc, TX_DATA);
    jdlJ1939ToCAN(&ff16);
}
