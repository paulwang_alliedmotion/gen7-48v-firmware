/*!
 * \file      mctrl.h
 * \author    Zach Haver
 * \brief     Main motor control definitions.
 */

/* $Id: $ */
/* $Name: $ */

/* NOTICE: This file is the property of Globe Motors and contains */
/* CONFIDENTIAL information.  Any use, copying or dissemination of the */
/* information contained in this file is strictly prohibited unless */
/* authorized by Globe Motors in writing. */

/* Copyright 2016, Globe Motors, Inc. */

/*
 * Revision History:
 *
 * $Log: $
 *
 */

#ifndef MCTRL_H
#define MCTRL_H

#include "IQmathLib.h"
#include "misra.h"

typedef struct {
    UINT_16 mode;
    _iq v1;
    _iq v2;
} MCTRLCMD_t;

/* control mode definitions */
#define MCTRL_CMD_OFF             0U
#define MCTRL_CMD_I               1U
#define MCTRL_CMD_T               2U
#define MCTRL_CMD_S               3U
#define MCTRL_CMD_ST             20U
#define MCTRL_CMD_RAM_TEST     0x20U
#define MCTRL_CMD_STOP_WDOG    0x30U
#define MCTRL_CMD_START_WDOG   0x31U
#define MCTRL_CMD_TOFF         0x44U
#define MCTRL_CMD_CLR_ADC      0x60U
#define MCTRL_CMD_THETA_NORM   0x80U
#define MCTRL_CMD_THETA_SET    0x81U
#define MCTRL_CMD_THETA_RAMP   0x82U
#define MCTRL_CMD_EMC          0x90U
#define MCTRL_CMD_UPDATE_I_LIM 0xa0U
#define MCTRL_CMD_KA_AUTO      0xd0U
#define MCTRL_CMD_KA_ON        0xd1U
#define MCTRL_CMD_KA_OFF       0xd2U
#define MCTRL_CMD_FAULT_TOGGLE 0xe0U
#define MCTRL_CMD_FAULT_ON     0xe1U
#define MCTRL_CMD_FAULT_OFF    0xe2U
#define MCTRL_CMD_FDBK_MSG     0xfdU
#define MCTRL_CMD_DTHN_MSG     0xfeU
#define MCTRL_CMD_ALL_MSG      0xffU

#define MCTRLCMD_DEFAULTS {MCTRL_CMD_OFF, (_iq)0UL, (_iq)0UL}

/* function prototypes */
void mctrlInit(void);
void mctrlControl(void);
void mctrlSetCMD(MCTRLCMD_t cmd);

#endif /* MCTRL_H */
