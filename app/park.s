;//###########################################################################
;//
;// FILE:  park.s
;//
;// TITLE: park transform Assembly Code.
;//
;// This file contains the park transform assembly code.
;//
;//###########################################################################
park_CLA        .macro

_park_CLA_START

                MMOV32          MR0, @_co_cla
                MMOV32          MR1, @_clarkeAlpha_cla

                MMPYF32         MR3,MR1,MR0 ; alpha * cosine

                MMOV32          MR0, @_si_cla
                MMOV32          MR1, @_clarkeBeta_cla

                MMPYF32         MR2,MR1,MR0 ; beta * sine

                MADDF32         MR3,MR3,MR2 ; (alpha * cosine) + (beta * sine)

                MMOV32          @_parkDs_cla,MR3 ; store result

                MMOV32          MR0, @_co_cla
                MMOV32          MR1, @_clarkeBeta_cla

                MMPYF32         MR3,MR1,MR0 ; beta * cosine

                MMOV32          MR0, @_si_cla
                MMOV32          MR1, @_clarkeAlpha_cla

                MMPYF32         MR2,MR1,MR0 ; alpha * sine

                MSUBF32         MR3,MR3,MR2 ; (beta * cosine) - (alpha * sine)

                MMOV32          @_parkQs_cla,MR3 ; store result

_park_CLA_END
                .endm
