;//###########################################################################
;//
;// FILE:  drift.s
;//
;// TITLE: drift correction Assembly Code.
;//
;// This file contains the drift correction assembly code.
;//
;//###########################################################################

drift_CLA      .macro

_drift_CLA_START
        MMOV32          MR0,@_driftExec_cla ; get execution count
        MUI32TOF32      MR0,MR0 ; convert to F32 for compare
        MMOV32          MR1,@_piQRef_cla
        MMOV32          MR2,@_piDRef_cla

DRIFTSTART
        ;; Check if IqCmd is 0 if not set exec to 0 and exit
        MCMPF32         MR1,#0.0 ; compare to see if command is 0
        MNOP                    ; wait for compare
        MNOP                    ; wait for compare
        MNOP                    ; wait for compare
        MBCNDD          DRIFTERROR,NEQ ; Branch to error if not 0
        MNOP                    ; wait for branch
        MNOP                    ; wait for branch
        MNOP                    ; wait for branch

        ;; Check if IdCmd is 0 if not set exec to 0 and exit
        MCMPF32         MR2,#0.0 ; compare to see if command is 0
        MNOP                    ; wait for compare
        MNOP                    ; wait for compare
        MNOP                    ; wait for compare
        MBCNDD          DRIFTERROR,NEQ ; Branch to error if not 0
        MNOP                    ; wait for branch
        MNOP                    ; wait for branch
        MNOP                    ; wait for branch

        ;; Precheck complete increment exec and continue
        MMOV32          MR0,@_driftExec_cla
        MMOVF32         MR1,#1.0
        MF32TOUI32      MR1,MR1
        MADD32          MR2,MR0,MR1
        MMOV32          @_driftExec_cla,MR2
        MUI32TOF32      MR0,MR0 ; convert to F32 for compare
        MCMPF32         MR0,#6.0 ; compare count to 24 to test for delay
        MNOP                    ; wait for compare
        MNOP                    ; wait for compare
        MNOP                    ; wait for compare
        MBCNDD          DRIFTDELAY,LEQ ; Branch to DRIFTDELAY if timeout
        MNOP                    ; wait to branch
        MNOP                    ; wait to branch
        MNOP                    ; wait to branch
        MCMPF32         MR0,#7.0 ; compare count to 32 to test for delay
        MNOP                    ; wait for compare
        MNOP                    ; wait for compare
        MNOP                    ; wait for compare
        MBCNDD          DRIFTSAMPLE,LEQ ; Branch to DRIFTDELAY if timeout
        MNOP                    ; wait to branch
        MNOP                    ; wait to branch
        MNOP                    ; wait to branch

DRIFTSAMPLE
        MMOVF32         MR0,#0.0
        MMOV32          @_piQUP_cla,MR0
        MMOV32          @_piQV1_cla,MR0
        MMOV32          @_piQI1_cla,MR0
        MMOV32          @_piQUI_cla,MR0
        MMOV32          @_piDUP_cla,MR0
        MMOV32          @_piDV1_cla,MR0
        MMOV32          @_piDI1_cla,MR0
        MMOV32          @_piDUI_cla,MR0
        MMOV32          MR1,@_corrA_cla ; load current correction fdbk
;        MMOVF32         MR3,#-0.0732421875 ; load for conversion to amps
        MMOVF32         MR3,#-0.0325488281 ; counts to amps 66.66*2/4096
        MMOV32          MR0,@_adcOffA_cla ; get the adc counts of UOFF
        MUI32TOF32      MR0,MR0 ; convert to F32 for math operations
        MMPYF32         MR0,MR0,MR3 ; convert U to amps
;        MADDF32         MR0,#150.0,MR0 ; add offset
        MADDF32         MR0,#66.66,MR0 ; add offset

        MSUBF32         MR2,MR1,MR0 ; correction change = fdbk - new reading
        MMAXF32         MR2,#-0.001 ; limit correction change neg
        MMINF32         MR2,#0.001 ; limit correction change pos
        MSUBF32         MR0,MR1,MR2 ; adjust correction with limited value
        MMOV32          @_corrA_cla,MR0

        MMOV32          MR1,@_corrB_cla ; load current correction fdbk
;        MMOVF32         MR3,#-0.0732421875 ; load for conversion to amps
        MMOVF32         MR3,#-0.0325488281 ; counts to amps 66.66*2/4096
        MMOV32          MR0,@_adcOffB_cla ; get the adc counts of VOFF
        MUI32TOF32      MR0,MR0 ; convert to F32 for math operations
        MMPYF32         MR0,MR0,MR3 ; convert V to amps
;        MADDF32         MR0,#150.0,MR0 ; add offset
        MADDF32         MR0,#66.66,MR0 ; add offset

        MSUBF32         MR2,MR1,MR0 ; correction change = fdbk - new reading
        MMAXF32         MR2,#-0.001 ; limit correction change neg
        MMINF32         MR2,#0.001 ; limit correction change pos
        MSUBF32         MR0,MR1,MR2 ; adjust correction with limited value
        MMOV32          @_corrB_cla,MR0

DRIFTDELAY
        ;; delay is only here incase we want to do something here later
        ;; we rely on the c2000 side to look at exec and determine bus state
        MBCNDD          DRIFTEXIT,UNC ; branch to exit
        MNOP                    ; wait to branch
        MNOP                    ; wait to branch
        MNOP                    ; wait to branch

DRIFTERROR
        ;; set executions to zero to restart process
        MMOVF32         MR0,#0.0 ; load zero for update
        MF32TOUI32      MR0,MR0 ; convert to U16
        MMOV32          @_driftExec_cla,MR0 ; update executions

DRIFTEXIT

_drift_CLA_END
                .endm
