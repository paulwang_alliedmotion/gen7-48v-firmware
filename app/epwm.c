/*!
 * \file      epwm.c
 * \author    Zach Haver
 * \brief     ePWM initialization functions for the TI TMS320C28035 Piccolo.
 */

/* $Id: $ */
/* $Name: $ */

/* NOTICE: This file is the property of Globe Motors and contains */
/* CONFIDENTIAL information.  Any use, copying or dissemination of the */
/* information contained in this file is strictly prohibited unless */
/* authorized by Globe Motors in writing. */

/* Copyright 2016, Globe Motors, Inc. */

/*
 * Revision History:
 *
 * $Log: $
 *
 */

#include "epwm.h"
#include "misra.h"
#include "piccolo.h"

/* public functions */

/*!
 * \fn     void epwmInit (void)
 *
 * \brief  Initialize the ePWM modules
 */
void
epwmInit(void) {
    /*
     * Enable the appropriate ePWM module clocks, then disable the
     * clock sync to the ePWM modules so that all ePWM modules are
     * synchronized.
     */
    /* Enable EALLOW protected register access */
    PICCOLO_EALLOW();

    *PCLKCR1 |= 0x0001U;  /* EPWM1ENCLK - Phase U */
    *PCLKCR1 |= 0x0002U;  /* EPWM2ENCLK - Phase V */
    *PCLKCR1 |= 0x0004U;  /* EPWM3ENCLK - Phase W */
    *PCLKCR1 |= 0x0010U;  /* EPWM5ENCLK - Theta Injection */
    *PCLKCR0 &= ~0x0004U;

    /* Set the timer period */
    *EPWM1_TBPRD = EPWM_PERIOD;
    *EPWM2_TBPRD = EPWM_PERIOD;
    *EPWM3_TBPRD = EPWM_PERIOD;
    *EPWM5_TBPRD = EPWM_PERIOD;

    /* Clear the timer counter */
    *EPWM1_TBCTR = 0x0000U;
    *EPWM2_TBCTR = 0x0000U;
    *EPWM3_TBCTR = 0x0000U;
    *EPWM5_TBCTR = 0x0000U;

    /* Set the EPWM mode
     * EPWMx_TBCTL
     * bits 15-14 = (10)  FREE,SOFT
     * bit  13    = (1)   PHSDIR
     * bits 12-10 = (000) CLKDIV
     * bits 9-7   = (000) HSPCLKDIV
     * bit  6     = (0)   SWFSYNC
     * bits 5-4   = (00)  SYNCOSEL
     * bit  3     = (0)   PRDLD
     * bit  2     = (0)   PHSEN
     * bits 1-0   = (10)  CTRMODE (00) for sepic
     */
    *EPWM1_TBCTL = 0xa002U; //free run, count up after after the synchronization event, up-down-count mode
    *EPWM2_TBCTL = 0xa002U;
    *EPWM3_TBCTL = 0xa002U;
    *EPWM5_TBCTL = 0xa002U;

    /* Initialize duty cycles to 50% */
    *EPWM1_CMPA = EPWM_PERIOD >> 1;
    *EPWM2_CMPA = EPWM_PERIOD >> 1;
    *EPWM3_CMPA = EPWM_PERIOD >> 1;
    /* delay current sampling */
    *EPWM1_CMPB = 120U;          //delay time = EPWM1_CMPB * 0.0166667us

    /* CMPCTL
     * bits 15-10 - (x)  Reserved
     * bit  9     - (0)  SHDWBFULL
     * bit  8     - (0)  SHDWAFULL
     * bit  7     - (x)  Reserved
     * bit  6     - (0)  SHDWBMODE = SHADOW
     * bit  5     - (x)  Reserved
     * bit  4     - (0)  SHDWAMODE = SHADOW
     * bits 3-2   - (01) LOADBMODE = CTR - PRD
     * bits 1-0   - (01) LOADAMODE = CTR - PRD
     */
    *EPWM1_CMPCTL = 0x0005U;
    *EPWM2_CMPCTL = 0x0005U;
    *EPWM3_CMPCTL = 0x0005U;
    *EPWM5_CMPCTL = 0x0005U;

    /* Set the action qualifier output A
     * AQCTLA
     * bits 15-12 = (xx) Reserved
     * bits 11-10 = (00) CBD
     * bits 9-8   = (00) CBU
     * bits 7-6   = (01) CAD (00) for SEPIC no down count
     * bits 5-4   = (01) CAU
     * bits 3-2   = (00) PRD (01) for SEPIC
     * bits 1-0   = (00) ZRO (01) for SEPIC
     */
    *EPWM1_AQCTLA = 0x00a0U;
    *EPWM2_AQCTLA = 0x00a0U;
    *EPWM3_AQCTLA = 0x00a0U;
    *EPWM5_AQCTLA = 0x00a0U;

    /* Set the action qualifier output B
     * AQCTLB
     * bits 15-12 = (xx) Reserved
     * bits 11-10 = (00) CBD
     * bits 9-8   = (00) CBU
     * bits 7-6   = (01) CAD
     * bits 5-4   = (01) CAU
     * bits 3-2   = (00) PRD
     * bits 1-0   = (00) ZRO
     */
    *EPWM1_AQCTLB = 0x00a0U;
    *EPWM2_AQCTLB = 0x00a0U;
    *EPWM3_AQCTLB = 0x00a0U;
    *EPWM5_AQCTLB = 0x00a0U;

    /* Enable the deadband
     * DBCTL
     * bit  15   = (0)  HALFCYCLE = Full Cycle
     * bits 14-6 = (xx) Reserved
     * bits 5-4  = (00) IN_MODE = EPWMxA both
     * bits 3-2  = (10) POLSEL =  Active High Complimentary
     * bits 1-0  = (11) OUT_MODE = Fully Enabled
     */
    *EPWM1_DBCTL = 0x0000U;
    *EPWM2_DBCTL = 0x0000U;
    *EPWM3_DBCTL = 0x0000U;

    /* Set the deadband rising edge delay
     * DBRED
     * bits 15-10 = (xxxxxxxx)
     * bits 9-0   = (00000000) DEL
     */
    *EPWM1_DBRED = EPWM_DB_COUNT;
    *EPWM2_DBRED = EPWM_DB_COUNT;
    *EPWM3_DBRED = EPWM_DB_COUNT;

    /* Set the deadband falling edge delay
     * DBFED
     * bits 15-10 = (xxxxxxxx)
     * bits 9-0   = (00000000) DEL
     */
    *EPWM1_DBFED = EPWM_DB_COUNT;
    *EPWM2_DBFED = EPWM_DB_COUNT;
    *EPWM3_DBFED = EPWM_DB_COUNT;

    /* Set up ePWM1 ADC trigger for unerflow */
    /* Enable start of conversion trigger for ePWM1 channel A
     * ETSEL
     * bit  15    = (0)    SOCBEN
     * bits 14-12 = (000)  SOCBSEL
     * bit  11    = (1)    SOCAEN
     * bits 10-8  = (110)  SOCASEL
     * bits 7-4   = (xxxx) Reserved
     * bit  3     = (0)    INTEN
     * bits 2-0   = (000)  INTSEL
     */
    *EPWM1_ETSEL = 0x0e00U;

    /* Set the period for the event trigger
     * ETPS
     * bits 15-14 = (00) SOCBCNT
     * bits 13-12 = (00) SOCBPRD
     * bits 11-10 = (00) SOCACNT
     * bits 9-8   = (01) SOCAPRD
     * bits 7-4   = (xxxx) Reserved
     * bits 3-2   = (00) INTCNT
     * bits 1-0   = (00) INTPRD
     */
    *EPWM1_ETPS = 0x0100U;

    /* Set up ePWM2 ADC trigger for overflow */
    /* Enable start of conversion trigger for ePWM2 channel B
     * ETSEL
     * bit  15    = (0)    SOCBEN
     * bits 14-12 = (000)  SOCBSEL
     * bit  11    = (1)    SOCAEN
     * bits 10-8  = (010)  SOCASEL
     * bits 7-4   = (xxxx) Reserved
     * bit  3     = (0)    INTEN
     * bits 2-0   = (000)  INTSEL
     */
    *EPWM2_ETSEL = 0x0a00U;

    /* Set the period for the event trigger
     * ETPS
     * bits 15-14 = (00) SOCBCNT
     * bits 13-12 = (00) SOCBPRD
     * bits 11-10 = (00) SOCACNT
     * bits 9-8   = (10) SOCAPRD
     * bits 7-4   = (xxxx) Reserved
     * bits 3-2   = (00) INTCNT
     * bits 1-0   = (00) INTPRD
     */
    *EPWM2_ETPS = 0x0200U;

    /* Set up ePWM5 INT trigger for compare */
    /* ETSEL
     * bit  15    = (0)    SOCBEN
     * bits 14-12 = (000)  SOCBSEL
     * bit  11    = (0)    SOCAEN
     * bits 10-8  = (000)  SOCASEL
     * bits 7-4   = (xxxx) Reserved
     * bit  3     = (1)    INTEN
     * bits 2-0   = (001)  INTSEL
     */
    *EPWM5_ETSEL = 0x0009U;

    /* Set the period for the event trigger
     * ETPS
     * bits 15-14 = (00) SOCBCNT
     * bits 13-12 = (00) SOCBPRD
     * bits 11-10 = (00) SOCACNT
     * bits 9-8   = (00) SOCAPRD
     * bits 7-4   = (xxxx) Reserved
     * bits 3-2   = (00) INTCNT
     * bits 1-0   = (01) INTPRD
     */
    *EPWM5_ETPS = 0x0001U;

    /* enable clock sync */
    *PCLKCR0 |= 0x0004U;

    /* Disable EALLOW protected register access */
    PICCOLO_EDIS();
}

void
epwmON(void) {
    *EPWM1_AQCTLA = 0x0090U;
    *EPWM2_AQCTLA = 0x0090U;
    *EPWM3_AQCTLA = 0x0090U;
    *EPWM1_AQCTLB = 0x0060U;
    *EPWM2_AQCTLB = 0x0060U;
    *EPWM3_AQCTLB = 0x0060U;
    *EPWM1_DBCTL = 0x000bU;
    *EPWM2_DBCTL = 0x000bU;
    *EPWM3_DBCTL = 0x000bU;
}

void
epwmOFF(void) {
    *EPWM1_AQCTLA = 0x00a0U;
    *EPWM2_AQCTLA = 0x00a0U;
    *EPWM3_AQCTLA = 0x00a0U;
    *EPWM1_AQCTLB = 0x00a0U;
    *EPWM2_AQCTLB = 0x00a0U;
    *EPWM3_AQCTLB = 0x00a0U;
    *EPWM1_DBCTL = 0x0000U;
    *EPWM2_DBCTL = 0x0000U;
    *EPWM3_DBCTL = 0x0000U;
}

void
epwmINT5Handler(void) {
    *EPWM5_ETCLR |= 0x0001U;
}
