/*!
 * \file        epwm.h
 * \author      Zach Haver
 * \brief       ePWM initialization definitions for
 *              the TI TMS320C28033/35 Piccolo.
 */

/* $Id: $ */
/* $Name: $ */

/* NOTICE: This file is the property of Globe Motors and contains */
/* CONFIDENTIAL information.  Any use, copying or dissemination of the */
/* information contained in this file is strictly prohibited unless */
/* authorized by Globe Motors in writing. */

/* Copyright 2016, Globe Motors, Inc. */

/*
 * Revision History:
 *
 * $Log: $
 *
 */

#ifndef EPWM_H
#define EPWM_H

#include "misra.h"
#include "piccolo.h"

/* period definitions */
#define EPWM_PERIOD          1500U       //40KHz
#define EPWM_DB_COUNT        0x0019U     //406.25ns

/* public function prototypes */
void epwmInit(void);
void epwmON(void);
void epwmOFF(void);
void epwmINT5Handler(void);

#endif /* EPWM_H */
