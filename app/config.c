/*!
 * \file      config.c
 * \author    Zach Haver
 * \brief     Automatic configuration detection algorithms.
 */

/* $Id: $ */
/* $Name: $ */

/* NOTICE: This file is the property of Globe Motors and contains */
/* CONFIDENTIAL information.  Any use, copying or dissemination of the */
/* information contained in this file is strictly prohibited unless */
/* authorized by Globe Motors in writing. */

/* Copyright 2017, Globe Motors, Inc. */

/*
 * Revision History:
 *
 * $Log: $
 *
 */

#include "config.h"
#include "crc.h"
#include "dtc.h"
#include "misra.h"
#include "utils.h"

static CONFIGState_t configState = CONFIG_CORRUPT;

void
configInit(void) {
    UINT_32 address = CONFIG_START_ADDRESS + CONFIG_BASE_OFFSET;
    UINT_32 crc = crcVerifyBlockFlash(address, 0x5ea1UL);
    const UINT_32 knownCRC = (UINT_32)mapIQ(CONFIG_START_ADDRESS);

    if (crc == knownCRC) {
        configState = CONFIG_VALID;
    } else {
        dtcSet(DTCSW_CONFIG_CAL);
    }
}

CONFIGState_t
configGetState(void) {
    return configState;
}
