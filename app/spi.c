/*!
 * \file      spi.c
 * \author    Zach Haver
 * \brief     SPI driver functions for the TI TMS320C28033/35 Piccolo.
 */

/* $Id: $ */
/* $Name: $ */

/* NOTICE: This file is the property of Globe Motors and contains */
/* CONFIDENTIAL information.  Any use, copying or dissemination of the */
/* information contained in this file is strictly prohibited unless */
/* authorized by Globe Motors in writing. */

/* Copyright 2010, Globe Motors, Inc. */

/*
 * Revision History:
 *
 * $Log: $
 *
 */

#include "dtc.h"
#include "misra.h"
#include "piccolo.h"
#include "spi.h"

/* private function prototypes */

/* private variables */
static UINT_16 reg = 0U;
static UINT_16 safetyW = 0U;
static UINT_16 writeReg = 0U;
static SINT_16 spiState = SPI_ERROR;
static UINT_16 spiMode = SPI_3WIRE;

/* public functions */

void
spiInit(void) {
    PICCOLO_EALLOW();

    CS_INF_HIGH;
    CS_DRV_HIGH;
    *PCLKCR0 |= 0x0100U;

    /* SPIA_SPICCR
     * bit  7   = (0)    SPI SW Reset disable SPI-A
     * bit  6   = (0)    CLOCK POLARITY
     * bit  5   = (X)    RESERVED
     * bit  4   = (0)    SPILBK
     * bits 3-0 = (1111) SPICHAR 16 bit operation
     */
    *SPIA_SPICCR = 0x0fU;
    /* SPIA_SPICTL
     * bits 7-5 = (X) RESERVED
     * bit  4   = (0) OVERRUN INT ENA
     * bit  3   = (0) CLOCK PHASE
     * bit  2   = (1) MASTER/SLAVE
     * bit  1   = (1) TALK
     * bit  0   = (0) SPI INT ENA
     */
    *SPIA_SPICTL = 0x06U;
    /* SPIA_SPIBRR
     * bit  7   = (X) RESERVED
     * bits 6-0 = (0x1E) SPI BIT RATE
     */
    *SPIA_SPIBRR = 0x1eU;
    /* SPIA_SPIPRI set free run */
    *SPIA_SPIPRI = 0x10U;
    /* enable SPI-A */
    *SPIA_SPICCR |= 0x80U;

    PICCOLO_EDIS();
}

SINT_16
spiDataRead3Wire(UINT_16 command, UINT_16 *data, UINT_16 *sw) {
    UINT_32 timeout = 0UL;
    SINT_16 ret;

    *SPIA_SPIPRI = 0x11U;
    spiMode = SPI_3WIRE;
    spiState = SPI_TX_COMMAND_RX_WORD;
    SPI_XMIT_ENA;
    SPI_INT_ENA;
    CS_INF_LOW;

    *SPIA_SPITXBUF = command;

    while(((*SPIA_SPICTL & 0x0001U) == 0x0001U) && (65000UL > timeout)) {
        timeout++;
    }

    *sw = safetyW;
    *data = reg;

    CS_INF_HIGH;

    if ((0xffffU == safetyW) || (0U == safetyW) || (65000UL <= timeout)) {
        dtcSet(DTCHW_ENCODER_SPI);
        SPI_INT_DIS;
        ret = SPI_ERROR;
    } else {
        ret = spiState;
    }

    return ret;
}

SINT_16
spiDataWrite3Wire(UINT_16 command, UINT_16 data, UINT_16 *sw) {
    UINT_32 timeout = 0UL;
    SINT_16 ret;

    *SPIA_SPIPRI = 0x11U;
    spiMode = SPI_3WIRE;
    spiState = SPI_TX_COMMAND_TX_WORD;
    writeReg = data;
    SPI_XMIT_ENA;
    SPI_INT_ENA;
    CS_INF_LOW;

    *SPIA_SPITXBUF = command;

    while(((*SPIA_SPICTL & 0x0001U) == 0x0001U) && (65000UL > timeout)) {
        timeout++;
    }

    *sw = safetyW;

    CS_INF_HIGH;

    if (65000UL <= timeout) {
        dtcSet(DTCHW_ENCODER_SPI);
        SPI_INT_DIS;
        ret = SPI_ERROR;
    } else {
        ret = spiState;
    }

    return ret;
}

SINT_16
spiDataRead4Wire(UINT_16 command, UINT_16 *data) {
    UINT_32 timeout = 0UL;
    SINT_16 ret;

    *SPIA_SPIPRI = 0x10U;
    spiMode = SPI_4WIRE;
    reg = 0xffffU;
    SPI_XMIT_ENA;
    SPI_INT_ENA;
    CS_DRV_LOW;

    *SPIA_SPITXBUF = command;

    while(((*SPIA_SPICTL & 0x0001U) == 0x0001U) && (65000UL > timeout)) {
        timeout++;
    }

    *data = reg;

    CS_DRV_HIGH;

    if (65000UL <= timeout) {
        SPI_INT_DIS;
        ret = SPI_ERROR;
    } else {
        ret = SPI_SUCCESS;
    }

    return ret;
}

SINT_16
spiDataWrite4Wire(UINT_16 command) {
    UINT_32 timeout = 0UL;
    SINT_16 ret;

    *SPIA_SPIPRI = 0x10U;
    spiMode = SPI_4WIRE;
    SPI_XMIT_ENA;
    SPI_INT_ENA;
    CS_DRV_LOW;

    *SPIA_SPITXBUF = command;

    while(((*SPIA_SPICTL & 0x0001U) == 0x0001U) && (65000UL > timeout)) {
        timeout++;
    }

    CS_DRV_HIGH;

    if (65000UL <= timeout) {
        SPI_INT_DIS;
        ret = SPI_ERROR;
    } else {
        ret = SPI_SUCCESS;
    }

    return ret;
}

void
spiInterruptHandler(void) {
    UINT_16 data;

    if (SPI_3WIRE == spiMode) {
        data = *SPIA_SPIRXBUF;

        switch (spiState) {
        case SPI_TX_COMMAND_TX_WORD:
            spiState = SPI_TX_REG;
            *SPIA_SPITXBUF = writeReg;
            break;
        case SPI_TX_REG:
            spiState = SPI_RX_SAFETY;
            SPI_XMIT_DIS;
            *SPIA_SPITXBUF = 0U;
            break;
        case SPI_TX_COMMAND_RX_WORD:
            spiState = SPI_RX_REG;
            SPI_XMIT_DIS;
            *SPIA_SPITXBUF = 0U;
            break;
        case SPI_RX_REG:
            reg = data;
            spiState = SPI_RX_SAFETY;
            *SPIA_SPITXBUF = 0U;
            break;
        case SPI_RX_SAFETY:
            safetyW = data;
            spiState = SPI_SUCCESS;
            SPI_INT_DIS;
            break;
        default:
            spiState = SPI_ERROR;
            SPI_INT_DIS;
            break;
        }
    } else {
        reg = *SPIA_SPIRXBUF;
        SPI_INT_DIS;
    }
}
