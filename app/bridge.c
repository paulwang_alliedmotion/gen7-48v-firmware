/*!
 * \file       bridge.c
 * \author     Zach Haver
 * \brief      This file will aloow control of the bridge state.
 */

/* $Id: $ */
/* $Name: $ */

/* NOTICE: This file is the property of Globe Motors and contains */
/* CONFIDENTIAL information.  Any use, copying or dissemination of the */
/* information contained in this file is strictly prohibited unless */
/* authorized by Globe Motors in writing. */

/* Copyright 2016, Globe Motors, Inc. */

/*
 * Revision History:
 *
 * $Log: $
 *
 */

#include "bridge.h"
#include "dtc.h"
#include "epwm.h"
#include "misra.h"
#include "piccolo.h"
#include "spi.h"

static BRIDGERet_t bridgeRecovered(void);

static BRIDGEStates_t bridgeState = BRIDGE_ON;
static BRIDGEStates_t bridgeRecoveryState = BRIDGE_OFF;
static UINT_16 wdogKill = 0U;

void
bridgeWDOGState(UINT_16 ns) {
    wdogKill = ns;
}

void
bridgeChk(void) {
    switch (bridgeState) {
    case BRIDGE_FAULTED:
        /* check for warning or fault */
        if (BRIDGE_RECOVERED == bridgeRecovered()) {
            bridgeState = bridgeRecoveryState;
        } else {
            dtcSet(DTCHW_DRV_ERROR);
        }
        if (BRIDGE_ON == bridgeState) {
            epwmON();
            *XINT1CR |= 0x1U;
        }
        break;
    case BRIDGE_TIMEOUT:
    case BRIDGE_OFF:
    case BRIDGE_ON:
    default:
        /* do nothing */
        ;
        break;
    }
}

void
bridgeWDogKick(void) {
    if (0U == wdogKill) {
        *GPBTOGGLE |= 0x00000001UL;
    }
}

void
bridgeStateSet(BRIDGEStates_t newState) {
    /* only try to turn on all other commands considered off */
    if (BRIDGE_ON == newState) {
        if (BRIDGE_OFF == bridgeState) {
            /* turn on PWM */
            epwmON();
            /* swith to on only if coming from off */
            bridgeState = BRIDGE_ON;
        }
    } else {
        if (BRIDGE_ON == bridgeState) {
            /* turn off PWM */
            epwmOFF();
            /* switch to off state only if we are coming from on */
            bridgeState = BRIDGE_OFF;
        }
    }
}

void
bridgeFaultINT(void) {
    /* faulted turn off epwm */
    epwmOFF();
    bridgeRecoveryState = bridgeState;
    bridgeState = BRIDGE_FAULTED;
    /* disable interrupt until handled outside of interrupt scope */
    *XINT1CR &= ~0x1U;
}

void
bridgeTimeoutINT(void) {
    /* timedout turn off epwm */
    epwmOFF();
    bridgeRecoveryState = bridgeState;
    bridgeState = BRIDGE_TIMEOUT;
    /* single shot error disable interrupt */
    *XINT2CR &= ~0x1U;
}

static BRIDGERet_t
bridgeRecovered(void) {
    BRIDGERet_t ret = BRIDGE_ERROR;

    if ((*GPADAT & 0x0040) != 0) {
        ret = BRIDGE_RECOVERED;
    }

    return ret;
}
