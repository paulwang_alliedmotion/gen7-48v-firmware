/*!
 * \file      vbat.c
 * \author    Zach Haver
 * \brief     Battery voltage monitoring functions.
 */

/* $Id: $ */
/* $Name: $ */

/* NOTICE: This file is the property of Globe Motors and contains */
/* CONFIDENTIAL information.  Any use, copying or dissemination of the */
/* information contained in this file is strictly prohibited unless */
/* authorized by Globe Motors in writing. */

/* Copyright 2016, Globe Motors, Inc. */

/*
 * Revision History:
 *
 * $Log: $
 *
 */

#include "adc.h"
#include "dtc.h"
#include "misra.h"
#include "vbat.h"

/* public functions */

/*!
 * \fn     void vbatChk (void)
 *
 * \brief  This function checks the battery voltage for fault conditions
 */
void
vbatChk(void) {
    UINT_16 vBattery = adcGetFilt(ADC_BATT_ON);
    static UINT_16 highErrorCtr = 0U;
    static UINT_16 lowErrorCtr = 0U;

    /* check for high battery error */
    if (VBAT_HIGH_SET < vBattery) {
        if ((VBAT_HIGH_TIMEOUT + 2U) > highErrorCtr) {
            highErrorCtr++;
        }
    } else if (VBAT_HIGH_REC > vBattery) {
        if (0U < highErrorCtr) {
            highErrorCtr--;
        }
    } else {
        /* in hysteresis band, do nothing */
    }

    /* check for low battery error */
    if (VBAT_LOW_SET > vBattery) {
        if ((VBAT_LOW_TIMEOUT + 2U) > lowErrorCtr) {
            lowErrorCtr++;
        }
    } else if (VBAT_LOW_REC < vBattery) {
        if (0U < lowErrorCtr) {
            lowErrorCtr--;
        }
    } else {
        /* in hysteresis band, do nothing */
    }

    /* set or clear errors */
    if (VBAT_HIGH_TIMEOUT < highErrorCtr) {
        dtcSet(DTCHW_BATT_HIGH);
    } else if (0U == highErrorCtr) {
        dtcClear(DTCHW_BATT_HIGH);
    } else {
        /* in hysteresis band, do nothing */
    }
    if (VBAT_LOW_TIMEOUT < lowErrorCtr) {
        dtcSet(DTCHW_BATT_LOW);
    } else if (0U == lowErrorCtr) {
        dtcClear(DTCHW_BATT_LOW);
    } else {
        /* in hysteresis band, do nothing */
    }
}
