/*!
 * \file      isr.h
 * \author    Zach Haver
 * \brief     ADC definitions for the TI TMS320C28033/35 Piccolo.
 */

/* $Id: $ */
/* $Name: $ */

/* NOTICE: This file is the property of Globe Motors and contains */
/* CONFIDENTIAL information.  Any use, copying or dissemination of the */
/* information contained in this file is strictly prohibited unless */
/* authorized by Globe Motors in writing. */

/* Copyright 2016, Globe Motors, Inc. */

/*
 * Revision History:
 *
 * $Log: $
 *
 */

#ifndef ISR_H
#define ISR_H

#include "misra.h"

/* Interrupt Group Acknowledge Masks */
#define ISR_ACK_GROUP1   0x0001U
#define ISR_ACK_GROUP3   0x0004U
#define ISR_ACK_GROUP4   0x0008U
#define ISR_ACK_GROUP6   0x0020U
#define ISR_ACK_GROUP8   0x0080U
#define ISR_ACK_GROUP9   0x0100U
#define ISR_ACK_GROUP11  0x0400U
#define ISR_ACK_GROUP12  0x0800U

/* Mask to obtain PIEVECT from PIECTRL */
#define ISR_VECTOR_MASK  0xfffeU

#define ISR_XINT1        0x0d46U
#define ISR_XINT2        0x0d48U
#define ISR_EPWM5INT     0x0d68U
#define ISR_ECAP1INT     0x0d70U
#define ISR_SPIRXINTA    0x0d90U
#define ISR_SPITXINTA    0x0d92U
#define ISR_I2C_1A       0x0db0U
#define ISR_ECAN1INTA    0x0dcaU
#define ISR_CLA1INT      0x0de0U
#define ISR_CLA2INT      0x0de2U
#define ISR_CLA8INT      0x0deeU
#define ISR_XINT3        0x0df0U

void isrVectorInit(void);
void isrSetThetaPoles(UINT_32 poles);

#endif /* ISR_H */
