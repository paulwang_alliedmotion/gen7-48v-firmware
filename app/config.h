/*!
 * \file    config.h
 * \author  Zach Haver
 * \brief   Automatic configuuration algorithm definitions
 *
 */

/* $Id: $ */
/* $Name: $ */

/* NOTICE: This file is the property of Globe Motors and contains */
/* CONFIDENTIAL information.  Any use, copying or dissemination of the */
/* information contained in this file is strictly prohibited unless */
/* authorized by Globe Motors in writing. */

/* Copyright 2017, Globe Motors, Inc. */

/*
 * Revision History:
 *
 * $Log: $
 *
 */

#ifndef CONFIG_H
#define CONFIG_H

#include "misra.h"

#define CONFIG_START_ADDRESS  0x3ee000UL

#define CONFIG_BASE_OFFSET         351UL

#define CONFIG_MAP(x)  (x + CONFIG_START_ADDRESS + CONFIG_BASE_OFFSET)

#define CONFIG_DDEAD               CONFIG_MAP(0UL)
#define CONFIG_DMAX                CONFIG_MAP(2UL)
#define CONFIG_DGAIN               CONFIG_MAP(4UL)
#define CONFIG_DTHRESH             CONFIG_MAP(6UL)
#define CONFIG_TDEAD               CONFIG_MAP(8UL)
#define CONFIG_TSLOPE              CONFIG_MAP(10UL)
#define CONFIG_TMAX                CONFIG_MAP(12UL)

#define CONFIG_FDBK0               CONFIG_MAP(30UL)
#define CONFIG_FDBK1               CONFIG_MAP(31UL)
#define CONFIG_FDBK2               CONFIG_MAP(32UL)
#define CONFIG_FDBK3               CONFIG_MAP(33UL)
#define CONFIG_FDBK4               CONFIG_MAP(34UL)
#define CONFIG_FDBK5               CONFIG_MAP(35UL)
#define CONFIG_FDBK6               CONFIG_MAP(36UL)
#define CONFIG_FDBK7               CONFIG_MAP(37UL)
#define CONFIG_FDBK8               CONFIG_MAP(38UL)
#define CONFIG_FDBK9               CONFIG_MAP(39UL)

#define CONFIG_DTHN1               CONFIG_MAP(50UL)
#define CONFIG_DTHN2               CONFIG_MAP(51UL)
#define CONFIG_DTHN3               CONFIG_MAP(52UL)
#define CONFIG_DTHN4               CONFIG_MAP(53UL)
#define CONFIG_DTHN5               CONFIG_MAP(54UL)
#define CONFIG_DTHN6               CONFIG_MAP(55UL)

typedef enum {
    CONFIG_VALID,
    CONFIG_CORRUPT
}CONFIGState_t;

/* function declarations */
void configInit(void);
CONFIGState_t configGetState(void);

#endif /* CONFIG_H */
