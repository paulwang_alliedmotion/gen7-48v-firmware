/*!
 * \file        j1939Diagnostics.h
 * \author      Zach Haver
 * \brief       J1939 data link layer definitions.
 */

/* $Id: $ */
/* $Name: $ */

/* NOTICE: This file is the property of Globe Motors and contains */
/* CONFIDENTIAL information.  Any use, copying or dissemination of the */
/* information contained in this file is strictly prohibited unless */
/* authorized by Globe Motors in writing. */

/* Copyright 2016, Globe Motors, Inc. */

/*
 * Revision History:
 *
 * $Log: $
 *
 */

#ifndef JDIAG_H
#define JDIAG_H

#include "j1939DataLink.h"
#include "misra.h"

/*! \def JDIAG_PGN_DM1 - Section 73 DM1 parameter group number. */
#define JDIAG_PGN_DM1            0xfecaU
/*! \def JDIAG_PGN_DM2 - Section 73 DM2 parameter group number. */
#define JDIAG_PGN_DM2            0xfecbU
/*! \def JDIAG_PGN_DM3 - Section 73 DM3 parameter group number. */
#define JDIAG_PGN_DM3            0xfeccU
/*! \def JDIAG_PGN_DM7 - Section 73 DM7 parameter group number. */
#define JDIAG_PGN_DM7            0xe300U
#define JDIAG_PGN_DM8            0xfed0U
/*! \def JDIAG_PGN_DM13 - Section 73 DM13 parameter group number. */
#define JDIAG_PGN_DM13           0xdf00U
/*! \def JDIAG_PGN_DM14 - Section 73 DM14 parameter group number. */
#define JDIAG_PGN_DM14           0xd900U
/*! \def JDIAG_PGN_DM15 - Section 73 DM15 parameter group number. */
#define JDIAG_PGN_DM15           0xd800U
/*! \def JDIAG_PGN_DM16 - Section 73 DM16 parameter group number. */
#define JDIAG_PGN_DM16           0xd700U
/*! \def JDIAG_PGN_DM18 - Section 73 DM18 parameter group number. */
#define JDIAG_PGN_DM18           0xd400U
/*! \def JDIAG_DM1_PERIOD - period for teh DM1 message in ms. */
#define JDIAG_DM1_PERIOD           1000U
/*! \def JDIAG_DM13_6_SECONDS - timeout for the DM13 message. */
#define JDIAG_DM13_6_SECONDS       6001U

/*! \enum JDIAGSecurityState_t - memory security state. */
typedef enum {
    JDIAG_SECURITY_LOCKED, /*!< Security Locked No Secure Access. */
    JDIAG_SECURITY_L1     /*!< Security Open Secure Access to all memory. */
}JDIAGSecurityState_t;
/*! \def JDIAG_SECURITY_TIMEOUT - timeout for the secure module for access. */
#define JDIAG_SECURITY_TIMEOUT    10000U

#define JDIAG_DM7_OFFSET        0x0fU
#define JDIAG_DM7_G6_NEO_UV     0x20U
#define JDIAG_DM7_G6_FERRITE_UV 0x21U
#define JDIAG_DM7_G6_REWOUND_UV 0x22U
#define JDIAG_DM7_G6_NEO_ATV    0x23U
#define JDIAG_DM7_G6_NEO_ATV75  0x24U
#define JDIAG_DM7_G6_NEO_UV75   0x25U
#define JDIAG_DM7_G6_OFFSET4    0xdfU
#define JDIAG_DM7_G6_OFFSET5    0xefU

/*! \enum JDIAGMemAccessState_t - memory access states. */
typedef enum {
    MEM_FREE,          /*!< No memory access in progress. */
    MEM_SECURITY_WAIT,       /*!< Waiting for command. */
    MEM_OPERATING
}JDIAGMemAccessState_t;
typedef enum {
    MEM_OP_FREE,
    MEM_OP_WAIT_EEPROM,
    MEM_OP_READ_EEPROM,
    MEM_OP_READ_FLASH
}JDIAGOperationState_t;

/*! \def JDIAG_DM14_CMD_ERASE - DM14 erase command value. */
#define JDIAG_DM14_CMD_ERASE          0U
/*! \def JDIAG_DM14_CMD_Read - DM14 read command value. */
#define JDIAG_DM14_CMD_READ           1U
/*! \def JDIAG_DM14_CMD_WRITE - DM14 write command value. */
#define JDIAG_DM14_CMD_WRITE          2U
/*! \def JDIAG_DM14_CMD_STATUS - DM14 status command value. */
#define JDIAG_DM14_CMD_STATUS         3U
/*! \def JDIAG_DM14_CMD_COMPLETE - DM14 complete command value. */
#define JDIAG_DM14_CMD_COMPLETE       4U
/*! \def JDIAG_DM14_CMD_FAILED - DM14 failed command value. */
#define JDIAG_DM14_CMD_FAILED         5U
/*! \def JDIAG_DM14_CMD_BOOT_LOAD - DM14 boot load command value. */
#define JDIAG_DM14_CMD_BOOT_LOAD      6U
/*! \def JDIAG_DM14_CMD_EDCP - DM14 error detectoin correction param value. */
#define JDIAG_DM14_CMD_EDCP           7U

#define JDIAG_DM15_PROCEED       0x0000U
#define JDIAG_DM15_FAIL_GENERAL  0x0001U
#define JDIAG_DM15_BUSY_ERASE    0x0010U
#define JDIAG_DM15_BUSY_READ     0x0011U
#define JDIAG_DM15_BUSY_WRITE    0x0012U
#define JDIAG_DM15_BUSY_EDCP     0x0017U
#define JDIAG_DM15_BUSY_GENERIC  0x001fU
#define JDIAG_DM15_FAIL_ADDRESS  0x0100U
#define JDIAG_DM15_FAIL_DATA     0x0109U
#define JDIAG_DM15_FAIL_INVALID_KEY  0x1003U
#define JDIAG_DM15_FAIL_SECURITY 0x1000U
#define JDIAG_DM15_COMPLETE 0xffffU

/*! \def JDIAG_MEMSPACE_FLASH - memory space containg the application. */
#define JDIAG_MEMSPACE_FLASH          1U
/*! \def JDIAG_MEMSPACE_EEPROM - memory space for the eeprom. */
#define JDIAG_MEMSPACE_EEPROM         4U
/*! \def JDIAG_MEMSPACE_BOOT - memory space containing the bootkernel. */
#define JDIAG_MEMSPACE_BOOT        0xfeU
/*! \def JDIAG_BOOT_ADDRESS - address for boot load to bk. */
#define JDIAG_BOOT_ADDRESS         0x10U

/*! \def JDAIG_FLASH ADDRESS_LIMIT - max address for the app flash space. */
#define JDIAG_FLASH_ADDRESS_LIMIT   0x18000UL
/*! \def JDIAG_EEPROM_ADDRESS_LIMIT - max read address for the eeprom space. */
#define JDIAG_EEPROM_ADDRESS_LIMIT  0x0800UL
/*! \def JDIAG_MAX_WRITE_LENGTH - max number of bytes per write access. */
#define JDIAG_MAX_WRITE_LENGTH      (JDL_MAX_RX_SIZE - 1U)
/*! \def JDIAG_MAX_READ_LENGTH - max number of bytes per read access. */
#define JDIAG_MAX_READ_LENGTH       (JDL_MAX_TX_SIZE - 1U)

/*! \def JDIAG_FLASH_START - start address in flash for the application. */
#define JDIAG_FLASH_START         0x3e8000UL
/*! \struct JDiagMemAccess_t
 *  \brief  Structure to hold the information on a single memory access.
 */
typedef struct {
    JDIAGMemAccessState_t state; /*!< State of the current access. */
    JDIAGOperationState_t opState;
    UINT_16 length;              /*!< length of the current access. */
    UINT_8 command;              /*!< current access command. */
    UINT_32 address;             /*!< address of the current access. */
    UINT_8 memSpace;             /*!< memory space of the current access. */
    UINT_16 userLevel;           /*!< user level of the current access. */
    UINT_32 seed;                 /*!< security seed. */
    JDIAGSecurityState_t securityState; /*!< current state of security. */
    UINT_16 securityTime;        /*!< time left for open security. */
    J1939msg_t msg;
} JDiagMemAccess_t;

void jdiagInit(void);
void jdiagMessageProcess(const J1939msg_t *pmsg);
void jdiagTimer(void);
void jdiagTripDM1(void);
void jdiagDM1(const J1939msg_t *pmsg);
void jdiagDM2(const J1939msg_t *pmsg);
void jdiagDM3(const J1939msg_t *pmsg);
void jdiagDM15Reboot(void);

#endif /* JDIAG_H */
