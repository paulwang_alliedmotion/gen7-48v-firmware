/*!
 * \file      mctrl.c
 * \author    Zach Haver
 * \brief     Main motor control algorithms. The routines in this file will
 *            determine the correct mode of operation and call the
 *            appropriate routines.
 */

/* $Id: $ */
/* $Name: $ */

/* NOTICE: This file is the property of Globe Motors and contains */
/* CONFIDENTIAL information.  Any use, copying or dissemination of the */
/* information contained in this file is strictly prohibited unless */
/* authorized by Globe Motors in writing. */

/* Copyright 2016, Globe Motors, Inc. */

/*
 * Revision History:
 *
 * $Log: $
 *
 */

#include "adc.h"
#include "bridge.h"
#include "config.h"
#include "eqep.h"
#include "faultLamp.h"
#include "ictrl.h"
#include "ign.h"
#include "IQmathLib.h"
#include "j1939Application.h"
#include "mctrl.h"
#include "misra.h"
#include "opstate.h"
#include "tctrl.h"
#include "utils.h"

static _iq mctrlCalcD(void);
static void mctrlGenEMCCMD(_iq *q, _iq *d);
/* private variables */

/*! \var mctrlCmd - Motor control structure. */
static MCTRLCMD_t mctrlCmd = MCTRLCMD_DEFAULTS;

static _iq dAxisDead = _IQ(0.5L);
static _iq dAxisMax = _IQ(60.0L);
static _iq dAxisGain = _IQ(50.0L);
static _iq dAxisThresh = _IQ(10.0L);

/* public functions */
void
mctrlInit(void) {
    if (CONFIG_VALID == configGetState()) {
        /*lint -save -e835 */
        dAxisDead = mapIQ(CONFIG_DDEAD);
        /*lint -restore */
        dAxisMax = mapIQ(CONFIG_DMAX);
        dAxisGain = mapIQ(CONFIG_DGAIN);
        dAxisThresh = mapIQ(CONFIG_DTHRESH);
    }
}

/*!
 * \fn     void mctrlControl (void)
 *
 * \brief  This function handles the control modes of the motor
 */
void
mctrlControl(void) {
    _iq q = 0L;
    _iq d = 0L;

    tctrlCalcInput();
    switch (mctrlCmd.mode) {
    case MCTRL_CMD_OFF:
        opsUpdateFlag(OPS_SET_OFF);
        q = 0L;
        d = 0L;
        break;
    case MCTRL_CMD_I:
        opsUpdateFlag(OPS_CLR_OFF);
        q = mctrlCmd.v1;
        d = mctrlCmd.v2;
        break;
    case MCTRL_CMD_T:
        opsUpdateFlag(OPS_CLR_OFF);
        tctrlGenCMD(&q, &d);
        break;
    case MCTRL_CMD_S:
        opsUpdateFlag(OPS_SET_OFF);
        q = 0L;
        d = 0L;
        break;
    case MCTRL_CMD_ST:
        opsUpdateFlag(OPS_CLR_OFF);
        q = mctrlCmd.v1;
        d = mctrlCalcD();
        break;
    case MCTRL_CMD_RAM_TEST:
        opsUpdateFlag(OPS_SET_OFF);
        q = 0L;
        d = 0L;
        jappChangeRAMTest();
        mctrlCmd.mode = MCTRL_CMD_OFF;
        break;
    case MCTRL_CMD_STOP_WDOG:
        bridgeWDOGState(1U);
        break;
    case MCTRL_CMD_START_WDOG:
        bridgeWDOGState(0U);
        break;
    case MCTRL_CMD_TOFF:
        opsUpdateFlag(OPS_SET_OFF);
        q = 0L;
        d = 0L;
        tctrlUpdateOffset(mctrlCmd.v1);
        mctrlCmd.mode = MCTRL_CMD_OFF;
        break;
    case MCTRL_CMD_CLR_ADC:
        adcClearMaxMin();
        mctrlCmd.mode = MCTRL_CMD_OFF;
        break;
    case MCTRL_CMD_THETA_NORM:
        eqepSetThetaNorm();
        mctrlCmd.mode = MCTRL_CMD_OFF;
        break;
    case MCTRL_CMD_THETA_SET:
        eqepSetThetaFixed(mctrlCmd.v1);
        mctrlCmd.mode = MCTRL_CMD_OFF;
        break;
    case MCTRL_CMD_THETA_RAMP:
        eqepSetThetaRamp(mctrlCmd.v1);
        mctrlCmd.mode = MCTRL_CMD_OFF;
        break;
    case MCTRL_CMD_KA_AUTO:
        ignSetMode(IGN_KA_AUTO);
        break;
    case MCTRL_CMD_KA_ON:
        ignSetMode(IGN_KA_ON);
        break;
    case MCTRL_CMD_KA_OFF:
        ignSetMode(IGN_KA_OFF);
        break;
    case MCTRL_CMD_FAULT_TOGGLE:
        faultLampStateSet(FAULT_TOGGLE);
        mctrlCmd.mode = MCTRL_CMD_OFF;
        break;
    case MCTRL_CMD_FAULT_ON:
        faultLampStateSet(FAULT_ON);
        mctrlCmd.mode = MCTRL_CMD_OFF;
        break;
    case MCTRL_CMD_FAULT_OFF:
        faultLampStateSet(FAULT_OFF);
        mctrlCmd.mode = MCTRL_CMD_OFF;
        break;
    case MCTRL_CMD_FDBK_MSG:
        jappSetTxState(JAPP_FDBK);
        mctrlCmd.mode = MCTRL_CMD_OFF;
        break;
    case MCTRL_CMD_DTHN_MSG:
        jappSetTxState(JAPP_DTHN);
        mctrlCmd.mode = MCTRL_CMD_OFF;
        break;
    case MCTRL_CMD_ALL_MSG:
        jappSetTxState(JAPP_ALL);
        mctrlCmd.mode = MCTRL_CMD_OFF;
        break;
    case MCTRL_CMD_UPDATE_I_LIM:
        ictrlUpdateILimit();
        tctrlUpdateILimit();
        mctrlCmd.mode = MCTRL_CMD_OFF;
        break;
    case MCTRL_CMD_EMC:
        opsUpdateFlag(OPS_CLR_OFF);
        mctrlGenEMCCMD(&q, &d);
        break;
    default:
        mctrlCmd.mode = MCTRL_CMD_OFF;
        break;
    }

    ictrlSetRef(q, d);
}

/*!
 * \fn     void mctrlSetCMD (MCTRLCMD_t cmd)
 *
 * \param  cmd - motor control command container
 * \brief  This function set the control mode of the motor
 */
void
mctrlSetCMD(MCTRLCMD_t cmd) {
    mctrlCmd.mode = cmd.mode;
    mctrlCmd.v1 = cmd.v1;
    mctrlCmd.v2 = cmd.v2;
}

static void
mctrlGenEMCCMD(_iq *q, _iq *d) {
    static UINT_16 timerCount = 0U;
    static _iq slope;
    static _iq time;

    if (250U >= timerCount) {
        /* ramping up 250ms */
        slope = mctrlCmd.v2 - mctrlCmd.v1;
        slope = _IQdiv(slope, _IQ(250.0L));
        time = (_iq)((UINT_32)timerCount << 18);
        slope = _IQmpy(time, slope);
        slope += mctrlCmd.v1;
        *q = slope;
    } else if (550U >= timerCount) {
        /* hold max for 300 ms */
        *q = mctrlCmd.v2;
    } else if (800U >= timerCount) {
        /* ramping down 250 ms */
        slope = mctrlCmd.v1 - mctrlCmd.v2;
        slope = _IQdiv(slope, _IQ(250.0L));
        time = (_iq)((UINT_32)timerCount << 18);
        time -= _IQ(550.0L);
        slope = _IQmpy(time, slope);
        slope += mctrlCmd.v2;
        *q = slope;
    } else {
        /* hold min for 1000 ms */
        *q = mctrlCmd.v1;
    }
    timerCount++;
    if (1800U < timerCount) {
        timerCount = 0U;
    }
    *d = _IQ(0.0L);
}

static _iq
mctrlCalcD(void) {
    _iq gain = _IQmpy(dAxisGain, _IQ(0.0016666667L));
    _iq dSpeed = _IQabs(eqepGetVelocity());
    UINT_16 batt = adcGetRaw(ADC_BATT_ON);
    UINT_32 shifter = (UINT_32)batt << 18;
    _iq vBatt = _IQmpy((_iq)shifter, _IQ(0.037037L));
    _iq thresh = _IQmpy(dAxisThresh, _IQ(3.6363636L));
    _iq wBase = _IQmpy(vBatt, thresh);
    _iq dI = 0L;
    _iq tIn = tctrlGetTin();

    if (dSpeed > wBase) {
        dI = _IQmpy((dSpeed - wBase), gain); /* Gain */
    }

    if (dI > dAxisMax) { /* Max */
        dI = dAxisMax;
    } else if (dI < 0L) {
        dI = 0L;
    } else {
        /* no change needed */
        ;
    }

    if (_IQabs(tIn) < dAxisDead) { /* Deadband */
        dI = 0L;
    }

    return dI;
}
