/*!
 * \file      isr.c
 * \author    Zach Haver
 * \brief     interrupt service routines for the TI TMS320C28033/35 Piccolo.
 */

/* $Id: $ */
/* $Name: $ */

/* NOTICE: This file is the property of Globe Motors and contains */
/* CONFIDENTIAL information.  Any use, copying or dissemination of the */
/* information contained in this file is strictly prohibited unless */
/* authorized by Globe Motors in writing. */

/* Copyright 2016, Globe Motors, Inc. */

/*
 * Revision History:
 *
 * $Log: $
 *
 */

#include "adc.h"
#include "bridge.h"
#include "can.h"
#include "claShared.h"
#include "ecap.h"
#include "epwm.h"
#include "eqep.h"
#include "i2c.h"
#include "ictrl.h"
#include "isr.h"
#include "misra.h"
#include "panic.h"
#include "piccolo.h"
#include "opstate.h"
#include "sched.h"
#include "spi.h"
#include "tmr0.h"

/* interrupts */
static interrupt void isrPANIC(void);
static interrupt void isrINT1(void);
static interrupt void isrINT3(void);
static interrupt void isrINT4(void);
static interrupt void isrINT6(void);
static interrupt void isrINT8(void);
static interrupt void isrINT9(void);
static interrupt void isrINT11(void);
static interrupt void isrINT12(void);

/* private function declarations */

/* private variable declarations */
#pragma DATA_SECTION (theta_cpu, "CpuToClaMsgRAM");
static UINT_32 theta_cpu;
#pragma DATA_SECTION (thetaP_cpu, "CpuToClaMsgRAM");
static UINT_32 thetaP_cpu;

static UINT_32 isrThetaPoles = 3UL;

/* public functions */

/*!
 * \fn     void isrVectorInit (void)
 * \brief  This function initializes the PIE vector table
 *         with the interrupt routines
 *
 */
void
isrVectorInit(void) {
    /*lint -save -e923 */
    *PIE_INT1 = isrINT1;
    *PIE_INT2 = isrPANIC;
    *PIE_INT3 = isrINT3;
    *PIE_INT4 = isrINT4;
    *PIE_INT5 = isrPANIC;
    *PIE_INT6 = isrINT6;
    *PIE_INT7 = isrPANIC;
    *PIE_INT8 = isrINT8;
    *PIE_INT9 = isrINT9;
    *PIE_INT10 = isrPANIC;
    *PIE_INT11 = isrINT11;
    *PIE_INT12 = isrINT12;
    *PIE_INT13 = isrPANIC;
    *PIE_INT14 = isrPANIC;
    *PIE_DATALOG = isrPANIC;
    *PIE_RTOSINT = isrPANIC;
    *PIE_EMUINT = isrPANIC;
    *PIE_NMI = isrPANIC;
    *PIE_ILLEGAL = isrPANIC;
    *PIE_INT1_1 = isrPANIC;
    *PIE_INT1_2 = isrPANIC;
    *PIE_INT1_3 = isrPANIC;
    *PIE_INT1_4 = isrINT1;
    *PIE_INT1_5 = isrINT1;
    *PIE_INT1_6 = isrPANIC;
    *PIE_INT1_7 = isrPANIC;
    *PIE_INT1_8 = isrPANIC;
    *PIE_INT2_1 = isrPANIC;
    *PIE_INT2_2 = isrPANIC;
    *PIE_INT2_3 = isrPANIC;
    *PIE_INT2_4 = isrPANIC;
    *PIE_INT2_5 = isrPANIC;
    *PIE_INT2_6 = isrPANIC;
    *PIE_INT2_7 = isrPANIC;
    *PIE_INT2_8 = isrPANIC;
    *PIE_INT3_1 = isrPANIC;
    *PIE_INT3_2 = isrPANIC;
    *PIE_INT3_3 = isrPANIC;
    *PIE_INT3_4 = isrPANIC;
    *PIE_INT3_5 = isrINT3;
    *PIE_INT3_6 = isrPANIC;
    *PIE_INT3_7 = isrPANIC;
    *PIE_INT3_8 = isrPANIC;
    *PIE_INT4_1 = isrINT4;
    *PIE_INT4_2 = isrPANIC;
    *PIE_INT4_3 = isrPANIC;
    *PIE_INT4_4 = isrPANIC;
    *PIE_INT4_5 = isrPANIC;
    *PIE_INT4_6 = isrPANIC;
    *PIE_INT4_7 = isrPANIC;
    *PIE_INT4_8 = isrPANIC;
    *PIE_INT5_1 = isrPANIC;
    *PIE_INT5_2 = isrPANIC;
    *PIE_INT5_3 = isrPANIC;
    *PIE_INT5_4 = isrPANIC;
    *PIE_INT5_5 = isrPANIC;
    *PIE_INT5_6 = isrPANIC;
    *PIE_INT5_7 = isrPANIC;
    *PIE_INT5_8 = isrPANIC;
    *PIE_INT6_1 = isrINT6;
    *PIE_INT6_2 = isrINT6;
    *PIE_INT6_3 = isrPANIC;
    *PIE_INT6_4 = isrPANIC;
    *PIE_INT6_5 = isrPANIC;
    *PIE_INT6_6 = isrPANIC;
    *PIE_INT6_7 = isrPANIC;
    *PIE_INT6_8 = isrPANIC;
    *PIE_INT7_1 = isrPANIC;
    *PIE_INT7_2 = isrPANIC;
    *PIE_INT7_3 = isrPANIC;
    *PIE_INT7_4 = isrPANIC;
    *PIE_INT7_5 = isrPANIC;
    *PIE_INT7_6 = isrPANIC;
    *PIE_INT7_7 = isrPANIC;
    *PIE_INT7_8 = isrPANIC;
    *PIE_INT8_1 = isrINT8;
    *PIE_INT8_2 = isrINT8;
    *PIE_INT8_3 = isrPANIC;
    *PIE_INT8_4 = isrPANIC;
    *PIE_INT8_5 = isrPANIC;
    *PIE_INT8_6 = isrPANIC;
    *PIE_INT8_7 = isrPANIC;
    *PIE_INT8_8 = isrPANIC;
    *PIE_INT9_1 = isrPANIC;
    *PIE_INT9_2 = isrPANIC;
    *PIE_INT9_3 = isrPANIC;
    *PIE_INT9_4 = isrPANIC;
    *PIE_INT9_5 = isrPANIC;
    *PIE_INT9_6 = isrINT9;
    *PIE_INT9_7 = isrPANIC;
    *PIE_INT9_8 = isrPANIC;
    *PIE_INT10_1 = isrPANIC;
    *PIE_INT10_2 = isrPANIC;
    *PIE_INT10_3 = isrPANIC;
    *PIE_INT10_4 = isrPANIC;
    *PIE_INT10_5 = isrPANIC;
    *PIE_INT10_6 = isrPANIC;
    *PIE_INT10_7 = isrPANIC;
    *PIE_INT10_8 = isrPANIC;
    *PIE_INT11_1 = isrINT11;
    *PIE_INT11_2 = isrINT11;
    *PIE_INT11_3 = isrPANIC;
    *PIE_INT11_4 = isrPANIC;
    *PIE_INT11_5 = isrPANIC;
    *PIE_INT11_6 = isrPANIC;
    *PIE_INT11_7 = isrPANIC;
    *PIE_INT11_8 = isrINT11;
    *PIE_INT12_1 = isrINT12;
    *PIE_INT12_2 = isrPANIC;
    *PIE_INT12_3 = isrPANIC;
    *PIE_INT12_4 = isrPANIC;
    *PIE_INT12_5 = isrPANIC;
    *PIE_INT12_6 = isrPANIC;
    *PIE_INT12_7 = isrPANIC;
    *PIE_INT12_8 = isrPANIC;
    /*lint -restore */

    /* Set External Interrupt Pins */
    /* XINT1 will be the fault line of the DRV8305 */
    *GPIOXINT1SEL = 0x0006U;
    /* XINT2 will be the timeout of the external watchdog */
    *GPIOXINT2SEL = 0x0008U;
    /* XINT3 will be used for the RPM discrete signal */
    *GPIOXINT3SEL = 0x0016U;

    /* enable external interrupts */
    *XINT1CR = 0x0009U;
    *XINT2CR = 0x0009U;
    *XINT3CR = 0x0009U;

    thetaTrans_cpu = &theta_cpu;
    thetaPoles_cpu = &thetaP_cpu;
}

void
isrSetThetaPoles(UINT_32 poles) {
    isrThetaPoles = poles;
}

/* interrupts */

/*!
 * \fn     static interrupt void isrPANIC (void)
 * \brief  This function is used by all interrupts that should not
 *         be enabled. It replaces the reset vector to do housekeeping
 *         then use the "ESTOP" command to halt the processor
 *
 */
static interrupt void
isrPANIC(void) {
    PANIC ();
}

/*!
 * \fn     static interrupt void isrINT1 (void)
 * \brief  This function handles the level 1 interrupt executions
 *
 */
static interrupt void
isrINT1(void) {
    UINT_16 vectorAddress;

    vectorAddress = (*PIECTRL & ISR_VECTOR_MASK);

    switch (vectorAddress) {
    case ISR_XINT1:
        bridgeFaultINT();
        break;
    case ISR_XINT2:
        bridgeTimeoutINT();
        break;
    default:
        /* Unused interrupt */
        PANIC();
        break;
    }

    *PIEACK |= ISR_ACK_GROUP1;
}

static interrupt void
isrINT3(void) {
    UINT_16 vectorAddress;

    vectorAddress = (*PIECTRL & ISR_VECTOR_MASK);

    switch (vectorAddress) {
    case ISR_EPWM5INT:
        theta_cpu = eqepGetThetaCounts();
        thetaP_cpu = isrThetaPoles;
        epwmINT5Handler();
        break;
    default:
        /* Unused interrupt */
        PANIC();
        break;
    }
    *PIEACK |= ISR_ACK_GROUP3;
}

static interrupt void
isrINT4(void) {
    UINT_16 vectorAddress;

    vectorAddress = (*PIECTRL & ISR_VECTOR_MASK);

    switch (vectorAddress) {
    case ISR_ECAP1INT:
        ecapEvent();
        break;
    default:
        /* Unused interrupt */
        PANIC();
        break;
    }
    *PIEACK |= ISR_ACK_GROUP4;
}

/*!
 * \fn     static interrupt void isrINT6 (void)
 * \brief  This function handles the level 6 interrupt executions
 *
 */
static interrupt void
isrINT6(void) {
    UINT_16 vectorAddress;

    vectorAddress = (*PIECTRL & ISR_VECTOR_MASK);

    switch (vectorAddress) {
    case ISR_SPIRXINTA:
    case ISR_SPITXINTA:
        spiInterruptHandler();
        break;
    default:
        /* Unused interrupt */
        PANIC();
        break;
    }
    *PIEACK |= ISR_ACK_GROUP6;
}

static interrupt void
isrINT8(void) {
    UINT_16 vectorAddress;

    vectorAddress = (*PIECTRL & ISR_VECTOR_MASK);

    switch (vectorAddress) {
    case ISR_I2C_1A :
         i2cInterruptHandler();
        break;
    default:
        /* Unused interrupt */
        PANIC();
        break;
    }
    *PIEACK |= ISR_ACK_GROUP8;
}

/*!
 * \fn     static interrupt void isrINT9 (void)
 * \brief  This function handles the level 9 interrupt executions
 *
 */
static interrupt void
isrINT9(void) {
    UINT_16 vectorAddress;
    UINT_16 mb;
    UINT_32 shadow = 0UL;

    vectorAddress = (*PIECTRL & ISR_VECTOR_MASK);
    switch (vectorAddress) {
    case ISR_ECAN1INTA:
        shadow = *ECANA_CANGIF1;
        mb = (UINT_16)(shadow & 0x1fUL);
        if ((shadow & BIT32_15) != 0UL) {
            if (0U == mb) {
                canTAHandler();
            } else {
                canRMHandler(mb);
            }
        }
        break;
    default:
        /* Unused interrupt */
        PANIC();
        break;
    }
    *PIEACK |= ISR_ACK_GROUP9;
}

/*!
 * \fn     static interrupt void isrINT11 (void)
 * \brief  This function handles the level 11 interrupt executions
 *
 */
static interrupt void
isrINT11(void) {
    UINT_16 vectorAddress;

    vectorAddress = (*PIECTRL & ISR_VECTOR_MASK);

    switch (vectorAddress) {
    case ISR_CLA1INT:
        if (71U == exec_cla) {
            opsUpdateFlag(OPS_CLR_INIT_I);
        }
        adcINT1();
        break;
    case ISR_CLA2INT:
        adcINT2();
        *GPATOGGLE |= 0x00000200UL;
        bridgeWDogKick();
        schedTick();
        if (4UL < driftExec_cla) {
            opsUpdateFlag(OPS_SET_DRIFT);
        } else {
            opsUpdateFlag(OPS_CLR_DRIFT);
        }
        break;
    case ISR_CLA8INT:
        break;
    default:
        /* Unused interrupt */
        PANIC();
        break;
    }
    *PIEACK |= ISR_ACK_GROUP11;
}

/*!
 * \fn     static interrupt void isrINT12(void)
 * \brief  This function handles the level 12 interrupt executions
 *
 */
static interrupt void
isrINT12(void) {
    UINT_16 vectorAddress;

    vectorAddress = (*PIECTRL & ISR_VECTOR_MASK);

    switch (vectorAddress) {
    case ISR_XINT3:
        tmr0RisingEdge();
        break;
    default:
        /* Unused interrupt */
        PANIC();
        break;
    }
    *PIEACK |= ISR_ACK_GROUP12;
}
