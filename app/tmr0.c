/*!
 * \file    tmr0.c
 * \author  Zach Haver
 * \brief   Timer 0 implementation for RPM signal
 *
 */

/* $Id: $ */
/* $Name: $ */

/* NOTICE: This file is the property of Globe Motors and contains */
/* CONFIDENTIAL information.  Any use, copying or dissemination of the */
/* information contained in this file is strictly prohibited unless */
/* authorized by Globe Motors in writing. */

/* Copyright 2016, Globe Motors, Inc. */

/*
 * Revision History:
 *
 * $Log: $
 *
 */

#include "eeprom.h"
#include "misra.h"
#include "piccolo.h"
#include "tmr0.h"

/* private variable declarations */
static UINT_16 tmr0Hz = 0U;
static UINT_16 tmr0PulseCount = 0U;
/* public functions */

void
tmr0Init(void) {
    PICCOLO_EALLOW();
    *PCLKCR3 |= 0x0100U;

    /* stop the timer */
    *TIMER0TCR |= 0x0010U;
    /* set the period of the timer to the maximum */
    *TIMER0PRD = 0xffffU;
    *TIMER0PRDH = 0xffffU;
    /* clear the interrupt flag */
    *TIMER0TCR |= 0x8000U;
    /* start the timer */
    *TIMER0TCR &= ~0x0010U;
    /* reset the period */
    *TIMER0TCR |= 0x0020U;

    PICCOLO_EDIS();
}

void
tmr0RisingEdge(void) {
    UINT_16 high = *TIMER0TIMH;
    UINT_16 low = *TIMER0TIM;
    UINT_32 count = ((UINT_32)high << 16) | (UINT_32)low;
    UINT_32 hz;
    UINT_16 tempHz;
    static UINT_16 hzOld = 0U;

    /* invert count to accomodate count down */
    count = 0xffffffffUL - count;
    /* load to tmr0 variable clamp between 1Hz and 7.5kHz */
    if (8000UL >= count) {
        count = 8000U;
    }
    if (60000000UL <= count) {
        count = 60000000UL;
    }
    /* calculate rpm */
    hz = 60000000UL / count;
    tempHz = (UINT_16)(((31UL * (UINT_32)hzOld) + (1UL * hz)) >> 5);
    tmr0Hz = tempHz;
    hzOld = tmr0Hz;

    /* clear the interrupt flag */
    *TIMER0TCR |= 0x8000U;
    /* reset the timer */
    *TIMER0TCR |= 0x0020U;

    /* increment the pulse counter */
    tmr0PulseCount++;
}

UINT_16
tmr0GetHz(void) {
    UINT_16 ret = 0U;

    if (0U < tmr0PulseCount) {
        ret = tmr0Hz;
        tmr0PulseCount = 0;
    }

    return ret;
}
