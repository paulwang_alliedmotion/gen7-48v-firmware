/*!
 * \file      faultlamp.c
 * \author    Zach Haver
 * \brief     Handles fault lamp toggling and inversion determination.
 */

/* $Id: $ */
/* $Name: $ */

/* NOTICE: This file is the property of Globe Motors and contains */
/* CONFIDENTIAL information.  Any use, copying or dissemination of the */
/* information contained in this file is strictly prohibited unless */
/* authorized by Globe Motors in writing. */

/* Copyright 2016, Globe Motors, Inc. */

/*
 * Revision History:
 *
 * $Log: $
 *
 */

#include "faultLamp.h"
#include "piccolo.h"

static FAULTLAMPState_t faultLampState = FAULT_TOGGLE;

void
faultLampStateSet(FAULTLAMPState_t newState) {
    faultLampState = newState;
}

void
faultLampToggle(void) {
    switch (faultLampState) {
    case FAULT_ON:
        *GPADAT |= 0x00000400UL;
        break;
    case FAULT_OFF:
        *GPADAT &= ~0x00000400UL;
        break;
    case FAULT_TOGGLE:
    default:
        *GPATOGGLE |= 0x00000400UL;
        break;

    }
}
