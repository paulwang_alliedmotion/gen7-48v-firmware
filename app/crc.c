/*!
 * \file      crc.c
 * \author    Zach Haver
 * \brief     Cyclic Redundancy Check functionality
 */

/* $Id: $ */
/* $Name: $ */

/* NOTICE: This file is the property of Globe Motors and contains */
/* CONFIDENTIAL information.  Any use, copying or dissemination of the */
/* information contained in this file is strictly prohibited unless */
/* authorized by Globe Motors in writing. */

/* Copyright 2016, Globe Motors, Inc. */

/*
 * Revision History:
 *
 * $Log: $
 *
 */

#include "crc.h"
#include "dtc.h"
#include "eeprom.h"
#include "misra.h"
#include "utils.h"

static UINT_32 crc32(UINT_32 newByte, UINT_32 crc);

void
crcBK(void) {
    UINT_32 crcCalc = crcVerifyBlockFlash(0x3f4000UL, 0x3f00UL);
    UINT_8 crcBytes[4];
    UINT_32 oldCRC;

    (void)eepromReadXBytes(EEADDR_BK_CRC_L, crcBytes, 4U);
    oldCRC = bytesToU32(crcBytes, LSB);
    if (oldCRC != crcCalc) {
        (void)eepromWriteDWord(EEADDR_BK_CRC_L, crcCalc);
    }
}

/*!
 * \fn     UINT_32 crcVerifyBlockFlash(UINT_32 offset, UINT_32 length)
 *
 * \param  offset - memory offset in flash for calculation to start.
 * \param  length - number of flash addresses to includ in calculation.
 * \return The calculated crc of the requested space.
 * \brief  This function will calculate the crc of the passed memory space.
 */
UINT_32
crcVerifyBlockFlash(UINT_32 offset, UINT_32 length) {
    UINT_32 x;
    UINT_32 crc = CRC_INIT;
    const UINT_16 *data;

    /*lint -save -e923 */
    data = (UINT_16 *)offset;
    /*lint -restore */

    for (x = 0UL; x < length; x++) {
        crc = crc32((((UINT_32)data[x] >> 8) & 0xffUL), crc);
        crc = crc32(((UINT_32)data[x] & 0xffUL), crc);
    }
    return ~crc;
}

/*!
 * \fn     UINT_32 crc32(UINT_32 newByte, UINT_32)
 *
 * \param  newByte - next byte to add to the calculation.
 * \param  crc - current crc to add the new byte to.
 * \return new crc value.
 * \brief  This function adds newByte to the crc value that is passed,
 *         and returns the new crc value.
 *         (Note: this function is executed in RAM.)
 */
#pragma CODE_SECTION (crc32, "secureRamFuncs");
static UINT_32
crc32(UINT_32 newByte, UINT_32 crc) {
    /*lint -save -e923 */
    const UINT_32 *crcTable = (UINT_32 *)CRC_TABLE_START;
    /*lint -restore */

    return ((crc >> 8) ^ crcTable[(newByte ^ (crc & 0xffUL))]);
}
