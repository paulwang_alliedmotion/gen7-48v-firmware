/*!
 * \file      utils.h
 * \author    Zach Haver
 * \brief     conversion definitions
 */

/* $Id: $ */
/* $Name: $ */

/* NOTICE: This file is the property of Globe Motors and contains */
/* CONFIDENTIAL information.  Any use, copying or dissemination of the */
/* information contained in this file is strictly prohibited unless */
/* authorized by Globe Motors in writing. */

/* Copyright 2017, Globe Motors, Inc. */

/*
 * Revision History:
 *
 * $Log: $
 *
 */

#ifndef UTILS_H
#define UTILS_H

#include "IQmathLib.h"
#include "misra.h"

#define MAP_U8(x) ((UINT_8 *) (x))
#define MAP_U16(x) ((UINT_16 *) (x))
#define MAP_S16(x) ((SINT_16 *) (x))

typedef enum {
    LSB,
    MSB
} UtilEnd_t;

_iq mapIQ(const UINT_32 loc);
UINT_16 bytesToU16(const UINT_8 *bytes, UtilEnd_t endian);
UINT_32 bytesToU32(const UINT_8 *bytes, UtilEnd_t endian);
_iq bytesToIQ(const UINT_8 *bytes);
void u16ToBytes(UINT_16 val, UINT_8 *bytes, UtilEnd_t endian);
void u32ToBytes(UINT_32 val, UINT_8 *bytes, UtilEnd_t endian);

#endif /* UTILS_H */
