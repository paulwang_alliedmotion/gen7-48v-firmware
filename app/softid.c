/*!
 * \file      softid.c
 * \author    Zach Haver
 * \author    Devin Kloos
 * \brief     Software Identifier string definition module
 *
 */

/* $Id: $ */
/* $Name: $ */

/* NOTICE: This file is the property of Globe Motors and contains */
/* CONFIDENTIAL information.  Any use, copying or dissemination of the */
/* information contained in this file is strictly prohibited unless */
/* authorized by Globe Motors in writing. */

/* Copyright 2016, Globe Motors, Inc. */

/*
 * Revision History:
 *
 * $Log: $
 *
 */

#include <string.h>

#include "crc.h"
#include "eeprom.h"
#include "misra.h"
#include "softid.h"

/* private function prototypes */
static UINT_16 f0(UINT_8 *d);
static UINT_16 f1(UINT_8 *d);
static UINT_16 f2(UINT_8 *d);
static UINT_16 f3(UINT_8 *d);
static UINT_16 f4(UINT_8 *d);
static UINT_16 f5(UINT_8 *d);
static UINT_8 crc2Ascii(UINT_8 num);

/* private variables */
/* Declaring a function pointer array */
typedef UINT_16 (*fp_t)(UINT_8 *d);
static fp_t fp_arr[NUM_FIELDS];

extern UINT_32 VERSION;
extern UINT_32 MODEL;
/* public functions */

/*!
 * \fn    void softIdInit(void)
 *
 * \brief This function will fetch or set all information to be reported in
 *        the software identification message.
 */
void
softIdInit(void) {
    fp_arr[0] = f0;
    fp_arr[1] = f1;
    fp_arr[2] = f2;
    fp_arr[3] = f3;
    fp_arr[4] = f4;
    fp_arr[5] = f5;
}

/*!
 * \fn     UINT_16 softIdLoadData(UINT_8 *data)
 *
 * \param  *data - pointer to a data array to be filled with the info.
 * \return length of data added to the data array.
 * \brief  This function will load the passed array with the data for
 *         the software identification message and return the length
 *         of that data.
 */
UINT_16
softIdLoadData(UINT_8 *data) {
    UINT_8 *dst = data;
    UINT_8 curr = 0;
    UINT_8 i;

    for (i = 0;  i < NUM_FIELDS;  i++) {
        curr += fp_arr[i](&dst[curr]);
    }

    return curr;
}

/* private functions */

/*
* This function fetchs data field 1, the number of data fields
*/
static UINT_16 f0(UINT_8 * d) {
    d[0] = NUM_FIELDS - 1U;
    d[1] = (UINT_8)'*';
    return 2U;
}

static UINT_16 f1(UINT_8 * d) {
    const UINT_8 *data = (UINT_8 *)&MODEL;
    UINT_16 len = strlen((const CHAR_8 *)data);

    memcpy(d, data, len);
    d[len] = (UINT_8)'*';
    return len + 1U;
}

static UINT_16 f2(UINT_8 * d) {
    UINT_16 len = 11U;
    UINT_8 temp[11];

    (void)eepromReadXBytes(EEADDR_SN, temp, len);
    memcpy(d, temp, len);
    d[len] = (UINT_8)'*';
    return len + 1U;
}

static UINT_16 f3(UINT_8 * d) {
    const UINT_8 *data = (UINT_8 *)&VERSION;
    UINT_16 len = strlen((const CHAR_8 *)data);

    memcpy(d, data, len);
    d[len] = (UINT_8)'*';
    return len + 1U;
}

static UINT_16 f4(UINT_8 * d) {
    UINT_8 len = 8U;
    UINT_8 crc[4U];
    (void)eepromReadXBytes(EEADDR_APP_CRC_L, crc, 4U);

    d[0] = crc2Ascii((crc[3] >> 4U) & 0xfU);
    d[1] = crc2Ascii(crc[3] & 0xfU);
    d[2] = crc2Ascii((crc[2] >> 4U) & 0xfU);
    d[3] = crc2Ascii(crc[2] & 0xfU);
    d[4] = crc2Ascii((crc[1] >> 4U) & 0xfU);
    d[5] = crc2Ascii(crc[1] & 0xfU);
    d[6] = crc2Ascii((crc[0] >> 4U) & 0xfU);
    d[7] = crc2Ascii(crc[0] & 0xfU);

    d[len] = (UINT_8)'*';
    return len + 1U;
}

static UINT_16 f5(UINT_8 * d) {
    UINT_8 len = 8U;
    UINT_8 crc[4U];
    (void)eepromReadXBytes(EEADDR_BK_CRC_L, crc, 4U);

    d[0] = crc2Ascii((crc[3] >> 4U) & 0xfU);
    d[1] = crc2Ascii(crc[3] & 0xfU);
    d[2] = crc2Ascii((crc[2] >> 4U) & 0xfU);
    d[3] = crc2Ascii(crc[2] & 0xfU);
    d[4] = crc2Ascii((crc[1] >> 4U) & 0xfU);
    d[5] = crc2Ascii(crc[1] & 0xfU);
    d[6] = crc2Ascii((crc[0] >> 4U) & 0xfU);
    d[7] = crc2Ascii(crc[0] & 0xfU);

    d[len] = (UINT_8)'*';
    return len + 1U;
}

/*!
 * \fn     UINT_8 crc2Ascii(UINT_16 num)
 *
 * \param  num - the number to be converted to hex ascii.
 * \return the hex ascii representation of num.
 * \brief  This function will take a number representing a single hex digit,
 *         and return the hex ascii represetation of the number.
 */
static UINT_8
crc2Ascii(UINT_8 num) {
    UINT_8 ret = 0x30U;

    switch (num) {
    case 0x0U:
        ret = 0x30U;
        break;
    case 0x1U:
        ret = 0x31U;
        break;
    case 0x2U:
        ret = 0x32U;
        break;
    case 0x3U:
        ret = 0x33U;
        break;
    case 0x4U:
        ret = 0x34U;
        break;
    case 0x5U:
        ret = 0x35U;
        break;
    case 0x6U:
        ret = 0x36U;
        break;
    case 0x7U:
        ret = 0x37U;
        break;
    case 0x8U:
        ret = 0x38U;
        break;
    case 0x9U:
        ret = 0x39U;
        break;
    case 0xAU:
        ret = 0x41U;
        break;
    case 0xBU:
        ret = 0x42U;
        break;
    case 0xCU:
        ret = 0x43U;
        break;
    case 0xDU:
        ret = 0x44U;
        break;
    case 0xEU:
        ret = 0x45U;
        break;
    case 0xFU:
        ret = 0x46U;
        break;
    default:
        /* Not Hex */
        break;
    }

    return ret;
}
