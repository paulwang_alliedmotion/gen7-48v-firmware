;//###########################################################################
;//
;// FILE:  sine.s
;//
;// TITLE: sine calculation Assembly Code.
;//
;// This file contains the sine calculation assembly code.
;//
;//###########################################################################
CLAsin          .macro

_CLAsin_START

                MMOV32          MR0,@_thetaRad_cla
                MMOV32          MR1,@_CLAsincosTable_TABLE_SIZEDivTwoPi
                MMPYF32         MR1,MR0,MR1
        ||      MMOV32          MR2,@_CLAsincosTable_TABLE_MASK

                MF32TOI32       MR3,MR1
                MAND32          MR3,MR3,MR2
                MLSL32          MR3,#1
                MMOV16          MAR0,MR3,#_CLAsincosTable_Cos0

                MFRACF32        MR1,MR1
                MMOV32          MR0,@_CLAsincosTable_TwoPiDivTABLE_SIZE
                MMPYF32         MR1,MR1,MR0
        ||      MMOV32          MR0,@_CLAsincosTable_Coef3

                MMOV32          MR2,*MAR0[#-64]++
                MMPYF32         MR3,MR0,MR2
        ||      MMOV32          MR2,*MAR0[#+64]++
                MMPYF32         MR3,MR3,MR1
        ||      MMOV32          MR0,@_CLAsincosTable_Coef2

                MMPYF32         MR2,MR0,MR2
                MADDF32         MR3,MR3,MR2
        ||      MMOV32          MR2,*MAR0[#-64]++
                MMPYF32         MR3,MR3,MR1
        ||      MMOV32          MR0,@_CLAsincosTable_Coef1

                MMPYF32         MR2,MR0,MR2
                MADDF32         MR3,MR3,MR2
        ||      MMOV32          MR2,*MAR0[#+64]++
                MMPYF32         MR3,MR3,MR1
        ||      MMOV32          MR0,@_CLAsincosTable_Coef0

                MMPYF32         MR2,MR0,MR2
                MADDF32         MR3,MR3,MR2
        ||      MMOV32          MR2,*MAR0[#-64]++
                MMPYF32         MR3,MR3,MR1

                MADDF32         MR3,MR3,MR2
                MMPYF32         MR3,MR3,MR1
        ||      MMOV32          MR2,*MAR0[#0]++

                MADDF32         MR3,MR2,MR3
                MMOV32          @_si_cla,MR3

_CLAsin_END
                .endm
