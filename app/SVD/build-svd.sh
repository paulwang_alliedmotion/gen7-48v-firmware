#! /bin/sh

# NOTICE: This file is the property of Globe Motors and contains
# CONFIDENTIAL information.  Any use, copying or dissemination of the
# information contained in this file is strictly prohibited unless
# authorized by Globe Motors in writing.

# Copyright 2016, Globe Motors, Inc.

# $Id: $
# $Name:  $

if [ $1 = "--beta" ] ; then
    echo "Building abbreviated version description"
    SVD_ABBREVIATED="True"
    TEXFILE="svd-beta.tex"
    STATUS="Beta"
else
    echo "Building normal version description"
    SVD_ABBREVIATED="False"
    TEXFILE="svd.tex"
    STATUS="Alpha"
fi

#
# Load the configuration file with all the per-project definitions.
source "../svd.conf"

DATE=`/bin/date +"%d %B %Y"`
YEAR=`/bin/date +"%Y"`

# move to GIT
# Get the CVS information for this project.  Assumes that we are in a
# sub-directory of the top level project directory.
#CVSROOT=":pserver:anoncvs@"`cut -f 2 --delim=@ ../CVS/Root`
#if [ -f ../CVS/Tag ] ; then
#    CVSTAG=`cut -b 2- ../CVS/Tag`
#else
#    CVSTAG="This build was not tagged"
#fi
#CVSREPOSITORY=`cat ../CVS/Repository`

#
# Spit out the latex source as: svd.tex.
echo "\documentclass [12pt,twoside]{article}
\usepackage[dvips]{graphicx}
\usepackage{times}
\usepackage{color}

\topmargin 0pt
\headheight 0pt
\headsep 0pt
\textheight 8.5in

\topmargin 0in
\oddsidemargin 0in
\evensidemargin 0in
\textwidth 6.0in

\parindent 0pt
\parskip 6 pt

\setcounter{secnumdepth}{5}
\setcounter{tocdepth}{5}

\title {\bf {Software Version Description}\\\\${PROGRAM} ${PRODUCT}\\\\Version: ${VERSION} (${STATUS} Release) \vspace{2.75 in}}
\author{Globe Motors\\\\2275 Stanley Avenue\\\\Dayton, OH\\\\45404 \vspace{0.75 in}}
\date{${DATE}}

\begin {document}
\maketitle
\thispagestyle{empty}

\newpage
\vspace*{7.4 in}
{\footnotesize
\begin{center}
\begin{tabular}{|l|}
\hline
Copyright ${YEAR} Globe Motors, Inc.\\\\ The information contained in this
document is privileged and confidential\\\\ and shall be treated as
such.  This document may not be distributed or copied\\\\ in part or in
full without the direct written permission of Globe Motors.\\\\ \hline
\end{tabular}
\end{center}
}

\newpage
\tableofcontents

\listoffigures

\newpage
\section{Introduction}
This document provides a version description for the ${STATUS} release
of the ${PRODUCT}, version ${VERSION}, for the ${PROGRAM_DESC} program.

" >>${TEXFILE}
cat ../CHANGES.tex >>${TEXFILE}
echo "
\section{Identification}

\subsection{Version Number}
${VERSION}

\subsection{GIT Information}
${DATE}

\subsection{Release Date}
${DATE}

\subsection{Binary Filename}
{\footnotesize \begin{verbatim}
${PROJFILE}.out
\end{verbatim}}

\subsection{Binary MD5 Checksum}" >>${TEXFILE}

if [ -f ../${PROJFILE}.out.md5sum ] ; then
    echo "{\footnotesize \begin{verbatim}" >>${TEXFILE}
    cat ../${PROJFILE}.out.md5sum >>${TEXFILE}
    echo "\end{verbatim}}" >>${TEXFILE}
else
    echo "Not available." >>${TEXFILE}
fi

echo "
\subsection{Binary Digital Signature}" >>${TEXFILE}

if [ -f ../${PROJFILE}.out.sig ] ; then
    echo "The validity of the binary may be verified with the PGP signature.
          The PGP signature may be cut and pasted from here.  It may also be
          found as the file: {\verb ${PROJFILE}.out.sig }, in the top level
          directory of the distribution." >>${TEXFILE}
    echo "{\footnotesize \begin{verbatim}" >>${TEXFILE}
    cat ../${PROJFILE}.out.sig >>${TEXFILE}
    echo "\end{verbatim}}" >>${TEXFILE}
else
    echo "A PGP signature is not available." >>${TEXFILE}
fi
 
echo "
\subsection{Binary Image Identification}" >>${TEXFILE}
if [ -f ../${PROJFILE}.out.dat ] ; then
    echo "{\footnotesize \begin{verbatim}" >>${TEXFILE}
    cat ../${PROJFILE}.out.dat >>${TEXFILE}
    echo "\end{verbatim}}" >>${TEXFILE}
else
    echo "Not available." >>${TEXFILE}
fi

if [ $SVD_ABBREVIATED = "False" ] ; then

echo "
\section{Static Analysis Results}

\subsection{Lint Log}" >>${TEXFILE}

if [ -f ../lint.log ] ; then
    echo "{\footnotesize \begin{verbatim}" >>${TEXFILE}
    cat ../lint.log >>${TEXFILE}
    echo "\end{verbatim}}" >>${TEXFILE}
else
    echo "Not available." >>${TEXFILE}
fi

echo "
\subsection{Cyclomatic Complexity Results}" >>${TEXFILE}

if [ -f ../cyclo.log ] ; then
    echo "{\footnotesize \begin{verbatim}" >>${TEXFILE}
    expand ../cyclo.log >>${TEXFILE}
    echo "\end{verbatim}}" >>${TEXFILE}
else
    echo "Not available." >>${TEXFILE}
fi # [ -f ../cyclo.log ]
fi # [ $SVD_ABBREVIATED = "False" ]

echo "
\newpage
\appendix" >>${TEXFILE}

echo "
\section{Binary Image Map}" >>${TEXFILE} 

if [ -f ../docs/${PROJFILE}.map ] ; then
    echo "{\footnotesize \begin{verbatim}" >>${TEXFILE}
    cat ../docs/${PROJFILE}.map >>${TEXFILE}
    echo "\end{verbatim}}" >>${TEXFILE}
else
    echo "Not available." >>${TEXFILE}
fi

echo "
\section{Build Log}
\label{Build Log}" >>${TEXFILE} 

if [ -f ../build.log ] ; then
    echo "{\footnotesize \begin{verbatim}" >>${TEXFILE}
    fold ../build.log >>${TEXFILE}
    echo "\end{verbatim}}" >>${TEXFILE}
else
    echo "Not available." >>${TEXFILE}
fi

echo "
\end{document}" >>${TEXFILE}
