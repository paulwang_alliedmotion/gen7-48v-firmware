;//##########################################################################
;//
;// FILE:  claRoutines.s
;//
;// TITLE: CLA Assembly Code.
;//
;// This file contains the CLA assembly code.  When building the project
;//
;//##########################################################################

        .include "piccolo_cla.inc"

        .cdecls   C,LIST,"claShared.h"

        .include "clarke.s"
        .include "cosine.s"
        .include "drift.s"
        .include "ipark.s"
        .include "park.s"
        .include "pi_D.s"
        .include "pi_Q.s"
        .include "sin.s"
        .include "sincosTable.s"
        .include "svgen2.s"
        .include "theta.s"

        .sect   "ClaProg"
        .align  2

_piDRef_cpu   .usect "CpuToClaMsgRAM",2
_piQRef_cpu   .usect "CpuToClaMsgRAM",2
_thetaTrans_cpu .usect"CpuToClaMsgRAM",2
_thetaPoles_cpu .usect"CpuToClaMsgRAM",2
_thetaCPR_cpu .usect"CpuToClaMsgRAM",2


        .def    _piDRef_cpu
        .def    _piQRef_cpu
        .def    _thetaTrans_cpu
        .def    _thetaPoles_cpu
        .def    _thetaCPR_cpu

_ClaProg_Start:

_ClaTask1:
        MMOVZ16         MR0, @ADCPF0R0 ; load adc results to the cpu
        MMOV32          @_adcOnA_cla, MR0
        MMOVZ16         MR1, @ADCPF0R1
        MMOV32          @_adcOnB_cla, MR1
        MMOVZ16         MR2, @ADCPF0R2
        MMOV32          @_adcOnC_cla, MR2
        MMOVZ16         MR3, @ADCPF0R3
        MMOV32          @_adcBattOn_cla,MR3

;        MMOVF32         MR3,#-0.0732421875 ; counts to amps 300/4096
        MMOVF32         MR3,#-0.0325488281 ; counts to amps 66.66*2/4096
        

        MUI32TOF32      MR0,MR0
        MMPYF32         MR0,MR3,MR0    ; calc phase A
;        MADDF32         MR0,#150.0,MR0 ; offset of 150 amps
        MADDF32         MR0,#66.66,MR0 ; offset of 66.66 amps
        MMOV32          @_ampsOnA_cla,MR0 ; store phase A

        MUI32TOF32      MR1,MR1
        MMPYF32         MR1,MR3,MR1    ; calc phase B
;        MADDF32         MR1,#150.0,MR1 ; offset of 150 amps
        MADDF32         MR1,#66.66,MR1 ; offset of 66.66 amps
        MMOV32          @_ampsOnB_cla,MR1 ; store phase B

        MUI32TOF32      MR2,MR2
        MMPYF32         MR2,MR3,MR2    ; calc phase C
;        MADDF32         MR2,#150.0,MR2 ; offset of 150 amps
        MADDF32         MR2,#66.66,MR2 ; offset of 66.66 amps
        MMOV32          @_ampsOnC_cla,MR2 ; store phase C

        MMOVF32         MR2,#38.0 ; offset iteration timeout counter limit
        MF32TOUI32      MR2,MR2
        MMOV32          MR3,@_exec_cla ; fetch and compare iterations
        MCMP32          MR3,MR2
        MNOP
        MNOP
        MNOP
        MBCNDD          offsetSummation,GT ; delay finished? goto summation
        MNOP
        MNOP
        MNOP

offsetDelay
        MMOVF32         MR2,#1.0 ; continue delay and increment
        MF32TOUI32      MR2,MR2
        MADD32          MR3,MR2,MR3
        MMOV32          @_exec_cla,MR3
        MSTOP
        MNOP
        MNOP
        MNOP

offsetSummation
        MMOVF32         MR2,#70.0 ; new count limit to give time to sum I
        MF32TOUI32      MR2,MR2
        MCMP32          MR3,MR2
        MNOP
        MNOP
        MNOP
        MBCNDD          offsetAverage,GT ; summed enough data, goto average
        MNOP
        MNOP
        MNOP
        MMOVF32         MR2,#1.0 ; increment iterations
        MF32TOUI32      MR2,MR2
        MADD32          MR3,MR2,MR3
        MMOV32          @_exec_cla,MR3
        MMOV32          MR0,@_ampsOnA_cla ; get latest amps and add to sum
        MMOV32          MR1,@_offsetA_cla
        MADDF32         MR1,MR0,MR1
        MMOV32          @_offsetA_cla,MR1 ; save new sum
        MMOV32          MR0,@_ampsOnB_cla ; get latest amps and add to sum
        MMOV32          MR1,@_offsetB_cla
        MADDF32         MR1,MR0,MR1
        MMOV32          @_offsetB_cla,MR1 ; save new sum
        MMOV32          MR0,@_ampsOnC_cla ; get latest amps and add to sum
        MMOV32          MR1,@_offsetC_cla
        MADDF32         MR1,MR0,MR1
        MMOV32          @_offsetC_cla,MR1 ; save new sum
        MSTOP
        MNOP
        MNOP
        MNOP

offsetAverage
        MMOVF32         MR2,#71.0 ; new count limit for averaging
        MF32TOUI32      MR2,MR2
        MCMP32          MR3,MR2
        MNOP
        MNOP
        MNOP
        MBCNDD          controlLoop,GT ; average complete? goto main loop
        MNOP
        MNOP
        MNOP
        MMOVF32         MR2,#1.0 ; increment iterations
        MF32TOUI32      MR2,MR2
        MADD32          MR3,MR2,MR3
        MMOV32          @_exec_cla,MR3
        MMOV32          MR0,@_offsetA_cla ; grab A summation
        MMOV32          MR1,@_offsetB_cla ; grab B summation
        MMOV32          MR2,@_offsetC_cla ; grab C summation
        MMOVF32         MR3,#0.03125      ; divisor for average
        MMPYF32         MR0,MR0,MR3       ; calc average A
        MMPYF32         MR1,MR1,MR3       ; calc average B
        MMPYF32         MR2,MR2,MR3       ; calc average C
        MMOV32          @_offsetA_cla,MR0 ; save average A
        MMOV32          @_offsetB_cla,MR1 ; save average B
        MMOV32          @_offsetC_cla,MR2 ; save average C
        MSTOP
        MNOP
        MNOP
        MNOP

controlLoop
        MMOV32          MR0,@_ampsOnA_cla ; grab amps and apply offset A
        MMOV32          MR1,@_offsetA_cla
        MMOV32          MR2,@_corrA_cla

        MSUBF32         MR2,MR1,MR2
        MMAXF32         MR2,#-2.0
        MMINF32         MR2,#2.0
        MSUBF32         MR2,MR1,MR2
        MSUBF32         MR0,MR0,MR2

        MMOV32          @_clarkeAs_cla,MR0 ; A amps to clarke As

        MMOV32          MR0,@_ampsOnB_cla ; grab amps and apply offset B
        MMOV32          MR1,@_offsetB_cla
        MMOV32          MR2,@_corrB_cla

        MSUBF32         MR2,MR1,MR2
        MMAXF32         MR2,#-2.0
        MMINF32         MR2,#2.0
        MSUBF32         MR2,MR1,MR2
        MSUBF32         MR0,MR0,MR2

        MMOV32          @_clarkeBs_cla,MR0 ; B amps to clarke Bs

        MMOV16          MAR0,@_thetaTrans_cpu ; grab theta from CPU
        MNOP
        MNOP
        MNOP
        MMOV32          MR0,*MAR0
        MMOV32          @_thetaCounts_cla,MR0

        MMOV16          MAR0,@_thetaPoles_cpu
        MNOP
        MNOP
        MNOP
        MMOV32          MR0,*MAR0
        MMOV32          @_thetaPoles_cla,MR0

        MMOV16          MAR0,@_thetaCPR_cpu
        MNOP
        MNOP
        MNOP
        MMOV32          MR0,*MAR0
        MMOV32          @_thetaCPR_cla,MR0

        theta_CLA               ; calc theta radians
        CLAsin                  ; calc sine theta
        CLAcos                  ; calc cosine theta

        clarke_CLA              ; execute clarke transform, get clarkAlpha and clarkBeta, which will be used in park

        park_CLA                ; execute park transform, get parkDs and ParkQs

        pi_q_CLA                ; execute pid on QAxis
        pi_d_CLA                ; execute pid on DAxis

        ipark_CLA               ; execute inverse park transform

        svgen2_CLA              ; execute spave vector generation

        MMOV32          MR0,@_svgen2PWMA_cla ; load A duty cycle
        MMOV16          @CMPA1,MR0
        MMOV32          MR1,@_svgen2PWMB_cla ; load B duty cycle
        MMOV16          @CMPA2,MR1
        MMOV32          MR2,@_svgen2PWMC_cla ; load C duty cycle
        MMOV16          @CMPA3,MR2

        MSTOP
        MNOP
        MNOP
        MNOP
_ClaT1End:

_ClaTask2:
        MMOVZ16         MR0,@ADCPF0R4
        MMOV32          @_adcT1_cla,MR0
        MMOVZ16         MR0,@ADCPF0R5
        MMOV32          @_adcT2_cla,MR0
        MMOVZ16         MR0,@ADCPF0R6
        MMOV32          @_adcOffA_cla,MR0
        MMOVZ16         MR0,@ADCPF0R7
        MMOV32          @_adcOffB_cla,MR0
        MMOVZ16         MR0,@ADCPF0R8
        MMOV32          @_adcOffC_cla,MR0
        MMOVZ16         MR0,@ADCPF0R9
        MMOV32          @_adcP2_cla,MR0
        MMOVZ16         MR0,@ADCPF0R10
        MMOV32          @_adcDTemp_cla,MR0
        MMOVZ16         MR0,@ADCPF0R11
        MMOV32          @_adcIgn_cla,MR0
        MMOVZ16         MR0,@ADCPF0R12
        MMOV32          @_adcP1_cla,MR0
        MMOVZ16         MR0,@ADCPF0R13
        MMOV32          @_adcP3_cla,MR0
        MMOVZ16         MR0,@ADCPF0R14
        MMOV32          @_adcVREF2_cla,MR0

        drift_CLA

        MSTOP
        MNOP
        MNOP
        MNOP
_ClaT2End:

_ClaTask3:
        MSTOP
        MNOP
        MNOP
        MNOP
_ClaT3End:

_ClaTask4:
        MSTOP
        MNOP
        MNOP
        MNOP
_ClaT4End:

_ClaTask5:
        MSTOP
        MNOP
        MNOP
        MNOP
_ClaT5End:

_ClaTask6:
        MSTOP
        MNOP
        MNOP
        MNOP
_ClaT6End:

_ClaTask7:
        MSTOP
        MNOP
        MNOP
        MNOP
_ClaT7End:

_ClaTask8:
        MMOVF32         MR3,#0.0
        MF32TOUI32      MR3,MR3
        MMOV32          @_exec_cla,MR3
        MMOV32          @_offsetA_cla,MR3
        MMOV32          @_offsetB_cla,MR3
        MMOV32          @_offsetC_cla,MR3
        MMOV32          @_corrA_cla,MR3
        MMOV32          @_corrB_cla,MR3
        MMOV32          @_driftExec_cla,MR3
        MMOV32          @_piQRef_cla,MR3
        MMOV32          @_piQUP_cla,MR3
        MMOV32          @_piQOut_cla,MR3
        MMOV32          @_piQV1_cla,MR3
        MMOV32          @_piQI1_cla,MR3
        MMOV32          @_piQUI_cla,MR3
        MMOV32          @_piDRef_cla,MR3
        MMOV32          @_piDUP_cla,MR3
        MMOV32          @_piDOut_cla,MR3
        MMOV32          @_piDV1_cla,MR3
        MMOV32          @_piDI1_cla,MR3
        MMOV32          @_piDUI_cla,MR3
        MMOV32          @_svgen2t1Old_cla,MR3
        MMOV32          @_svgen2t2Old_cla,MR3
        MMOV32          @_svgen2t3Old_cla,MR3
        MSTOP
        MNOP
        MNOP
        MNOP
_ClaT8End:

_ClaProg_End:
