/*!
 * \file      tctrl.h
 * \author    Zach Haver
 * \brief     Torque control definitions.
 */

/* $Id: $ */
/* $Name: $ */

/* NOTICE: This file is the property of Globe Motors and contains */
/* CONFIDENTIAL information.  Any use, copying or dissemination of the */
/* information contained in this file is strictly prohibited unless */
/* authorized by Globe Motors in writing. */

/* Copyright 2016, Globe Motors, Inc. */

/*
 * Revision History:
 *
 * $Log: $
 *
 */

#ifndef TCTRL_H
#define TCTRL_H

#include "IQmathLib.h"
#include "misra.h"

void tctrlInit(void);
void tctrlUpdateILimit(void);
void tctrlGenCMD(_iq *q, _iq *d);
void tctrlCalcInput(void);
_iq tctrlGetTin(void);
_iq tctrlGetOut(void);
void tctrlUpdateOffset(_iq nOff);
_iq tctrlGetOffset(void);

#endif /* TCTRL_H */
