;//###########################################################################
;//
;// FILE:  svgen.s
;//
;// TITLE: space vector generation Assembly Code.
;//
;// This file contains the space vector generation assembly code.
;//
;//###########################################################################
svgen2_CLA       .macro

_svgen2_CLA_START
        MMOV32  MR0,@_iparkBeta_cla  ;t1 = beta
        MMOV32  MR3,@_iparkAlpha_cla ;
        MMPYF32 MR1,#0.5,MR0         ;MR1 = 0.5 * beta
        MMPYF32 MR3,#0.866,MR3       ;MR3 = 0.866 * alpha
        MADDF32 MR1,MR1,MR3          ;t2 = (0.5 * beta) + (0.866 * alpha)
        MSUBF32 MR2,MR1,MR0          ;t3 = t2 - t1

        MMOV32  @_svgen2t1_cla,MR0
        MMOV32  @_svgen2t2_cla,MR1
        MMOV32  @_svgen2t3_cla,MR2

        MMOV32  MR3,@_svgen2t1Old_cla
        MADDF32 MR0,MR0,MR3
        MMPYF32 MR0,#0.5,MR0
        MMOV32  @_svgen2t1Old_cla,MR0

        MMOV32  MR3,@_svgen2t2Old_cla
        MADDF32 MR1,MR1,MR3
        MMPYF32 MR1,#0.5,MR1
        MMOV32  @_svgen2t2Old_cla,MR1

        MMOV32  MR3,@_svgen2t3Old_cla
        MADDF32 MR2,MR2,MR3
        MMPYF32 MR2,#0.5,MR2
        MMOV32  @_svgen2t3Old_cla,MR2

        MMOVF32 MR3,#3.0             ;sector = 3

        MCMPF32 MR1,#0.0        ;is t2 > 0
        MNOP
        MNOP
        MNOP
        MBCNDD  testT3,LEQ      ;branch if false
        MNOP
        MNOP
        MNOP
        MADDF32 MR3,MR3,#-1.0    ;sector = sector - 1
testT3
        MCMPF32 MR2,#0.0        ;is t3 > 0
        MNOP
        MNOP
        MNOP
        MBCNDD  testT1,LEQ      ;branch if false
        MNOP
        MNOP
        MNOP
        MADDF32 MR3,MR3,#-1.0    ;sector = sector - 1
testT1
        MCMPF32 MR0,#0.0        ;is t1 < 0
        MNOP
        MNOP
        MNOP
        MBCNDD  secDone,GEQ      ;branch if false
        MNOP
        MNOP
        MNOP
        MSUBF32 MR3,#7.0,MR3    ;sector = 7 - sector
secDone
        MMOV32  @_svgen2Sector_cla,MR3
        MCMPF32 MR3,#1.0        ;sector == 1
        MNOP
        MNOP
        MNOP
        MBCNDD  sec_1_4,EQ      ;branch if true
        MNOP
        MNOP
        MNOP
        MCMPF32 MR3,#4.0        ;sector == 4
        MNOP
        MNOP
        MNOP
        MBCNDD  sec_1_4,EQ      ;branch if true
        MNOP
        MNOP
        MNOP
        MCMPF32 MR3,#2.0        ;sector == 2
        MNOP
        MNOP
        MNOP
        MBCNDD  sec_2_5,EQ      ;branch if true
        MNOP
        MNOP
        MNOP
        MCMPF32 MR3,#5.0        ;sector == 5
        MNOP
        MNOP
        MNOP
        MBCNDD  sec_2_5,EQ      ;branch if true
        MNOP
        MNOP
        MNOP
        MMOV32  @_svgen2A_cla,MR2   ;if here, sector 3 or 6 so A = t3
        MMPYF32 MR3,#-1.0,MR2
        MMOV32  @_svgen2B_cla,MR3   ;B = -t3
        MADDF32 MR3,MR0,MR1     ;MR3 = t1 + t2
        MMPYF32 MR3,#-1.0,MR3   ;MR3 = -(t1 + t2)
        MMOV32  @_svgen2C_cla,MR3   ;C = -(t1 + t2)
        MBCNDD  pwmCalc
        MNOP
        MNOP
        MNOP
sec_1_4
        MMOV32  @_svgen2A_cla,MR1   ;A = t2
        MSUBF32 MR3,MR0,MR2     ;MR3 = t1 - t3
        MMOV32  @_svgen2B_cla,MR3   ;B = t1 - t3
        MMPYF32 MR3,#-1.0,MR1   ;MR3 = -t2
        MMOV32  @_svgen2C_cla,MR3   ;C = -t2
        MBCNDD  pwmCalc
        MNOP
        MNOP
        MNOP
sec_2_5
        MADDF32 MR3,MR2,MR1     ;MR3 = t3 + t2
        MMOV32  @_svgen2A_cla,MR3   ;A = t3 + t2
        MMOV32  @_svgen2B_cla,MR0   ;B = t1
        MMPYF32 MR3,#-1.0,MR0   ;MR3 = -t1
        MMOV32  @_svgen2C_cla,MR3   ;C = -t1
pwmCalc
        MMOV32  MR0,@_svgen2A_cla
        MMOV32  MR1,@_svgen2B_cla
        MMOV32  MR2,@_svgen2C_cla

        MMPYF32 MR0,#-1.0,MR0
        MMPYF32 MR1,#-1.0,MR1
        MMPYF32 MR2,#-1.0,MR2

        MMOVF32 MR3,#0.9
        MMINF32 MR0,MR3
        MMINF32 MR1,MR3
        MMINF32 MR2,MR3
        MMOVF32 MR3,#-0.9
        MMAXF32 MR0,MR3
        MMAXF32 MR1,MR3
        MMAXF32 MR2,MR3
        MMPYF32 MR0,#750.0,MR0
        MMPYF32 MR1,#750.0,MR1
        MMPYF32 MR2,#750.0,MR2
        MADDF32 MR0,#750.0,MR0
        MADDF32 MR1,#750.0,MR1
        MADDF32 MR2,#750.0,MR2
        MF32TOUI16 MR0,MR0
        MF32TOUI16 MR1,MR1
        MF32TOUI16 MR2,MR2
        MMOV16  @_svgen2PWMA_cla,MR0
        MMOV16  @_svgen2PWMB_cla,MR1
        MMOV16  @_svgen2PWMC_cla,MR2

_svgen2_CLA_END
                .endm
