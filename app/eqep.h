/*!
 * \file    eqep.h
 * \author  Zach Haver
 * \brief   eQEP initialization definitions for the TI TMS320C28033/35 piccolo.
 */

/* $Id: $ */
/* $Name: $ */

/* NOTICE: This file is the property of Globe Motors and contains */
/* CONFIDENTIAL information.  Any use, copying or dissemination of the */
/* information contained in this file is strictly prohibited unless */
/* authorized by Globe Motors in writing. */

/* Copyright 2016, Globe Motors, Inc. */

/*
 * Revision History:
 *
 * $Log: $
 *
 */

#ifndef EQEP_H
#define EQEP_H

#include "IQmathLib.h"
#include "misra.h"
#include "tle5012.h"

typedef enum {
    THETA_NORM,
    THETA_FIXED,
    THETA_RAMP
}EQEPThetaOp_t;

typedef enum {
    EQEP_OFF_NORM,
    EQEP_OFF_LOCK1,
    EQEP_OFF_LOCK2,
    EQEP_OFF_LOCK3,
    EQEP_OFF_LOCK4,
    EQEP_OFF_LOCK5,
    EQEP_OFF_LOCK6
}EQEPOffState_t;

/* public function prototypes */
void eqepInit(void);
void eqepSetCPR(TLE_MOD4_CPR_t cpr);
UINT_32 eqepGetThetaCounts(void);
void eqepCalcVelocity(void);
_iq eqepGetVelocity(void);
UINT_32 eqepGetFaultPos(void);
void eqepSetThetaNorm(void);
void eqepSetThetaFixed(_iq fixed);
void eqepSetThetaRamp(_iq freq);
EQEPThetaOp_t eqepGetThetaMode(void);
void eqepSetOffsetState(EQEPOffState_t state);

#endif /* EQEP_H */
