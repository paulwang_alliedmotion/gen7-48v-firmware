/*!
 * \file      eqep.c
 * \author    Zach Haver
 * \brief     eQEP initialization functions for the TI TMS320C28035 Piccolo.
 */

/* $Id: $ */
/* $Name: $ */

/* NOTICE: This file is the property of Globe Motors and contains */
/* CONFIDENTIAL information.  Any use, copying or dissemination of the */
/* information contained in this file is strictly prohibited unless */
/* authorized by Globe Motors in writing. */

/* Copyright 2016, Globe Motors, Inc. */

/*
 * Revision History:
 *
 * $Log: $
 *
 */

#include "claShared.h"
#include "dtc.h"
#include "eeprom.h"
#include "eqep.h"
#include "IQmathLib.h"
#include "isr.h"
#include "misra.h"
#include "piccolo.h"
#include "tle5012.h"
#include "offset.h"

/* private function prototypes */

/* private variable declarations */
static UINT_16 encoOffset = 231U;
static _iq eqepVMotor = 0L;
static EQEPThetaOp_t thetaMode = THETA_NORM;
static UINT_32 fixedTheta = 0UL;
static _iq rampFreqInc = 0L;
static EQEPOffState_t eqepOffsetState = EQEP_OFF_NORM;

static UINT_32 thetaMask = 0x3fffUL;
static UINT_16 thetaLock1 = 0U;
static UINT_16 thetaLock2 = 0U;
static UINT_16 thetaLock3 = 0U;
static UINT_16 thetaLock4 = 0U;
static UINT_16 thetaLock5 = 0U;
static UINT_16 thetaLock6 = 0U;
static _iq velFactor = 0L;
static _iq rampFactor = 0L;

#pragma DATA_SECTION (thetaC_cpu, "CpuToClaMsgRAM");
static UINT_32 thetaC_cpu;

/* public functions */

void
eqepInit(void) {
    UINT_8 bytes[4] = {0U, 0U, 0U, 0U};
    UINT_16 temp = 0U;

    thetaCPR_cpu = &thetaC_cpu;
    /* read the encoder offset from the eeprom */
    if (EE_ERR_NONE == eepromReadXBytes(EEADDR_ENCO_OFFSET, bytes, 4U)) {
        encoOffset = (UINT_16)bytes[0] | ((UINT_16)bytes[1] << 8);
    }
    if (3U == bytes[2]) {
        isrSetThetaPoles(3UL);
    } else {
        isrSetThetaPoles(4UL);
    }
    /* Enable the EALLOW registers */
    PICCOLO_EALLOW();
    /* Enable the eqep clock */
    *PCLKCR1 |= 0x4000U;
    /* Disable the EALLOW registers */
    PICCOLO_EDIS();

    /* Setup up the decoder register
     * QDECCTL
     * bits 15-14 = (00) QSRC - Quadrature count
     * bit  13    = (0)  SOEC - Disable sync output
     * bit  12    = (0)  SPSEL - Index used for synch
     * bit  11    = (0)  XCR - count on rising and falling
     * bit  10    = (1)  SWAP - quadrature inputs not swapped
     * bit  9     = (0)  IGATE - no index gating
     * bit  8     = (0)  QAP - no effect
     * bit  7     = (0)  QBP - no effect
     * bit  6     = (0)  QIP - no effect
     * bit  5     = (0)  QSP - no effect
     * bits 4-0   = (x)  Reserved
     */
    *QDECCTL = 0x0000U;

    /* Set up the QEP control register
     * QEPCTL
     * bits 15-14 = (11) FREE,SOFT
     * bits 13-12 = (10) PCRM - Reset only on first index event
     * bits 11-10 = (00) SEI
     * bits 9-8   = (11) IEI
     * bit  7     = (0)  SWI
     * bit  6     = (0)  SEL
     * bits 5-4   = (10) IEL - Latch on rising edge
     * bit  3     = (0)  QPEN
     * bit  2     = (0)  QCLM
     * bit  1     = (0)  UTE
     * bit  0     = (0)  WDE
     */
    *QEPCTL = 0xe000U;

    /* Set the position counter to the abs position of the as5040 */
    if (TLE_SUCCESS != tleGetPos(&temp)) {
        temp = 0U;
    }
    *QPOSCNT = temp;

    /* Set the position counter initialization value to 0UL */
    *QPOSINIT = 0UL;

    /* Set the position counter max value to 32bit */
    *QPOSMAX = 0xffffffffUL;

    /* clear all interrupts */
    *QCLR = 0x0fffU;

    /* Enable needed interrupts
     * QEINT
     * bits 15-12 = (x) Reserved
     * bit  11    = (0) UTO - unit timeout interrupt
     * bit  10    = (0) IEL - index event latch interrupt
     * bit  9     = (0) SEL - strobe event latch interrupt
     * bit  8     = (0) PCM - position compare match interrupt
     * bit  7     = (0) PCR - position compare ready interrupt
     * bit  6     = (0) PCO - position counter overflow interrupt
     * bit  5     = (0) PCU - position counter underflow interrupt
     * bit  4     = (0) WTO - watchdog timeout interrupt
     * bit  3     = (0) QDC - quadrature direction change interrupt
     * bit  2     = (0) QPE - quadrature phase error interrupt
     * bit  1     = (0) PCE - position counter error interrupt
     * bit  0     = (x) Reserved
     */
    *QEINT = 0x0000U;

    /* Enable the QEP */
    *QEPCTL |= 0x0008U;
}

void
eqepSetCPR(TLE_MOD4_CPR_t cpr) {
    switch (cpr) {
    case TLE_MOD4_512CPR:
        thetaMask = 0x7ffUL;
        thetaLock1 = 228U;
        thetaLock2 = 340U;
        thetaLock3 = 171U;
        thetaLock4 = 256U;
        thetaLock5 = 137U;
        thetaLock6 = 205U;
        velFactor = _IQ(5.859375L);
        rampFactor= _IQ(0.0256L);
        thetaC_cpu = 2048UL;
        break;
    case TLE_MOD4_1024CPR:
        thetaMask = 0xfffUL;
        thetaLock1 = 456U;
        thetaLock2 = 680U;
        thetaLock3 = 342U;
        thetaLock4 = 512U;
        thetaLock5 = 274U;
        thetaLock6 = 410U;
        velFactor = _IQ(2.9296875L);
        rampFactor= _IQ(0.0512L);
        thetaC_cpu = 4096UL;
        break;
    case TLE_MOD4_2048CPR:
        thetaMask = 0x1fffUL;
        thetaLock1 = 912U;
        thetaLock2 = 1360U;
        thetaLock3 = 684U;
        thetaLock4 = 1024U;
        thetaLock5 = 548U;
        thetaLock6 = 820U;
        velFactor = _IQ(1.46484375L);
        rampFactor= _IQ(0.1024L);
        thetaC_cpu = 8192UL;
        break;
    case TLE_MOD4_4096CPR:
    default:
        thetaMask = 0x3fffUL;
        thetaLock1 = 1824U;
        thetaLock2 = 2720U;
        thetaLock3 = 1368U;
        thetaLock4 = 2048U;
        thetaLock5 = 1096U;
        thetaLock6 = 1640U;
        velFactor = _IQ(0.732421875L);
        rampFactor= _IQ(0.2048L);
        thetaC_cpu = 16384UL;
        break;
    }
    offsetSetCPR(cpr);
}

UINT_32
eqepGetThetaCounts(void) {
    UINT_32 ret = 0UL;
    static _iq rampTheta = (_iq)0L;

    if (EQEP_OFF_NORM == eqepOffsetState) {
        switch (thetaMode) {
        case THETA_FIXED:
            ret = fixedTheta & thetaMask;
            break;
        case THETA_RAMP:
            rampTheta += rampFreqInc;
            ret = ((UINT_32)rampTheta >> 18) & thetaMask;
            break;
        case THETA_NORM:
        default:
            ret = *QPOSCNT;
            ret += (UINT_32)encoOffset;
            ret &= thetaMask;
            break;
        }
    } else {
        switch (eqepOffsetState) {
        case EQEP_OFF_LOCK1:
            ret = thetaLock1;
            break;
        case EQEP_OFF_LOCK2:
            ret = thetaLock2;
            break;
        case EQEP_OFF_LOCK3:
            ret = thetaLock3;
            break;
        case EQEP_OFF_LOCK4:
            ret = thetaLock4;
            break;
        case EQEP_OFF_LOCK5:
            ret = thetaLock5;
            break;
        case EQEP_OFF_LOCK6:
            ret = thetaLock6;
            break;
        case EQEP_OFF_NORM:
        default:
            ret = *QPOSCNT;
            ret += (UINT_32)encoOffset;
            ret &= thetaMask;
            break;
        }
    }
    return ret;
}

void
eqepCalcVelocity(void) {
    static SINT_32 posOld = 0L;
    SINT_32 posNew = (SINT_32)*QPOSCNT;
    SINT_32 delta;
    UINT_32 shifting;
    _iq deltaQ;
    static UINT_16 iterCtr = 0U;

    if(0U == iterCtr) {
        posOld = posNew;
        iterCtr++;
    }

    delta = posNew - posOld;

    /* limit to 5859 RPM */
    if ((delta > 8000L) || (delta < -8000L)) {
        /* do nothing */
    } else {
        shifting = ((UINT_32)delta << 18);
        deltaQ = (_iq)shifting;
        eqepVMotor = _IQmpy (deltaQ, velFactor);/*rev/min*/
    }
    posOld = posNew;
}

_iq
eqepGetVelocity(void) {
    return eqepVMotor;
}

UINT_32
eqepGetFaultPos(void) {
    return *QPOSCNT;
}

void
eqepSetThetaNorm(void) {
    thetaMode = THETA_NORM;
}

void
eqepSetThetaFixed(_iq fixed) {
    fixedTheta = ((UINT_32)_IQabs(fixed) >> 18);
    thetaMode = THETA_FIXED;
}

void
eqepSetThetaRamp(_iq freq) {
    rampFreqInc = _IQmpy(freq, rampFactor);
    thetaMode = THETA_RAMP;
}

EQEPThetaOp_t
eqepGetThetaMode(void) {
    return thetaMode;
}

void
eqepSetOffsetState(EQEPOffState_t state) {
    eqepOffsetState = state;
}
