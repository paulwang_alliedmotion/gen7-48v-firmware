/*!
 * \file      cla_shared.c
 * \author    Zach Haver
 * \brief     Contrl Law Accelerator (CLA) shared variable establishment
 *            for the TMS320C28033/35 Piccolo.
 */

/* $Id: $ */
/* $Name: $ */

/* NOTICE: This file is the property of Globe Motors and contains */
/* CONFIDENTIAL information.  Any use, copying or dissemination of the */
/* information contained in this file is strictly prohibited unless */
/* authorized by Globe Motors in writing. */

/* Copyright 2016, Globe Motors, Inc. */

/*
 * Revision History:
 *
 * $Log: $
 *
 */

#include "claShared.h"
#include "misra.h"

/*! \var adcOnA_cla - CLA variable for phase A adc counts FET on time. */
#pragma DATA_SECTION (adcOnA_cla, "ClaToCpuMsgRAM");
UINT_32 adcOnA_cla = 0UL;

/*! \var adcOnB_cla - CLA variable for phase B adc counts FET on time. */
#pragma DATA_SECTION (adcOnB_cla, "ClaToCpuMsgRAM");
UINT_32 adcOnB_cla = 0UL;

/*! \var adcOnC_cla - CLA variable for phase C adc counts FET on time. */
#pragma DATA_SECTION (adcOnC_cla, "ClaToCpuMsgRAM");
UINT_32 adcOnC_cla = 0UL;

#pragma DATA_SECTION (adcBattOn_cla, "ClaToCpuMsgRAM");
UINT_32 adcBattOn_cla = 0UL;

/*! \var adcT1_cla - CLA variable for T1 adc counts. */
#pragma DATA_SECTION (adcT1_cla, "ClaToCpuMsgRAM");
UINT_32 adcT1_cla = 0UL;

/*! \var adcT2_cla - CLA variable for T2 adc counts. */
#pragma DATA_SECTION (adcT2_cla, "ClaToCpuMsgRAM");
UINT_32 adcT2_cla = 0UL;

/*! \var adcOffA_cla - CLA variable for phase A adc counts FET off time. */
#pragma DATA_SECTION (adcOffA_cla, "ClaToCpuMsgRAM");
UINT_32 adcOffA_cla = 0UL;

/*! \var adcOffB_cla - CLA variable for phase B adc counts FET off time. */
#pragma DATA_SECTION (adcOffB_cla, "ClaToCpuMsgRAM");
UINT_32 adcOffB_cla = 0UL;

/*! \var adcOffC_cla - CLA variable for phase C adc counts FET off time. */
#pragma DATA_SECTION (adcOffC_cla, "ClaToCpuMsgRAM");
UINT_32 adcOffC_cla = 0UL;

#pragma DATA_SECTION (adcP2_cla, "ClaToCpuMsgRAM");
UINT_32 adcP2_cla = 0UL;

/*! \var adcDTemp_cla - CLA variable for board temperature adc counts. */
#pragma DATA_SECTION (adcDTemp_cla, "ClaToCpuMsgRAM");
UINT_32 adcDTemp_cla = 0UL;

#pragma DATA_SECTION (adcIgn_cla, "ClaToCpuMsgRAM");
UINT_32 adcIgn_cla = 0UL;

#pragma DATA_SECTION (adcP1_cla, "ClaToCpuMsgRAM");
UINT_32 adcP1_cla = 0UL;

#pragma DATA_SECTION (adcP3_cla, "ClaToCpuMsgRAM");
UINT_32 adcP3_cla = 0UL;

/*! \var adcVREF2_cla - CLA variable for adc half ref voltage adc counts. */
#pragma DATA_SECTION (adcVREF2_cla, "ClaToCpuMsgRAM");
UINT_32 adcVREF2_cla = 0UL;

/*! \var ampsOnA_cla - CLA variable for Phase A amps. */
#pragma DATA_SECTION (ampsOnA_cla, "ClaToCpuMsgRAM");
FLOAT_32 ampsOnA_cla = 0.0;

/*! \var ampsOnB_cla - CLA variable for Phase B amps. */
#pragma DATA_SECTION (ampsOnB_cla, "ClaToCpuMsgRAM");
FLOAT_32 ampsOnB_cla = 0.0;

/*! \var ampsOnC_cla - CLA variable for Phase C amps. */
#pragma DATA_SECTION (ampsOnC_cla, "ClaToCpuMsgRAM");
FLOAT_32 ampsOnC_cla = 0.0;

/*! \var offsetA_cla - CLA variable for Phase A offset amps. */
#pragma DATA_SECTION (offsetA_cla, "ClaToCpuMsgRAM");
FLOAT_32 offsetA_cla = 0.0;

/*! \var offsetB_cla - CLA variable for Phase B offset amps. */
#pragma DATA_SECTION (offsetB_cla, "ClaToCpuMsgRAM");
FLOAT_32 offsetB_cla = 0.0;

/*! \var offsetC_cla - CLA variable for Phase C offset amps. */
#pragma DATA_SECTION (offsetC_cla, "ClaToCpuMsgRAM");
FLOAT_32 offsetC_cla = 0.0;

/*! \var clarkeAs_cla - CLA variable for As of the clarke transform. */
#pragma DATA_SECTION (clarkeAs_cla, "ClaToCpuMsgRAM");
FLOAT_32 clarkeAs_cla = 0.0;

/*! \var clarkeBs_cla - CLA variable for Bs of the clarke transform. */
#pragma DATA_SECTION (clarkeBs_cla, "ClaToCpuMsgRAM");
FLOAT_32 clarkeBs_cla = 0.0;

/*! \var clarkeAlpha_cla - CLA variable for Alpha of the clarke transform. */
#pragma DATA_SECTION (clarkeAlpha_cla, "ClaToCpuMsgRAM");
FLOAT_32 clarkeAlpha_cla = 0.0;

/*! \var clarkeBeta_cla - CLA variable for Beta of the clarke transform. */
#pragma DATA_SECTION (clarkeBeta_cla, "ClaToCpuMsgRAM");
FLOAT_32 clarkeBeta_cla = 0.0;

/*! \var thetaCounts_cla - CLA variable for the theta raw counts. */
#pragma DATA_SECTION (thetaCounts_cla, "ClaToCpuMsgRAM");
UINT_32 thetaCounts_cla = 0UL;

#pragma DATA_SECTION (thetaPoles_cla, "ClaToCpuMsgRAM");
UINT_32 thetaPoles_cla = 0UL;

#pragma DATA_SECTION (thetaCPR_cla, "ClaToCpuMsgRAM");
UINT_32 thetaCPR_cla = 0UL;

/*! \var thetaRad_cla - CLA variable for theta in radians. */
#pragma DATA_SECTION (thetaRad_cla, "ClaToCpuMsgRAM");
FLOAT_32 thetaRad_cla = 0.0;

/*! \var si_cla - CLA variable for sine of theta. */
#pragma DATA_SECTION (si_cla, "ClaToCpuMsgRAM");
FLOAT_32 si_cla = 0.0;

/*! \var co_cla - CLA variable for the cosine of theta. */
#pragma DATA_SECTION (co_cla, "ClaToCpuMsgRAM");
FLOAT_32 co_cla = 0.0;

/*! \var parkQs_cla - CLA variable for Qs of the park transform. */
#pragma DATA_SECTION (parkQs_cla, "ClaToCpuMsgRAM");
FLOAT_32 parkQs_cla = 0.0;

/*! \var parkDs_cla - CLA variable for Ds of the park transform. */
#pragma DATA_SECTION (parkDs_cla, "ClaToCpuMsgRAM");
FLOAT_32 parkDs_cla = 0.0;

/*! \var piQRef_cla - CLA variable for the Q reference. */
#pragma DATA_SECTION (piQRef_cla, "ClaToCpuMsgRAM");
FLOAT_32 piQRef_cla = 0.0;

/*! \var piQUP_cla - CLA variable for the Q p term. */
#pragma DATA_SECTION (piQUP_cla, "ClaToCpuMsgRAM");
FLOAT_32 piQUP_cla = 0.0;

/*! \var piQPOut_cla - CLA variable for the Q output term. */
#pragma DATA_SECTION (piQOut_cla, "ClaToCpuMsgRAM");
FLOAT_32 piQOut_cla = 0.0;

/*! \var piQV1_cla - CLA variable for the Q V1 term. */
#pragma DATA_SECTION (piQV1_cla, "ClaToCpuMsgRAM");
FLOAT_32 piQV1_cla = 0.0;

/*! \var pidQI1_cla - CLA variable for the Q I1 term. */
#pragma DATA_SECTION (piQI1_cla, "ClaToCpuMsgRAM");
FLOAT_32 piQI1_cla = 0.0;

/*! \var piQUI_cla - CLA variable for the Q i term. */
#pragma DATA_SECTION (piQUI_cla, "ClaToCpuMsgRAM");
FLOAT_32 piQUI_cla = 0.0;

/*! \var piDRef_cla - CLA variable for the D reference. */
#pragma DATA_SECTION (piDRef_cla, "ClaToCpuMsgRAM");
FLOAT_32 piDRef_cla = 0.0;

/*! \var piDUP_cla - CLA variable for the D p term. */
#pragma DATA_SECTION (piDUP_cla, "ClaToCpuMsgRAM");
FLOAT_32 piDUP_cla = 0.0;

/*! \var pidDPOld_cla - CLA variable for the D output term. */
#pragma DATA_SECTION (piDOut_cla, "ClaToCpuMsgRAM");
FLOAT_32 piDOut_cla = 0.0;

/*! \var piDV1_cla - CLA variable for the D V1 term. */
#pragma DATA_SECTION (piDV1_cla, "ClaToCpuMsgRAM");
FLOAT_32 piDV1_cla = 0.0;

/*! \var piDI1_cla - CLA variable for the D I1 term. */
#pragma DATA_SECTION (piDI1_cla, "ClaToCpuMsgRAM");
FLOAT_32 piDI1_cla = 0.0;

/*! \var piDUI_cla - CLA variable for the D UI term. */
#pragma DATA_SECTION (piDUI_cla, "ClaToCpuMsgRAM");
FLOAT_32 piDUI_cla = 0.0;

/*! \var iparkAlpha_cla - CLA variable for inverse park transform alpha. */
#pragma DATA_SECTION (iparkAlpha_cla, "ClaToCpuMsgRAM");
FLOAT_32 iparkAlpha_cla = 0.0;

/*! \var iparkBeta_cla - CLA variable for inverse park transform beta. */
#pragma DATA_SECTION (iparkBeta_cla, "ClaToCpuMsgRAM");
FLOAT_32 iparkBeta_cla = 0.0;

#pragma DATA_SECTION (svgen2t1_cla, "ClaToCpuMsgRAM");
FLOAT_32 svgen2t1_cla = 0.0;

#pragma DATA_SECTION (svgen2t2_cla, "ClaToCpuMsgRAM");
FLOAT_32 svgen2t2_cla = 0.0;

#pragma DATA_SECTION (svgen2t3_cla, "ClaToCpuMsgRAM");
FLOAT_32 svgen2t3_cla = 0.0;

/*! \var svgen2t1_cla - CLA variable for space vector sector. */
#pragma DATA_SECTION (svgen2t1Old_cla, "ClaToCpuMsgRAM");
FLOAT_32 svgen2t1Old_cla = 0.0;

/*! \var svgen2t2_cla - CLA variable for space vector Va. */
#pragma DATA_SECTION (svgen2t2Old_cla, "ClaToCpuMsgRAM");
FLOAT_32 svgen2t2Old_cla = 0.0;

/*! \var svgen2t3_cla - CLA variable for space vector Vb. */
#pragma DATA_SECTION (svgen2t3Old_cla, "ClaToCpuMsgRAM");
FLOAT_32 svgen2t3Old_cla = 0.0;

#pragma DATA_SECTION (svgen2Sector_cla, "ClaToCpuMsgRAM");
FLOAT_32 svgen2Sector_cla = 0.0;

#pragma DATA_SECTION (svgen2PWMA_cla, "ClaToCpuMsgRAM");
UINT_32 svgen2PWMA_cla = 0UL;

#pragma DATA_SECTION (svgen2PWMB_cla, "ClaToCpuMsgRAM");
UINT_32 svgen2PWMB_cla = 0.0;

#pragma DATA_SECTION (svgen2PWMC_cla, "ClaToCpuMsgRAM");
UINT_32 svgen2PWMC_cla = 0UL;

#pragma DATA_SECTION (svgen2A_cla, "ClaToCpuMsgRAM");
FLOAT_32 svgen2A_cla = 0.0;

#pragma DATA_SECTION (svgen2B_cla, "ClaToCpuMsgRAM");
FLOAT_32 svgen2B_cla = 0.0;

#pragma DATA_SECTION (svgen2C_cla, "ClaToCpuMsgRAM");
FLOAT_32 svgen2C_cla = 0.0;

/*! \var exec_cla - CLA variable for the number of I offset executions. */
#pragma DATA_SECTION (exec_cla, "ClaToCpuMsgRAM");
UINT_32 exec_cla = 0UL;

#pragma DATA_SECTION (driftExec_cla, "ClaToCpuMsgRAM");
UINT_32 driftExec_cla = 0UL;

#pragma DATA_SECTION (corrA_cla, "ClaToCpuMsgRAM");
FLOAT_32 corrA_cla = 0.0;

#pragma DATA_SECTION (corrB_cla, "ClaToCpuMsgRAM");
FLOAT_32 corrB_cla = 0.0;
