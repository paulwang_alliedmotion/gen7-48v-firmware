/*!
 * \file       i2c.c
 * \author     Zach Haver
 * \brief      I2C Driver - this driver was created as a move to interrupt
 *             driven i2c access.
 *
 */

/* $Id: $ */
/* $Name: $ */

/* NOTICE: This file is the property of Globe Motors and contains */
/* CONFIDENTIAL information.  Any use, copying or dissemination of the */
/* information contained in this file is strictly prohibited unless */
/* authorized by Globe Motors in writing. */

/* Copyright 2016, Globe Motors, Inc. */

/*
 * Revision History:
 *
 * $Log: $
 *
 */

#include <string.h>

#include "i2c.h"
#include "misra.h"
#include "piccolo.h"

/* private function declarations */

/* private variable declarations */
static I2Ctransfer_t activeTransfer;

/* public functions */

/*!
 * \fn     void i2cInit (void)
 * \brief  Initialization of the i2c driver
 */
void
i2cInit(void) {
    PICCOLO_EALLOW();

    /* turn on i2c clock */
    *PCLKCR0 |= 0x0010U;

    /* I2CMDR
     * bit  15  = (0)   NACKMOD
     * bit  14  = (0)   FREE
     * bit  13  = (0)   STT
     * bit  12  = (x)   Reserved
     * bit  11  = (0)   STP
     * bit  10  = (0)   MST
     * bit  9   = (0)   TRX
     * bit  8   = (0)   XA
     * bit  7   = (0)   RM
     * bit  6   = (0)   DLB
     * bit  5   = (0)   IRS
     * bit  4   = (0)   STB
     * bit  3   = (0)   FDF
     * bits 2-0 = (000) BC
     */
    *I2CMDR = 0U;

    /* I2CIER
     * bits 7-15 = (x) Reserved
     * bit  6    = (0) AAS
     * bit  5    = (0) SCD
     * bit  4    = (0) XRDY
     * bit  3    = (0) RRDY
     * bit  2    = (0) ARDY
     * bit  1    = (0) NACK
     * bit  0    = (0) AL
     */
    *I2CIER = 0U;

    /* I2CFFTX
     * bit 15 = (x) Reserved
     * bit 14 = (0) I2CFFEN
     * bit 13 = (0) TXFFRST
     * bit 12 = (0) TXFFST4
     * bit 11 = (0) TXFFST3
     * bit 10 = (0) TXFFST2
     * bit 9  = (0) TXFFST1
     * bit 8  = (0) TXFFST0
     * bit 7  = (0) TXFFINT
     * bit 6  = (0) TXFFINTCLR
     * bit 5  = (0) TXFFIENA
     * bit 4  = (0) TXFFIL4
     * bit 3  = (0) TXFFIL3
     * bit 2  = (0) TXFFIL2
     * bit 1  = (0) TXFFIL1
     * bit 0  = (0) TXFFIL0
     */
    *I2CFFTX = 0U;

    /* I2CFFRX
     * bits 15-14 = (x) Reserved
     * bit  13    = (0) RXFFRST
     * bit  12    = (0) RXFFST4
     * bit  11    = (0) RXFFST3
     * bit  10    = (0) RXFFST2
     * bit  9     = (0) RXFFST1
     * bit  8     = (0) RXFFST0
     * bit  7     = (0) RXFFINT
     * bit  6     = (0) RXFFINTCLR
     * bit  5     = (0) RXFFIENA
     * bit  4     = (0) RXFFIL4
     * bit  3     = (0) RXFFIL3
     * bit  2     = (0) RXFFIL2
     * bit  1     = (0) RXFFIL1
     * bit  0     = (0) RXFFIL0
     */
    *I2CFFRX = 0U;

    /* I2CPSC
     * bits 15-8 = (x)   Reserved
     * bits 7-0  = (0x5) IPSC module clock = i2c input clock/(IPSC + 1) (10MHz)
     */
    *I2CPSC = 0x0005U;
    *I2CCLKL = 10U;
    *I2CCLKH = 5U;

    PICCOLO_EDIS();
}

/*!
 * \fn    void i2cInterruptHandler(void)
 *
 * \brief This function handler the interrupt triggers to
 *        complete the i2c bus access.
 */
void
i2cInterruptHandler(void) {
    /* act based on the source of the interrupt */
    switch (*I2CISRC) {
    case I2C_INT_SRC_RDR:
        /* data ready to be read */
        activeTransfer.data[activeTransfer.index] = *I2CDRR;
        activeTransfer.index++;
        break;
    case I2C_INT_SRC_TDR:
        /* ready to transmit data */
        switch(activeTransfer.status) {
        case I2C_TX_ADDY:
            *I2CDXR = (activeTransfer.address & 0xffU);
            if (I2C_READ == activeTransfer.rw) {
                activeTransfer.status = I2C_RX_START;
            } else {
                activeTransfer.status = I2C_TX_DATA;
            }
            break;
        case I2C_TX_DATA:
            *I2CDXR = activeTransfer.data[activeTransfer.index];
            activeTransfer.index++;
            break;
        case I2C_RX_DATA:
        case I2C_WPOLL:
            *I2CDXR = 0U;
            break;
        case I2C_COMPLETED:
        case I2C_RX_START:
        default:
            /* do nothing */
            break;
        }
        break;
    case I2C_INT_SRC_SCD:
        /* stop condition detected */
        if ((I2C_RX_DATA == activeTransfer.status) &&
            (activeTransfer.index == activeTransfer.bytes)) {
            if (I2C_STATUS_NACK_SNT == (I2C_STATUS_NACK_SNT & *I2CSTR)) {
                *I2CSTR |= I2C_STATUS_NACK_SNT;
                activeTransfer.status = I2C_COMPLETED;
            }
        } else if ((I2C_TX_DATA == activeTransfer.status) &&
                   (activeTransfer.index == activeTransfer.bytes)) {
            if (I2C_STATUS_NACK_SNT != (I2C_STATUS_NACK_SNT & *I2CSTR)) {
                activeTransfer.status = I2C_WPOLL;
            }
        } else if (I2C_WPOLL == activeTransfer.status) {
                activeTransfer.status = I2C_COMPLETED;
        } else {
            /* Unknown status */
        }
        break;
    case I2C_INT_SRC_RRBA:
        if (I2C_WPOLL == activeTransfer.status) {
            if (I2C_STATUS_NACK == (I2C_STATUS_NACK & *I2CSTR)) {
                *I2CSTR |= I2C_STATUS_NACK;
                *I2CMDR = MST_PL_STR;
            } else {
                *I2CMDR = MST_PL_STP;
            }
        } else if (I2C_RX_START == activeTransfer.status) {
            activeTransfer.status = I2C_RX_DATA;
            *I2CCNT = activeTransfer.bytes;
            *I2CMDR = MST_RX_STR;
        } else {
            /* Do nothing */
            ;
        }
        break;
    default:
        /* Unknown state. Do nothing and let a failure occur */
        break;
    }
}

/*!
 * \fn     I2CErr_t i2cTxAndWait(const UINT_8 *data, UINT_16 addr, UINT_16 len)
 *
 * \param  *data - pointer to an array containing the Tx data
 * \param  addr - address to write the data to
 * \param  len - number of bytes to write
 * \return I2C_RET_BUS_BUSY - i2c bus can't be accessed
 * \return I2C_RET_NO_ERROR - success, no errors
 * \brief  This function will write to the i2c bus and wait
 *         on completion before returning.
 */
I2CErr_t
i2cTxAndWait(const UINT_8 *data, UINT_16 addr, UINT_16 len) {
    UINT_16 time = 0U;
    I2CErr_t ret;

    activeTransfer.rw = I2C_WRITE;
    activeTransfer.address = addr;
    activeTransfer.bytes = len;
    activeTransfer.index = 0U;
    activeTransfer.status = I2C_TX_ADDY;
    memcpy(activeTransfer.data, data, len);

    *I2CSAR = 0x0050U | ((addr >> 8) & 0x7U);

    *I2CCNT = len + 1U;
    *I2CIER = I2C_WRITE_INT_ENABLE;
    *I2CMDR = MST_TX_STR_STP;

    while ((10000U > time) && (I2C_WPOLL != activeTransfer.status)) {
        time++;
    }

    if (I2C_WPOLL == activeTransfer.status) {
        /* begin polling to see when the write has completed */
        *I2CIER = I2C_POLL_INT_ENABLE;
        *I2CMDR = MST_PL_STR;
    }

    time = 0U;
    while ((20000U > time) && (I2C_COMPLETED != activeTransfer.status)) {
        time++;
    }

    *I2CIER = I2C_INT_OFF;

    if (I2C_COMPLETED == activeTransfer.status) {
        ret = I2C_RET_NO_ERROR;
    } else {
        ret = I2C_RET_BUS_BUSY;
    }

    return ret;
}

/*!
 * \fn     I2CErr_t i2cRxAndWait(UINT_8 *data, UINT_16 addr, UINT_16 len)
 *
 * \param  *data - array pointer to store read data in
 * \param  addr - address to read
 * \param  len - number of bytes to read
 * \return I2C_RET_BUS_BUSY - i2c bus can't be accessed
 * \return I2C_RET_NO_ERROR - success, no errors
 * \brief  This function will read from the i2c bus and wait
 *         on the data before returning.
 */
I2CErr_t
i2cRxAndWait(UINT_8 *data, UINT_16 addr, UINT_16 len) {
    UINT_16 time = 0U;
    I2CErr_t ret;

    activeTransfer.rw = I2C_READ;
    activeTransfer.address = addr;
    activeTransfer.bytes = len;
    activeTransfer.index = 0U;
    activeTransfer.status = I2C_TX_ADDY;

    *I2CSAR = 0x0050U | ((addr >> 8) & 0x7U);
    *I2CCNT = 1U;
    *I2CIER = I2C_READ_INT_ENABLE;
    *I2CMDR = MST_TX_STR;

    while ((20000U > time) && (I2C_COMPLETED != activeTransfer.status)) {
        time++;
    }

    *I2CIER = I2C_INT_OFF;

    if (I2C_COMPLETED == activeTransfer.status) {
        memcpy(data, activeTransfer.data, len);
        ret = I2C_RET_NO_ERROR;
    } else {
        ret = I2C_RET_BUS_BUSY;
    }

    return ret;
}
