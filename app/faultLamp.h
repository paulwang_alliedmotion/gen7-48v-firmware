/*!
 * \file    faultLamp.h
 * \author  Zach Haver
 * \brief   Fault Lamp use macros
 *
 */

/* $Id: $ */
/* $Name :$ */

/* NOTICE: This file is the property of Globe Motors and contains */
/* CONFIDENTIAL information.  Any use, copying or dissemination of the */
/* information contained in this file is strictly prohibited unless */
/* authorized by Globe Motors in writing. */

/* Copyright 2016, Globe Motors, Inc. */

/*
 * Revision History:
 *
 * $Log: $
 *
 */

#ifndef FAULTLAMP_H
#define FAULTLAMP_H

#include "piccolo.h"

typedef enum {
    FAULT_TOGGLE,
    FAULT_ON,
    FAULT_OFF
}FAULTLAMPState_t;

void faultLampStateSet(FAULTLAMPState_t newState);
void faultLampToggle(void);

#endif /* FAULTLAMP_H */
