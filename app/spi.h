/*!
 * \file    spi.h
 * \author  Zach Haver
 * \brief   SPI driver definitions for the TI TMS320C28033/35 piccolo.
 */

/* $Id: $ */
/* $Name: $ */

/* NOTICE: This file is the property of Globe Motors and contains */
/* CONFIDENTIAL information.  Any use, copying or dissemination of the */
/* information contained in this file is strictly prohibited unless */
/* authorized by Globe Motors in writing. */

/* Copyright 2016, Globe Motors, Inc. */

/*
 * Revision History:
 *
 * $Log: $
 *
 */

#ifndef SPI_H
#define SPI_H

#include "misra.h"

/* SPI Modes */
#define SPI_3WIRE                                   1U
#define SPI_4WIRE                                   0U

/* SPI control register defines SPICTL */
#define SPI_CONTROL_XMIT_EN                    0x0002U
#define SPI_CONTROL_INT_EN                     0x0001U

/* chip select macros */
#define CS_INF_HIGH            (*GPADAT |= 0x00080000UL)
#define CS_INF_LOW             (*GPADAT &= ~0x00080000UL)
#define CS_DRV_HIGH            (*GPADAT |= 0x00000200UL)
#define CS_DRV_LOW             (*GPADAT &= ~0x00000200UL)

/* tranmist enable macros */
#define SPI_XMIT_ENA           (*SPIA_SPICTL |= SPI_CONTROL_XMIT_EN)
#define SPI_XMIT_DIS           (*SPIA_SPICTL &= ~SPI_CONTROL_XMIT_EN)

/* interrupt enable macros */
#define SPI_INT_ENA            (*SPIA_SPICTL |= SPI_CONTROL_INT_EN)
#define SPI_INT_DIS            (*SPIA_SPICTL &= ~SPI_CONTROL_INT_EN)

/* return codes */
#define SPI_TX_REG              5
#define SPI_TX_COMMAND_TX_WORD  4
#define SPI_RX_SAFETY           3
#define SPI_RX_REG              2
#define SPI_TX_COMMAND_RX_WORD  1

#define SPI_SUCCESS          0
#define SPI_ERROR           -1

/* public function prototypes */
void spiInit(void);
SINT_16 spiDataRead3Wire(UINT_16 command, UINT_16 *data, UINT_16 *sw);
SINT_16 spiDataWrite3Wire(UINT_16 command, UINT_16 data, UINT_16 *sw);
SINT_16 spiDataRead4Wire(UINT_16 command, UINT_16 *data);
SINT_16 spiDataWrite4Wire(UINT_16 command);
void spiInterruptHandler(void);

#endif /* SPI_H */
