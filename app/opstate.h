/*!
 * \file      opstate.h
 * \author    Zach Haver
 * \brief     Operating state definitions.
 */

/* $Id: $ */
/* $Name: $ */

/* NOTICE: This file is the property of Globe Motors and contains */
/* CONFIDENTIAL information.  Any use, copying or dissemination of the */
/* information contained in this file is strictly prohibited unless */
/* authorized by Globe Motors in writing. */

/* Copyright 2016, Globe Motors, Inc. */

/*
 * Revision History:
 *
 * $Log: $
 *
 */

#ifndef OPSTATE_H
#define OPSTATE_H

#include "misra.h"

/* operating state return values */
typedef enum {
    OPS_RUN,
    OPS_SAFE,
    OPS_SILENT,
    OPS_SEVERE
}OPSStateRet_t;

/* operating state definitions */
#define OPS_SEVERE_FLAG          1U
#define OPS_SILENT_FLAG          2U
#define OPS_INIT_I_FLAG          4U
#define OPS_SAFE_FLAG            8U
#define OPS_OFF_FLAG            32U
#define OPS_PWM_FLAG            64U
#define OPS_DRIFT_FLAG         128U

/* flag command definitions */
typedef enum {
    OPS_SET_SEVERE,
    OPS_CLR_SEVERE,
    OPS_SET_SILENT,
    OPS_CLR_SILENT,
    OPS_SET_INIT_I,
    OPS_CLR_INIT_I,
    OPS_SET_SAFE,
    OPS_CLR_SAFE,
    OPS_SET_OFF,
    OPS_CLR_OFF,
    OPS_SET_PWM,
    OPS_CLR_PWM,
    OPS_SET_DRIFT,
    OPS_CLR_DRIFT
}OPSFlagCmd_t;

void opsControl(void);
void opsUpdateFlag(OPSFlagCmd_t cmd);
UINT_16 opsGetFlags(void);

#endif /* OPSTATE_H */
