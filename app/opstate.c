/*!
 * \file      opstate.c
 * \author    Zach Haver
 * \brief     Operating states module.
 */

/* $Id: $ */
/* $Name: $ */

/* NOTICE: This file is the property of Globe Motors and contains */
/* CONFIDENTIAL information.  Any use, copying or dissemination of the */
/* information contained in this file is strictly prohibited unless */
/* authorized by Globe Motors in writing. */

/* Copyright 2016, Globe Motors, Inc. */

/*
 * Revision History:
 *
 * $Log: $
 *
 */

#include "bridge.h"
#include "dtc.h"
#include "epwm.h"
#include "ictrl.h"
#include "IQmathLib.h"
#include "misra.h"
#include "opstate.h"

/* private function declarations */
static void opsExSevere(void);
static void opsExSilent(void);
static void opsExInitI(void);
static void opsExOff(void);
static void opsExSafe(void);
static void opsExRun(void);

/* private variables */

/*! \var opFlags - operating state flags. */
static UINT_16 opFlags = OPS_INIT_I_FLAG | OPS_OFF_FLAG;

/* public functions */

/*!
 * \fn     void opsControl (void)
 *
 * \brief  This function executes different operating state parameters
 *         based on the state of the operatinf state flags
 */
void
opsControl(void) {
    /* check operating state and execute appropriate routine */
    if (OPS_SEVERE_FLAG == (opFlags & OPS_SEVERE_FLAG)) {
        opsExSevere();
    } else if (OPS_SILENT_FLAG == (opFlags & OPS_SILENT_FLAG)) {
        opsExSilent();
    } else if (OPS_INIT_I_FLAG == (opFlags & OPS_INIT_I_FLAG)) {
        opsExInitI();
    } else if (OPS_OFF_FLAG == (opFlags & OPS_OFF_FLAG)) {
        opsExOff();
    } else if (OPS_SAFE_FLAG == (opFlags & OPS_SAFE_FLAG)) {
        opsExSafe();
    } else {
        opsExRun();
    }
}

/*!
 * \fn     void opsUpdateFlag (UINT_16 cmd)
 *
 * \param  cmd - the command to set or clear a flag
 * \brief  This function sets or clears an operating state flag
 */
void
opsUpdateFlag(OPSFlagCmd_t cmd) {
    switch (cmd) {
    case OPS_SET_SEVERE:
        opFlags |= OPS_SEVERE_FLAG;
        break;
    case OPS_CLR_SEVERE:
        /* This case is for possible future use. Right now we want the
           SEVERE state to be unrecoverable without a reset. */
        ;
        break;
    case OPS_SET_SILENT:
        opFlags |= OPS_SILENT_FLAG;
        break;
    case OPS_CLR_SILENT:
        opFlags &= ~OPS_SILENT_FLAG;
        break;
    case OPS_SET_INIT_I:
        opFlags |= OPS_INIT_I_FLAG;
        break;
    case OPS_CLR_INIT_I:
        opFlags &= ~OPS_INIT_I_FLAG;
        break;
    case OPS_SET_SAFE:
        opFlags |= OPS_SAFE_FLAG;
        break;
    case OPS_CLR_SAFE:
        opFlags &= ~OPS_SAFE_FLAG;
        break;
    case OPS_SET_OFF:
        opFlags |= OPS_OFF_FLAG;
        break;
    case OPS_CLR_OFF:
        opFlags &= ~OPS_OFF_FLAG;
        break;
    case OPS_SET_PWM:
        opFlags |= OPS_PWM_FLAG;
        break;
    case OPS_CLR_PWM:
        opFlags &= ~OPS_PWM_FLAG;
        break;
    case OPS_SET_DRIFT:
        opFlags |= OPS_DRIFT_FLAG;
        break;
    case OPS_CLR_DRIFT:
        opFlags &= ~OPS_DRIFT_FLAG;
        break;
    default:
        /* unknown case do nothing */
        break;
    }
}

UINT_16
opsGetFlags(void) {
    return opFlags;
}

/* private functions */

/*!
 * \fn     static void opsExSevere (void)
 *
 * \brief  This function executes action desired for the OPS_SEVERE state
 */
static void
opsExSevere(void) {
    /* We are here for good shut down the fets */
    bridgeStateSet(BRIDGE_OFF);
    /* Set the current multiplier to 0% just incase */
    ictrlSetMultiplier(0L);
}

/*!
 * \fn     static void opsExSilent (void)
 *
 * \brief  This function executes action desired for the OPS_SILENT state
 */
static void
opsExSilent(void) {
    OPSStateRet_t state;
    /* Disable the fets then check for a recovery from this state */
    bridgeStateSet(BRIDGE_OFF);
    /* Set the current multiplier to 0% just incase */
    ictrlSetMultiplier(0L);
    /* check for recovery from this state */
    state = dtcDetermineOpstate();
    switch (state) {
    case OPS_SEVERE:
        /* if this value is returned flag should already be set
           but if it is not set it again */
        opsUpdateFlag(OPS_SET_SEVERE);
        break;
    case OPS_SILENT:
        break;
    case OPS_SAFE:
        /* SILENT error has cleared but SAFE error active */
        opsUpdateFlag(OPS_CLR_SILENT);
        /* the SAFE flag should already be set but incase it isn't set it */
        opsUpdateFlag(OPS_SET_SAFE);
        /* set the current multiplier to the 75% SAFE level */
        ictrlSetMultiplier(_IQ(0.75L));
        break;
    case OPS_RUN:
        /* SILENT error has cleared and no errors active */
        opsUpdateFlag(OPS_CLR_SILENT);
        /* return the current multiplier to 100% */
        ictrlSetMultiplier(_IQ(1.0L));
        break;
    default:
        /* unknown state do nothing */
        break;
    }
}

/*!
 * \fn     static void opsExInitI (void)
 *
 * \brief  This function executes for the current initialization period.
 */
static void
opsExInitI(void) {
    /* Keep the fets disabled until we have calculated the I offsets */
    bridgeStateSet(BRIDGE_OFF);
    /* This flag will be cleared in ictrl.c when offset calc is finished */
}

/*!
 * \fn     static void opsExOff (void)
 *
 * \brief  This function executes action desired for the OPS_OFF state
 */
static void
opsExOff(void) {
    /* This flag is controlled by the mctrl loop. */
    bridgeStateSet(BRIDGE_OFF);
    /* used when the unit is commanded off. */
}

/*!
 * \fn     static void opsExSafe (void)
 *
 * \brief  This function executes action desired for the OPS_SAFE state
 */
static void
opsExSafe(void) {
    OPSStateRet_t state;

    /* Enable fets but set cuurent multiplier to 75% */
    if ((OPS_PWM_FLAG == (opFlags & OPS_PWM_FLAG)) ||
        (OPS_DRIFT_FLAG == (opFlags & OPS_DRIFT_FLAG))) {
        bridgeStateSet(BRIDGE_OFF);
    } else {
        bridgeStateSet(BRIDGE_ON);
    }
    ictrlSetMultiplier(_IQ(0.75L));
    /* check for recovery from this state */
    state = dtcDetermineOpstate();
    switch (state) {
    case OPS_SEVERE:
        /* if this value is returned flag should already be set
           but if it is not set it again */
        opsUpdateFlag(OPS_SET_SEVERE);
        break;
    case OPS_SILENT:
        /* if this value is returned flag should already be set
           but if it is not set it again */
        opsUpdateFlag(OPS_SET_SILENT);
        break;
    case OPS_SAFE:
        break;
    case OPS_RUN:
        /* SAFE error has cleared move to run state */
        opsUpdateFlag(OPS_CLR_SAFE);
        /* restore the current mulitplier to 100% */
        ictrlSetMultiplier(_IQ(1.0L));
        break;
    default:
        /* unknown state do nothing */
        break;
    }
}

/*!
 * \fn     static void opsExRun (void)
 *
 * \brief  This function executes action desired for the OPS_RUN state
 */
static void
opsExRun(void) {
    /* No errors active just keep the fets enabled and wait */
    if ((OPS_PWM_FLAG == (opFlags & OPS_PWM_FLAG)) ||
        (OPS_DRIFT_FLAG == (opFlags & OPS_DRIFT_FLAG))) {
        bridgeStateSet(BRIDGE_OFF);
    } else {
        bridgeStateSet(BRIDGE_ON);
    }
    ictrlSetMultiplier(_IQ(1.0L));
}
