/*!
 * \file      i2c.h
 * \author    Zach Haver
 * \brief     I2C module initialization definitions
 *            for the TI TMS320C28035 Piccolo.
 */

/* $Id: $ */
/* $Name: $ */

/* NOTICE: This file is the property of Globe Motors and contains */
/* CONFIDENTIAL information.  Any use, copying or dissemination of the */
/* information contained in this file is strictly prohibited unless */
/* authorized by Globe Motors in writing. */

/* Copyright 2016, Globe Motors, Inc. */

/*
 * Revision History:
 *
 * $Log: $
 *
 */

#ifndef I2C_H
#define I2C_H

#include "misra.h"

/* status register bits */
/*! \def I2C_STATUS_NACK - bit settings for sending a NACK. */
#define I2C_STATUS_NACK      0x0002U
/*! \def I2C)STATUS_NACK_SNT - bit settings for confirmation of NACK. */
#define I2C_STATUS_NACK_SNT  0x2000U

/* interrupt sources */
/*! \def I2C_INT_SRC_RRBA - *I2CISRC register setting for RRBA interrupt. */
#define I2C_INT_SRC_RRBA          3U
/*! \def I2C_INT_SRC_RDR - *I2CISRC register setting for RDR interrupt. */
#define I2C_INT_SRC_RDR           4U
/*! \def I2C_INT_SRC_TDR - *I2CISRC register setting for TDR interrupt. */
#define I2C_INT_SRC_TDR           5U
/*! \def I2C_INT_SRC_SCD - *I2CISRC register setting for SCD interrupt. */
#define I2C_INT_SRC_SCD           6U

/* common interrupt configurations */
#define I2C_READ_INT_ENABLE    0x3cU /* XRDY | RRDY | SCD | ARDY */
#define I2C_WRITE_INT_ENABLE   0x30U /* XRDY | SCD */
#define I2C_POLL_INT_ENABLE    0x24U /* ARDY | SCD */
#define I2C_INT_OFF               0U /* All OFF */

/* common master configurations */
#define MST_TX_STR_STP       0x2e20U /* TRX | MST | STP | IRS | STT */
#define MST_TX_STR           0x2620U /* TRX | MST | IRS | STT */
#define MST_RX_STR           0x2c20U /* MST | IRS | STP | STT */
#define MST_PL_STR           0x26a0U /* MST | TRX | IRS | STT | RM */
#define MST_PL_STP           0x0ea0U /* MST | TRX | IRS | STP | RM */

/*! \enum  I2CStates_t
 *  \brief States for the interru[pt driven state machine.
 */
typedef enum {
    I2C_COMPLETED, /*!< I2C_COMPLETED - transfer completed. */
    I2C_TX_ADDY,   /*!< I2C_TX_ADDY - tranfer the address byte */
    I2C_RX_DATA,   /*!< I2C_RX_DATA - receive the data byte. */
    I2C_TX_DATA,   /*!< I2C_TX_DATA - transfer data byte. */
    I2C_RX_START,  /*!< I2C_RX_START - move to Rx mode. */
    I2C_WPOLL      /*!< I2C_WPOLL - poll for write acknowledge. */
} I2CStates_t;

/*! \enum  I2CAccessType_t
 *  \brief Read or Write access to the i2c bus.
 */
typedef enum {
    I2C_WRITE, /*!< I2C_WRITE - write access to the bus. */
    I2C_READ   /*!< I2C_READ - read access to the bus. */
} I2CAccessType_t;

/*! \enum  I2CReturn_t
 *  \brief Return status of an i2c bus access.
 */
typedef enum {
    I2C_RET_NO_ERROR, /*!< I2C_RET_NO_ERROR - no errors. */
    I2C_RET_BUS_BUSY  /*!< I2C_RET_BUS_BUSY - bus in use. */
} I2CErr_t;

/*! \struct I2Ctransfer_t
 *  \brief  Structure to hold information about the current i2c transfer.
 */
typedef struct {
    I2CAccessType_t rw; /*!< rw - read or write access. */
    UINT_16 address;    /*!< address - address for transfer. */
    UINT_16 bytes;      /*!< bytes - number of bytes to transfer. */
    UINT_16 index;      /*!< index - index of current transfer byte. */
    I2CStates_t status; /*!< status - current transfer state. */
    UINT_8 data[64];    /*!< data - data bytes for transfer. */
} I2Ctransfer_t;

/* public function prototypes */
void i2cInit(void);
void i2cInterruptHandler(void);
I2CErr_t i2cTxAndWait(const UINT_8 *data, UINT_16 addr, UINT_16 len);
I2CErr_t i2cRxAndWait(UINT_8 *data, UINT_16 addr, UINT_16 len);

#endif /* I2C_H */
