/*!
 * \file      ign.c
 * \author    Zach Haver
 * \brief     Ignition voltage monitoring functions.
 */

/* $Id: $ */
/* $Name: $ */

/* NOTICE: This file is the property of Globe Motors and contains */
/* CONFIDENTIAL information.  Any use, copying or dissemination of the */
/* information contained in this file is strictly prohibited unless */
/* authorized by Globe Motors in writing. */

/* Copyright 2018, Globe Motors, Inc. */

/*
 * Revision History:
 *
 * $Log: $
 *
 */

#include "adc.h"
#include "ign.h"
#include "misra.h"

static IGNMode_t mode = IGN_KA_AUTO;
/* public functions */

/*!
 * \fn     void ignChk (void)
 *
 * \brief  This function checks the ignition voltage to control the
 *         keep alive functionality.
 */
void
ignChk(void) {
    UINT_16 vIgn = adcGetFilt(ADC_IGN);
    static UINT_16 switchCtr = 0U;

    switch (mode) {
    case IGN_KA_ON:
        IGN_KEEP_ALIVE_ENA();
        break;
    case IGN_KA_OFF:
        IGN_KEEP_ALIVE_DIS();
        break;
    case IGN_KA_AUTO:
    default:
        if (IGN_KEEP_ALIVE_THRESHOLD < vIgn) {
            if (IGN_KEEP_ALIVE_TIME > switchCtr) {
                switchCtr++;
            }
        } else {
            switchCtr = 0U;
        }

        if (IGN_KEEP_ALIVE_TIME <= switchCtr) {
            /* enable keep alive */
            IGN_KEEP_ALIVE_ENA();
        } else {
            /* disable keep alive */
            IGN_KEEP_ALIVE_DIS();
        }
        break;
    }
}

void
ignSetMode(IGNMode_t nm) {
    mode = nm;
}
