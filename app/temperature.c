/*!
 * \file       temperature.c
 * \author     Zach Haver
 * \brief      Temperature monitoring functions.
 */

/* $Id: $ */
/* $Name: $ */

/* NOTICE: This file is the property of Globe Motors and contains */
/* CONFIDENTIAL information.  Any use, copying or dissemination of the */
/* information contained in this file is strictly prohibited unless */
/* authorized by Globe Motors in writing. */

/* Copyright 2016, Globe Motors, Inc. */

/*
 * Revision History:
 *
 * $Log: $
 *
 */

#include "dtc.h"
#include "eeprom.h"
#include "misra.h"
#include "temperature.h"
#include "tle5012.h"
#include "utils.h"

/* private function declarations */

/* private variables */
static UINT_16 maxStemp = 0U;

/* public functions */

void
temperatureInit(void) {
    UINT_8 data[2] = {0U, 0U};
    (void)eepromReadXBytes(EEADDR_MAX_STEMP, data, 2U);
    maxStemp = bytesToU16(data, LSB);
}

/*!
 * \fn     void temperatureChk (void)
 * \brief  This function checks the board temperature for fault conditions
 */
void
temperatureChk(void) {
    UINT_16 stemp = tleGetTemp();
    UINT_16 sCase;

    if (stemp > maxStemp) {
        (void)eepromWriteWord(EEADDR_MAX_STEMP, stemp);
        maxStemp = stemp;
    }

    if (STEMP_OVER_LIMIT < stemp) {
        sCase = 0U;
    } else if ((STEMP_OVER_REC < stemp) && (STEMP_OVER_LIMIT > stemp)) {
        sCase = 1U;
    } else if ((STEMP_HIGH_LIMIT < stemp) && (STEMP_OVER_REC > stemp)) {
        sCase = 2U;
    } else if ((STEMP_HIGH_REC < stemp) && (STEMP_HIGH_LIMIT > stemp)) {
        sCase = 3U;
    } else {
        sCase = 4U;
    }

    switch (sCase) {
    case 0U:
        /* greater than 120 */
        /* set over and high */
        dtcSet(DTCHW_TEMPERATURE_OVER);
        dtcSet(DTCHW_TEMPERATURE_HIGH);
        break;
    case 1U:
        /* greater than 115 but less than 120 */
        /* high active, don't change over */
        dtcSet(DTCHW_TEMPERATURE_HIGH);
        break;
    case 2U:
        /* greater than 110 but less than 115 */
        /* high set, over clear */
        dtcClear(DTCHW_TEMPERATURE_OVER);
        dtcSet(DTCHW_TEMPERATURE_HIGH);
        break;
    case 3U:
        /* greater than 105 but less than 110 */
        /* clear over, don't change high */
        dtcClear(DTCHW_TEMPERATURE_OVER);
        break;
    case 4U:
        /* less than 105 */
        /* clear high and over */
        dtcClear (DTCHW_TEMPERATURE_OVER);
        dtcClear (DTCHW_TEMPERATURE_HIGH);
        break;
    default:
        /* unused cases do nothing */
        break;
    }
}
