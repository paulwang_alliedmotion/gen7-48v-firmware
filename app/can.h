/*!
 * \file        can.h
 * \author      Zach Haver
 * \brief       eCAN module initialization definitions
 *              for the TI TMS320C28035 Piccolo.
 */

/* $Id: $ */
/* $Name: $ */

/* NOTICE: This file is the property of Globe Motors and contains */
/* CONFIDENTIAL information.  Any use, copying or dissemination of the */
/* information contained in this file is strictly prohibited unless */
/* authorized by Globe Motors in writing. */

/* Copyright 2016, Globe Motors, Inc. */

/*
 * Revision History:
 *
 * $Log: $
 *
 */

#ifndef CAN_H
#define CAN_H

#include "misra.h"

/*! \struct CANMBInit_t strructure to hold initialization info.
 *          (Note: only 31 message filters and masks can be set.)
 */
typedef struct {
    UINT_32 ids[31];
    UINT_32 masks[31];
    UINT_8 len;
}CANMBInit_t;

/*! \struct CANobj_t structure to hold information on a CAN message. */
typedef struct {
    UINT_32 id;                     /*!< Arbitration Id */
    UINT_16 dlc;                    /*!< Data Length Count */
    UINT_8 data[8];                 /*!< Message Data */
} CANobj_t;

/* public function prototypes */
void canSetMB(const CANMBInit_t *init);
void canTx(const CANobj_t *pobj);
void canTAHandler(void);
void canRMHandler(UINT_16 mbId);

#endif /* CAN_H */
