/*!
 * \file      boot.h
 * \author    Zach Haver
 * \brief     Bootkernel main and initialization definitions
 */

/* $Id: $*/
/* $Name: $ */

/* NOTICE: This file is the property of Globe Motors and contains */
/* CONFIDENTIAL information.  Any use, copying or dissemination of the */
/* information contained in this file is strictly prohibited unless */
/* authorized by Globe Motors in writing. */

/* Copyright 2016, Globe Motors, Inc. */

/*
 * Revision History:
 *
 * $Log: $
 *
 */

#ifndef BOOT_H
#define BOOT_H

/*! /def BOOT_NONE - flag indicating no special action needs to be taken. */
#define BOOT_NONE             0U
/*! /def BOOT_REQUESTRED - flag indicating the app has requested the boot. */
#define BOOT_REQUESTED        1U
/*! /def BOOT_RESET - flag indicating a reset was requested from the app. */
#define BOOT_RESET            2U

#endif /* BOOT_H */
