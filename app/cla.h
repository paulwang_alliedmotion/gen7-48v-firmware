/*!
 * \file      cla.h
 * \author    Zach Haver
 * \brief     Contrl Law Accelerator (CLA) definitions
 *            for the TMS320C28033/35 Piccolo.
 */

/* $Id: $ */
/* $Name: $ */

/* NOTICE: This file is the property of Globe Motors and contains */
/* CONFIDENTIAL information.  Any use, copying or dissemination of the */
/* information contained in this file is strictly prohibited unless */
/* authorized by Globe Motors in writing. */

/* Copyright 2016, Globe Motors, Inc. */

/*
 * Revision History:
 *
 * $Log: $
 *
 */

#ifndef CLA_H
#define CLA_H

#include "piccolo.h"

/* public function prototypes */
void claInit(void);
void claForceVariableInit(void);

#endif /* CLA_H */
