/*!
 * \file     ecap.c
 * \author   Zach Haver
 * \brief    Ecap implementation for the TI TMS320C2803x.
 */

/* $Id: $ */
/* $Name: $ */

/* NOTICE: This file is the property of Globe Motors and contains */
/* CONFIDENTIAL information.  Any use, copying or dissemination of the */
/* information contained in this file is strictly prohibited unless */
/* authorized by Globe Motors in writing. */

/* Copyright 2016, Globe Motors, Inc. */

/*
 * Revision History:
 *
 * $Log: $
 *
 */

#include "ecap.h"
#include "IQmathLib.h"
#include "misra.h"
#include "piccolo.h"

/* private variable declarations */

/*!
 * \brief  The calculated discrete vehicle speed
 */
static UINT_16 ecapHz = 0U;

static UINT_16 ecapPulseCount = 0U;

/* public functions */

/*!
 * \fn     void ecapInit (void)
 *
 * \brief  Initialize the ecap counter modules
 */
void
ecapInit(void) {
    /* Enable EALLOW protected register access */
    PICCOLO_EALLOW();
    /* Enable ecap 1 and 2 module clocks */
    *PCLKCR1 |= 0x0100U;
    /* Disable all ecap interrupts */
    *ECEINT = 0x0000U;
        /* Clear all event flags */
    *ECCLR = 0xffffU;
    /* Disable capture register loads */
    *ECCTL1 &= 0xfeffU;
    /* Disable the timer/counter */
    *ECCTL2 &= 0xffefU;
    /* Configure ECAP 1 and 2 */
    /* ECEINT
     * bits 15-8 (x) Reserved
     * bit  7    (0) CTR=CMP - Disable Counter Compare
     * bit  6    (0) CTR=CMP - Disable Counter Compare
     * bit  5    (0) CTROVF  - Disable Counter Overflow
     * bit  4    (0) CEVT4   - Disable Capture Event 4
     * bit  3    (0) CEVT3   - Disable Capture Event 3
     * bit  2    (0) CEVT2   - Disable Capture Event 2
     * bit  1    (1) CEVT1   - Enable Capture Event 1
     * bit  0    (x) Reserved
     */
    *ECEINT |= 0x0002U;
    /* ECCTL1
     * bits 15-14 (00)    Free/Soft
     * bits 13-9  (00000) PRESCALE - no prescaler
     * bit  8     (0)     CAPLDEN - enable cap register loading
     * bit  7     (0)     CTRRST4 - do not reset counter on event
     * bit  6     (0)     CAP4POL - trigger on rising edge
     * bit  5     (0)     CTRRST3 - do not reset counter on event
     * bit  4     (0)     CAP3POL - trigger on rising edge
     * bit  3     (0)     CTRRST2 - do not reset counter on event
     * bit  2     (0)     CAP2POL - trigger on rising edge
     * bit  1     (1)     CTRRST1 - reset counter on event
     * bit  0     (0)     CAP1POL - trigger on rising edge
     */
    *ECCTL1 = 0x0002U;
    /* ECCTL2
     * bits 15-11 (x)  - Reserved
     * bit  10    (0)  - APWMPOL - active high
     * bit  9     (0)  - CAP/APWM - capture
     * bit  8     (0)  - SWSYNC - no effect
     * bits 7-6   (10) - SYNCO_SEL - sync out disabled
     * bit  5     (0)  - SYNCI_EN - sync input disabled
     * bit  4     (1)  - TXCTRSTOP - time stamp counter free run
     * bit  3     (1)  - RE-ARM - resets and starts the counter
     * bits 2-1   (00) - STOP_WRAP - wrap after 1 event
     * bit  0     (0)  - CONT/ONESHT - continuous
     */
    *ECCTL2 = 0x0098U;
    /* Enable counters */
    *ECCTL1 |= 0x0100U;

    /* Disable EALLOW protected register access */
    PICCOLO_EDIS();
}

/*!
 * \fn     void ecapEvent (void)
 * \brief  This function claculates the vehicle speed
 *         after an ECAP interrupt is generated
 */
void
ecapEvent(void) {
    UINT_32 count;

    /* load ecap variable for error detection */
    count = *CAP1;
    /* calculate MPH */
    ecapHz = (UINT_16)(60000000UL / count);
    ecapPulseCount++;
    /* clear the ecap interrupt flag */
    *ECCLR |= 0x03U;
}

/*!
 * \fn     UINT_16 ecapGetHz (void)
 *
 * \return The calculated discrete vehicle speed
 */
UINT_16
ecapGetHz(void) {
    UINT_16 ret = 0U;

    if (0U < ecapPulseCount) {
        ret = ecapHz;
        ecapPulseCount = 0U;
    }
    return ret;
}
