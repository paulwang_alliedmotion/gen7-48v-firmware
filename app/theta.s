;//###########################################################################
;//
;// FILE:  theta.s
;//
;// TITLE: theta conversion Assembly Code.
;//
;// This file contains the assembly code to convert from theta counts to
;// to theta radians.
;//
;//###########################################################################
theta_CLA       .macro

_theta_CLA_START

                MMOV32          MR3, @_thetaPoles_cla
                MUI32TOF32      MR3, MR3
                MMOV32          MR0, @_thetaCounts_cla
                MUI32TOF32      MR0, MR0
                MMOV32          MR1, @_thetaCPR_cla
                MUI32TOF32      MR1,MR1
                MMOV32          MR2,MR1                ;MR2 = CPR
                MEINVF32        MR1,MR1                ;MR1 = 1/CPR
                MMPYF32         MR0,MR0,MR1            ;Counts/CPR
                MFRACF32        MR0,MR0                ;to per unit
                MMPYF32         MR0,MR0,MR2            ;1 mech rev limited
                MEINVF32        MR3,MR3 ; invert poles for division
                MMPYF32         MR1,MR2,MR3 ; MR1 = CPR/poles
                MEINVF32        MR1,MR1 ; MR1 = 1/(16384/poles)
                MMPYF32         MR0,MR0,MR1
                MFRACF32        MR0,MR0 ;theta per unit
                MMOVF32         MR2, #6.283185307179586
                MMPYF32         MR0,MR0,MR2
                MMOV32          @_thetaRad_cla, MR0

_theta_CLA_END
                .endm
