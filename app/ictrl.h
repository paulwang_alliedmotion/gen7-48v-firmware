/*!
 * \file      ictrl.h
 * \author    Zach Haver
 * \brief     Current control definitions.
 */

/* $Id: $ */
/* $Name: $ */

/* NOTICE: This file is the property of Globe Motors and contains */
/* CONFIDENTIAL information.  Any use, copying or dissemination of the */
/* information contained in this file is strictly prohibited unless */
/* authorized by Globe Motors in writing. */

/* Copyright 2016, Globe Motors, Inc. */

/*
 * Revision History:
 *
 * $Log: $
 *
 */

#ifndef ICTRL_H
#define ICTRL_H

#include "IQmathLib.h"
#include "misra.h"

void ictrlInit(void);
void ictrlUpdateILimit(void);
void ictrlSetRef(_iq qRef, _iq dRef);
void ictrlSetMultiplier(_iq mult);
void ictrlIExcessChk(void);

#endif /* ICTRL_H */
