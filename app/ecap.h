/*!
 * \file        ecap.h
 * \author      Zach Haver
 * \brief       eCAP initialization definitions for the TI TMS320C2803x.
 */

/* $Id: $ */
/* $Name: $ */

/* NOTICE: This file is the property of Globe Motors and contains */
/* CONFIDENTIAL information.  Any use, copying or dissemination of the */
/* information contained in this file is strictly prohibited unless */
/* authorized by Globe Motors in writing. */

/* Copyright 2016, Globe Motors, Inc. */

/*
 * Revision History:
 *
 * $Log: $
 *
 */

#ifndef ECAP_H
#define ECAP_H

#include "misra.h"
#include "piccolo.h"

/* public function prototypes */
void ecapInit(void);
void ecapEvent(void);
UINT_16 ecapGetHz(void);

#endif /* ECAP_H */
