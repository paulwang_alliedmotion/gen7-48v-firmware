/*!
 * \file      utils.c
 * \author    Zach Haver
 * \brief     byte conversions
 */

/* $Id: $ */
/* $Name: $ */

/* NOTICE: This file is the property of Globe Motors and contains */
/* CONFIDENTIAL information.  Any use, copying or dissemination of the */
/* information contained in this file is strictly prohibited unless */
/* authorized by Globe Motors in writing. */

/* Copyright 2017, Globe Motors, Inc. */

/*
 * Revision History:
 *
 * $Log: $
 *
 */

#include "IQmathLib.h"
#include "misra.h"
#include "utils.h"

_iq
mapIQ(UINT_32 loc) {
    UINT_32 location = loc;
    UINT_16 lo = *MAP_U16(location);
    UINT_16 hi;
    UINT_32 ret;
    location++;
    hi = *MAP_U16(location);
    ret = hi;
    ret <<= 16;
    ret |= (UINT_32)lo;
    return (_iq)ret;
}

UINT_16
bytesToU16(const UINT_8 *bytes, UtilEnd_t endian) {
    UINT_16 ret;

    if (LSB == endian) {
        ret = (UINT_16)bytes[1];
        ret <<= 8;
        ret |= (UINT_16)bytes[0];
    } else {
        ret = (UINT_16)bytes[0];
        ret <<= 8;
        ret |= (UINT_16)bytes[1];
    }

    return ret;
}

UINT_32
bytesToU32(const UINT_8 *bytes, UtilEnd_t endian) {
    UINT_32 ret;

    if (LSB == endian) {
        ret = (UINT_32)bytes[3];
        ret <<= 8;
        ret |= (UINT_32)bytes[2];
        ret <<= 8;
        ret |= (UINT_32)bytes[1];
        ret <<= 8;
        ret |= (UINT_32)bytes[0];
    } else {
        ret = (UINT_32)bytes[0];
        ret <<= 8;
        ret |= (UINT_32)bytes[1];
        ret <<= 8;
        ret |= (UINT_32)bytes[2];
        ret <<= 8;
        ret |= (UINT_32)bytes[3];
    }

    return ret;
}

_iq
bytesToIQ(const UINT_8 *bytes) {
    UINT_32 ret = (UINT_32)bytes[3];

    ret <<= 8;
    ret |= (UINT_32)bytes[2];
    ret <<= 8;
    ret |= (UINT_32)bytes[1];
    ret <<= 8;
    ret |= (UINT_32)bytes[0];
    return (_iq)ret;
}

void
u16ToBytes(UINT_16 val, UINT_8 *bytes, UtilEnd_t endian) {
    if (LSB == endian) {
        bytes[0] = (UINT_8)(val & 0xffU);
        bytes[1] = (UINT_8)((val >> 8) & 0xffU);
    } else {
        bytes[1] = (UINT_8)(val & 0xffU);
        bytes[0] = (UINT_8)((val >> 8) & 0xffU);
    }
}

void
u32ToBytes(UINT_32 val, UINT_8 *bytes, UtilEnd_t endian) {
    if (LSB == endian) {
        bytes[0] = (UINT_8)(val & 0xffUL);
        bytes[1] = (UINT_8)((val >> 8) & 0xffUL);
        bytes[2] = (UINT_8)((val >> 16) & 0xffUL);
        bytes[3] = (UINT_8)((val >> 24) & 0xffUL);
    } else {
        bytes[3] = (UINT_8)(val & 0xffUL);
        bytes[2] = (UINT_8)((val >> 8) & 0xffUL);
        bytes[1] = (UINT_8)((val >> 16) & 0xffUL);
        bytes[0] = (UINT_8)((val >> 24) & 0xffUL);
    }
}
