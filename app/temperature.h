/*!
 * \file      temperature.h
 * \author    Zach Haver
 * \brief     Temperature monitoring definitions.
 */

/* $Id: $ */
/* $Name: $ */

/* NOTICE: This file is the property of Globe Motors and contains */
/* CONFIDENTIAL information.  Any use, copying or dissemination of the */
/* information contained in this file is strictly prohibited unless */
/* authorized by Globe Motors in writing. */

/* Copyright 2016, Globe Motors, Inc. */

/*
 * Revision History:
 *
 * $Log: $
 *
 */

#ifndef TEMPERATURE_H
#define TEMPERATURE_H

/* temp limit definitions */
#define STEMP_OVER_LIMIT     160U
#define STEMP_OVER_REC       155U
#define STEMP_HIGH_LIMIT     150U
#define STEMP_HIGH_REC       145U

/* function prototypes */
void temperatureInit(void);
void temperatureChk(void);

#endif /* TEMPERATURE_H */
