/*!
 * \file      rand32.c
 * \author    Zach Haver
 * \brief     Random number generator module
 */

/* $Id: $ */
/* $Name: $ */

/* NOTICE: This file is the property of Globe Motors and contains */
/* CONFIDENTIAL information.  Any use, copying or dissemination of the */
/* information contained in this file is strictly prohibited unless */
/* authorized by Globe Motors in writing. */

/* Copyright 2016, Globe Motors, Inc. */

/*
 * Revision History:
 *
 * $Log: $
 *
 */

#include "misra.h"
#include "rand32.h"

/*! \var r32 - active random number. */
static SINT_32 r32;


/*!
 * \fn    SINT_32 rand32(void)
 *
 * \brief This function is suggested by Numerical Recipes in C as a decent,
 *        fast random number generator with 32 bit integer result.  The TI
 *        supplied library, by contrast, supplies a 16 bit rand() function.
 */
SINT_32
rand32(void) {
    r32 = (1664525L * r32) + 1013904223L;
    return r32;
}

/*!
 * \fn    void randShuffle(void)
 *
 * \brief This function is ran periodically to add entropy to the calculation.
 */
void
randShuffle(void) {
    r32 = (1664525L * r32) + 1013904223L;
}
