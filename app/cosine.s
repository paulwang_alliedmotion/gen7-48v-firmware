;//###########################################################################
;//
;// FILE:  cosine.s
;//
;// TITLE: cosine calculation Assembly Code.
;//
;// This file contains the cosine calculation assembly code.
;//
;//###########################################################################
CLAcos          .macro

_CLAcos_START

                MMOV32          MR0,@_thetaRad_cla
                MMOV32          MR1,@_CLAsincosTable_TABLE_SIZEDivTwoPi
                MMPYF32         MR1,MR0,MR1
        ||      MMOV32          MR2,@_CLAsincosTable_TABLE_MASK

                MF32TOI32       MR3,MR1
                MAND32          MR3,MR3,MR2
                MLSL32          MR3,#1
                MMOV16          MAR0,MR3,#_CLAsincosTable_Sin0
                MFRACF32        MR1,MR1
                MMOV32          MR0,@_CLAsincosTable_TwoPiDivTABLE_SIZE
                MMPYF32         MR1,MR1,MR0
        ||      MMOV32          MR0,@_CLAsincosTable_Coef3_neg

                MMOV32          MR2,*MAR0[#+64]++
                MMPYF32         MR3,MR0,MR2
        ||      MMOV32          MR2,*MAR0[#-64]++
                MMPYF32         MR3,MR3,MR1
        ||      MMOV32          MR0,@_CLAsincosTable_Coef2

                MMPYF32         MR2,MR0,MR2
                MADDF32         MR3,MR3,MR2
        ||      MMOV32          MR2,*MAR0[#+64]++
                MMPYF32         MR3,MR3,MR1
        ||      MMOV32          MR0,@_CLAsincosTable_Coef1_pos

                MMPYF32         MR2,MR0,MR2
                MADDF32         MR3,MR3,MR2
        ||      MMOV32          MR2,*MAR0[#-64]++
                MMPYF32         MR3,MR3,MR1
        ||      MMOV32          MR0,@_CLAsincosTable_Coef0

                MMPYF32         MR2,MR0,MR2
                MADDF32         MR3,MR3,MR2
        ||      MMOV32          MR2,*MAR0[#+64]++
                MMPYF32         MR3,MR3,MR1

                MSUBF32         MR3,MR3,MR2
                MMPYF32         MR3,MR3,MR1
        ||      MMOV32          MR2,*MAR0[#0]++

                MADDF32         MR3,MR2,MR3
                MMOV32          @_co_cla,MR3

_CLAcos_END
                .endm
