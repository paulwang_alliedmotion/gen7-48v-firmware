/*!
 * \file      offset.h
 * \author    Zach Haver
 * \brief     Definitions for the offset detection function.
 */

/* $Id$ */
/* $Name$ */

/* NOTICE: This file is the property of Globe Motors and contains */
/* CONFIDENTIAL information.  Any use, copying or dissemination of the */
/* information contained in this file is strictly prohibited unless */
/* authorized by Globe Motors in writing. */

/* Copyright 2017, Globe Motors, Inc. */

/*
 * Revision History:
 *
 * $Log$
 *
 */

#ifndef OFFSETDETECT_H
#define OFFSETDETECT_H

#include "misra.h"
#include "tle5012.h"

/* function prototypes */
void offsetSetCPR(TLE_MOD4_CPR_t cpr);
UINT_16 offsetDetect3Pole(void);
UINT_16 offsetDetect4Pole(void);
UINT_16 offsetDetect5Pole(void);

#endif /* OFFSETDETECT_H */
