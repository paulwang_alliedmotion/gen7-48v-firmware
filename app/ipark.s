;//###########################################################################
;//
;// FILE:  ipark.s
;//
;// TITLE: inverse park transform Assembly Code.
;//
;// This file contains the inverse park transform assembly code.
;//
;//###########################################################################
ipark_CLA .macro

_ipark_CLA_START

        MMOV32  MR0, @_co_cla
        MMOV32  MR1, @_piDOut_cla

        MMPYF32 MR3,MR1,MR0     ; cosine * Dout

        MMOV32  MR0, @_si_cla
        MMOV32  MR1, @_piQOut_cla

        MMPYF32 MR2,MR1,MR0     ; sine * Qout

        MSUBF32 MR3,MR3,MR2     ; (cosine * Dout) - (sine * Qout)

        MMOV32  @_iparkAlpha_cla,MR3 ; store result

        MMOV32  MR0, @_co_cla
        MMOV32  MR1, @_piQOut_cla

        MMPYF32 MR3,MR1,MR0     ; cosine * Qout

        MMOV32  MR0, @_si_cla
        MMOV32  MR1, @_piDOut_cla

        MMPYF32 MR2,MR1,MR0     ; sine * Dout

        MADDF32 MR3,MR3,MR2     ; (cosine * Qout) + (sine * Dout)

        MMOV32  @_iparkBeta_cla,MR3 ; store result

_ipark_CLA_END
        .endm
