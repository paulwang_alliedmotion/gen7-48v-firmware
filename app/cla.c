/*!
 * \file      cla.c
 * \author    Zach Haver
 * \brief     Control Law Accelerator (CLA) initialization
 *            functions for the TMS320C28033/35 Piccolo.
 */

/* $Id: $ */
/* $Name: $ */

/* NOTICE: This file is the property of Globe Motors and contains */
/* CONFIDENTIAL information.  Any use, copying or dissemination of the */
/* information contained in this file is strictly prohibited unless */
/* authorized by Globe Motors in writing. */

/* Copyright 2016, Globe Motors, Inc. */

/*
 * Revision History:
 *
 * $Log: $
 *
 */

#include "cla.h"
#include "claShared.h"
#include "piccolo.h"

/* public functions */

/*!
 * \fn    void claInit(void)
 *
 * \brief This function initializes the CLA registers and function pointers.
 */
void
claInit(void) {
    PICCOLO_EALLOW();

    /* Enabled the CLA clock */
    *PCLKCR3 |= 0x4000U;

    /* MMEMCFG
     * bits 15-6 - (x) Reserved
     * bit  5    - (1) RAM1E - CPU use
     * bit  4    - (0) RAM0E - CPU use
     * bits 3-1  - (x) Reserved
     * bit  0    - (1) PROGE - Program use enabled
     */
    *MMEMCFG = 0x0021U;

    /*lint -save -e946 -e947 -e9033 -e845 -e831 */
    /* Initialize the vector mapping */
    *MVECT1 = (UINT_16)(&ClaTask1 - &ClaProg_Start)*sizeof(UINT_32);
    *MVECT2 = (UINT_16)(&ClaTask2 - &ClaProg_Start)*sizeof(UINT_32);
    *MVECT3 = (UINT_16)(&ClaTask3 - &ClaProg_Start)*sizeof(UINT_32);
    *MVECT4 = (UINT_16)(&ClaTask4 - &ClaProg_Start)*sizeof(UINT_32);
    *MVECT5 = (UINT_16)(&ClaTask5 - &ClaProg_Start)*sizeof(UINT_32);
    *MVECT6 = (UINT_16)(&ClaTask6 - &ClaProg_Start)*sizeof(UINT_32);
    *MVECT7 = (UINT_16)(&ClaTask7 - &ClaProg_Start)*sizeof(UINT_32);
    *MVECT8 = (UINT_16)(&ClaTask8 - &ClaProg_Start)*sizeof(UINT_32);
    /*lint -restore */

    /* MPISRCSEL1
     * bits 31-28 - (xxx1) PERINT8SEL = No Interrupt
     * bits 27-24 - (xxx1) PERINT7SEL = No Interrupt
     * bits 23-20 - (xxx1) PERINT6SEL = No Interrupt
     * bits 19-16 - (xxx1) PERINT5SEL = No Interrupt
     * bits 15-12 - (xxx1) PERINT4SEL = No Interrupt
     * bits 11-8  - (xxx1) PERINT3SEL = No Interrupt
     * bits 7-4   - (0000) PERINT2SEL = ADCINT2
     * bits 3-0   - (0000) PERINT1SEL = ADCINT1
     */
    *MPISRCSEL1 = 0x11111100UL;

    *MCTL = 0x0004U;

    /* MIER
     * bits 15-8 - (x) Reserved
     * bit  7    - (1) INT8 = disabled
     * bit  6    - (0) INT7 = disabled
     * bit  5    - (0) INT6 = disabled
     * bit  4    - (0) INT5 = disabled
     * bit  3    - (0) INT4 = disabled
     * bit  2    - (0) INT3 = disabled
     * bit  1    - (0) INT2 = disabled
     * bit  0    - (1) INT1 = disabled
     */
    *MIER = 0x0083U;

    PICCOLO_EDIS();
}

/*!
 * \fn    void claForceVariableInit(void)
 *
 * \brief This function will force CLA task 8 to execute causing an
 *        initialization of the CLA variables in claShared.c.
 */
void
claForceVariableInit(void) {
    PICCOLO_EALLOW();
    *MIFRC |= 0x0080U;
    PICCOLO_EDIS();
}
