/*!
 * \file      tle5012.c
 * \author    Zach Haver
 * \brief     Infineon tle5012 initialization and use functions
 */

/* $Id: $ */
/* $Name: $ */

/* NOTICE: This file is the property of Globe Motors and contains */
/* CONFIDENTIAL information.  Any use, copying or dissemination of the */
/* information contained in this file is strictly prohibited unless */
/* authorized by Globe Motors in writing. */

/* Copyright 2016, Globe Motors, Inc. */

/*
 * Revision History:
 *
 * $Log: $
 *
 */

#include "dtc.h"
#include "eqep.h"
#include "IQmathLib.h"
#include "piccolo.h"
#include "spi.h"
#include "tle5012.h"

/* private function prototypes */
static TLERet_t tleCheckFailPersistent(void);
static UINT_16 modShift = 1;
static UINT_16 highError = 16112U;
static UINT_16 lowError = 272U;
static UINT_32 modMask = 0x3fffUL;

/* public functions */

/*!
 * \fn    void tleInit(void)
 *
 * \brief This function sets the initial state if the TLE5012.
 */
void
tleInit(TLE_MOD4_CPR_t cpr) {
    UINT_16 mod4;

    switch (cpr) {
    case TLE_MOD4_512CPR:
        mod4 = 0x18U;
        modShift = 4;
        modMask = 0x7ffUL;
        highError = 2014U;
        lowError = 34U;
        break;
    case TLE_MOD4_1024CPR:
        mod4 = 0x10U;
        modShift = 3;
        modMask = 0xfffUL;
        highError = 4028U;
        lowError = 68U;
        break;
    case TLE_MOD4_2048CPR:
        mod4 = 0x8U;
        modShift = 2;
        modMask = 0x1fffUL;
        highError = 8056U;
        lowError = 136U;
        break;
    case TLE_MOD4_4096CPR:
    default:
        mod4 = 0U;
        modShift = 1;
        modMask = 0x3fffUL;
        highError = 16112U;
        lowError = 272U;
        break;
    }
    /* set 9-bit resolution on the pre-quad encoder signal */
    if (TLE_SUCCESS != tleSetReg(MOD_4, mod4)) {
        dtcSet(DTCHW_ENCODER_SPI);
    }
    eqepSetCPR(cpr);
}

/*!
 * \fn     TLERet_t tleGetTemp(UINT_16 *temp)
 *
 * \param  *temp pointer to load temperature date into.
 * \return TLE_SUCCESS on successful read.
 * \return TLE_FAIL on failure.
 * \brief  This function will fetch the tempreature of the tle5012.
 */
UINT_16
tleGetTemp(void) {
    UINT_16 data = 0xffffU;
    SINT_16 stemp;
    UINT_16 ret;

    if (TLE_SUCCESS == tleGetReg(FSYNC, &data)) {
        /* scale to temperature */
        data = data & 0x01ffU;
        if (0x100U == (data & 0x100U)) {
            data |= 0xff00U;
            stemp = (SINT_16)data;
        } else {
            stemp = (SINT_16)data;
        }
    } else {
        stemp = -82;
        dtcSet(DTCHW_ENCODER_SPI);
    }

    stemp += 152;
    stemp *= 3;
    ret = (UINT_16)stemp >> 3;
    if (0 > stemp) {
        ret |= 0xe000U;
    }
    stemp = (SINT_16)ret;
    if (-40 > stemp) {
        stemp = -40;
    }
    stemp += 40;
    ret = (UINT_16)stemp;

    return ret;
}

/*!
 * \fn     TLERet_t tleGetPos(UINT_16 *pos)
 *
 * \param  *pos pointer to load position data into.
 * \return TLE_SUCCESS on successful read.
 * \return TLE_FAIL on failure.
 * \brief  This function will fetch the position of the tle5012.
 */
TLERet_t
tleGetPos(UINT_16 *pos) {
    UINT_16 data = 0xffffU;
    TLERet_t ret = TLE_FAIL;

    if (TLE_SUCCESS == tleGetReg(AVAL, &data)) {
        data = (data & 0x7fffU) >> modShift;
        *pos = data;
        ret = TLE_SUCCESS;
    } else {
        dtcSet(DTCHW_ENCODER_SPI);
    }
    return ret;
}

/*!
 * \fn    void tleChkPos(void)
 *
 * \brief This function can be called periodically the check the setup
 *        of the tle5012 is intact, and the check for and possibly correct
 *        position drift.
 */
void
tlePosChk(void) {
    UINT_16 data = 0U;
    _iq eqepVel = _IQabs(eqepGetVelocity());
    UINT_16 tlePos = 0U;
    UINT_32 correctedPos = 0UL;
    UINT_16 eqepPos = 0U;
    SINT_32 diff = 0L;
    static UINT_16 errorCtr = 0U;

    if (TLE_SUCCESS == tleGetReg(STAT, &data)) {
        if (0x0U != (data & 0x1ef6U)) {
            dtcSet(DTCHW_ENCODER_BITS);
        }
    }

    if (_IQ(500.0L) > eqepVel) {
        if (TLE_SUCCESS == tleGetPos(&tlePos)) {
            if ((highError > tlePos) && (lowError < tlePos)) {
                correctedPos = eqepGetFaultPos();
                eqepPos = ((UINT_16)correctedPos & (UINT_16)modMask);
                correctedPos &= ~modMask;
                correctedPos |= (UINT_32)tlePos;
                *QPOSCNT = correctedPos;
                diff = (SINT_32)eqepPos - (SINT_32)tlePos;
                if (((SINT_32)lowError < diff) || (((SINT_32)lowError * -1) > diff)) {
                    errorCtr++;
                } else {
                    errorCtr = 0U;
                }
                if (5U < errorCtr) {
                    dtcSet(DTCHW_ENCODER_VAR);
                }
            }
        }
    }
}

/* private functions */

TLERet_t
tleSetReg(UINT_16 rNo, UINT_16 data) {
    UINT_16 commandWord = 1U;
    UINT_16 safetyWord = 0U;
    TLERet_t ret = TLE_FAIL;

    if (4U < rNo) {
        commandWord |= 0x5000U;
    }
    commandWord |= ((rNo << 4) & 0x03f0U);

    if (SPI_SUCCESS == spiDataWrite3Wire(commandWord, data, &safetyWord)) {
        if (0xf000U == (safetyWord & 0xf000U)) {
            ret = TLE_SUCCESS;
        } else {
            ret = tleCheckFailPersistent();
        }
    }
    return ret;
}

TLERet_t
tleGetReg(UINT_16 rNo, UINT_16 *data) {
    UINT_16 commandWord = 0x8001U;
    UINT_16 safetyWord = 0U;
    TLERet_t ret = TLE_FAIL;

    commandWord |= ((rNo << 4) & 0x03f0U);

    if (SPI_SUCCESS == spiDataRead3Wire(commandWord, data, &safetyWord)) {
        if (0xf000U == (safetyWord & 0xf000U)) {
            ret = TLE_SUCCESS;
        } else {
            ret = tleCheckFailPersistent();
        }
    }
    return ret;
}

static TLERet_t
tleCheckFailPersistent(void) {
    UINT_16 cW = 0x8001U;
    UINT_16 data = 0U;
    UINT_16 sW = 0U;
    TLERet_t ret = TLE_FAIL;

    if (SPI_SUCCESS == spiDataRead3Wire(cW, &data, &sW)) {
        if (0U == (0x1ef6U & data)) {
            ret = TLE_SUCCESS;
        } else {
            if (SPI_SUCCESS == spiDataRead3Wire(cW, &data, &sW)) {
                if (0U == (0x1ef6U & data)) {
                    ret = TLE_SUCCESS;
                }
            }
        }
    }
    return ret;
}
