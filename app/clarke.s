;//###########################################################################
;//
;// FILE:  clarke_cla.s
;//
;// TITLE: clarke transform Assembly Code.
;//
;// This file contains the clarke transform assembly code.
;//
;//###########################################################################

clarke_CLA      .macro

_clarke_CLA_START

                MMOVF32         MR2,#0.57735026918963 ; 1/SQRT(3)
                MMOV32          MR0, @_clarkeAs_cla  ; load first phase
                MMOV32          MR1, @_clarkeBs_cla  ; load second phase

                MMPYF32         MR3,#2.0,MR1 ; MR3 = 2 * BS
                MADDF32         MR3,MR3,MR0 ; MR3 = (2 * BS) + AS
                MMPYF32         MR3,MR3,MR2 ; MR3 = .5773502 * ((2 * BS) + AS)

                MMOV32          @_clarkeAlpha_cla,MR0 ; export results
                MMOV32          @_clarkeBeta_cla,MR3

_clarke_CLA_END
                .endm
