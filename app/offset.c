/*!
 * \file      offset.c
 * \author    Zach Haver
 * \brief     Function to detect the motor encoder offset.
 */

/* $Id$ */
/* $Name$ */

/* NOTICE: This file is the property of Globe Motors and contains */
/* CONFIDENTIAL information.  Any use, copying or dissemination of the */
/* information contained in this file is strictly prohibited unless */
/* authorized by Globe Motors in writing. */

/* Copyright 2017, Globe Motors, Inc. */

/*
 * Revision History:
 *
 * $Log$
 *
 */

#include "bridge.h"
#include "eeprom.h"
#include "eqep.h"
#include "misra.h"
#include "ictrl.h"
#include "IQmathLib.h"
#include "offset.h"
#include "piccolo.h"
#include "tle5012.h"

/* private function declarations */

/* private variables */
static UINT_16 offsetMod3 = 5461U;
static UINT_16 offsetMod4 = 4096U;
static UINT_16 offCPR = 16384U;
static UINT_16 offFudge3 = 480U;
static UINT_16 offFudge4 = 335U;

/* public functions */

/*!
 * \fn     UINT_16 offsetDetect (void)
 * \return rotor phase offset
 * \brief  This function determines the rotor phase offset
 *
 */
void
offsetSetCPR(TLE_MOD4_CPR_t cpr) {
    switch (cpr) {
    case TLE_MOD4_512CPR:
        offsetMod3 = 682U;
        offsetMod4 = 512U;
        offCPR = 2048U;
        offFudge3 = 60U;
        offFudge4 = 42U;
        break;
    case TLE_MOD4_1024CPR:
        offsetMod3 = 1365U;
        offsetMod4 = 1024U;
        offCPR = 4096U;
        offFudge3 = 120U;
        offFudge4 = 84U;
        break;
    case TLE_MOD4_2048CPR:
        offsetMod3 = 2730U;
        offsetMod4 = 2048U;
        offCPR = 8192U;
        offFudge3 = 240U;
        offFudge4 = 167U;
        break;
    case TLE_MOD4_4096CPR:
    default:
        offsetMod3 = 5461U;
        offsetMod4 = 4096U;
        offCPR = 16384U;
        offFudge3 = 480U;
        offFudge4 = 335U;
        break;
    }
}

UINT_16
offsetDetect3Pole(void) {
    UINT_16 x;
    UINT_32 delay = 0UL;
    UINT_16 offset = 0U;
    UINT_16 off_temp[4] = {0U, 0U, 0U, 0U};
    SINT_16 temp = 0;

    bridgeStateSet(BRIDGE_ON);
    ictrlSetRef (_IQ(20.0L), 0L);

    for (x = 0U; x < 4U; x++) {
        eqepSetOffsetState(EQEP_OFF_LOCK2);
        /* delay to allow movement and control sepic voltage */
        for (delay = 0UL; delay < 1000000UL; delay++) {
            asm (" NOP");
        }
        eqepSetOffsetState(EQEP_OFF_LOCK1);
        /* delay to allow movement and control sepic voltage */
        for (delay = 0UL; delay < 1000000UL; delay++) {
            asm (" NOP");
        }
        if (TLE_SUCCESS == tleGetPos(&offset)) {
            offset %= offsetMod3;
            offset = offCPR - offset;
            offset += offFudge3;
            if (offCPR < offset) {
                offset -= offCPR;
            }
            offset %= offsetMod3;
        } else {
            offset = 0xffffU;
        }
        off_temp[x] = offset;
    }

    ictrlSetRef (0L, 0L);
    bridgeStateSet(BRIDGE_OFF);
    eqepSetOffsetState(EQEP_OFF_NORM);

    for (x = 0U; x < 4U; x++) {
        temp = (SINT_16)off_temp[x] - (SINT_16)off_temp[((x+1U) & 0x03U)];
        if (20 < temp) {
            offset = 0xffffU;
        } else if (-20 > temp) {
            offset = 0xffffU;
        } else {
            /* good offset */
            ;
        }
    }

    if (0xffffU != offset) {
        (void)eepromWriteWord(EEADDR_ENCO_OFFSET, offset);
        (void)eepromWriteWord(EEADDR_ENCO_POLES, 3U);
    }

    return offset;
}

UINT_16 offsetDetect4Pole(void) {
    UINT_16 x;
    UINT_32 delay = 0UL;
    UINT_16 offset = 0U;
    UINT_16 off_temp[4] = {0U, 0U, 0U, 0U};
    UINT_16 offCompare = 0U;
    SINT_16 temp = 0;

    bridgeStateSet(BRIDGE_ON);
    ictrlSetRef(_IQ(20.0L), 0L);

    for (x = 0U; x < 4U; x++) {
        eqepSetOffsetState(EQEP_OFF_LOCK4);
        /* delay to allow movement and control sepic voltage */
        for (delay = 0UL; delay < 1000000UL; delay++) {
            asm (" NOP");
        }
        (void)tleGetPos(&offCompare);
        eqepSetOffsetState(EQEP_OFF_LOCK3);
        /* delay to allow movement and control sepic voltage */
        for (delay = 0UL; delay < 1000000UL; delay++) {
            asm (" NOP");
        }
        if (TLE_SUCCESS == tleGetPos(&offset)) {
            if (offCompare != offset) {
                offset %= offsetMod4;
                offset = offCPR - offset;
                offset += offFudge4;
                if (offCPR < offset) {
                    offset -= offCPR;
                }
                offset %= offsetMod4;
            } else {
                offset = 0xffffU;
            }
        } else {
            offset = 0xffffU;
        }
        off_temp[x] = offset;
    }

    ictrlSetRef (0L, 0L);
    bridgeStateSet(BRIDGE_OFF);
    eqepSetOffsetState(EQEP_OFF_NORM);

    for (x = 0U; x < 4U; x++) {
        temp = (SINT_16)off_temp[x] - (SINT_16)off_temp[((x+1U) & 0x03U)];
        if (20 < temp) {
            offset = 0xffffU;
        } else if (-20 > temp) {
            offset = 0xffffU;
        } else {
            /* good offset */
            ;
        }
    }

    if (0xffffU != offset) {
        (void)eepromWriteWord(EEADDR_ENCO_OFFSET, offset);
        (void)eepromWriteWord(EEADDR_ENCO_POLES, 4U);
    }

    return offset;
}

UINT_16 offsetDetect5Pole(void) {
    return 0xffffU;
}
