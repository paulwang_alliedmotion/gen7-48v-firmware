/*!
 * \file        tle5012.h
 * \author      Eric Firestone
 * \author      Zach Haver
 * \brief       Infineon tle5012 initialization definitions
 */

/* $Id: $ */
/* $Name: $ */

/* NOTICE: This file is the property of Globe Motors and contains */
/* CONFIDENTIAL information.  Any use, copying or dissemination of the */
/* information contained in this file is strictly prohibited unless */
/* authorized by Globe Motors in writing. */

/* Copyright 2016, Globe Motors, Inc. */

/*
 * Revision History:
 *
 * $Log: $
 *
 */

#ifndef TLE5012_H
#define TLE5012_H

#include "misra.h"
#include "piccolo.h"

/* tle5012 registers non secure */
#define STAT                                  0x00U
#define ACSTAT                                0x01U
/* Angle Value Register H 37 */
#define AVAL                                  0x02U
#define ASPD                                  0x03U
#define AREV                                  0x04U
/* Frame Synchronization Register */
#define FSYNC                                 0x05U
#define MOD_1                                 0x06U
#define SIL                                   0x07U
#define MOD_2                                 0x08U
#define MOD_3                                 0x09U
#define OFFX                                  0x0aU
#define OFFY                                  0x0bU
#define SYNCH                                 0x0cU
#define IFAB                                  0x0dU
#define MOD_4                                 0x0eU
#define TCO_Y                                 0x0fU
#define ADC_X                                 0x10U
#define ADC_Y                                 0x11U
#define IIF_CNT                               0x20U

/* return status */
typedef enum {
    TLE_SUCCESS,
    TLE_FAIL
}TLERet_t;

typedef enum {
    TLE_MOD4_4096CPR,
    TLE_MOD4_2048CPR,
    TLE_MOD4_1024CPR,
    TLE_MOD4_512CPR
}TLE_MOD4_CPR_t;

void tleInit(TLE_MOD4_CPR_t cpr);
UINT_16 tleGetTemp(void);
TLERet_t tleGetPos(UINT_16 *pos);
void tlePosChk(void);
TLERet_t tleSetReg(UINT_16 rNo, UINT_16 data);
TLERet_t tleGetReg(UINT_16 rNo, UINT_16 *data);

#endif /* TLE5012_H */
