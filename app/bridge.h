/*!
 * \file      bridge.h
 * \author    Zach Haver
 * \brief     Bridge control states.
 */

/* $Id: $ */
/* $Name: $ */

/* NOTICE: This file is the property of Globe Motors and contains */
/* CONFIDENTIAL information.  Any use, copying or dissemination of the */
/* information contained in this file is strictly prohibited unless */
/* authorized by Globe Motors in writing. */

/* Copyright 2016, Globe Motors, Inc. */

/*
 * Revision History:
 *
 * $Log: $
 *
 */

#include "misra.h"
#include "piccolo.h"

#ifndef BRIDGE_H
#define BRIDGE_H

typedef enum {
    BRIDGE_ON,
    BRIDGE_OFF,
    BRIDGE_FAULTED,
    BRIDGE_TIMEOUT
}BRIDGEStates_t;

typedef enum {
    BRIDGE_ERROR,
    BRIDGE_RECOVERED
}BRIDGERet_t;

/* function prototypes */
void bridgeWDOGState(UINT_16 ns);
void bridgeChk(void);
void bridgeWDogKick(void);
void bridgeStateSet(BRIDGEStates_t newState);
void bridgeFaultINT(void);
void bridgeTimeoutINT(void);

#endif /* BRIDGE_H */
