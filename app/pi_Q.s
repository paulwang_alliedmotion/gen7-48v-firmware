;//###########################################################################
;//
;// FILE:  pi_Q.s
;//
;// TITLE: Q-axis pi calculation Assembly Code.
;//
;// This file contains the Q-axis pi calculation assembly code.
;//
;//###########################################################################

pi_q_CLA        .macro

_pi_q_CLA_START
        MMOV16  MAR0,@_piQRef_cpu
        MNOP
        MNOP
        MNOP
        MMOV32  MR0,*MAR0
        MMOV32  @_piQRef_cla,MR0 ;ref
        MMOV32  MR1,@_parkQs_cla ;fdbk

        MSUBF32 MR0,MR0,MR1     ;ref - fdbk
        MMOV32  @_piQUP_cla,MR0 ;up = ref - fbdk

        MMOV32  MR1,@_piQOut_cla;out
        MMOV32  MR2,@_piQV1_cla ;v1
        MCMPF32 MR1,MR2         ;out == v1
        MNOP
        MNOP
        MNOP
        MBCNDD  cmpNotEq,NEQ
        MNOP
        MNOP
        MNOP
        MMOVF32 MR3,#0.00352    ;Ki
        MMPYF32 MR3,MR3,MR0     ;Ki * up
        MMOV32  MR1,@_piQI1_cla
        MADDF32 MR3,MR3,MR1     ;(ki * up) + i1
        MBCNDD  iComp
        MNOP
        MNOP
        MNOP
cmpNotEq
        MMOV32  MR3,@_piQI1_cla
iComp
        MMOV32  @_piQUI_cla,MR3 ;ui
        MMOV32  @_piQI1_cla,MR3 ;i1 = ui

        MADDF32 MR1,MR0,MR3     ;up + ui
        MMOVF32 MR2,#0.0176     ;Kp
        MMPYF32 MR1,MR1,MR2     ;Kp * (up + ui)
        MMOV32  @_piQV1_cla,MR1

        MMOVF32 MR2,#0.85       ;Umax
        MMINF32 MR1,MR2
        MMOVF32 MR2,#-0.85      ;Umin
        MMAXF32 MR1,MR2
        MMOV32  @_piQOut_cla,MR1

_pi_q_CLA_END
        .endm
