/*!
 * \file      cla_shared.h
 * \author    Zach Haver
 * \brief     Contrl Law Accelerator (CLA) definitions
 *            for the TMS320C28033/35 Piccolo.
 */

/* $Id: $ */
/* $Name: $ */

/* NOTICE: This file is the property of Globe Motors and contains */
/* CONFIDENTIAL information.  Any use, copying or dissemination of the */
/* information contained in this file is strictly prohibited unless */
/* authorized by Globe Motors in writing. */

/* Copyright 2016, Globe Motors, Inc. */

/*
 * Revision History:
 *
 * $Log: $
 *
 */

#ifndef CLA_SHARED_H
#define CLA_SHARED_H

#include "misra.h"

extern UINT_32 ClaTask1;
extern UINT_32 ClaTask2;
extern UINT_32 ClaTask3;
extern UINT_32 ClaTask4;
extern UINT_32 ClaTask5;
extern UINT_32 ClaTask6;
extern UINT_32 ClaTask7;
extern UINT_32 ClaTask8;
extern UINT_32 ClaProg_Start;

/* To the CLA */
extern UINT_32 *thetaTrans_cpu;
extern UINT_32 *thetaPoles_cpu;
extern UINT_32 *thetaCPR_cpu;
extern FLOAT_32 *piQRef_cpu;
extern FLOAT_32 *piDRef_cpu;

/* From the CLA */
extern UINT_32 adcOnA_cla;
extern UINT_32 adcOnB_cla;
extern UINT_32 adcOnC_cla;
extern UINT_32 adcBattOn_cla;
extern UINT_32 adcT1_cla;
extern UINT_32 adcT2_cla;
extern UINT_32 adcOffA_cla;
extern UINT_32 adcOffB_cla;
extern UINT_32 adcOffC_cla;
extern UINT_32 adcP2_cla;
extern UINT_32 adcDTemp_cla;
extern UINT_32 adcIgn_cla;
extern UINT_32 adcP1_cla;
extern UINT_32 adcP3_cla;
extern UINT_32 adcVREF2_cla;

extern FLOAT_32 ampsOnA_cla;
extern FLOAT_32 ampsOnB_cla;
extern FLOAT_32 ampsOnC_cla;
extern FLOAT_32 offsetA_cla;
extern FLOAT_32 offsetB_cla;
extern FLOAT_32 offsetC_cla;

extern FLOAT_32 clarkeAs_cla;
extern FLOAT_32 clarkeBs_cla;
extern FLOAT_32 clarkeAlpha_cla;
extern FLOAT_32 clarkeBeta_cla;

extern UINT_32 thetaCounts_cla;
extern UINT_32 thetaPoles_cla;
extern UINT_32 thetaCPR_cla;
extern FLOAT_32 thetaRad_cla;
extern FLOAT_32 si_cla;
extern FLOAT_32 co_cla;

extern FLOAT_32 parkQs_cla;
extern FLOAT_32 parkDs_cla;

extern FLOAT_32 piQRef_cla;
extern FLOAT_32 piQUP_cla;
extern FLOAT_32 piQOut_cla;
extern FLOAT_32 piQV1_cla;
extern FLOAT_32 piQI1_cla;
extern FLOAT_32 piQUI_cla;

extern FLOAT_32 piDRef_cla;
extern FLOAT_32 piDUP_cla;
extern FLOAT_32 piDOut_cla;
extern FLOAT_32 piDV1_cla;
extern FLOAT_32 piDI1_cla;
extern FLOAT_32 piDUI_cla;

extern FLOAT_32 iparkAlpha_cla;
extern FLOAT_32 iparkBeta_cla;

extern FLOAT_32 svgen2t1_cla;
extern FLOAT_32 svgen2t2_cla;
extern FLOAT_32 svgen2t3_cla;
extern FLOAT_32 svgen2t1Old_cla;
extern FLOAT_32 svgen2t2Old_cla;
extern FLOAT_32 svgen2t3Old_cla;
extern FLOAT_32 svgen2Sector_cla;
extern UINT_32 svgen2PWMA_cla;
extern UINT_32 svgen2PWMB_cla;
extern UINT_32 svgen2PWMC_cla;
extern FLOAT_32 svgen2A_cla;
extern FLOAT_32 svgen2B_cla;
extern FLOAT_32 svgen2C_cla;

extern UINT_32 exec_cla;

extern UINT_32 driftExec_cla;

extern FLOAT_32 corrA_cla;
extern FLOAT_32 corrB_cla;

#endif /* CLA_SHARED_H */
