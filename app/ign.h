/*!
 * \file      ign.h
 * \author    Zach Haver
 * \brief     Ignition voltage monitoring definitions.
 */

/* $Id: $ */
/* $Name: $ */

/* NOTICE: This file is the property of Globe Motors and contains */
/* CONFIDENTIAL information.  Any use, copying or dissemination of the */
/* information contained in this file is strictly prohibited unless */
/* authorized by Globe Motors in writing. */

/* Copyright 2018, Globe Motors, Inc. */

/*
 * Revision History:
 *
 * $Log: $
 *
 */

#ifndef IGN_H
#define IGN_H

#include "misra.h"

/* IGN Threshold = 7.5V * 26.824counts/volt = 201.18 counts */
#define IGN_KEEP_ALIVE_THRESHOLD    201U

/* IGN Threshold time = 7 = 70ms/10ms execution period */
#define IGN_KEEP_ALIVE_TIME           7U

#define IGN_KEEP_ALIVE_ENA() (*GPADAT |= 0x00000080UL)

#define IGN_KEEP_ALIVE_DIS() (*GPADAT &= ~0x00000080UL)

typedef enum {
    IGN_KA_ON,
    IGN_KA_OFF,
    IGN_KA_AUTO
} IGNMode_t;

/* function prototypes */
void ignChk(void);
void ignSetMode(IGNMode_t nm);

#endif /* IGN_H */
