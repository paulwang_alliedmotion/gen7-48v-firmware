/*!
 * \file      crc.h
 * \author    Zach Haver
 * \brief     Cyclic Redundancy Check definitions
 */

/* $Id: $ */
/* $Name: $ */

/* NOTICE: This file is the property of Globe Motors and contains */
/* CONFIDENTIAL information.  Any use, copying or dissemination of the */
/* information contained in this file is strictly prohibited unless */
/* authorized by Globe Motors in writing. */

/* Copyright 2016, Globe Motors, Inc. */

/*
 * Revision History:
 *
 * $Log: $
 *
 */

#ifndef CRC_H
#define CRC_H

#include "misra.h"

/*! \def CRC_INIT - default starting value for a CRC calculation. */
#define CRC_INIT                0xffffffffUL
/*! \def CRC_TABLE_START - start of the CRC table in flash. */
#define CRC_TABLE_START         0x003f7d00UL

void crcBK(void);
UINT_32 crcVerifyBlockFlash(UINT_32 offset, UINT_32 length);

#endif /* CRC_H */
