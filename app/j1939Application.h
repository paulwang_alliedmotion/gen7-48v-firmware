/*!
 * \file      j1939Application.h
 * \author    Zach Haver
 * \brief     Implementation of the J1939 application layer.
 *
 */

/* $Id: $ */
/* $Name: $ */

/* NOTICE: This file is the property of Globe Motors and contains */
/* CONFIDENTIAL information.  Any use, copying or dissemination of the */
/* information contained in this file is strictly prohibited unless */
/* authorized by Globe Motors in writing. */

/* Copyright 2016, Globe Motors, Inc. */

/*
 * Revision History:
 *
 * $Log: $
 *
 */

#ifndef JAPP_H
#define JAPP_H

#include "j1939Application.h"
#include "j1939DataLink.h"
#include "misra.h"

/*! \def JAPP_PGN_FEDA - Parameter Group Number for the SOFTID */
#define JAPP_PGN_FEDA        0xfedaU
/*! \def JAPP_PGN_CCVS - Parameter Group Number for CCVS */
#define JAPP_PGN_CCVS        0xfef1U
/*! \def JAPP_PGN_EEC1 - Parameter Group Number for EEC1 */
#define JAPP_PGN_EEC1        0xf004U
#define JAPP_PGN_FF00        0xff00U
#define JAPP_PGN_FF11        0xff11U
#define JAPP_PGN_FF12        0xff12U
#define JAPP_PGN_FF13        0xff13U
#define JAPP_PGN_FF14        0xff14U
#define JAPP_PGN_FF15        0xff15U
#define JAPP_PGN_FF16        0xff16U
#define JAPP_PGN_FF20        0xff20U
#define JAPP_PGN_FF21        0xff21U
#define JAPP_PGN_FF22        0xff22U
#define JAPP_PGN_FF23        0xff23U
#define JAPP_PGN_FF24        0xff24U
#define JAPP_PGN_FF25        0xff25U
#define JAPP_PGN_FF26        0xff26U
#define JAPP_PGN_FF27        0xff27U
#define JAPP_PGN_FF28        0xff28U
#define JAPP_PGN_FF29        0xff29U
#define JAPP_PGN_FF2A        0xff2aU
#define JAPP_PGN_FF40        0xff40U
#define JAPP_PGN_FF41        0xff41U
#define JAPP_PGN_FF50        0xff50U
#define JAPP_PGN_FF51        0xff51U
#define JAPP_PGN_FF60        0xff60U
#define JAPP_PGN_FF61        0xff61U

typedef enum {
    JAPP_DM13_TRANSMIT,
    JAPP_DM13_HOLD
}JAPPDM13State_t;

typedef enum {
    JAPP_ALL,
    JAPP_FDBK,
    JAPP_DTHN
}JAPPTxState_t;

void jappInit(void);
void jappMessageProcess(const J1939msg_t *pmsg);
void jappTimer(void);
void jappSetDM13State(JAPPDM13State_t newState);
void jappSetTxState(JAPPTxState_t newState);
void jappFEDA(const J1939msg_t *pmsg);
void jappChangeRAMTest(void);

#endif /* JAPP_H */
