/*!
 * \file        rand32.h
 * \author      Zach Haver
 * \brief       Random number generator module
 */

/* $Id: $ */
/* $Name: $ */

/* NOTICE: This file is the property of Globe Motors and contains */
/* CONFIDENTIAL information.  Any use, copying or dissemination of the */
/* information contained in this file is strictly prohibited unless */
/* authorized by Globe Motors in writing. */

/* Copyright 2016, Globe Motors, Inc. */

/*
 * Revision history:
 *
 * $Log: $
 *
 */

#ifndef RAND32_H
#define RAND32_H

#include "misra.h"

/* public function prototypes */

SINT_32 rand32(void);
void randShuffle(void);

#endif /* RAND32_H */
