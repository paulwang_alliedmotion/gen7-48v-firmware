/*!
 * \file      g6ManufacturingAPP.c
 * \author    Zach Haver
 * \brief     Application main and initialization functions
 */

/* $Id: $ */
/* $Name: $ */

/* NOTICE: This file is the property of Globe Motors and contains */
/* CONFIDENTIAL information.  Any use, copying or dissemination of the */
/* information contained in this file is strictly prohibited unless */
/* authorized by Globe Motors in writing. */

/* Copyright 2016, Globe Motors, Inc. */

/*
 * Revision History:
 *
 * $Log: $
 *
 */

#include <string.h> /* for memcpy() */

#include "adc.h"
#include "bridge.h"
#include "cla.h"
#include "claShared.h"
#include "config.h"
#include "crc.h"
#include "debugLED.h"
//#include "drv8305.h"
#include "dtc.h"
#include "ecap.h"
#include "eeprom.h"
#include "epwm.h"
#include "eqep.h"
#include "faultLamp.h"
#include "i2c.h"
#include "ictrl.h"
#include "ign.h"
#include "j1939DataLink.h"
#include "mctrl.h"
#include "misra.h"
#include "opstate.h"
#include "piccolo.h"
#include "pie.h"
#include "rand32.h"
#include "sched.h"
#include "softid.h"
#include "spi.h"
#include "tctrl.h"
#include "temperature.h"
#include "tle5012.h"
#include "tmr0.h"
#include "vbat.h"

extern UINT_16 secureRamFuncsLoadStart;
extern UINT_16 secureRamFuncsLoadEnd;
extern UINT_16 secureRamFuncsRunStart;
extern UINT_16 claFuncsLoadStart;
extern UINT_16 claFuncsLoadEnd;
extern UINT_16 claFuncsRunStart;
extern UINT_16 claMathTablesLoadStart;
extern UINT_16 claMathTablesLoadEnd;
extern UINT_16 claMathTablesRunStart;

void
main(void) {
    /* Globally disable interrupts */
    PICCOLO_DINT();
    /* Copy RAM items */
    /*lint -save -e946 -e947 -e9034 */
    memcpy(&secureRamFuncsRunStart,
           &secureRamFuncsLoadStart,
           &secureRamFuncsLoadEnd - &secureRamFuncsLoadStart);
    memcpy(&claFuncsRunStart,
           &claFuncsLoadStart,
           &claFuncsLoadEnd - &claFuncsLoadStart);
    memcpy(&claMathTablesRunStart,
           &claMathTablesLoadStart,
           &claMathTablesLoadEnd - &claMathTablesLoadStart);
    /*lint -restore */
    /* Initialize Peripheral Interrupts */
    pieInit();
    /* Initialize the ADC */
    adcInit();
    /* Initialize the PWM */
    epwmInit();
    /* Initialize the CLA */
    claInit();
    /* Initialize the SPI bus */
    spiInit();
    /* Initialize the I2C bus */
    i2cInit();
    /* Enable I2C and SPI interrupts for immediate use */
    IER = 0x00a0U;
    PICCOLO_EINT();
    PICCOLO_ERTM();
    /* Initialize the DTC layer */
    dtcInit();
    /* check config */
    configInit();
    /* Initialize the DRV */
    //drvInit();
    /* Initialize the rotor encoder */
    tleInit(TLE_MOD4_4096CPR);
    /* Initialize the QEP */
    eqepInit();
    /* Initialize the ECAP */
    ecapInit();
    /* Initialize TMR0 */
    tmr0Init();
    /* Initialize the C2000 current control */
    ictrlInit();
    /* Intiailize the torque assist module */
    tctrlInit();
    /* Initialize the J1939 Data Link Layer */
    jdlInit();
    /* Initialize the temperature monitoring */
    temperatureInit();
    /* Initiailize the software identification */
    softIdInit();
    mctrlInit();
    crcBK();

    /* Initialize and populate the scheduler */
    schedInit();
    schedAddTask(opsControl, 10U);
    schedAddTask(jdlTimer, 10U);
    schedAddTask(mctrlControl, 10U);
    schedAddTask(dtcControl, 1000U);
    schedAddTask(eqepCalcVelocity, 50U);
    schedAddTask(tlePosChk, 100U);
    schedAddTask(vbatChk, 1000U);
    schedAddTask(temperatureChk, 10000U);
    schedAddTask(bridgeChk, 100U);
    schedAddTask(ictrlIExcessChk, 20U);
    schedAddTask(ignChk, 100U);
    schedAddTask(debugLEDToggle, 1000U);
    schedAddTask(randShuffle, 1000U);
    schedAddTask(faultLampToggle, 5000U);

    /* Enable All Used Interrupts */
    PICCOLO_DINT();
    IER |= 0x0dadU;
    PICCOLO_EINT();
    PICCOLO_ERTM();

    /* Force CLA Variable Initialization */
    claForceVariableInit();

    /* Clear the boot count */
    (void)eepromWriteWord(EEADDR_BOOT_COUNT, 0U);

    for (;;) {
        schedExec();
    }
}
