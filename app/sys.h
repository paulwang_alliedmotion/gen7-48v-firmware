/*!
 * \file      sys.h
 * \author    Zach Haver
 * \brief     System initialization definitions for the TI TMS320C28035.
 */

/* $Id: $ */
/* $Name: $ */

/* NOTICE: This file is the property of Globe Motors and contains */
/* CONFIDENTIAL information.  Any use, copying or dissemination of the */
/* information contained in this file is strictly prohibited unless */
/* authorized by Globe Motors in writing. */

/* Copyright 2016, Globe Motors, Inc. */

/*
 * Revision History:
 *
 * $Log: $
 *
 */

#ifndef SYS_H
#define SYS_H

#include "piccolo.h"

/* public function prototypes */
void sysReset(void);

#endif /* SYS_H */
