/*!
 * \file      tctrl.c
 * \author    Zach Haver
 * \brief     Torque control module. This module generates an output torque
 *            based off of an input torque and vehicle speed.
 */

/* $Id: $ */
/* $Name: $ */

/* NOTICE: This file is the property of Globe Motors and contains */
/* CONFIDENTIAL information.  Any use, copying or dissemination of the */
/* information contained in this file is strictly prohibited unless */
/* authorized by Globe Motors in writing. */

/* Copyright 2016, Globe Motors, Inc. */

/*
 * Revision History:
 *
 * $Log: $
 *
 */

#include "adc.h"
#include "config.h"
#include "dtc.h"
#include "eeprom.h"
#include "eqep.h"
#include "ictrl.h"
#include "IQmathLib.h"
#include "misra.h"
#include "tctrl.h"
#include "utils.h"

/* private function declarations */
static _iq tctrlCompensate(_iq in);
static _iq tctrlCalcDI(void);
static _iq tctrlBasicAssist(_iq input);


/* private variables */

/*!
 * \brief  Calculated input torque
 */
static _iq tIn = (_iq)0UL;

/*!
 * \brief  Torsion shaft offset
 */
static _iq tOffset = (_iq)0UL;

/*!
 * \brief  Torsion shaft gain
 */
static _iq tGain = _IQ(0.01518L);

/*!
 * \brief  Temporary calculated torque output
 */
static _iq tCmdOut = (_iq)0UL;

/*!
 * \brief  Max Current Ouput
 */
static _iq tMaxIOut = _IQ(60.0L);

static _iq dead = _IQ(1.0L);
static _iq slope = _IQ(16.7L);
static _iq maxAssist = _IQ(100.0L);

static _iq dAxisDead = _IQ(0.5L);
static _iq dAxisMax = _IQ(60.0L);
static _iq dAxisGain = _IQ(0.0L);
static _iq dAxisThresh = _IQ(10.0L);

/* public functions */

/*!
 * \fn     void tctrlInit (void)
 * \brief  This function initializes the torque control module
 *
 */
void
tctrlInit(void) {
    UINT_8 bytes[8];

    /* read calibrations for the tctrl module */
    if (EE_ERR_NONE == eepromReadXBytes(EEADDR_T_GAIN, bytes, 8U)) {
        tOffset = bytesToIQ(&bytes[4]);
        tGain = bytesToIQ(bytes);
    }

    if (CONFIG_VALID == configGetState()) {
        /*lint -save -e835 */
        dAxisDead = mapIQ(CONFIG_DDEAD);
        /*lint -restore */
        dAxisMax = mapIQ(CONFIG_DMAX);
        dAxisGain = mapIQ(CONFIG_DGAIN);
        dAxisThresh = mapIQ(CONFIG_DTHRESH);
        dead = mapIQ(CONFIG_TDEAD);
        slope = mapIQ(CONFIG_TSLOPE);
        maxAssist = mapIQ(CONFIG_TMAX);
    }
}

void
tctrlUpdateILimit(void) {
    UINT_8 data[4] = {0U, 0U, 0U, 0U};

    if (EE_ERR_NONE == eepromReadXBytes(EEADDR_I_LIM, data, 4U)) {
        tMaxIOut = bytesToIQ(data);
    }
}

/*!
 * \fn     void tctrlGenCMD (_iq *q, _iq *d)
 * \param  *q - q-axis current command
 * \param  *d - d-axis current command
 * \brief  This function calculates the q-axis and d-axis current commands
 *         when the motor is in torque control mode
 *
 */
void
tctrlGenCMD(_iq *q, _iq *d) {
    _iq pcTorque;

    pcTorque = tctrlCompensate(tIn);

    *d = tctrlCalcDI();
    *q = tctrlBasicAssist(pcTorque);
}

/*!
 * \fn     _iq tctrlGetTin (void)
 * \return tirque_in - the calculated torque input
 * \brief  Accesor function for the calcualted torque input
 *
 */
_iq
tctrlGetTin(void) {
    return tIn;
}

/*!
 * \fn     void tctrlCalcInput (void)
 * \brief  This function fetches the ADC values from the torque sensor
 *         and converts them to actual torque values
 *
 */
void
tctrlCalcInput(void) {
    UINT_16 t1 = adcGetFilt(ADC_T1);
    UINT_16 t2 = adcGetFilt(ADC_T2);
    UINT_32 shift1 = (UINT_32)t1 << 18;
    UINT_32 shift2 = (UINT_32)t2 << 18;
    _iq t1Q = (_iq)shift1;
    _iq t2Q = (_iq)shift2;
    _iq torque = t1Q - t2Q;
    static UINT_16 tRangeCtr = 0U;
    static UINT_16 tLinCtr = 0U;
    static UINT_16 faultTimeout = 0U;

    if (250U <= faultTimeout) {
        /* check t1 and t2 signals for errors */
        /* check range errors 205 = 5% and 3892 = 95% */
        if ((205U >= t1) || (3892U <= t1) || (205U >= t2) || (3892U <= t2)) {
            if (140U > tRangeCtr) {
                tRangeCtr += 4U;
            }
        } else {
            if (0U < tRangeCtr) {
                tRangeCtr--;
            }
        }
        if (128U <= tRangeCtr) {
            dtcSet(DTCHW_T_RANGE);
        }
        /* check linearity 3686 = 90% and 4506 = 110% */
        if ((3686U > (t1 + t2)) || (4506U < (t1 + t2))) {
            if (140U > tLinCtr) {
                tLinCtr += 2U;
            }
        } else {
            if (0U < tLinCtr) {
                tLinCtr--;
            }
        }
        if (128U <= tLinCtr) {
            dtcSet(DTCHW_T_LINE);
        }
    } else {
        faultTimeout++;
    }

    torque = _IQmpy (torque , tGain);

    torque = torque + tOffset;

    tIn = torque;
}

/*!
 * \fn     _iq tctrlGetOut (void)
 * \return calculated output torque to generate
 * \brief  Accessor funtion for the desired torque output
 *
 */
_iq
tctrlGetOut(void) {
    return tCmdOut;
}

void
tctrlUpdateOffset(_iq nOff) {
    if (tOffset != nOff) {
        if (EE_ERR_NONE == eepromWriteDWord(EEADDR_T_OFFSET, (UINT_32)nOff)) {
            tOffset = nOff;
        }
    }
}

_iq
tctrlGetOffset(void) {
    return tOffset;
}

/* private functions */

/*!
 * \fn     static _iq tctrlCalcDI (void)
 * \return D-axis current command
 * \brief  This function calculates the d-axis current command
 *
 */
static _iq
tctrlCalcDI(void) {
    _iq gain = _IQmpy(dAxisGain, _IQ(0.0016666667L));
    _iq dSpeed = _IQabs(eqepGetVelocity());
    UINT_16 batt = adcGetRaw(ADC_BATT_ON);
    UINT_32 shifter = (UINT_32)batt << 18;
    _iq vBatt = _IQmpy((_iq)shifter, _IQ(0.037037L));
    _iq thresh = _IQmpy(dAxisThresh, _IQ(3.6363636L));
    _iq wBase = _IQmpy(vBatt, thresh);
    _iq dI = 0L;

    if (dSpeed > wBase) {
        dI = _IQmpy((dSpeed - wBase), gain); /* Gain */
    }

    if (dI > dAxisMax) { /* Max */
        dI = dAxisMax;
    } else if (dI < 0L) {
        dI = 0L;
    } else {
        /* no change needed */
        ;
    }

    if (_IQabs(tIn) < dAxisDead) { /* Deadband */
        dI = 0L;
    }

    return dI;
}

/*!
 * \fn     static _iq tctrlCompensate (_iq in)
 * \param  in - input torque value
 * \return phase compensated torque value
 * \brief  This function adds phase compensations to the input torque
 *
 */
static _iq
tctrlCompensate(_iq in) {
    _iq out = (_iq)0UL;
    _iq temp = (_iq)0UL;
    static _iq inLast = (_iq)0UL;
    static _iq outLast = (_iq)0UL;
    static UINT_16 iterCtr = 0U;

    if(0U < iterCtr) {
        temp = _IQmpy (inLast, _IQ(0.8L));
        out = in - temp;
        out = _IQmpy (out, _IQ(4.0L));
        temp = _IQmpy (outLast, _IQ(0.2L));
        out = out + temp;
    } else {
        iterCtr++;
    }

    inLast = in;
    outLast = out;

    return out;
}

/*!
 * \fn     static _iq tctrlBasicAssist (_iq input)
 * \param  input - input torque value
 * \return generated output torque command
 * \brief  This function does a 3d look up/linear calculation based
 *         on input torque and speed to generate a desired output torque
 *
 */
static _iq
tctrlBasicAssist(_iq input) {
    _iq in = _IQabs(input);
    _iq out;

    /* clamp the input to 30Nm */
    if (in > _IQ(30.0L)) {
        in = _IQ(30.0L);
    }

    if (dead > in) {
        out = 0L;
    } else {
        in = in - dead;
        out = _IQmpy(in, slope);
    }

    if (maxAssist < out) {
        out = maxAssist;
    }

    if (0L > input) {
        out = _IQmpy(out, _IQ(-1.0L));
    }

    tCmdOut = out;

    out = _IQmpy(out, _IQ(0.01L));

    return _IQmpy(out, tMaxIOut);
}
