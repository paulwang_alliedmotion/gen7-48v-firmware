/*!
 * \file      adc.c
 * \author    Zach Haver
 * \brief     ADC driver functions for the TI TMS320C28035 Piccolo.
 */

/* $Id: $ */
/* $Name: $ */

/* NOTICE: This file is the property of Globe Motors and contains */
/* CONFIDENTIAL information.  Any use, copying or dissemination of the */
/* information contained in this file is strictly prohibited unless */
/* authorized by Globe Motors in writing. */

/* Copyright 2016, Globe Motors, Inc. */

/*
 * Revision History:
 *
 * $Log: $
 *
 */

#include "adc.h"
#include "claShared.h"
#include "misra.h"
#include "piccolo.h"

/* private function declarations */
static UINT_16 adcWindowAvg(UINT_16 newVal, adcWindowAvg_t *avg);
static UINT_16 adcWeightAvg(UINT_16 ch, UINT_16 cfOld, UINT_16 cfNew);
static void adcCheckMaxMin(void);

/* private variable declarations */

/*! \var adc - adcData_t data array. */
static adcData_t adc[NMR_ADC_CHANNELS];
/*! \var adcWinT1 - windowed average structure for the T1 signal. */
static adcWindowAvg_t adcWinT1 = ADC_WINAVG_DEF;
/*! \var adcWinT2 - windowed average structure for the T2 signal. */
static adcWindowAvg_t adcWinT2 = ADC_WINAVG_DEF;
static UINT_16 phaseMax[3] = {2048U, 2048U, 2048U};
static UINT_16 phaseMin[3] = {2048U, 2048U, 2048U};

/* public functions */
/*!
 * \fn    void adcInit(void)
 *
 * \brief This function sets the state of the ADC registers for the
 *        desired sampling.
 */
void
adcInit(void) {
    UINT_16 x;

    /* Enable Eallow register access */
    PICCOLO_EALLOW();

    /* Enable the ADC clock */
    *PCLKCR0 |= 0x0008U;

    /* Set up the ADC */
    /* ADCCTL1
     * bit  15   - (0)    RESET = No Effect
     * bit  14   - (1)    ADCENABLE = Enabled
     * bit  13   - (x)    ADCBSY
     * bits 12-8 - (0xXX) ADCBSYCHN
     * bit  7    - (1)    ADCPWDN = Powered Up
     * bit  6    - (1)    ADCBGPWD = Powered Up
     * bit  5    - (1)    ADCREFPWD = Powered Up
     * bit  4    - (x)    Reserved
     * bit  3    - (1)    ADCREFSEL = External
     * bit  2    - (1)    INTPULSEPOS = 1 cycle before latch
     * bit  1    - (1)    VREFLOCONV = Internal
     * bit  0    - (1)    TEMPCONV = Convert Temperature
     */
    *ADCCTL1 = 0x40efU;

    /* Clear interrupt flags */
    *ADCINTFLGCLR = 0xffffU;

    /* INTSEL1N2
     * bit  15   - (x)    Reserved
     * bit  14   - (0)    INT2CONT = Interrupt must be cleared
     * bit  13   - (1)    INT2E
     * bits 12-8 - (0x0e) INT2SEL
     * bit  7    - (x)    Reserved
     * bit  6    - (0)    INT1CONT = Interrupt must be cleared
     * bit  5    - (1)    INT1E
     * bits 4-0  - (0x03) INT1SEL
     */
    *INTSEL1N2 = 0x2e23U;

    /* INTSEL3N4
     * bit  15   - (x)    Reserved
     * bit  14   - (0)    INT4CONT = Interrupt must be cleared
     * bit  13   - (0)    INT4E
     * bits 12-8 - (0x00) INT4SEL
     * bit  7    - (x)    Reserved
     * bit  6    - (0)    INT3CONT = Interrupt must be cleared
     * bit  5    - (0)    INT3E
     * bits 4-0  - (0x00) INT3SEL
     */
    *INTSEL3N4 = 0x0000U;

    /* INTSEL5N6
     * bit  15   - (x)    Reserved
     * bit  14   - (0)    INT6CONT = Interrupt must be cleared
     * bit  13   - (0)    INT6E
     * bits 12-8 - (0x00) INT6SEL
     * bit  7    - (x)    Reserved
     * bit  6    - (0)    INT5CONT = Interrupt must be cleared
     * bit  5    - (0)    INT5E
     * bits 4-0  - (0x00) INT5SEL
     */
    *INTSEL5N6 = 0x0000U;

    /* INTSEL7N8
     * bit  15   - (x)    Reserved
     * bit  14   - (0)    INT8CONT = Interrupt must be cleared
     * bit  13   - (0)    INT8E
     * bits 12-8 - (0x00) INT8SEL
     * bit  7    - (x)    Reserved
     * bit  6    - (0)    INT7CONT = Interrupt must be cleared
     * bit  5    - (0)    INT7E
     * bits 4-0  - (0x00) INT7SEL
     */
    *INTSEL7N8 = 0x0000U;

    /* INTSEL9N10
     * bit  15-7 - (x)    Reserved
     * bit  6    - (0)    INT9CONT = Interrupt must be cleared
     * bit  5    - (0)    INT9E
     * bits 4-0  - (0x00) INT9SEL
     */
    *INTSEL9N10 = 0x0000U;

    /* SOCPRICTL
     * bits 15-11 - (x)    Reserved
     * bits 10-5  - (x)    RRPOINTER
     * bits 4-0   - (0x10) SOCPRIORITY = All SOC0 have priority
     */
    *SOCPRICTL = 0x0010U;

    /* ADCSAMPLEMODE
     * bits 15-8 - (x) Reserved
     * bit  7    - (0) SIMULEN14 = Single
     * bit  6    - (0) SIMULEN12 = Single
     * bit  5    - (1) SIMULEN10 = Single
     * bit  4    - (1) SIMULEN8  = Single
     * bit  3    - (1) SIMULEN6  = Simultaneous
     * bit  2    - (1) SIMULEN4  = Simultaneous
     * bit  1    - (0) SIMULEN2  = Single
     * bit  0    - (1) SIMULEN0  = Simultaneous
     */
    *ADCSAMPLEMODE = 0x003dU;

    /* ADCINTSOCSEL1
     * bits 15-14 - (00) SOC7 = No ADCINT Trigger
     * bits 13-12 - (00) SOC6 = No ADCINT Trigger
     * bits 11-10 - (00) SOC5 = No ADCINT Trigger
     * bits 9-8   - (00) SOC4 = No ADCINT Trigger
     * bits 7-6   - (00) SOC3 = No ADCINT Trigger
     * bits 5-4   - (00) SOC2 = No ADCINT Trigger
     * bits 3-2   - (00) SOC1 = No ADCINT Trigger
     * bits 1-0   - (00) SOC0 = No ADCINT Trigger
     */
    *ADCINTSOCSEL1 = 0x0000U;

    /* ADCINTSOCSEL2
     * bits 15-14 - (00) SOC15 = No ADCINT Trigger
     * bits 13-12 - (00) SOC14 = No ADCINT Trigger
     * bits 11-10 - (00) SOC13 = No ADCINT Trigger
     * bits 9-8   - (00) SOC12 = No ADCINT Trigger
     * bits 7-6   - (00) SOC11 = No ADCINT Trigger
     * bits 5-4   - (00) SOC10 = No ADCINT Trigger
     * bits 3-2   - (00) SOC9  = No ADCINT Trigger
     * bits 1-0   - (00) SOC8  = No ADCINT Trigger
     */
    *ADCINTSOCSEL2 = 0x0000U;

    /* ADCSOC0CTL
     * bits 15-11 - (00101)  TRIGSEL = EPWM1-ADCSOCA
     * bit  10    - (x)      Reserved
     * bits 9-6   - (0001)   CHSEL = ADCINA1(Phase A Simultaneous)
     * bits 5-0   - (000110) ACQPS = 7 cycles
     */
    *ADCSOC0CTL = 0x2846U;

    /* ADCSOC1CTL
     * bits 15-11 - (00101)  TRIGSEL = EPWM1-ADCSOCA
     * bit  10    - (x)      Reserved
     * bits 9-6   - (0001)   CHSEL = ADCINB1(Phase B Simultaneous)
     * bits 5-0   - (000110) ACQPS = 7 cycles
     */
    *ADCSOC1CTL = 0x2846U;

    /* ADCSOC2CTL
     * bits 15-11 - (00101)  TRIGSEL = EPWM1-ADCSOCA
     * bit  10    - (x)      Reserved
     * bits 9-6   - (0010)   CHSEL = ADCINA2(Phase C)
     * bits 5-0   - (000110) ACQPS = 7 cycles
     */
    *ADCSOC2CTL = 0x2886U;

    /* ADCSOC3CTL
     * bits 15-11 - (00101)  TRIGSEL = EPWM-ADCSOCA
     * bit  10    - (x)      Reserved
     * bits 9-6   - (0011)   CHSEL = ADCINA3(Battery On Time)
     * bits 5-0   - (000110) ACQPS = 7 cycles
     */
    /* Unused */
    *ADCSOC3CTL = 0x28c6U;

    /* ADCSOC4CTL
     * bits 15-11 - (00111)  TRIGSEL = EPWM2-ADCSOCA
     * bit  10    - (x)      Reserved
     * bits 9-6   - (0100)   CHSEL = ADCINA4(T1 Simultaneous)
     * bits 5-0   - (000110) ACQPS = 7 cycles
     */
    *ADCSOC4CTL = 0x3906U;

    /* ADCSOC5CTL
     * bits 15-11 - (00111)  TRIGSEL = EPWM2-ADCSOCA
     * bit  10    - (x)      Reserved
     * bits 9-6   - (0100)   CHSEL = ADCINB4(T2 Simultaneous)
     * bits 5-0   - (000110) ACQPS = 7 cycles
     */
    *ADCSOC5CTL = 0x3906U;

    /* ADCSOC6CTL
     * bits 15-11 - (00111)  TRIGSEL = EPWM2-ADCSOCA
     * bit  10    - (x)      Reserved
     * bits 9-6   - (0001)   CHSEL = ADCINA1(Phase A Simultaneous)
     * bits 5-0   - (000110) ACQPS = 7 cycles
     */
    *ADCSOC6CTL = 0x3846U;

    /* ADCSOC7CTL
     * bits 15-11 - (00111)  TRIGSEL = EPWM2-ADCSOCA
     * bit  10    - (x)      Reserved
     * bits 9-6   - (0001)   CHSEL = ADCINB1(Phase B Simultaneous)
     * bits 5-0   - (000110) ACQPS = 7 cycles
     */
    *ADCSOC7CTL = 0x3846U;

    /* ADCSOC8CTL
     * bits 15-11 - (00111)  TRIGSEL = EPWM2-ADCSOCA
     * bit  10    - (x)      Reserved
     * bits 9-6   - (0010)   CHSEL = ADCINA2(Phase C Simultaneous)
     * bits 5-0   - (000110) ACQPS = 7 cycles
     */
    *ADCSOC8CTL = 0x3886U;

    /* ADCSOC9CTL
     * bits 15-11 - (00111)  TRIGSEL = EPWM2-ADCSOCA
     * bit  10    - (x)      Reserved
     * bits 9-6   - (0010)   CHSEL = ADCINB2(P2 Simultaneous)
     * bits 5-0   - (000110) ACQPS = 7 cycles
     */
    *ADCSOC9CTL = 0x3886U;

    /* ADCSOC10CTL
     * bits 15-11 - (00111)  TRIGSEL = EPWM2-ADCSOCA
     * bit  10    - (x)      Reserved
     * bits 9-6   - (0110)   CHSEL = ADCINA6(DTEMP Simultaneous)
     * bits 5-0   - (000110) ACQPS = 7 cycles
     */
    *ADCSOC10CTL = 0x3986U;

    /* ADCSOC11CTL
     * bits 15-11 - (00111)  TRIGSEL = EPWM2-ADCSOCA
     * bit  10    - (x)      Reserved
     * bits 9-6   - (0110)   CHSEL = ADCINB6(Ignition Simultaneous)
     * bits 5-0   - (000110) ACQPS = 7 cycles
     */
    *ADCSOC11CTL = 0x3986U;

    /* ADCSOC12CTL
     * bits 15-11 - (00111)  TRIGSEL = EPWM2-ADCSOCA
     * bit  10    - (x)      Reserved
     * bits 9-6   - (1000)   CHSEL = ADCINB0(P1)
     * bits 5-0   - (000110) ACQPS = 7 cycles
     */
    /* Unused */
    *ADCSOC12CTL = 0x3a06U;

    /* ADCSOC13CTL
     * bits 15-11 - (00111)  TRIGSEL = EPWM2-ADCSOCA
     * bit  10    - (x)      Reserved
     * bits 9-6   - (0111)   CHSEL = ADCINA7(P3)
     * bits 5-0   - (000110) ACQPS = 7 cycles
     */
    /* Unused */
    *ADCSOC13CTL = 0x39c6U;

    /* ADCSOC14CTL
     * bits 15-11 - (00111)  TRIGSEL = EPWM2-ADCSOCA
     * bit  10    - (x)      Reserved
     * bits 9-6   - (1011)   CHSEL = ADCINB3(VREF2)
     * bits 5-0   - (000110) ACQPS = 7 cycles
     */
    /* Unused */
    *ADCSOC14CTL = 0x3ac6U;

    /* ADCSOC15CTL
     * bits 15-11 - (00000)  TRIGSEL =
     * bit  10    - (x)      Reserved
     * bits 9-6   - (0000)   CHSEL =
     * bits 5-0   - (000000) ACQPS =
     */
    /* Unused */
    *ADCSOC15CTL = 0x0000U;

    /* Restore Eallow security */
    PICCOLO_EDIS();

    for (x = 0U; x < NMR_ADC_CHANNELS; x++) {
        adc[x].raw = 0U;
        adc[x].filt = 0U;
    }
}

/*!
 * \fn    void adcINT1(void)
 *
 * \brief This function gathers the data from sequence 1 of the ADC,
 *        stores it, and cleans up the interrupt.
 */
void
adcINT1(void) {
    static UINT_8 first = 1U;

    adc[ADC_ON_A].raw = (UINT_16)adcOnA_cla & 0x0fffU;
    adc[ADC_ON_B].raw = (UINT_16)adcOnB_cla & 0x0fffU;
    adc[ADC_ON_C].raw = (UINT_16)adcOnC_cla & 0x0fffU;
    adc[ADC_BATT_ON].raw = (UINT_16)adcBattOn_cla & 0x0fffU;
    adc[ADC_ON_A].filt = adc[ADC_ON_A].raw;
    adc[ADC_ON_B].filt = adc[ADC_ON_B].raw;
    adc[ADC_ON_C].filt = adc[ADC_ON_C].raw;
    adc[ADC_BATT_ON].filt = adcWeightAvg(ADC_BATT_ON, 8U, 8U);
    if (1U == first) {
        adcClearMaxMin();
        first = 0U;
    }
    adcCheckMaxMin();
    /* Clear the interrupt */
    *ADCINTFLGCLR |= 0x0001U;
}

/*!
 * \fn    void adcINT2(void)
 *
 * \brief This function gathers the data from sequence 2 of the ADC,
 *        stores it, and cleans up the interrupt.
 */
void
adcINT2(void) {
    adc[ADC_T1].raw = (UINT_16)adcT1_cla & 0x0fffU;
    adc[ADC_T2].raw = (UINT_16)adcT2_cla & 0x0fffU;
    adc[ADC_OFF_A].raw = (UINT_16)adcOffA_cla & 0x0fffU;
    adc[ADC_OFF_B].raw = (UINT_16)adcOffB_cla & 0x0fffU;
    adc[ADC_OFF_C].raw = (UINT_16)adcOffC_cla & 0x0fffU;
    adc[ADC_P2].raw = (UINT_16)adcP2_cla & 0x0fffU;
    adc[ADC_DTEMP].raw = (UINT_16)adcDTemp_cla & 0x0fffU;
    adc[ADC_IGN].raw = (UINT_16)adcIgn_cla & 0x0fffU;
    adc[ADC_P1].raw = (UINT_16)adcP1_cla & 0x0fffU;
    adc[ADC_P3].raw = (UINT_16)adcP3_cla & 0x0fffU;
    adc[ADC_VREF2].raw = (UINT_16)adcVREF2_cla & 0x0fffU;
    adc[ADC_T1].filt = adcWindowAvg(adc[ADC_T1].raw, &adcWinT1);
    adc[ADC_T2].filt = adcWindowAvg(adc[ADC_T2].raw, &adcWinT2);
    adc[ADC_OFF_A].filt = adc[ADC_OFF_A].raw;
    adc[ADC_OFF_B].filt = adc[ADC_OFF_B].raw;
    adc[ADC_OFF_C].filt = adc[ADC_OFF_C].raw;
    adc[ADC_P2].filt = adc[ADC_P2].raw;
    adc[ADC_DTEMP].filt = adcWeightAvg(ADC_DTEMP, 12U, 4U);
    adc[ADC_IGN].filt = adcWeightAvg(ADC_IGN, 8U, 8U);
    adc[ADC_P1].filt = adc[ADC_P1].raw;
    adc[ADC_P3].filt = adc[ADC_P3].raw;
    adc[ADC_VREF2].filt = adc[ADC_VREF2].raw;

    /* Clear the interrupt */
    *ADCINTFLGCLR |= 0x0002U;
}

/*!
 * \fn     UINT_16 adcGetRaw(UINT_16 ch)
 *
 * \param  ch - channel to fetch.
 * \return latest raw adc conversion of the supplied channel.
 * \brief  This function will fetch and return the latest adc
 *         conversion of the passed channel. If the channel is
 *         not supported an invalid 0xffff is returned.
 */
UINT_16
adcGetRaw(UINT_16 ch) {
    UINT_16 ret = 0xffffU;

    if (NMR_ADC_CHANNELS > ch) {
        ret = adc[ch].raw;
    }
    return ret;
}

/*!
 * \fn     UINT_16 adcGetFilt(UINT_16 ch)
 *
 * \param  ch - channel to fetch.
 * \return latest filtered adc conversion of the supplied channel.
 * \brief  This function will fetch and return the latest adc
 *         conversion of the passed channel. If the channel is
 *         not supported an invalid 0xffff is returned.
 */
UINT_16
adcGetFilt(UINT_16 ch) {
    UINT_16 ret = 0xffffU;

    if (NMR_ADC_CHANNELS > ch) {
        ret = adc[ch].filt;
    }
    return ret;
}

void
adcClearMaxMin(void) {
    phaseMin[0] = adc[ADC_ON_A].raw;
    phaseMin[1] = adc[ADC_ON_B].raw;
    phaseMin[2] = adc[ADC_ON_C].raw;
    phaseMax[0] = adc[ADC_ON_A].raw;
    phaseMax[1] = adc[ADC_ON_B].raw;
    phaseMax[2] = adc[ADC_ON_C].raw;
}

void
adcGetMax(UINT_16 *data) {
    data[0] = phaseMax[0];
    data[1] = phaseMax[1];
    data[2] = phaseMax[2];
}

void
adcGetMin(UINT_16 *data) {
    data[0] = phaseMin[0];
    data[1] = phaseMin[1];
    data[2] = phaseMin[2];
}

/*!
 * \fn     UINT_16 adcWindowAvg(UINT_16 newVal, adcWindowAvg_t *avg)
 *
 * \param  newVal - latest adc conversion.
 * \param  *avg - window average structure holding calculation data.
 * \return filtered adc counts.
 * \brief  This function will perform a ADC_AVG_POINTS point window
 *         filter on the adc counts of a single channel.
 */
static UINT_16
adcWindowAvg(UINT_16 newVal, adcWindowAvg_t *avg) {
    UINT_16 x, idx;

    avg->old -= avg->avg[0];
    for (x = 0U; x < (ADC_AVG_POINTS - 1U); x++) {
        idx = x + 1U;
        avg->avg[x] = avg->avg[idx];
    }
    avg->avg[ADC_AVG_POINTS-1U] = newVal;
    avg->old += newVal;
    return avg->old >> ADC_AVG_SHIFT;
}

/*!
 * \fn     UINT_16 adcWeightAvg(UINT_16 ch, UINT_16 cfOld, UINT_16 cfNew)
 *
 * \param  ch - channel to filter.
 * \param  cfOld - weighting of the old value.
 * \param  cfNew - weighting if the new value.
 * \return filtered adc counts.
 * \brief  This function will perform a weighted filtering operation on
 *         the adc counts of the passed channel using the passed
 *         co-efficients. (Note: co-efficients must sum to 16)
 */
static UINT_16
adcWeightAvg(UINT_16 ch, UINT_16 cfOld, UINT_16 cfNew) {
    return (((adc[ch].filt * cfOld) + (adc[ch].raw * cfNew)) >> 4);
}

static void
adcCheckMaxMin(void) {
    if (phaseMax[0] < adc[ADC_ON_A].raw) {
        phaseMax[0] = adc[ADC_ON_A].raw;
    }
    if (phaseMax[1] < adc[ADC_ON_B].raw) {
        phaseMax[1] = adc[ADC_ON_B].raw;
    }
    if (phaseMax[2] < adc[ADC_ON_C].raw) {
        phaseMax[2] = adc[ADC_ON_C].raw;
    }
    if (phaseMin[0] > adc[ADC_ON_A].raw) {
        phaseMin[0] = adc[ADC_ON_A].raw;
    }
    if (phaseMin[1] > adc[ADC_ON_B].raw) {
        phaseMin[1] = adc[ADC_ON_B].raw;
    }
    if (phaseMin[2] > adc[ADC_ON_C].raw) {
        phaseMin[2] = adc[ADC_ON_C].raw;
    }
}
