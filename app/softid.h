/*!
 * \file      ecuid.h
 * \author    Zach Haver
 * \author    Devin Kloos
 * \brief     Software Identifier string definition module
 *
 */

/* $Id: $ */
/* $Name: $ */

/* NOTICE: This file is the property of Globe Motors and contains */
/* CONFIDENTIAL information.  Any use, copying or dissemination of the */
/* information contained in this file is strictly prohibited unless */
/* authorized by Globe Motors in writing. */

/* Copyright 2016, Globe Motors, Inc. */

/*
 * Revision History:
 *
 * $Log: $
 *
 */

#ifndef SOFTID_H
#define SOFTID_H

#include "misra.h"

#define NUM_FIELDS                       6U

#define SOFTID_MAX_LENGTH              256U

void softIdInit(void);
UINT_16 softIdLoadData(UINT_8 *data);

#endif /* SOFTID_H */
