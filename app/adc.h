/*!
 * \file      adc.h
 * \author    Zach Haver
 * \brief     ADC driver definitions for the TI TMS320C28035 piccolo.
 */

/* $Id: $ */
/* $Name: $ */

/* NOTICE: This file is the property of Globe Motors and contains */
/* CONFIDENTIAL information.  Any use, copying or dissemination of the */
/* information contained in this file is strictly prohibited unless */
/* authorized by Globe Motors in writing. */

/* Copyright 2016, Globe Motors, Inc. */

/*
 * Revision History:
 *
 * $Log: $
 *
 */

#ifndef ADC_H
#define ADC_H

#include "misra.h"
#include "piccolo.h"

/* ADC Channel Element Definitions */
#define ADC_ON_A               0U
#define ADC_ON_B               1U
#define ADC_ON_C               2U
#define ADC_BATT_ON            3U
#define ADC_T1                 4U
#define ADC_T2                 5U
#define ADC_OFF_A              6U
#define ADC_OFF_B              7U
#define ADC_OFF_C              8U
#define ADC_P2                 9U
#define ADC_DTEMP             10U
#define ADC_IGN               11U
#define ADC_P1                12U
#define ADC_P3                13U
#define ADC_VREF2             14U
#define NMR_ADC_CHANNELS      15U

/*! \struct adcData_t - structure to hold the adc conversion data. */
typedef struct {
    UINT_16 raw;  /*!< raw 12-bit ADC value */
    UINT_16 filt; /*!< filtered 12-bit ADC value */
}adcData_t;

/*! \def ADC_AVG_POINTS - number of points in the window average.
                          Note: this needs to be a power of 2. */
#define ADC_AVG_POINTS    8U
/*! \def ADC_AVG_SHIFT - shift divider for the window average. */
#define ADC_AVG_SHIFT     3
/*! \struct adcWindowAvg_t - structure to hold info on the window average. */
typedef struct {
    UINT_16 old;                 /*!< previous summation. */
    UINT_16 avg[ADC_AVG_POINTS]; /*!< array holding average points. */
}adcWindowAvg_t;
/*! \def ADC_WINAVG_DEF - default vaules for the window average structure. */
#define ADC_WINAVG_DEF {16384U, {2048U,2048U,2048U,2048U,2048U,2048U,2048U,2048U}}

/* public function prototypes */
void adcInit(void);
void adcINT1(void);
void adcINT2(void);
UINT_16 adcGetRaw(UINT_16 ch);
UINT_16 adcGetFilt(UINT_16 ch);
void adcClearMaxMin(void);
void adcGetMax(UINT_16 *data);
void adcGetMin(UINT_16 *data);

#endif /* ADC_H */
