/*!
 * \file      g7ManufacturingBK.c
 * \author    Zach Haver
 * \brief     Bootkernel main and initialization functions
 */

/* $Id: $ */
/* $Name: $ */

/* NOTICE: This file is the property of Globe Motors and contains */
/* CONFIDENTIAL information.  Any use, copying or dissemination of the */
/* information contained in this file is strictly prohibited unless */
/* authorized by Globe Motors in writing. */

/* Copyright 2016, Globe Motors, Inc. */

/*
 * Revision History:
 *
 * $Log: $
 */

#include <string.h> /* for memcpy() */

#include "boot.h"
#include "can.h"
#include "crc.h"
#include "dtc.h"
#include "eeprom.h"
#include "firstRun.h"
#include "fkernel.h"
#include "flash.h"
#include "gpio.h"
#include "i2c.h"
#include "misra.h"
#include "panic.h"
#include "piccolo.h"
#include "pie.h"
#include "sys.h"

/*!
 * /brief User RAM functions flash start address. Used for copying
 *         routines from flash to RAM.
 */
extern UINT_16 secureRamFuncsLoadStart;
/*! /brief User RAM functions flash end address. Used for copying
           routines from flash to RAM.
*/
extern UINT_16 secureRamFuncsLoadEnd;
/*! /brief User RAM functions ram start address. Used for copying
           routines from flash to RAM.
*/
extern UINT_16 secureRamFuncsRunStart;

/*!
 * \fn     void main(void)
 * \brief  This is the main function for the g6PolarisBK project.
 */
void
main(void) {
    UINT_8 bytes[32] = {0U};
    UINT_16 bootStatus = BOOT_NONE;
    UINT_16 bootCount;
    UINT_16 ignitionCount;
    UINT_16 word;
    UINT_32 dword;
    /*-----Begin Core Initializations-----*/

    /* Globally disable interrupts */
    PICCOLO_DINT();
    /* get the clocks going */
    sysInit();
     /* The section secureRamFuncs contains user defined CSM secured RAM
       routines that need to be copied from flash */
    /*lint -save -e946 -e947 -e9034 */
    memcpy(&secureRamFuncsRunStart,
           &secureRamFuncsLoadStart,
           (&secureRamFuncsLoadEnd - &secureRamFuncsLoadStart));
    /*lint -restore */
    /* Initialize the flash pipelining routines */
    flashInit();
    /* Initialize the GPIO pins for the G6 board. (Discrete unused) */
    gpioInit();
    /* Enable the desired interrupts in the PIE */
    pieInit();
    /* Initialize the I2C peripheral for accessing the EEPROM */
    i2cInit();
     /* Enable interrupt levels for TMR0, I2C, and CAN */
    IER |= BIT16_0 | BIT16_7 | BIT16_8;
    /* Enable global interrupts */
    PICCOLO_EINT();
    PICCOLO_ERTM();

    /*-----Begin Secondary Initiialization-----*/
    /* These peripheral initializations require the core items above.
       i.e. eeprom access */
    /* read baudrate from eeprom - default will be 250k */
    if (EE_ERR_NONE == eepromReadXBytes(EEADDR_BAUDRATE, bytes, 2U)) {
        word = (UINT_16)bytes[1];
        word <<= 8;
        word |= (UINT_16)bytes[0];
        if (500U == word) {
            canInit(CAN_CONF_BITRATE_500);
        } else {
            canInit(CAN_CONF_BITRATE_250);
        }
    } else {
        canInit(CAN_CONF_BITRATE_250);
    }

    if (EE_ERR_NONE == eepromReadXBytes(EEADDR_FIRSTRUN, bytes, 2U)) {
        if (0xffU == bytes[0]) {
            firstRun();
        }
    } else {
        dtcSet(DTCHW_LOAD_FAIL);
        fkernelRun(FK_EEPROM_FAIL, BOOT_RESET);
    }
    /*-----Begine Boot Sequence Checks-----*/
    /* get eeprom data */
    if (EE_ERR_NONE != eepromRead32Bytes(0U, bytes)) {
        fkernelRun(FK_EEPROM_FAIL, bootStatus);
    }

    /* check BOOT STATUS */
    bootStatus = (UINT_16)bytes[4];
    if (BOOT_REQUESTED == bootStatus) {
        (void)eepromWriteWord(EEADDR_BOOT_OPTIONS, BOOT_RESET);
        fkernelRun(FK_REQUESTED, bootStatus);
    }

    /* increment ignition count and boot count */
    ignitionCount = ((UINT_16)bytes[8] & 0xffU) | ((UINT_16)bytes[9] << 8);
    ignitionCount++;
    dtcSetCurrentIgnitionCount(ignitionCount);
    bootCount = ((UINT_16)bytes[6] & 0xffU) | ((UINT_16)bytes[7] << 8);
    bootCount++;
    bootCount %= BOOT_COUNT_LIMIT;
    dword = ((UINT_32)bootCount | ((UINT_32)ignitionCount << 16));
    /* write boot count  and ignition count back to eeprom */
    (void)eepromWriteDWord(EEADDR_BOOT_COUNT, dword);
    /* check BOOT_COUNT */
    if (BOOT_COUNT_THRESHOLD < bootCount) {
        fkernelRun(FK_BOOT_COUNT, bootStatus);
    }

    /* check APP crc */
//    if (CRC_INVALID == crcChkApp(&bytes[16])) {
//        fkernelRun(FK_CRC_APP, bootStatus);
//    }

    /*-----Checks Complete Jump To The Application-----*/

    /* Disbale interrupts before jump so the application gets a clean state. */
    PICCOLO_DINT();
    /* Branch to the application */
    BOOT_TO_APP;

    /* Extremely bad if we get this far, force reset. */
    PANIC();
    return;
}
