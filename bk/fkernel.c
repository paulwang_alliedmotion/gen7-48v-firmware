/*!
 * \file      fkernel.c
 * \author    Zach Haver
 * \brief     Flash kernel routines for the TMS320C28035.
 */

/* $Id: $ */
/* $Name: $ */

/* NOTICE: This file is the property of Globe Motors and contains */
/* CONFIDENTIAL information.  Any use, copying or dissemination of the */
/* information contained in this file is strictly prohibited unless */
/* authorized by Globe Motors in writing. */

/* Copyright 2016, Globe Motors, Inc. */

/*
 * Revision History:
 *
 * $Log: $
 *
 */

#include "boot.h"
#include "can.h"
#include "debugLED.h"
#include "dtc.h"
#include "eeprom.h"
#include "fkernel.h"
#include "Flash2803x_API_Library.h"
#include "j1939DataLink.h"
#include "j1939Diagnostic.h"
#include "misra.h"
#include "rand32.h"
#include "sched.h"
#include "softid.h"
#include "tmr0.h"

/* private function prototypes */

/* private variables */

/* public functions */

/*!
 * \fn      void fkernelRun (void)
 * \brief   This function starts the flash kernel by
 *          entering a loop that processes received messages.
 */
void
fkernelRun(FKReason_t reason, UINT_16 bootStatus) {
    /* set the cpu scale factor */
    /*lint -save -e912 -e524 -e923 -e9029 -e9034 */
    PICCOLO_EALLOW ();
    *FlashScalingVar = SCALE_FACTOR;
    *FlashCallbackVar = canFlashCallBack;
    /*lint -restore */
    PICCOLO_EDIS ();

    /* Initialize the DTC tables. */
    dtcInit();
    /* Create the soft ID. */
    softIdInit();
    /* Initialize the J1939 Data Link Layer for Communication. */
    jdlInit();
    /* Set up Timer0 to be used as our main timebase. */
    tmr0Init();
    /* Initialize the scheduler. */
    schedInit();
    /* Add periodic functoins to the schedule. */
    schedAddTask(debugLEDToggle, 5000U);
    schedAddTask(jdlTimer, 10U);
    schedAddTask(dtcControl, 100U);
    schedAddTask(randShuffle, 100U);

    if (BOOT_RESET == bootStatus) {
        jdiagDM15Reboot(JDIAG_DM15_FAIL_GENERAL);
    }

    /* handle reason for entering flash kernel */
    switch (reason) {
    case FK_REQUESTED:
        /* send DM15 for reset */
        jdiagDM15Reboot(JDIAG_DM15_COMPLETE);
        break;
    case FK_EEPROM_FAIL:
        /* fault wil already be set by the eeprom functions */
        jdlAddressClaim();
        break;
    case FK_BOOT_COUNT:
        dtcSet(DTCHW_BOOT_COUNT);
        jdlAddressClaim();
        break;
    case FK_CRC_APP:
        dtcSet(DTCHW_CRC_APP);
        jdlAddressClaim();
        break;
    default:
        /* Unknown Reason. Do Nothing */
        break;
    }

    for (;;) {
        schedExec();
    }
}
