/*!
 * \file    debugLED.h
 * \author  Zach Haver
 * \brief   debug LED definitions
 *
 */

/* $Id: $ */
/* $Name :$ */

/* NOTICE: This file is the property of Globe Motors and contains */
/* CONFIDENTIAL information.  Any use, copying or dissemination of the */
/* information contained in this file is strictly prohibited unless */
/* authorized by Globe Motors in writing. */

/* Copyright 2016, Globe Motors, Inc. */

/*
 * Revision History:
 *
 * $Log: $
 *
 */

#ifndef DEBUGLED_H
#define DEBUGLED_H

void debugLEDToggle(void);

#endif /* DEBUGLED_H */
