/*!
 * \file    tmr0.c
 * \author  Zach Haver
 * \brief   Timer 0 implementation for tick() scheduler timing function
 *
 */

/* $Id: $ */
/* $Name: $ */

/* NOTICE: This file is the property of Globe Motors and contains */
/* CONFIDENTIAL information.  Any use, copying or dissemination of the */
/* information contained in this file is strictly prohibited unless */
/* authorized by Globe Motors in writing. */

/* Copyright 2016, Globe Motors, Inc. */

/*
 * Revision History:
 *
 * $Log: $
 *
 */

#include "misra.h"
#include "piccolo.h"
#include "sched.h"
#include "tmr0.h"

/*!
 * \fn    void tmr0Init(void)
 *
 * \brief This function sets up our main timebase at 100us.
 */
void
tmr0Init(void) {
    /* Enable Eallow Register Access */
    PICCOLO_EALLOW();
    /* Turn on CPU Timer 0 clock */
    *PCLKCR3 |= 0x0100U;
    /* TIMER0CTR
     * bit  15    (1) TIF
     * bit  14    (1) TIE - Timer interrupt enabled
     * bits 13-12 (x) Reserved
     * bit  11    (1) FREE - free run
     * bit  10    (1) SOFT - free run
     * bits 9-6   (x) Reserved
     * bit  5     (0) TRB
     * bit  4     (1) TSS - timer stopped
     * bits 3-0   (x) Reserved
     */
    *TIMER0TCR = 0x4c10U;
    /* set the period of the timeout to 0.1ms
     * 6000counts/60MHz = 0.1ms */
    *TIMER0PRD = 6000U;
    *TIMER0TIM = 6000U;
    /* make sure the interrupt flag is cleared */
    *TIMER0TCR |= 0x8000U;
    /* start the timer */
    *TIMER0TCR &= ~0x0010U;
    /* Restore Eallow Register Security */
    PICCOLO_EDIS();
}

/*!
 * \fn    void tmr0ISR(void)
 *
 * \breif This function handles the 100us timebase execution.
 */
void
tmr0ISR(void) {
    /* reload the timer */
    *TIMER0TCR |= 0x0020U;
    /* clear the interrupt flag */
    *TIMER0TCR |= 0x8000U;
    schedTick();
}
