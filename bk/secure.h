/*!
 * \file        secure.h
 * \author      Zach Haver
 * \brief       Security algorithm constants
 */

/* $Id: $ */
/* $Name: $ */

/* NOTICE: This file is the property of Globe Motors and contains */
/* CONFIDENTIAL information.  Any use, copying or dissemination of the */
/* information contained in this file is strictly prohibited unless */
/* authorized by Globe Motors in writing. */

/* Copyright 2016, Globe Motors, Inc. */

/*
 * Revision history:
 *
 * $ Log: $
 */

#ifndef SECURE_H
#define SECURE_H

#include "misra.h"

/*! \def KNOWN_SALT - security algorithm modifier that is unique for and
 *                    provided to the customer.
 */
#define KNOWN_SALT         0xa56cdd32UL

UINT_32 secureSeed2Key(UINT_32 seed);

#endif /* SECURE_H */
