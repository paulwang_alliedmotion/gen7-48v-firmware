/*!
 * \file      j1939Application.h
 * \author    Zach Haver
 * \brief     Implementation of the J1939 application layer.
 *
 */

/* $Id: $ */
/* $Name: $ */

/* NOTICE: This file is the property of Globe Motors and contains */
/* CONFIDENTIAL information.  Any use, copying or dissemination of the */
/* information contained in this file is strictly prohibited unless */
/* authorized by Globe Motors in writing. */

/* Copyright 2016, Globe Motors, Inc. */

/*
 * Revision History:
 *
 * $Log: $
 *
 */

#ifndef JAPP_H
#define JAPP_H

#include "j1939Application.h"
#include "j1939DataLink.h"
#include "misra.h"

/*! \def JAPP_PGN_FEDA - Parameter Group Number for the SOFTID */
#define JAPP_PGN_FEDA        0xfedaU
/*! \def JAPP_PGN_FFF1 - Parameter Group Number for Polaris Flash Status */
#define JAPP_PGN_FFF1        0xfff1U

/* reflash status flags */
#define JAPP_BENABLE             0x0001U
#define JAPP_BBATT               0x0004U
#define JAPP_BNOTRUN             0x0010U
#define JAPP_BOPER               0x0040U
#define JAPP_B10SEC              0x0100U
#define JAPP_BMANCYC             0x0400U
#define JAPP_BLAZYCRC            0x1000U
#define JAPP_BSPARE              0xc000U

void jappFEDA(const J1939msg_t *pmsg);
void jappFFF1(const J1939msg_t *pmsg);
void jappSetReflashFlag(UINT_16 flag);
void jappClrReflashFlag(UINT_16 flag);

#endif /* JAPP_H */
