/*!
 * \file      flash.c
 * \author    Zach Haver
 * \brief     Flash initialization functions for the TMS320C28035 Piccolo.
 */

/* $Id: $ */
/* $Name: $ */

/* NOTICE: This file is the property of Globe Motors and contains */
/* CONFIDENTIAL information.  Any use, copying or dissemination of the */
/* information contained in this file is strictly prohibited unless */
/* authorized by Globe Motors in writing. */

/* Copyright 2016, Globe Motors, Inc. */

/*
 * Revision History:
 *
 * $Log: $
 *
 */

#include "flash.h"
#include "piccolo.h"

/* public functions */

/*!
 * \fn     void flashInit(void)
 *
 * \brief  Initialize the F28035 flash timing registers.
 *
 * Notes:
 *  1) This function MUST be executed out of RAM.  Executing it out of
 *     OTP/FLASH will produce unpredictable results.
 *  2) The flash registers are code security module protected.  Therefore,
 *     you must either run this function from L0/L1 RAM, or you must
 *     first unlock the CSM.  Note that unlocking the CSM as part of
 *     the program flow can compromise the code security.
 */
#pragma CODE_SECTION (flashInit, "secureRamFuncs")
void
flashInit(void) {
    /* Enable EALLOW protected register access */
    PICCOLO_EALLOW();
    /* Pump and bank set to active mode */
    *FPWR = 0x0003U;
    /* Clear the 3VSTAT bit */
    *FSTATUS |= 0x0100U;
    /* Sleep to standby: 511 transition cycles */
    *FSTDBYWAIT = 0x01ffU;
    /* Standby to active: 511 transition cycles */
    *FACTIVEWAIT = 0x01ffU;
    /* Random access waitstates = 1 */
    /* Paged access waitstates = 1 */
    *FBANKWAIT = 0x0101U;
    /* Random access waitstates = 1 */
    *FOTPWAIT = 0x0001U;
    /* Enable the flash pipeline */
    *FOPT = 0x0001U;
    /* Disable EALLOW protected register access */
    PICCOLO_EDIS();

    /* Force a complete pipeline flush to ensure that the write to the
     * last register configured occurs before returning.  Safest thing
     * is to wait 8 full cycles. */
    asm (" RPT #8 || NOP");
}
