/*!
 * \file      secure.c
 * \author    Zach Haver
 * \brief     security for memory access.
 */

/* $Id: $ */
/* $Name: $ */

/* NOTICE: This file is the property of Globe Motors and contains */
/* CONFIDENTIAL information.  Any use, copying or dissemination of the */
/* information contained in this file is strictly prohibited unless */
/* authorized by Globe Motors in writing. */

/* Copyright 2016, Globe Motors, Inc. */

/*
 * Revision History:
 *
 * $Log: $
 *
 */

#include "misra.h"
#include "secure.h"

/* public functions */

/*!
 * \fn     UINT_32 secureSeed2Key(UINT_32 seed)
 *
 * \param  seed - starting value for the security algorithm.
 * \return key value for comparision with client sent value.
 * \brief  This function will generate a key from the seed value
 *         passed to secure communications with the client which
 *         should generate the same key.
 */
UINT_32
secureSeed2Key(UINT_32 seed) {
    /*lint -save -e9027 -e9030 -e 9033 */
    UINT_32 key = ((seed >> 16) | (UINT_32)(!(seed & 0xffff0000UL)));
    /*lint -restore */
    UINT_32 iterations = (seed >> 12) & 0xfUL;
    UINT_32 salt = seed & 0xfffUL;
    UINT_32 x;

    /* Check iterations */
    if (0UL == iterations) {
        iterations = 0xaU;
    }

    /* Use customer modifier to add pepper to salt */
    salt = ((salt << 20) | salt) ^ KNOWN_SALT;

    /* Modify and rotate for iterations */
    for (x = 0UL; x < iterations; x++) {
        key ^= salt;

        /* Rotate key left */
        key = (key << 1) | (key >> 31);

        /* Rotate salt right */
        salt = (salt >> 1) | (salt << 31);
    }

    return key;
}
