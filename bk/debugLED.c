/*!
 * \file      debugLED.c
 * \author    Zach Haver
 * \brief     Handles control of the debug LED.
 */

/* $Id: $ */
/* $Name: $ */

/* NOTICE: This file is the property of Globe Motors and contains */
/* CONFIDENTIAL information.  Any use, copying or dissemination of the */
/* information contained in this file is strictly prohibited unless */
/* authorized by Globe Motors in writing. */

/* Copyright 2016, Globe Motors, Inc. */

/*
 * Revision History:
 *
 * $Log: $
 *
 */

#include "debugLED.h"
#include "piccolo.h"

/* public functions */

/*!
 * \fn    void debugLEDToggle(void)
 *
 * \brief This function toggles the output state of the debug LED.
 */
void
debugLEDToggle(void) {
    *GPBTOGGLE |= 0x00000004UL;
}
