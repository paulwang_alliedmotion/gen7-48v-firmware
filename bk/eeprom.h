/*!
 * \file    eeprom.h
 * \author  Zach Haver
 * \brief   I2C EEPROM driver definitions
 *
 */

/* $Id: $ */
/* $Name: $ */

/* NOTICE: This file is the property of Globe Motors and contains */
/* CONFIDENTIAL information.  Any use, copying or dissemination of the */
/* information contained in this file is strictly prohibited unless */
/* authorized by Globe Motors in writing. */

/* Copyright 2016, Globe Motors, Inc. */

/*
 * Revision History:
 *
 * $Log: $
 *
 */

#ifndef EEPROM_H
#define EEPROM_H

#include "misra.h"
#include "piccolo.h"

/* eeprom memory map definitions */
#define EEADDR_BOOT_OPTIONS          0x0004U
#define EEADDR_BOOT_COUNT            0x0006U
#define EEADDR_IGN_CNT               0x0008U

#define EEADDR_APP_CRC_L             0x0010U
#define EEADDR_APP_CRC_H             0x0012U
#define EEADDR_BK_CRC_L              0x0014U
#define EEADDR_BK_CRC_H              0x0016U

/* Manuf CRC is over the next 32 adddresses 0x20 - 0x40 */
#define EEADDR_KT                    0x0020U
#define EEADDR_I_LIM                 0x0024U
#define EEADDR_T_GAIN                0x0028U
#define EEADDR_T_OFFSET              0x002cU
#define EEADDR_ENCO_OFFSET           0x0030U

#define EEADDR_GLB_OCC               0x0040U
#define EEADDR_DTC_OCC_START         0x0060U
#define EEADDR_DTC_IGN_CNT_FIRST     0x0080U
#define EEADDR_DTC_IGN_CNT_LAST      0x00c0U
#define EEADDR_TORQUE_BIAS           0x0102U
#define EEADDR_MAX_STEMP             0x0106U
#define EEADDR_MAX_VTEMP             0x0108U

#define EEADDR_BOARD_PN              0x0200U
#define EEADDR_SN                    0x020aU

#define EEADDR_FIRSTRUN              0x0600U

#define EEADDR_BAUDRATE              0x0700U

#define EEADDR_MANUF_CRC_L           0x07fcU
#define EEADDR_MANUF_CRC_H           0x07feU

/*! \def EEPROM_WORD_MAX - maximum address for word access. */
#define EEPROM_WORD_MAX              2046U
/*! \def EEPROM_PAGE_MAX - maximum address for page access. */
#define EEPROM_PAGE_MAX              2016U

/*! \enum  EEAccess_t
 *  \brief Access lengths for the EEPROM.
 */
typedef enum {
    EE_WORD_ACCESS, /*!< EE_WORD_ACCESS - used for word length access. */
    EE_PAGE_ACCESS  /*!< EE_PAGE_ACCESS - used for page length access. */
} EEAccess_t;

/*! \enum  EEErr_t
 *  \brief return values for eeprom access.
 */
typedef enum {
    EE_ERR_NONE,         /*!< EE_ERR_NONE - no errors. */
    EE_ERR_TIMEOUT,      /*!< EE_ERR_TIMEOUT - timeout while accessing. */
    EE_ERR_ADDR_ILLEGAL, /*!< EE_ERR_ADDR_ILLEGAL - illegal address. */
    EE_ERR_I2C_FAIL      /*!< EE_ERR_I2C_FAIL - i2c communication failure. */
} EEErr_t;

/* function declarations */
EEErr_t eepromReadXBytes(UINT_16 addr, UINT_8 *data, UINT_16 len);
EEErr_t eepromRead32Bytes(UINT_16 addr, UINT_8 *data);
EEErr_t eepromRead32Words(UINT_16 addr, UINT_16 *data);
EEErr_t eepromWriteWord(UINT_16 addr, UINT_16 data);
EEErr_t eepromWriteDWord(UINT_16 addr, UINT_32 data);
EEErr_t eepromWrite16Bytes(UINT_16 addr, const UINT_8 *data);

#endif /* EEPROM_H */
