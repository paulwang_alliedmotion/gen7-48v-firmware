/*!
 * \file      isr.h
 * \author    Zach Haver
 * \brief     ISR definitions for the TI TMS320C28035 Piccolo.
 */

/* $Id: $ */
/* $Name: $ */

/* NOTICE: This file is the property of Globe Motors and contains */
/* CONFIDENTIAL information.  Any use, copying or dissemination of the */
/* information contained in this file is strictly prohibited unless */
/* authorized by Globe Motors in writing. */

/* Copyright 2016, Globe Motors, Inc. */

/*
 * Revision History:
 *
 * $Log: $
 *
 */

#ifndef ISR_H
#define ISR_H

/*! \def ISR_VECTOR_MASK -  Mask to obtain PIEVECT from PIECTRL */
#define ISR_VECTOR_MASK  0xfffeU

/* Interrupt Group Acknowledge Masks */
/*! \def ISR_ACK_GROUP1 - group 1 acknowledge mask. */
#define ISR_ACK_GROUP1   0x0001U
/*! \def ISR_ACK_GROUP8 - group 8 acknowledge mask. */
#define ISR_ACK_GROUP8   0x0080U
/*! \def ISR_ACK_GROUP9 - group 9 acknowledge mask. */
#define ISR_ACK_GROUP9   0x0100U

/* Interrupt Group 1 */
/*! \def ISR_TINT0 - refereance address for Timer0 interrupt. */
#define ISR_TINT0        0x0d4cU

/* Interrupt Group 8 */
/*! \def ISR_I2C_1A - reference address for I2C interrupt. */
#define ISR_I2C_1A       0x0db0U

/* Interrupt Group 9 */
/*! \def ISR_ECAN1INTA - reference address for the ECAN1 interrupt. */
#define ISR_ECAN1INTA    0x0dcaU

void isrVectorInit(void);

#endif /* ISR_H */
