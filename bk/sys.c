/*!
 * \file      sys.c
 * \author    Zach Haver
 * \brief     System initialization functions for the TI TMS320C28035.
 */

/* $Id: $ */
/* $Name: $ */

/* NOTICE: This file is the property of Globe Motors and contains */
/* CONFIDENTIAL information.  Any use, copying or dissemination of the */
/* information contained in this file is strictly prohibited unless */
/* authorized by Globe Motors in writing. */

/* Copyright 2016, Globe Motors, Inc. */

/*
 * Revision History:
 *
 * $Log: $
 *
 */

#include "piccolo.h"
#include "misra.h"
#include "sys.h"

/* public functions */

/*!
 * \fn     void sysInit (void)
 * \brief  Initialize the F28035 CPU
 *
 */
void
sysInit(void) {
    volatile UINT_16 dummy;

    /* Enable EALLOW protected register access */
    PICCOLO_EALLOW();

    /* Unlock the Code Security Module if CSM not in use */
    /* Unlocking the CSM will allow code running from non-secure
       memory to access code and data in secure memory.  One would
       only want to unsecure the CSM if code security were not
       desired, and therefore the CSM is not in use (otherwise,
       unlocking the CSM will compromise the security of user code).
       If the CSM is not in use, the best thing to do is leave the
       password locations programmed to 0xFFFF, which is the flash
       ERASED state.  When all passwords are 0xFFFF, all that is
       required to unlock the CSM are dummy reads of the PWL
       locations. */
    /* Dummy read of PWL locations */
    /*lint -save -e923 -e838 */
    dummy = *PASSWD0;
    dummy = *PASSWD1;
    dummy = *PASSWD2;
    dummy = *PASSWD3;
    dummy = *PASSWD4;
    dummy = *PASSWD5;
    dummy = *PASSWD6;
    dummy = *PASSWD7;
    /*lint -restore */

    /* usage of dummy for lint */
    if (0U == dummy) {
        ;
    }

    /* Disable the Watchdog Timer */
    *WDCR = 0x0068U;
    /*
      bit 15-8 0's: reserved
      bit 7    1:   WDFLAG, write 1 to clear
      bit 6    1:   WDDIS, 1=disable WD, 0=enable WD
      bit 5-3  101: WDCHK, WD check bits, always write as 101b
      bit 2-0  000: WDPS, WD prescale bits, 000: WDCLK=OSCCLK/512/1
    */

    /* System and Control Register */
    *SCSR = 0x0000U;
    /*
      bit 15-3 0's: reserved
      bit 2    0:   WDINTS, WD interrupt status bit (read-only)
      bit 1    0:   WDENINT, 0=WD causes reset, 1=WD causes WDINT
      bit 0    0:   WDOVERRIDE, write 1 to disable disabling of the WD
    */

    /* CLKCTL
     * bit  15  - (0)   NMIRESETSEL     = No NMI Reset Delay
     * bit  14  - (0)   XTALOSCOFF      = Crystal Oscillator On
     * bit  13  - (1)   XCLKINOFF       = XCLKIN Off
     * bit  12  - (0)   WDHALTI         = Accept Halt Command
     * bit  11  - (0)   INTOSC2HALTI    = Accept Halt Command
     * bit  10  - (0)   INTOSC2OFF      = Internal Oscillator2 On
     * bit  9   - (0)   INTOSC1HALTI    = Accept Halt Command
     * bit  8   - (0)   INTOSC1OFF      = Internal Oscillator1 On
     * bits 7-5 - (000) TMR2CLKPRESCALE = No Prescaling
     * bits 4-3 - (00)  TMR2CLKSRCSEL   = External Oscillator
     * bit  2   - (0)   WDCLKSRCSEL     = External Oscillator
     * bit  1   - (0)   OSCCLKSRC2SEL   = External Oscillator
     * bit  0   - (1)   OSCCLKSRCSEL    = External Oscillator
     */
    *CLKCTL |= 0x2001U;

    /* Configure the PLL */
    /* Check PLLSTS.MCLKSTS: make sure the PLL is not running in limp
     * mode */
    if (0U == (*PLLSTS & 0x0008U)) {
        /* PLL is not running in limp mode.  Good. */
        /* Set PLLSTS.CLKINDIV to 0 */
        *PLLSTS &= ~0x0180U;
        /* Set PLLSTS.MCLKOFF to 1: turn off missing clock detect
         * before setting PLLCR. */
        *PLLSTS |= 0x0040U;
        /* Set PLLCR for PLLx12/4 */
        *PLLCR = 0x000cU;

        /*
         * Wait for PLL to lock (Optional).  During this time the CPU
         * will run at OSCCLK/2 until the PLL is stable.  Once the PLL
         * is stable the CPU will automatically switch to the new
         * PLL value.  Code is not required to sit and wait for the
         * PLL to lock.  However, if the code does anything that is
         * timing critical (e.g. something that relies on the CPU
         * clock frequency to be at speed), then it is best to wait
         * until PLL lock is complete.  The watchdog should be
         * disabled before this loop (e.g., as was done above), or
         * fed within the loop.
         */

        /* Wait for PLLSTS.PLLLOCKS bit to set */
        while (0U == (*PLLSTS & 0x0001U)) {
            /* Don't like this infinite wait, should add a limit. */
            ;
        }

        /* Set PLLSTS.MCLKOFF to 0: enable missing clock detect
         * circuitry. */
        *PLLSTS &= ~0x0040U;
        /* Set DIVSEL to 1 to acheive 60Mhz */
        *PLLSTS |= 0x0080U;
    } else {
        /*
         * PLL is running in limp mode.  User should replace the below
         * with a call to an appropriate function.  For example,
         * shutdown the system (since something is _very_ wrong!).
         */
        ;
        asm(" ESTOP0");
    }

    /* Lo-speed peripheral clock prescaler, LOSPCLK = SYSCLKOUT/4 */
    *LOSPCP = 0x0002U;

    /* Make sure that all module clocks are disabled.  The individual
     * device driver init functions can turn them on as needed. */
    *PCLKCR0 = 0x0000U;
    *PCLKCR1 = 0x0000U;
    *PCLKCR3 = 0x0000U;

    /* Configure the low-power modes */
    /* LPMCR0 set to default value */
    *LPMCR0 = 0x0004U;

    /* XCLKOUT = OFF */
    *XCLK = 0x0003U;

    /* Disable EALLOW protected register access */
    PICCOLO_EDIS();
}

void
sysReset (void) {
    PICCOLO_EALLOW();
    /* Write an illegal value to the WDCR to force a reset */
    *WDCR = 0U;
    /*lint -save -e716 */
    for (;;) {
    /*lint -restore */
        /* just in case a reset doesn't occur trip the watchdog */
        ;
    }
}
