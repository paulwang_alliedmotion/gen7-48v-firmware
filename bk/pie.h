/*!
 * \file        pie.h
 * \author      Zach Haver
 * \brief       Periperal Interrupt Expansion controller initialization
 *              definitions for the TI TMS320C28035 Piccolo.
 */

/* $Id: $ */
/* $Name: $ */

/* NOTICE: This file is the property of Globe Motors and contains */
/* CONFIDENTIAL information.  Any use, copying or dissemination of the */
/* information contained in this file is strictly prohibited unless */
/* authorized by Globe Motors in writing. */

/* Copyright 2016, Globe Motors, Inc. */

/*
 * Revision History:
 *
 * $Log: $
 *
 */

#ifndef PIE_H
#define PIE_H

#include "piccolo.h"

/* public function prototypes */
void pieInit(void);

#endif /* PIE_H */
