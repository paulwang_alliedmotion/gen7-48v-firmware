/*!
 * \file        gpio.h
 * \author      Zach Haver
 * \brief       I/O initialization definitions for the TI TMS320C28033/35.
 */

/* $Id: $ */
/* $Name: $ */

/* NOTICE: This file is the property of Globe Motors and contains */
/* CONFIDENTIAL information.  Any use, copying or dissemination of the */
/* information contained in this file is strictly prohibited unless */
/* authorized by Globe Motors in writing. */

/* Copyright 2016, Globe Motors, Inc. */

/*
 * Revision History:
 *
 * $Log: $
 *
 */

#ifndef GPIO_H
#define GPIO_H

/* public function prototypes */
void gpioInit(void);

#endif /* GPIO_H */
