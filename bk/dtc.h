/*!
 * \file     dtc.h
 * \author   Zach Haver
 * \breif    DTC handling definitions
 */

/* $Id: $ */
/* $Name: $ */

/* NOTICE: This file is the property of Globe Motors and contains */
/* CONFIDENTIAL information.  Any use, copying or dissemination of the */
/* information contained in this file is strictly prohibited unless */
/* authorized by Globe Motors in writing. */

/* Copyright 2016, Globe Motors, Inc. */

/*
 * Revision History:
 *
 * $Log: $
 *
 */

#ifndef DTC_H
#define DTC_H

#include "misra.h"

/* DTC Structure Array Elements */
#define DTCHW_LOAD_FAIL                      0U
#define DTCHW_CRC_APP                        1U
#define DTCHW_BOOT_COUNT                     2U
#define DTCHW_I_EXCESS                       3U
#define DTCHW_T_RANGE                        4U
#define DTCHW_T_LINE                         5U
#define DTCHW_ENCODER_SPI                    6U
#define DTCHW_ENCODER_VAR                    7U
#define DTCHW_ENCODER_BITS                   8U
#define DTCHW_TEMPERATURE_HIGH               9U
#define DTCHW_TEMPERATURE_OVER              10U
#define DTCHW_CRC_MANUF                     11U
#define DTCHW_DRV_ERROR                     12U
#define DTCHW_BATT_LOW                      13U
#define DTCHW_BATT_HIGH                     14U
#define DTCHW_EEPROM                        15U

#define DTCSW_CONFIG_CAL                    16U
#define DTCSW_UNUSED15                      17U
#define DTCSW_UNUSED14                      18U
#define DTCSW_UNUSED13                      19U
#define DTCSW_UNUSED12                      20U
#define DTCSW_UNUSED11                      21U
#define DTCSW_UNUSED10                      22U
#define DTCSW_UNUSED9                       23U
#define DTCSW_UNUSED8                       24U
#define DTCSW_UNUSED7                       25U
#define DTCSW_UNUSED6                       26U
#define DTCSW_UNUSED5                       27U
#define DTCSW_UNUSED4                       28U
#define DTCSW_UNUSED3                       29U
#define DTCSW_UNUSED2                       30U
#define DTCSW_UNUSED1                       31U

#define DTC_COUNT                           32U

/*! \enum  DTC control states.
 *  \brief States to determine the need to update the EEPROM.
 */
typedef enum {
    DTC_CLEAN, /*!< DTC_CLEAN EEPROM doesn't need to be updated. */
    DTC_DIRTY  /*!< DTC_DIRTY EEPROM needs to be updated. */
} DTCControlState_t;

/*! \enum  DTC activity states.
 *  \brief States for fault reporting.
 */
typedef enum {
    DTC_INACTIVE, /*!< DTC_INACTIVE state for a cleared DTC. */
    DTC_ACTIVE /*!< DTC_ACTIVE state for a set DTC. */
} DTCActivityState_t;

void dtcInit(void);
void dtcControl(void);
void dtcSet(UINT_16 dtcN);
UINT_16 dtcLoadDataActive(UINT_8 *data);
UINT_16 dtcLoadDataStored(UINT_8 *data);
void dtcClearStored(void);
void dtcSetCurrentIgnitionCount(UINT_16 count);

#endif /* DTC_H */
