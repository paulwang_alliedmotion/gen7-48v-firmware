/*!
 * \file      dtc.c
 * \author    Zach Haver
 * \brief     This file handles the dtc services
 */

/* $Id: $ */
/* $Name: $ */

/* NOTICE: This file is the property of Globe Motors and contains */
/* CONFIDENTIAL information.  Any use, copying or dissemination of the */
/* information contained in this file is strictly prohibited unless */
/* authorized by Globe Motors in writing. */

/* Copyright 2015, Globe Motors, Inc. */

/*
 * Revision History:
 *
 * $Log: $
 *
 */

#include "dtc.h"
#include "eeprom.h"
#include "j1939Diagnostic.h"
#include "misra.h"

/* private variables */
/*! \var   dtcHWState
 *  \brief state of the DTC hardware information table.
 */
static DTCControlState_t dtcHWState = DTC_CLEAN;
/*! \var   dtcSWState
 *  \brief state of the DTC hardware information table.
 */
static DTCControlState_t dtcSWState = DTC_CLEAN;
/*! \var   dtcIGNFState
 *  \brief state of the DTC ignition first information table.
 */
static UINT_16 dtcIGNFIndex = DTC_COUNT;
/*! \var   dtcSWState
 *  \brief state of the DTC ignition last information table.
 */
static UINT_16 dtcIGNLIndex = DTC_COUNT;
/*! \var   dtcIgnitionCount
 *  \brief count of this ignition cycle.
 */
static UINT_16 dtcIgnitionCount = 0U;

/*! \struct dtcDef_t
 *  \brief  DTC constants data structure.
 */
static const struct {
    UINT_32 spn; /*!< spn (suspect parameter number). */
    UINT_8 fmi; /*!< fmi (failure mode identifier). */
}dtcDef[DTC_COUNT] = {
    /*   SPN      , FMI */
    {0x7ffffUL, 0x1fU}, /* Load Fail */
    {0x7fffeUL, 0x0dU}, /* CRC Application */
    {0x7fffdUL, 0x1fU}, /* Boot Count */
    {0x7fffcUL, 0x06U}, /* I Excess Error */
    {0x7fffbUL, 0x1fU}, /* Torque Range */
    {0x7fffbUL, 0x1fU}, /* Torque Linearity */
    {0x7fffaUL, 0x0cU}, /* Encoder SPI */
    {0x7fffaUL, 0x1fU}, /* Encoder Variance */
    {0x7fffaUL, 0x0dU}, /* Encoder Bits */
    {0x7fff9UL, 0x10U}, /* Temperature High */
    {0x7fff9UL, 0x00U}, /* Temperature Over */
    {0x7fff8UL, 0x0dU}, /* CRC Manufacturing */
    {0x7fff7UL, 0x1fU}, /* DRV Error */
    {0x000a8UL, 0x04U}, /* Battery Low */
    {0x000a8UL, 0x03U}, /* Battery High */
    {0x7fff6UL, 0x0cU}, /* EEPROM */
    {0x7fff5UL, 0x0dU}, /* CRC Conficguration */
    {0x00000UL, 0x1fU}, /* Unused 15 */
    {0x00000UL, 0x02U}, /* Unused 14 */
    {0x00000UL, 0x13U}, /* Unused 13 */
    {0x00000UL, 0x02U}, /* Unused 12 */
    {0x00000UL, 0x0aU}, /* Unused 11 */
    {0x00000UL, 0x13U}, /* Unused 10 */
    {0x00000UL, 0x02U}, /* Unused 9 */
    {0x00000UL, 0x1fU}, /* Unused 8 */
    {0x00000UL, 0x1fU}, /* Unused 7 */
    {0x00000UL, 0x1fU}, /* Unused 6 */
    {0x00000UL, 0x1fU}, /* Unused 5 */
    {0x00000UL, 0x1fU}, /* Unused 4 */
    {0x00000UL, 0x1fU}, /* Unused 3 */
    {0x00000UL, 0x1fU}, /* Unused 2 */
    {0x00000UL, 0x1fU}};/* Unused 1 */

/*! \struct dtcData_t
 *  \brief  DTC volatile data structure.
 */
static struct {
    UINT_8 occ; /*!< occ (occurence of this fault). */
    UINT_16 ignF; /*!< ignF (ignition count of the first occurrence). */
    UINT_16 ignL; /*!< ignL (ignition count of the last occurrence). */
    DTCActivityState_t state; /*!< state (state of the DTC). */
} dtcData[DTC_COUNT] = {
    /*   OCC, ACTIVE */
    {0U, 0U, 0U, DTC_INACTIVE}, /* Load Fail */
    {0U, 0U, 0U, DTC_INACTIVE}, /* CRC Application */
    {0U, 0U, 0U, DTC_INACTIVE}, /* Boot Count */
    {0U, 0U, 0U, DTC_INACTIVE}, /* I Excess Error */
    {0U, 0U, 0U, DTC_INACTIVE}, /* Torque Range */
    {0U, 0U, 0U, DTC_INACTIVE}, /* Torque Linearity */
    {0U, 0U, 0U, DTC_INACTIVE}, /* Encoder SPI */
    {0U, 0U, 0U, DTC_INACTIVE}, /* Encoder Variance */
    {0U, 0U, 0U, DTC_INACTIVE}, /* Encoder Bits */
    {0U, 0U, 0U, DTC_INACTIVE}, /* Temperature High */
    {0U, 0U, 0U, DTC_INACTIVE}, /* Temperature Over */
    {0U, 0U, 0U, DTC_INACTIVE}, /* CRC Manufacturing */
    {0U, 0U, 0U, DTC_INACTIVE}, /* DRV Error */
    {0U, 0U, 0U, DTC_INACTIVE}, /* Battery Low */
    {0U, 0U, 0U, DTC_INACTIVE}, /* Battery High */
    {0U, 0U, 0U, DTC_INACTIVE}, /* EEPROM */
    {0U, 0U, 0U, DTC_INACTIVE}, /* CRC Configuration */
    {0U, 0U, 0U, DTC_INACTIVE}, /* Timeout */
    {0U, 0U, 0U, DTC_INACTIVE}, /* CAN */
    {0U, 0U, 0U, DTC_INACTIVE}, /* Vehicle Speed CAN Loss */
    {0U, 0U, 0U, DTC_INACTIVE}, /* Vehicle Speed CAN Error */
    {0U, 0U, 0U, DTC_INACTIVE}, /* Vehicle Speed Plausibility */
    {0U, 0U, 0U, DTC_INACTIVE}, /* Engine Speed CAN Loss */
    {0U, 0U, 0U, DTC_INACTIVE}, /* Engine Speed CAN Error */
    {0U, 0U, 0U, DTC_INACTIVE}, /* Unused 8 */
    {0U, 0U, 0U, DTC_INACTIVE}, /* Unused 7 */
    {0U, 0U, 0U, DTC_INACTIVE}, /* Unused 6 */
    {0U, 0U, 0U, DTC_INACTIVE}, /* Unused 5 */
    {0U, 0U, 0U, DTC_INACTIVE}, /* Unused 4 */
    {0U, 0U, 0U, DTC_INACTIVE}, /* Unused 3 */
    {0U, 0U, 0U, DTC_INACTIVE}, /* Unused 2 */
    {0U, 0U, 0U, DTC_INACTIVE}};/* Unused 1 */

/* public functions */

/*!
 * \fn       void dtcInit (void)
 *
 * \brief    This function initializes the dtc structures with data
 *           stored in the eeprom.
 */
void
dtcInit(void) {
    UINT_8 bytes[32] = {0U};
    UINT_16 words[32] = {0U};
    UINT_16 x = 0U;

    if (EE_ERR_NONE == eepromRead32Bytes(EEADDR_DTC_OCC_START, bytes)) {
        for (x = 0U; x < DTC_COUNT; x++) {
            dtcData[x].occ += bytes[x];
        }
    }
    if (EE_ERR_NONE == eepromRead32Words(EEADDR_DTC_IGN_CNT_FIRST, words)) {
        for (x = 0U; x < DTC_COUNT; x++) {
            dtcData[x].ignF = words[x];
        }
    }
    if (EE_ERR_NONE == eepromRead32Words(EEADDR_DTC_IGN_CNT_LAST, words)) {
        for (x = 0U; x < DTC_COUNT; x++) {
            dtcData[x].ignL = words[x];
        }
    }
}

/*!
 * \fn       void dtcControl (void)
 *
 * \brief    This function periodically checks for dtc state changes
 *           and updates the eeprom. The will make use of page writes
 *           to reduce sequential calls to eeprom write functions which
 *           have a bottle neck with write timing of ~3.5ms.
 */
void
dtcControl(void) {
    UINT_16 x = 0U;
    UINT_8 bytes[16];
    UINT_16 addy;

    if (DTC_DIRTY == dtcHWState) {
        for (x = 0U; x < 16U; x++) {
            bytes[x] = dtcData[x].occ;
        }
        (void)eepromWrite16Bytes(EEADDR_DTC_OCC_START, bytes);
        dtcHWState = DTC_CLEAN;
    }
    if (DTC_DIRTY == dtcSWState) {
        for (x = 0U; x < 16U; x++) {
            addy = x + 16U;
            bytes[x] = dtcData[addy].occ;
        }
        (void)eepromWrite16Bytes(EEADDR_DTC_OCC_START + 16U, bytes);
        dtcSWState = DTC_CLEAN;
    }
    if (DTC_COUNT > dtcIGNFIndex) {
        addy = EEADDR_DTC_IGN_CNT_FIRST + (2U * dtcIGNFIndex);
        (void)eepromWriteWord(addy, dtcData[dtcIGNFIndex].ignF);
        dtcIGNFIndex = DTC_COUNT;
    }
    if (DTC_COUNT > dtcIGNLIndex) {
        addy = EEADDR_DTC_IGN_CNT_LAST + (2U * dtcIGNLIndex);
        (void)eepromWriteWord(addy, dtcData[dtcIGNLIndex].ignL);
        dtcIGNLIndex = DTC_COUNT;
    }
}

/*!
 * \fn       void dtcSet (UINT_16 dtcn)
 *
 * \param    dtcn - the dtc number or array element to transfer
 *                  to the active state
 * \brief    This function transfers the dtc related to dtcn to
 *           the active state and updates the dtc_state_changed
 *           variable to the DTC_ADDED state to tell the control
 *           function to update the eeprom occurence count
 */
void
dtcSet(UINT_16 dtcN) {
    if (DTC_COUNT > dtcN) {
        if (DTC_INACTIVE == dtcData[dtcN].state) {
            dtcData[dtcN].state = DTC_ACTIVE;
            jdiagTripDM1();
            if (128U > dtcData[dtcN].occ) {
                dtcData[dtcN].occ++;
            }
            if (16U <= dtcN) {
                dtcSWState = DTC_DIRTY;
            } else {
                dtcHWState = DTC_DIRTY;
            }
            if (1U == dtcData[dtcN].occ) {
                dtcData[dtcN].ignF = dtcIgnitionCount;
                dtcIGNFIndex = dtcN;
            }
            dtcData[dtcN].ignL = dtcIgnitionCount;
            dtcIGNLIndex = dtcN;
        }
    }
}

/*!
 * \fn     UINT_16 dtcLoadDataActive(UINT_8 *data)
 *
 * \param  *data - array pointer to laod the fault data
 * \return length of the data in the array.
 * \breief This function loads the passed array with the
 *         data representing the faults that are currently
 *         active.
 */
UINT_16
dtcLoadDataActive(UINT_8 *data) {
    UINT_16 x;
    UINT_16 length = 2U;
    UINT_16 numFaults = 0U;
    UINT_16 ret;

    /* load empty fault data incase none are active */
    data[0] = 0x00U;
    data[1] = 0xffU;
    data[2] = 0x00U;
    data[3] = 0x00U;
    data[4] = 0x00U;
    data[5] = 0x00U;
    data[6] = 0xffU;
    data[7] = 0xffU;

    for (x = 0U; x < DTC_COUNT; x++) {
        if (DTC_ACTIVE == dtcData[x].state) {
            data[0] |= 0x10U;
            data[length] = ((UINT_16)dtcDef[x].spn & 0xffU);
            length++;
            data[length] = ((UINT_16)(dtcDef[x].spn >> 8) & 0xffU);
            length++;
            data[length] = ((UINT_16)(dtcDef[x].spn >> 16) << 5);
            data[length] |= (UINT_16)dtcDef[x].fmi & 0x1fU;
            length++;
            data[length] = dtcData[x].occ;
            length++;
            numFaults++;
        }
    }
    if (1U < numFaults) {
        ret = length;
    } else {
        ret = 8U;
    }
    return ret;
}

/*!
 * \fn     UINT_16 dtcLoadDataStored(UINT_8 *data)
 *
 * \param  *data - array pointer to laod the fault data
 * \return length of the data in the array.
 * \breief This function loads the passed array with the
 *         data representing the faults that are currently
 *         stored.
 */
UINT_16
dtcLoadDataStored(UINT_8 *data) {
    UINT_16 x;
    UINT_16 length = 2U;
    UINT_16 numFaults = 0U;
    UINT_16 ret;

    /* load empty fault data incase none are stored */
    data[0] = 0x00U;
    data[1] = 0xffU;
    data[2] = 0x00U;
    data[3] = 0x00U;
    data[4] = 0x00U;
    data[5] = 0x00U;
    data[6] = 0xffU;
    data[7] = 0xffU;

    for (x = 0U; x < DTC_COUNT; x++) {
        if ((DTC_INACTIVE == dtcData[x].state) && (0U < dtcData[x].occ)) {
            data[0] |= 0x10U;
            data[length] = ((UINT_16)dtcDef[x].spn & 0xffU);
            length++;
            data[length] = ((UINT_16)(dtcDef[x].spn >> 8) & 0xffU);
            length++;
            data[length] = ((UINT_16)(dtcDef[x].spn >> 16) << 5);
            data[length] |= (UINT_16)dtcDef[x].fmi & 0x1fU;
            length++;
            data[length] = dtcData[x].occ;
            length++;
            numFaults++;
        }
    }

    if (1U < numFaults) {
        ret = length;
    } else {
        ret = 8U;
    }
    return ret;
}

/*!
 * \fn    void dtcClearStored(void)
 *
 * \brief This function will clear the stored fault counters.
 */
void
dtcClearStored(void) {
    UINT_16 x;
    UINT_8 bytes[32U];

    for (x = 0U; x < 32U; x++) {
        if (DTC_INACTIVE == dtcData[x].state) {
            dtcData[x].occ = 0U;
            bytes[x] = 0U;
        } else {
            bytes[x] = dtcData[x].occ;
        }
    }
    (void)eepromWrite16Bytes(EEADDR_DTC_OCC_START, bytes);
    (void)eepromWrite16Bytes(EEADDR_DTC_OCC_START + 16U, &bytes[16]);
}

/*!
 * \fn    void dtcSetCurrentIgnitionCount(UINT_16 count)
 *
 * \brief This function will set the RAM variable for the active igntion
 *        count to cut down on EEPROM accesses.
 */
void
dtcSetCurrentIgnitionCount(UINT_16 count) {
    dtcIgnitionCount = count;
}
