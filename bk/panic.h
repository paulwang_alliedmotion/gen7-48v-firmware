/*!
 * \file      panic.h
 * \author    Zach Haver
 * \brief     This file containes the panic routine for the
 *            TMS320C28033/35 Piccolo.
 */

/* $Id: $ */
/* $Name: $ */

/* NOTICE: This file is the property of Globe Motors and contains */
/* CONFIDENTIAL information.  Any use, copying or dissemination of the */
/* information contained in this file is strictly prohibited unless */
/* authorized by Globe Motors in writing. */

/* Copyright 2016, Globe Motors, Inc. */

/*
 * Revision History:
 *
 * $Log: $
 *
 */

#ifndef PANIC_H
#define PANIC_H

#define PANIC() asm(" ESTOP0")

#endif /* PANIC_H */
