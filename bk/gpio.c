/*!
 * \file      gpio.c
 * \author    Zach Haver
 * \brief     I/O initialization functions for the TI TMS320C2803x.
 */

/* $Id: $ */
/* $Name: $ */

/* NOTICE: This file is the property of Globe Motors and contains */
/* CONFIDENTIAL information.  Any use, copying or dissemination of the */
/* information contained in this file is strictly prohibited unless */
/* authorized by Globe Motors in writing. */

/* Copyright 2016, Globe Motors, Inc. */

/*
 * Revision History:
 *
 * $Log: $
 *
 */

#include "gpio.h"
#include "piccolo.h"

/*!
 * \fn    void gpioInit(void)
 *
 * \brief This function will initialize all gpio pins to the aapropriate
 *        state for the Faraday Rev2 board.
 */
void
gpioInit(void) {
    /* Enable EALLOW proteted register access */
    PICCOLO_EALLOW();

    /* Enable the GPIO Clock */
    *PCLKCR3 |= 0x2000U;

    /* Group A pins */
    /* GPAMUX1
     * bits 31-30 - (00) GPIO15 = GPI0(Unused)
     * bits 29-28 - (00) GPIO14 = GPI0(Unused)
     * bits 27-26 - (00) GPIO13 = GPI0(Unused)
     * bits 25-24 - (00) GPIO12 = GPIO(DEBUG)
     * bits 23-22 - (00) GPIO11 = GPI0(Fault Lamp Feedback)
     * bits 21-20 - (00) GPIO10 = GPI0(Fault Lamp Control)
     * bits 19-18 - (00) GPIO9  = GPI0(DRV Chip Select)
     * bits 17-16 - (00) GPIO8  = GPIO(WDC)
     * bits 15-14 - (00) GPIO7  = GPIO(KEEP Alive)
     * bits 13-12 - (00) GPIO6  = GPIO(/SCF DRV Fault XINT)
     * bits 11-10 - (01) GPIO5  = EPWM3B(Phase C Lo)
     * bits 9-8   - (01) GPIO4  = EPWM3A(Phase C Hi)
     * bits 7-6   - (01) GPIO3  = EPWM2B(Phase B Lo)
     * bits 5-4   - (01) GPIO2  = EPWM2A(Phase B Hi)
     * bits 3-2   - (01) GPIO1  = EPWM1B(Phase A Lo)
     * bits 1-0   - (01) GPIO0  = EPWM1A(Phase A Hi)
     */
    *GPAMUX1 = 0x00000555UL;

    /* GPAMUX2
     * bits 31-30 - (01) GPIO31 = CANTXA
     * bits 29-28 - (01) GPIO30 = CANRXA
     * bits 27-26 - (10) GPIO29 = SCLA(EEPROM)
     * bits 25-24 - (10) GPIO28 = SDAA(EEPROM)
     * bits 23-22 - (00) GPIO27 = GPIO(Unused)
     * bits 21-20 - (00) GPIO26 = GPIO(Unused)
     * bits 19-18 - (00) GPIO25 = GPIO(Unused)
     * bits 17-16 - (01) GPIO24 = ECAP1(Vehicle Speed)
     * bits 15-14 - (01) GPIO23 = EQEP1I(Infineon I)
     * bits 13-12 - (00) GPIO22 = GPIO(RPM XINT)
     * bits 11-10 - (01) GPIO21 = EQEP1B(Infineon B)
     * bits 9-8   - (01) GPIO20 = EQEP1A(Infineon A)
     * bits 7-6   - (00) GPIO19 = GPIO(Infineon Chip Select)
     * bits 5-4   - (01) GPIO18 = SPICLKA(Drv and Infineon)
     * bits 3-2   - (01) GPIO17 = SPISOMIA(Drv and Infineon)
     * bits 1-0   - (01) GPIO16 = SPISIMOA(DRV and Infineon)
     */
    *GPAMUX2 = 0x5a014515UL;

    /* GPACTRL
     * bits 31-24 - (0x00) QUALPRD3 = SYSCLKOUT PERIOD
     * bits 23-16 - (0x00) QUALPRD2 = SYSCLKOUT PERIOD
     * bits 15-8  - (0x0f) QUALPRD1 = SYSCLKOUT PERIOD
     * bits 7-0   - (0x00) QUALPRD0 = SYSCLKOUT PERIOD
     */
    *GPACTRL = 0x00000f00UL;

    /* GPAQSEL1
     * bits 31-30 - (00) GPIO15 = SYSCLKOUT(UNUSED)
     * bits 29-28 - (00) GPIO14 = SYSCLKOUT(UNUSED)
     * bits 27-26 - (00) GPIO13 = SYSCLKOUT(UNUSED)
     * bits 25-24 - (00) GPIO12 = SYSCLKOUT(UNUSED)
     * bits 23-22 - (00) GPIO11 = SYSCLKOUT(FAULT LAMP FEEDBACK)
     * bits 21-20 - (00) GPIO10 = SYSCLKOUT(FAULT LAMP CONTROL)
     * bits 19-18 - (00) GPIO9  = SYSCLKOUT(DRV CHIP SELECT)
     * bits 17-16 - (10) GPIO8  = 6 CLKS(WATCHDOG FEEDBACK XINT)
     * bits 15-14 - (00) GPIO7  = SYSCLKOUT(KEEP ALIVE)
     * bits 13-12 - (10) GPIO6  = 6 CLKS(/SCF DRV Fault XINT)
     * bits 11-10 - (11) GPIO5  = ASYNCHRONOUS(PWM)
     * bits 9-8   - (11) GPIO4  = ASYNCHRONOUS(PWM)
     * bits 7-6   - (11) GPIO3  = ASYNCHRONOUS(PWM)
     * bits 5-4   - (11) GPIO2  = ASYNCHRONOUS(PWM)
     * bits 3-2   - (11) GPIO1  = ASYNCHRONOUS(PWM)
     * bits 1-0   - (11) GPIO0  = ASYNCHRONOUS(PWM)
     */
    *GPAQSEL1 = 0x00022fffUL;

    /* GPAQSEL2
     * bits 31-30 - (11) GPIO31 = ASYNCHRONOUS(CAN TX)
     * bits 29-28 - (11) GPIO30 = ASYNCHRONOUS(CAN RX)
     * bits 27-26 - (11) GPIO29 = ASYNCHRONOUS(EE CLOCK)
     * bits 25-24 - (11) GPIO28 = ASYNCHRONOUS(EE DATA)
     * bits 23-22 - (00) GPIO27 = SYSCLKOUT(UNUSED)
     * bits 21-20 - (00) GPIO26 = SYSCLKOUT(UNUSED)
     * bits 19-18 - (00) GPIO25 = SYSCLKOUT(UNUSED)
     * bits 17-16 - (11) GPIO24 = ASYNCHRONOUS(VSPD ECAP)
     * bits 15-14 - (00) GPIO23 = SYSCLKOUT(INCREMENTAL INDEX)
     * bits 13-12 - (10) GPIO22 = 6 CLKS(RPM XINT)
     * bits 11-10 - (10) GPIO21 = 6 CLKS(INCREMENTAL B)
     * bits 9-8   - (10) GPIO20 = 6 CLKS(INCREMENTAL A)
     * bits 7-6   - (00) GPIO19 = SYSCLKOUT(INFINEON CHIP SELECT)
     * bits 5-4   - (11) GPIO18 = ASYNCHROMOUS(SPI CLK)
     * bits 3-2   - (11) GPIO17 = ASYNCHRONOUS(SPI SOMI)
     * bits 1-0   - (11) GPIO16 = ASYNCHRONOUS(SPI SIMO)
     */
    *GPAQSEL2 = 0xff032a3fUL;

    /* GPADIR
     * bit 31 - (1) GPIO31 = OUTPUT(CAN TX)
     * bit 30 - (0) GPIO30 = INPUT(CAN RX)
     * bit 29 - (1) GPIO29 = OUTPUT(EE CLOCK)
     * bit 28 - (1) GPIO28 = OUTPUT(EE DATA)
     * bit 27 - (0) GPIO27 = INPUT(UNUSED)
     * bit 26 - (0) GPIO26 = INPUT(UNSUED)
     * bit 25 - (0) GPIO25 = INPUT(UNUSED)
     * bit 24 - (0) GPIO24 = INPUT(VSD ECAP)
     * bit 23 - (0) GPIO23 = INPUT(INCREMENTAL INDEX)
     * bit 22 - (0) GPIO22 = INPUT(RPM XINT)
     * bit 21 - (0) GPIO21 = INPUT(INCREMENTAL B)
     * bit 20 - (0) GPIO20 = INPUT(INCREMENTAL A)
     * bit 19 - (1) GPIO19 = OUTPUT(INFINEON CHIP SELECT)
     * bit 18 - (1) GPIO18 = OUTPUT(SPI CLOCK)
     * bit 17 - (0) GPIO17 = INPUT(SPI SOMI)
     * bit 16 - (1) GPIO16 = OUTPUT(SPI SIMO)
     * bit 15 - (0) GPIO15 = INPUT(UNUSED)
     * bit 14 - (0) GPIO14 = INPUT(UNUSED)
     * bit 13 - (0) GPIO13 = INPUT(UNUSED)
     * bit 12 - (1) GPIO12 = OUTPUT(DEBUG)
     * bit 11 - (0) GPIO11 = INPUT(FAULT LAMP FEEDBACK)
     * bit 10 - (1) GPIO10 = OUTPUT(FAULT LAMP CONTROL)
     * bit 9  - (1) GPIO9  = OUTPUT(DRV CHIP SELECT)
     * bit 8  - (0) GPIO8  = INPUT(WDOG FEEDBACK)
     * bit 7  - (1) GPIO7  = OUTPUT(KEEP ALIVE)
     * bit 6  - (0) GPIO6  = INPUT(/SCF DRV Fault XINT)
     * bit 5  - (1) GPIO5  = OUTPUT(PWM)
     * bit 4  - (1) GPIO4  = OUTPUT(PWM)
     * bit 3  - (1) GPIO3  = OUTPUT(PWM)
     * bit 2  - (1) GPIO2  = OUTPUT(PWM)
     * bit 1  - (1) GPIO1  = OUTPUT(PWM)
     * bit 0  - (1) GPIO0  = OUTPUT(PWM)
     */
    *GPADIR = 0xb00d16bfUL;

    /* GPAPUD
     * bit 31 - (0) GPIO31 = ENABLED
     * bit 30 - (0) GPIO30 = ENABLED
     * bit 29 - (0) GPIO29 = ENABLED
     * bit 28 - (0) GPIO28 = ENABLED
     * bit 27 - (0) GPIO27 = ENABLED
     * bit 26 - (0) GPIO26 = ENABLED
     * bit 25 - (0) GPIO25 = ENABLED
     * bit 24 - (1) GPIO24 = ENABLED
     * bit 23 - (0) GPIO23 = ENABLED
     * bit 22 - (1) GPIO22 = ENABLED
     * bit 21 - (0) GPIO21 = ENABLED
     * bit 20 - (0) GPIO20 = ENABLED
     * bit 19 - (0) GPIO19 = ENABLED
     * bit 18 - (0) GPIO18 = ENABLED
     * bit 17 - (0) GPIO17 = ENABLED
     * bit 16 - (0) GPIO16 = ENABLED
     * bit 15 - (0) GPIO15 = ENABLED
     * bit 14 - (0) GPIO14 = ENABLED
     * bit 13 - (0) GPIO13 = ENABLED
     * bit 12 - (0) GPIO12 = ENABLED
     * bit 11 - (1) GPIO11 = ENABLED
     * bit 10 - (0) GPIO10 = ENABLED
     * bit 9  - (0) GPIO9  = ENABLED
     * bit 8  - (0) GPIO8  = ENABLED
     * bit 7  - (1) GPIO7  = DISABLED
     * bit 6  - (1) GPIO6  = DISABLED
     * bit 5  - (0) GPIO5  = ENABLED
     * bit 4  - (0) GPIO4  = ENABLED
     * bit 3  - (0) GPIO3  = ENABLED
     * bit 2  - (0) GPIO2  = ENABLED
     * bit 1  - (0) GPIO1  = ENABLED
     * bit 0  - (0) GPIO0  = ENABLED
     */
    *GPAPUD = 0x014008c0UL;

    /* Group B pins */
    /* GPBMUX1
     * bits 31-26 - (xx) Reserved
     * bits 25-24 - (00) GPIO44 = GPIO
     * bits 23-22 - (00) GPIO43 = GPIO
     * bits 21-20 - (00) GPIO42 = GPIO
     * bits 19-18 - (00) GPIO41 = GPIO
     * bits 17-16 - (00) GPIO40 = GPIO
     * bits 15-14 - (00) GPIO39 = GPIO
     * bits 13-12 - (00) GPIO38 = GPIO
     * bits 11-10 - (00) GPIO37 = GPIO
     * bits 9-8   - (00) GPIO36 = GPIO
     * bits 7-6   - (00) GPIO35 = GPIO
     * bits 5-4   - (00) GPIO34 = GPIO(DEBUG_LED)
     * bits 3-2   - (00) GPIO33 = GPIO(VSD)
     * bits 1-0   - (00) GPIO32 = GPIO(WDI)
     */
    *GPBMUX1 = 0x00000000UL;

    /* GPBCTRL
     * bits 31-16 - (0xXX) Reserved
     * bits 15-8  - (0x00) QUALPRD = SYSCLKOUT
     * bits 7-0   - (0x00) QUALPRD = SYSCLKOUT
     */
    *GPBCTRL = 0x00000000UL;

    /* GPBQSEL1
     * bits 31-26 - (xx) Reserved
     * bits 25-24 - (00) GPIO44 = SYSCLKOUT
     * bits 23-22 - (00) GPIO43 = SYSCLKOUT
     * bits 21-20 - (00) GPIO42 = SYSCLKOUT
     * bits 19-18 - (00) GPIO41 = SYSCLKOUT
     * bits 17-16 - (00) GPIO40 = SYSCLKOUT
     * bits 15-14 - (00) GPIO39 = SYSCLKOUT
     * bits 13-12 - (00) GPIO38 = SYSCLKOUT
     * bits 11-10 - (00) GPIO37 = SYSCLKOUT
     * bits 9-8   - (00) GPIO36 = SYSCLKOUT
     * bits 7-6   - (00) GPIO35 = SYSCLKOUT
     * bits 5-4   - (00) GPIO34 = SYSCLKOUT(DEBUG_LED)
     * bits 3-2   - (10) GPIO33 = 6 CLKS(VSD)
     * bits 1-0   - (00) GPIO32 = SYSCLKOUT(WDI)
     */
    *GPBQSEL1 = 0x00000080UL;

    /* GPBDIR
     * bits 31-13 - (x) Reserved
     * bit  12    - (0) GPIO44 = INPUT
     * bit  11    - (0) GPIO43 = INPUT
     * bit  10    - (0) GPIO42 = INPUT
     * bit  9     - (0) GPIO41 = INPUT
     * bit  8     - (0) GPIO40 = INPUT
     * bit  7     - (0) GPIO39 = INPUT
     * bit  6     - (0) GPIO38 = INPUT
     * bit  5     - (0) GPIO37 = INPUT
     * bit  4     - (0) GPIO36 = INPUT
     * bit  3     - (0) GPIO35 = INPUT
     * bit  2     - (1) GPIO34 = OUTPUT(DEBUG_LED)
     * bit  1     - (0) GPIO33 = INPUT(VSD)
     * bit  0     - (1) GPIO32 = OUTPUT(WDI)
     */
    *GPBDIR = 0x00000005UL;

    /* GPBPUD
     * bits 31-13 - (x) Reserved
     * bit  12    - (0) GPIO44 = ENABLED
     * bit  11    - (0) GPIO43 = ENABLED
     * bit  10    - (0) GPIO42 = ENABLED
     * bit  9     - (0) GPIO41 = ENABLED
     * bit  8     - (0) GPIO40 = ENABLED
     * bit  7     - (0) GPIO39 = ENABLED
     * bit  6     - (0) GPIO38 = ENABLED
     * bit  5     - (0) GPIO37 = ENABLED
     * bit  4     - (0) GPIO36 = ENABLED
     * bit  3     - (0) GPIO35 = ENABLED
     * bit  2     - (1) GPIO34 = DISABLED
     * bit  1     - (1) GPIO33 = DISABLED
     * bit  0     - (1) GPIO32 = DISABLED
     */
    *GPBPUD = 0x00000007UL;

    /* Set External Interrupt Pins */
    /* XINT1 will be the fault line of the DRV8305 */
    *GPIOXINT1SEL = 0x0006U;
    /* XINT2 will be the timeout of the external watchdog */
    *GPIOXINT2SEL = 0x0008U;
    /* XINT3 will be used for the RPM discrete signal */
    *GPIOXINT3SEL = 0x0016U;

    /* Return security to protested registers */
    PICCOLO_EDIS();
}
