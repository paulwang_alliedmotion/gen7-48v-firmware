        .ref _c_int00

***********************************************************************
* Function: codestart section
*
* Description: Branch to the routine to disable the watchdog
***********************************************************************
        .sect "codestart"
        LB _wd_disable       ;Branch to watchdog disable code

***********************************************************************
* Function: wd_disable
*
* Description: Disable the watchdog timer, then branch to the C init routine
***********************************************************************

        .text
        .global _wd_disable

_wd_disable:
        EALLOW                  ;Enable EALLOW protected register access
        MOVZ DP, #7029h>>6      ;Set data page for WDCR register
        MOV @7029h, #0068h      ;Set WDDIS bit in WDCR to disable WD
        EDIS                    ;Disable EALLOW protected register access
        LB _c_int00             ;Branch to start of boot.asm in RTS library

        .end
