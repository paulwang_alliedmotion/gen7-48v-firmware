/*!
 * \file      crc.h
 * \author    Zach Haver
 * \brief     Cyclic Redundancy Check definitions
 */

/* $Id: $ */
/* $Name: $ */

/* NOTICE: This file is the property of Globe Motors and contains */
/* CONFIDENTIAL information.  Any use, copying or dissemination of the */
/* information contained in this file is strictly prohibited unless */
/* authorized by Globe Motors in writing. */

/* Copyright 2016, Globe Motors, Inc. */

/*
 * Revision History:
 *
 * $Log: $
 *
 */

#ifndef CRC_H
#define CRC_H

#include "misra.h"

/*! \enum  CRCReturn_t
 *  \brief Return state for crc check calculations.
 */
typedef enum {
    CRC_INVALID, /*!< CRC_INVALID - value for a non-matching CRC calculation. */
    CRC_VALID /*!< CRC_VALID - value for a matching CRC calculation. */
} CRCReturn_t;

/*! \def CRC_INIT - default starting value for a CRC calculation. */
#define CRC_INIT                0xffffffffUL
/*! \def CRC_FLASH_START - start address in flash for the application. */
#define CRC_FLASH_START         0x003e8000UL
/*! \def CRC_FLASH_LENGTH - length of the application flash space. */
#define CRC_FLASH_LENGTH            0xc000UL
/*! \def CRC_TABLE_START - start of the CRC table in flash. */
#define CRC_TABLE_START         0x003f7d00UL
#define CRC_BOOT_START          0x003f4000UL
#define CRC_BOOT_LENGTH         0x00003f00UL

CRCReturn_t crcChkApp(const UINT_8 *data);
UINT_32 crcVerifyBlockFlash(UINT_32 offset, UINT_32 length);
UINT_32 crc32(UINT_32 newByte, UINT_32 crc);

#endif /* CRC_H */
