/*!
 * \file    firstRun.h
 * \author  Zach Haver
 * \brief   Definition for manufacturing to set default values and jump to
 *          JTAG programmed app instead of normal procedure.
 */

/* $Id: $ */
/* $Name: $ */

/* NOTICE: This file is the property of Globe Motors and contains */
/* CONFIDENTIAL information.  Any use, copying or dissemination of the */
/* information contained in this file is strictly prohibited unless */
/* authorized by Globe Motors in writing. */

/* Copyright 2017, Globe Motors, Inc. */

/*
 * Revision History:
 *
 * $Log: $
 *
 */

#ifndef FIRSTRUN_H
#define FIRSTRUN_H

void firstRun(void);

#endif /* FIRSTRUN_H */
