/*!
 * \file      j1939DataLink.c
 * \author    Zach Haver
 * \brief     Implementation of the J1939 data link layer.
 *
 */

/* $Id: $ */
/* $Name: $ */

/* NOTICE: This file is the property of Globe Motors and contains */
/* CONFIDENTIAL information.  Any use, copying or dissemination of the */
/* information contained in this file is strictly prohibited unless */
/* authorized by Globe Motors in writing. */

/* Copyright 2016, Globe Motors, Inc. */

/*
 * Revision History:
 *
 * $Log: $
 *
 */

#include <string.h>

#include "can.h"
#include "crc.h"
#include "j1939Application.h"
#include "j1939DataLink.h"
#include "j1939Diagnostic.h"
#include "misra.h"

/* private function prototypes */
static void jdlPushMsgToRxQ(const J1939msg_t *msg);
static void jdlPushMsgToTxQ(const CANobj_t *pobj, void (*fp)(void));
static void jdlTPCMProcess(const CANobj_t *pobj);
static void jdlTPDTProcess(const CANobj_t *pobj);
static void jdlTPTransmit(void);
static void jdlTPSendAbort(UINT_8 reas, UINT_16 pgn, UINT_8 pri, UINT_8 dst);
static void jdlTPSendCTS(void);
static void jdlTPSendEOM(void);
static void jdlTPSendRTS(void);
static void jdlTPSendBAM(void);
static void jdlTPRxTimer(void);
static void jdlTPTxTimer(void);
static void jdlMessageProcess(const J1939msg_t *pmsg);

/* private variables */
static J1939DataHandler_t jdlData;
static J1939RxMsgQ_t jdlRxQ;
static J1939TxMsgQ_t jdlTxQ;
/*! \var tpRx - structure to hold information about the active Rx TP session. */
static J1939TPRx_t tpRx;
/*! \var tpTx - structure to hold information about the active Tx TP session */
static J1939TPTx_t tpTx;
/*! \var jdlTxState - state of the transmit buffer. */
static JDLTxBuffStates_t jdlTxState = TX_BUFF_FREE;
/*! \var messageCallback - Tx message callback function. */
static void (*messageCallback)(void);

/*!
 * \fn    void jdlInit(void)
 *
 * \brief This function will set the reception information on the CAN
 *        mailboxes, along with initializing the Rx and Tx queues and
 *        the transport protocol Rx and Tx modules.
 *        (Note: the mailbox for DM14 needs to be used in fkernel.c for
 *               the flash api callback function. Make note of it.)
 */
void
jdlInit(void) {
    CANMBInit_t initializer;

    /* request message */
    initializer.ids[0] = 0x00ea00f9UL;
    initializer.masks[0] = 0xff00ff00UL;
    /* TPCM */
    initializer.ids[1] = 0x00ec00f9UL;
    initializer.masks[1] = 0xff00ff00UL;
    /* TPDT */
    initializer.ids[2] = 0x00eb00f9UL;
    initializer.masks[2] = 0xff00ff00UL;
    /* DM13 */
    initializer.ids[3] = 0x00df00f9UL;
    initializer.masks[3] = 0xff00ff00UL;
    /* DM14 */
    initializer.ids[4] = 0x00d913f9UL;
    initializer.masks[4] = 0xff000000UL;
    canSetCallBackBox(5U);
    /* DM16 */
    initializer.ids[5] = 0x00d713f9UL;
    initializer.masks[5] = 0xff000000UL;
    /* DM18 */
    initializer.ids[6] = 0x00d413f9UL;
    initializer.masks[6] = 0xff000000UL;

    initializer.len =  7U;

    canSetMB(&initializer);

    jdlData.rxIdx = 0U;
    jdlData.txIdx = 0U;

    jdlRxQ.head = 0U;
    jdlRxQ.tail = 0U;
    jdlRxQ.n = 0U;

    jdlTxQ.head = 0U;
    jdlTxQ.tail = 0U;
    jdlTxQ.n = 0U;

    tpRx.state = TPRX_FREE;
    tpRx.t1 = 0U;
    tpRx.t2 = 0U;
    tpTx.state = TPTX_FREE;
    tpTx.t3 = 0U;
    tpTx.t4 = 0U;

    jdiagInit();
}

UINT_16
jdlAddMsgData(const UINT_8 *data, UINT_16 len, JDLDataDir_t d) {
    UINT_16 tIdx;
    UINT_16 x = len;
    UINT_16 ret;

    switch (d) {
    case RX_DATA:
        tIdx = jdlData.rxIdx;
        ret = jdlData.rxIdx;
        x += jdlData.rxIdx;
        x %= JDL_DATA_RX_SIZE;
        jdlData.rxIdx = x;
        for (x = 0U; x < len; x++) {
            jdlData.rxData[tIdx] = data[x];
            tIdx++;
            tIdx %= JDL_DATA_RX_SIZE;
        }
        break;
    case RX_TP:
        ret = jdlData.rxIdx;
        x += jdlData.rxIdx;
        x %= JDL_DATA_RX_SIZE;
        jdlData.rxIdx = x;
        break;
    case TX_DATA:
    default:
        tIdx = jdlData.txIdx;
        ret = jdlData.txIdx;
        x += jdlData.txIdx;
        x %= JDL_DATA_TX_SIZE;
        jdlData.txIdx = x;
        for (x = 0U; x < len; x++) {
            jdlData.txData[tIdx] = data[x];
            tIdx++;
            tIdx %= JDL_DATA_TX_SIZE;
        }
        break;
    }
    return ret;
}

void
jdlGetMsgData(UINT_8 *data, UINT_16 idx, UINT_16 len, JDLDataDir_t d) {
    UINT_16 tIdx = idx;
    UINT_16 x;

    for (x = 0U; x < len; x++) {
        if (RX_DATA == d) {
            tIdx %= JDL_DATA_RX_SIZE;
            data[x] = jdlData.rxData[tIdx];
            tIdx++;
        } else {
            tIdx %= JDL_DATA_TX_SIZE;
            data[x] = jdlData.txData[tIdx];
            tIdx++;
        }
    }
}

UINT_32
jdlGetRAMBUFandCRC(UINT_16 *data, UINT_16 idx, UINT_16 len) {
    UINT_32 crc = CRC_INIT;
    UINT_16 tIdx = (idx + 1U) % JDL_DATA_RX_SIZE;
    UINT_16 x;
    UINT_16 hi, lo;

    for (x = 0U; x < ((len - 1U) >> 1); x++) {
        hi = jdlData.rxData[tIdx];
        tIdx++;
        tIdx %= JDL_DATA_RX_SIZE;
        lo = jdlData.rxData[tIdx];
        tIdx++;
        tIdx %= JDL_DATA_RX_SIZE;
        crc = crc32((UINT_32)hi, crc);
        crc = crc32((UINT_32)lo, crc);
        data[x] = (hi << 8) | (lo & 0xffU);
    }
    return ~crc;
}

void
jdlCANToJ1939(const CANobj_t *pobj) {
    J1939msg_t msg;

    msg.pgn = (UINT_16)(pobj->id >> 8);
    if (0xf000U > msg.pgn) {
        msg.dst = (UINT_8)(msg.pgn & 0xffU);
        msg.pgn &= 0xff00U;
    } else {
        msg.dst = JDL_GLOBAL_ADDY;
    }
    /* only process if the message is for us. */
    if ((JDL_OUR_ADDY == msg.dst) || (JDL_GLOBAL_ADDY == msg.dst)) {
        switch (msg.pgn) {
        case JDL_PGN_TPCM:
            jdlTPCMProcess(pobj);
            break;
        case JDL_PGN_TPDT:
            jdlTPDTProcess(pobj);
            break;
        default:
            /* continue assembling message and push to rxQ */
            msg.pri = (UINT_8)((pobj->id >> 26) & 0x7UL);
            msg.src = (UINT_8)(pobj->id & 0xffUL);
            msg.dlc = pobj->dlc;
            msg.dIdx = jdlAddMsgData(pobj->data, msg.dlc, RX_DATA);
            jdlPushMsgToRxQ(&msg);
            break;
        }
    }
}

void
jdlJ1939ToCAN(const J1939msg_t *pmsg) {
    CANobj_t obj;
    UINT_32 id;
    if (8U >= pmsg->dlc) {
        /*convert and push to Q */
        id = (UINT_32)pmsg->pri << 26;
        id |= ((UINT_32)pmsg->pgn << 8);
        if (0xf000U > pmsg->pgn) {
            id |= ((UINT_32)pmsg->dst << 8);
        }
        id |= (UINT_32)pmsg->src;
        obj.id = id;
        obj.dlc = pmsg->dlc;
        jdlGetMsgData(obj.data, pmsg->dIdx, pmsg->dlc, TX_DATA);
        jdlPushMsgToTxQ(&obj, pmsg->fp);
    } else {
        memcpy(&tpTx.msg, pmsg, sizeof(J1939msg_t));
        tpTx.state = TPTX_GO;
    }
}

/*!
 * \fn    void jdlTxConfimation(void)
 *
 * \brief This function will execute a Tx messages callback function after,
 *        the successful transmission of that message.
 */
void
jdlTxConfirmation(void) {
    jdlTxState = TX_BUFF_FREE;
    if (NULL != messageCallback) {
        (*(messageCallback)) ();
    }
}

/*!
 * \fn    void jdlTimer(void)
 *
 * \brief This function provides the main J1939 timebase.
 *        (Note: 1ms periodic execution recommended.)
 */
void
jdlTimer(void) {
    CANobj_t activeObj;
    J1939msg_t activeRxMsg;

    jdiagTimer();

    jdlTPRxTimer();
    jdlTPTxTimer();

    /* process the Tx queue */
    if ((0U < jdlTxQ.n) && (TX_BUFF_FREE == jdlTxState)) {
        memcpy(&activeObj, &jdlTxQ.data[jdlTxQ.tail].obj, sizeof(CANobj_t));
        messageCallback = jdlTxQ.data[jdlTxQ.tail].fp;
        jdlTxQ.tail++;
        if (JDL_TX_Q_SIZE <= jdlTxQ.tail) {
            jdlTxQ.tail = 0U;
        }
        jdlTxQ.n--;
        jdlTxState = TX_BUFF_ACTIVE;
        canTx(&activeObj);
    }
    if (TPTX_GO == tpTx.state) {
        jdlTPTransmit();
    }
    /* process the Rx queue */
    if (0U < jdlRxQ.n) {
        memcpy(&activeRxMsg, &jdlRxQ.data[jdlRxQ.tail], sizeof(J1939msg_t));
        jdlRxQ.tail++;
        if (JDL_RX_Q_SIZE <= jdlRxQ.tail) {
            jdlRxQ.tail = 0U;
        }
        jdlRxQ.n--;
        /* Note: add messages that are to be processed to this list. */
        switch(activeRxMsg.pgn) {
        case JDL_PGN_RQST:
            jdlMessageProcess(&activeRxMsg);
            break;
        case JDIAG_PGN_DM3:
        case JDIAG_PGN_DM7:
        case JDIAG_PGN_DM13:
        case JDIAG_PGN_DM14:
        case JDIAG_PGN_DM16:
        case JDIAG_PGN_DM18:
            jdiagMessageProcess(&activeRxMsg);
            break;
        default:
            /* Unused PGN discard message */
            break;
        }
    }
}

/*!
 * \fn    void jdlACK(UINT_16 controlByte, const J1939msg_t *pmsg)
 *
 * \param controlByte - byte indicating PACK or NACK.
 * \param *pmsg - pointer to information on the message being ACKed.
 * \brief This function will send a positive or negative acknoledge to
 *        the message pointed to by pmsg.
 */
void
jdlACK(UINT_16 controlByte, const J1939msg_t *pmsg) {
    J1939msg_t msg;
    UINT_8 data[8];

    msg.fp = NULL;
    msg.pri = pmsg->pri;
    msg.pgn = JDL_PGN_ACK;
    msg.dst = pmsg->src;
    msg.src = JDL_OUR_ADDY;
    msg.dlc = 8U;
    data[0] = (UINT_8)controlByte;
    data[1] = 0U;
    data[2] = 0xffU;
    data[3] = 0xffU;
    data[4] = 0xffU;
    data[5] = (UINT_8)(pmsg->pgn & 0xffU);
    data[6] = (UINT_8)(pmsg->pgn >> 8);
    data[7] = 0U;
    msg.dIdx = jdlAddMsgData(data, msg.dlc, TX_DATA);
    jdlJ1939ToCAN(&msg);
}

/*!
 * \fn    void jdlAddressClaim(void)
 *
 * \brief This function will send an address claim message.
 */
void
jdlAddressClaim(void) {
    J1939msg_t ac;
    UINT_8 data[8];

    ac.fp = NULL;
    ac.pri = 6U;
    ac.pgn = JDL_PGN_ADCL;
    ac.dst = JDL_GLOBAL_ADDY;
    ac.src = JDL_OUR_ADDY;
    ac.dlc = 8U;
    data[0] = 0x55U;
    data[1] = 0xAAU;
    data[2] = 0x55U;
    data[3] = 0x02U;
    data[4] = 0x00U;
    data[5] = 0x31U;
    data[6] = 0x02U;
    data[7] = 0x10U;
    ac.dIdx = jdlAddMsgData(data, ac.dlc, TX_DATA);
    jdlJ1939ToCAN(&ac);
}

static void
jdlPushMsgToRxQ(const J1939msg_t *msg) {
    if (JDL_RX_Q_SIZE > jdlRxQ.n) {
        memcpy(&jdlRxQ.data[jdlRxQ.head], msg, sizeof(J1939msg_t));
        jdlRxQ.head++;
        jdlRxQ.head %= JDL_RX_Q_SIZE;
        jdlRxQ.n++;
    } else {
        /* We will just ignore lost messages for now. Future Error? */
        ;
    }
}

static void
jdlPushMsgToTxQ(const CANobj_t *pobj, void (*fp)(void)) {
    if (JDL_TX_Q_SIZE > jdlTxQ.n) {
        memcpy(&jdlTxQ.data[jdlTxQ.head].obj, pobj, sizeof(CANobj_t));
        jdlTxQ.data[jdlTxQ.head].fp = fp;
        jdlTxQ.head++;
        jdlTxQ.head %= JDL_TX_Q_SIZE;
        jdlTxQ.n++;
    } else {
        /* We will just ignore lost messages for now. Future Error? */
        ;
    }
}

/*!
 * \fn    void jdlTPCMProcess(const CANobj_t *pobj)
 *
 * \param *pobj - pointer to a recevied TPCM CAN object.
 * \brief This function will handle a received transport protocol
 *        connection management message.
 */
static void
jdlTPCMProcess(const CANobj_t *pobj) {
    UINT_8 controlByte = pobj->data[0];
    UINT_8 pri = (UINT_8)((pobj->id >> 26) & 0x7UL);
    UINT_8 src = (UINT_8)(pobj->id & 0xffUL);
    UINT_8 dst = (UINT_8)((pobj->id >> 8) & 0xffUL);
    UINT_16 pgn = (UINT_16)pobj->data[5] & 0xffU;
    UINT_16 dlc;

    pgn |= ((UINT_16)pobj->data[6] << 8);

    switch (controlByte) {
    case JDL_TP_CONTROL_RTS:
        if (TPRX_FREE == tpRx.state) {
            dlc = (UINT_16)pobj->data[1] & 0xffU;
            dlc |= (UINT_16)pobj->data[2] << 8;
            tpRx.msg.dlc = dlc;
            if (JDL_MAX_RX_SIZE >= tpRx.msg.dlc) {
                tpRx.msg.pri = pri;
                tpRx.msg.pgn = pgn;
                tpRx.msg.dst = dst;
                tpRx.msg.src = src;
                tpRx.msg.dIdx = jdlAddMsgData(pobj->data, dlc, RX_TP);
                tpRx.index = tpRx.msg.dIdx;
                tpRx.numPckts = pobj->data[3];
                tpRx.maxPckts = pobj->data[4];
                tpRx.pcktsRcv = 0U;
                tpRx.bytesCopied = 0U;
                /* send CTS */
                jdlTPSendCTS();
                tpRx.state = TPRX_WAIT_DATA;
            } else {
                /* abort too big */
                jdlTPSendAbort(JDL_TP_ABORT_RESOURCES, pgn, pri, src);
            }
        } else {
            /* abort we're busy */
            jdlTPSendAbort(JDL_TP_ABORT_INUSE, pgn, pri, src);
        }
        break;
    case JDL_TP_CONTROL_CTS:
        if (TPTX_WAIT_CTS == tpTx.state) {
            /* enable some data to be sent. */
            tpTx.numPckts = pobj->data[1];
            tpTx.activePckt = pobj->data[2];
            tpTx.state = TPTX_SEND_DATA;
        } else {
            jdlTPSendAbort(JDL_TP_ABORT_INUSE, pgn, pri, src);
        }
        break;
    case JDL_TP_CONTROL_EOM:
        if (TPTX_WAIT_EOM == tpTx.state) {
            /* move back to free */
            tpTx.state = TPTX_FREE;
            tpTx.t3 = 0U;
            /* execute call back */
            if (NULL != tpTx.msg.fp) {
                (*(tpTx.msg.fp))();
            }
        } else {
            jdlTPSendAbort(JDL_TP_ABORT_INUSE, pgn, pri, src);
        }
        break;
    case JDL_TP_CONTROL_ABORT:
        if (TPRX_FREE != tpRx.state) {
            if (pgn == tpRx.msg.pgn) {
                tpRx.state = TPRX_FREE;
                tpRx.t1 = 0U;
                tpRx.t2 = 0U;
            }
        }
        if (TPTX_FREE != tpTx.state) {
            if (pgn == tpTx.msg.pgn) {
                tpTx.state = TPTX_FREE;
                tpTx.t3 = 0U;
                tpTx.t4 = 0U;
            }
        }
        break;
    case JDL_TP_CONTROL_BAM:
    default:
        /* we don't support BAM or unknown values do nothing. */
        break;
    }
}

/*!
 * \fn    void jdlTPDTProcess(const CANobj_t *pobj)
 *
 * \param *pobj - pointer to a recevied TPDT CAN object.
 * \brief This function will handle a received transport protocol
 *        data transfer message.
 */
static void
jdlTPDTProcess(const CANobj_t *pobj) {
    UINT_16 x;

    if (TPRX_WAIT_DATA == tpRx.state) {
        if ((tpRx.pcktsRcv + 1U) == pobj->data[0]) {
            for (x = 1U; x <= 7U; x++) {
                if (tpRx.msg.dlc > tpRx.bytesCopied) {
                    jdlData.rxData[tpRx.index] = pobj->data[x];
                    tpRx.index++;
                    tpRx.index %= JDL_DATA_RX_SIZE;
                    tpRx.bytesCopied++;
                }
            }
            tpRx.pcktsRcv++;
            if (tpRx.msg.dlc <= tpRx.bytesCopied) {
                /* message complete push to rxQ */
                jdlPushMsgToRxQ(&tpRx.msg);
                jdlTPSendEOM();
                tpRx.state = TPRX_FREE;
            }
        }
    } else {
        jdlTPSendAbort(JDL_TP_ABORT_RESOURCES, tpRx.msg.pgn, tpRx.msg.pri,
                       tpRx.msg.src);
    }
}

static void
jdlTPTransmit(void) {
    if (JDL_GLOBAL_ADDY == tpTx.msg.dst) {
        /* BAM transmit */
        jdlTPSendBAM();
        tpTx.state = TPTX_BAM_DATA;
    } else {
        jdlTPSendRTS();
        tpTx.state = TPTX_WAIT_CTS;
    }
}

/*!
 * \fn    void jdlTPSendAbort(UINT_8 reas, UINT_16 pgn, UINT_8 pri, UINT_8 dst)
 *
 * \param reas - reason for the abort.
 * \param pgn - parameter group number of the aborted message.
 * \param pri - priority of the aborted message.
 * \param dst - destination address for the aborted message.
 * \brief This function will send a tranport protocol abort message.
 */
static void
jdlTPSendAbort(UINT_8 reas, UINT_16 pgn, UINT_8 pri, UINT_8 dst) {
    J1939msg_t msg;
    UINT_8 data[8];

    if (tpTx.msg.pgn == pgn) {
        tpTx.t3 = 0U;
        tpTx.t4 = 0U;
        tpTx.state = TPTX_FREE;
    } else {
        tpRx.t1 = 0U;
        tpRx.t2 = 0U;
        tpRx.state = TPRX_FREE;
    }
    msg.fp = NULL;
    msg.pri = pri;
    msg.pgn = JDL_PGN_TPCM;
    msg.src = JDL_OUR_ADDY;
    msg.dst = dst;
    msg.dlc = 8U;
    data[0] = (UINT_8)JDL_TP_CONTROL_ABORT;
    data[1] = reas;
    data[2] = 0xffU;
    data[3] = 0xffU;
    data[4] = 0xffU;
    data[5] = (UINT_8)(pgn & 0xffU);
    data[6] = (UINT_8)(pgn >> 8);
    data[7] = 0U;
    msg.dIdx = jdlAddMsgData(data, msg.dlc, TX_DATA);
    jdlJ1939ToCAN(&msg);
}

/*!
 * \fn    void jdlTPSendCTS(void)
 *
 * \brief This function will send a transport protocol Clear To Send message.
 */
static void
jdlTPSendCTS(void) {
    J1939msg_t msg;
    UINT_8 data[8];

    msg.fp = NULL;
    msg.pri = tpRx.msg.pri;
    msg.pgn = JDL_PGN_TPCM;
    msg.dst = tpRx.msg.src;
    msg.src = JDL_OUR_ADDY;
    msg.dlc = 8U;
    data[0] = JDL_TP_CONTROL_CTS;
    data[1] = tpRx.maxPckts;
    data[2] = 1U;
    data[3] = 0xffU;
    data[4] = 0xffU;
    data[5] = (UINT_8)(tpRx.msg.pgn & 0xffU);
    data[6] = (UINT_8)(tpRx.msg.pgn >> 8);
    data[7] = 0U;
    msg.dIdx = jdlAddMsgData(data, msg.dlc, TX_DATA);
    jdlJ1939ToCAN(&msg);
    /* start T2 */
    tpRx.t2 = JDL_TP_RX_T2;
}
/*!
 * \fn    void jdlTPSendEOM(void)
 *
 * \brief This function will send a transport protocol End Of Message message.
 */
static void
jdlTPSendEOM(void) {
    J1939msg_t msg;
    UINT_8 data[8];

    msg.fp = NULL;
    msg.pri = tpRx.msg.pri;
    msg.pgn = JDL_PGN_TPCM;
    msg.dst = tpRx.msg.src;
    msg.src = JDL_OUR_ADDY;
    msg.dlc = 8U;
    data[0] = JDL_TP_CONTROL_EOM;
    data[1] = (UINT_8)tpRx.msg.dlc;
    data[2] = (UINT_8)(tpRx.msg.dlc >> 8);
    data[3] = tpRx.pcktsRcv;
    data[4] = 0xffU;
    data[5] = (UINT_8)(tpRx.msg.pgn & 0xffU);
    data[6] = (UINT_8)(tpRx.msg.pgn >> 8);
    data[7] = 0U;
    msg.dIdx = jdlAddMsgData(data, msg.dlc, TX_DATA);
    jdlJ1939ToCAN(&msg);
    tpRx.t2 = 0U;
}

/*!
 * \fn    void jdlTPSendRTS(void)
 *
 * \brief This function will send a transport protocol Request To Send msg.
 */
static void
jdlTPSendRTS(void) {
    J1939msg_t msg;
    UINT_8 data[8];

    msg.fp = NULL;
    msg.pri = tpTx.msg.pri;
    msg.pgn = JDL_PGN_TPCM;
    msg.dst = tpTx.msg.dst;
    msg.src = JDL_OUR_ADDY;
    msg.dlc = 8U;
    data[0] = JDL_TP_CONTROL_RTS;
    data[1] = (UINT_8)tpTx.msg.dlc;
    data[2] = (UINT_8)(tpTx.msg.dlc >> 8);
    tpTx.numPckts = tpTx.msg.dlc / 7U;
    if (0U != (tpTx.msg.dlc % 7U)) {
        tpTx.numPckts++;
    }
    data[3] = tpTx.numPckts;
    data[4] = 0xffU;
    data[5] = (UINT_8)(tpTx.msg.pgn & 0xffU);
    data[6] = (UINT_8)(tpTx.msg.pgn >> 8);
    data[7] = 0U;
    msg.dIdx = jdlAddMsgData(data, msg.dlc, TX_DATA);
    jdlJ1939ToCAN(&msg);
    tpTx.activePckt = 1U;
    tpTx.t3 = JDL_TP_TX_T3;
}

/*!
 * \fn    void jdlTPSendBAM(void)
 *
 * \brief This function will send a transport protocol
 *        Broadcast Announce Message.
 */
static void
jdlTPSendBAM(void) {
    J1939msg_t msg;
    UINT_8 data[8];

    msg.fp = NULL;
    msg.pri = tpTx.msg.pri;
    msg.pgn = JDL_PGN_TPCM;
    msg.dst = tpTx.msg.dst;
    msg.src = JDL_OUR_ADDY;
    msg.dlc = 8U;
    data[0] = JDL_TP_CONTROL_BAM;
    data[1] = (UINT_8)tpTx.msg.dlc;
    data[2] = (UINT_8)(tpTx.msg.dlc >> 8);
    tpTx.numPckts = tpTx.msg.dlc / 7U;
    if (0U != (tpTx.msg.dlc % 7U)) {
        tpTx.numPckts++;
    }
    data[3] = tpTx.numPckts;
    data[4] = 0xffU;
    data[5] = (UINT_8)(tpTx.msg.pgn & 0xffU);
    data[6] = (UINT_8)(tpTx.msg.pgn >> 8);
    data[7] = 0U;
    msg.dIdx = jdlAddMsgData(data, msg.dlc, TX_DATA);
    jdlJ1939ToCAN(&msg);
    tpTx.activePckt = 1U;
}

/*!
 * \fn    void jdlTPRxTimer(void)
 *
 * \brief This function is executed to tick the TPRX timers, and abort
 *        if a timeout occurs.
 */
static void
jdlTPRxTimer(void) {
    if (1U < tpRx.t1) {
        tpRx.t1--;
        if (1U == tpRx.t1) {
            /* timer 1 timeout abort and free */
            jdlTPSendAbort(JDL_TP_ABORT_TIMEOUT, tpRx.msg.pgn,
                           tpRx.msg.pri, tpRx.msg.src);
            tpRx.t1 = 0U;
            tpRx.t2 = 0U;
            tpRx.state = TPRX_FREE;
        }
    }
    if (1U < tpRx.t2) {
        tpRx.t2--;
        if (1U == tpRx.t2) {
            /* timer 2 timeout abort and free */
            jdlTPSendAbort(JDL_TP_ABORT_TIMEOUT, tpRx.msg.pgn,
                           tpRx.msg.pri, tpRx.msg.src);
            tpRx.t1 = 0U;
            tpRx.t2 = 0U;
            tpRx.state = TPRX_FREE;
        }
    }
}

/*!
 * \fn    void jdlTPTxTimer(void)
 *
 * \brief This function is executed to tick the TPTX timers, and abort
 *        if a timeout occurs. It also checks the TPTX state to see if
 *        more packets are ready to be sent.
 */
static void
jdlTPTxTimer(void) {
    J1939msg_t msg;
    UINT_8 data[8] = {0xffU,0xffU,0xffU,0xffU,0xffU,0xffU,0xffU,0xffU};
    UINT_16 remaining;
    UINT_16 idx;

    if (1U < tpTx.t3) {
        tpTx.t3--;
        if (1U == tpTx.t3) {
            jdlTPSendAbort(JDL_TP_ABORT_TIMEOUT, tpTx.msg.pgn,
                           tpTx.msg.pri, tpTx.msg.src);
            tpTx.t3 = 0U;
            tpTx.t4 = 0U;
            tpTx.state = TPTX_FREE;
        }
    }
    if (1U < tpTx.t4) {
        tpTx.t4--;
        if (1U == tpTx.t4) {
            jdlTPSendAbort(JDL_TP_ABORT_TIMEOUT, tpTx.msg.pgn,
                           tpTx.msg.pri, tpTx.msg.src);
            tpTx.t3 = 0U;
            tpTx.t4 = 0U;
            tpTx.state = TPTX_FREE;
        }
    }

    if (TPTX_BAM_DATA == tpTx.state) {
        /* send next message until all have been sent */
        msg.fp = NULL;
        msg.pri = tpTx.msg.pri;
        msg.pgn = JDL_PGN_TPDT;
        msg.src = JDL_OUR_ADDY;
        msg.dst = tpTx.msg.dst;
        msg.dlc = 8U;
        data[0] = tpTx.activePckt;
        idx = ((tpTx.activePckt - 1U) * 7U);
        remaining = tpTx.msg.dlc - idx;
        if (7U < remaining) {
            remaining = 7U;
        } else {
            msg.fp = tpTx.msg.fp;
            tpTx.state = TPTX_FREE;
        }
        idx += tpTx.msg.dIdx;
        jdlGetMsgData(&data[1], idx, remaining, TX_DATA);
        msg.dIdx = jdlAddMsgData(data, msg.dlc, TX_DATA);
        jdlJ1939ToCAN(&msg);
        tpTx.activePckt++;
    }

    if (TPTX_SEND_DATA == tpTx.state) {
        /* send next set of messages */
        if (0U < tpTx.numPckts) {
            msg.fp = NULL;
            msg.pri = tpTx.msg.pri;
            msg.pgn = JDL_PGN_TPDT;
            msg.src = JDL_OUR_ADDY;
            msg.dst = tpTx.msg.dst;
            msg.dlc = 8U;
            data[0] = tpTx.activePckt;
            idx = ((tpTx.activePckt - 1U) * 7U);
            remaining = tpTx.msg.dlc - idx;
            if (7U < remaining) {
                remaining = 7U;
            } else {
                msg.fp = tpTx.msg.fp;
                tpTx.state = TPTX_WAIT_EOM;
            }
            idx += tpTx.msg.dIdx;
            jdlGetMsgData(&data[1], idx, remaining, TX_DATA);
            msg.dIdx = jdlAddMsgData(data, msg.dlc, TX_DATA);
            jdlJ1939ToCAN(&msg);
            tpTx.t3 = JDL_TP_TX_T3;
            if (TPTX_WAIT_EOM != tpTx.state) {
                tpTx.activePckt++;
                tpTx.numPckts--;
                if (0U == tpTx.numPckts) {
                    tpTx.state = TPTX_WAIT_CTS;
                }
            }
        }
    }
}

/*!
 * \fn    void jdlMessageProcess(const J1939RxMsg_t *pmsg) {
 *
 * \param *pmsg - pointer to the received message information
 * \brief This function will process a received and assembled J1939
 *        request message.
 */
static void
jdlMessageProcess(const J1939msg_t *pmsg) {
    UINT_16 rqstPGN = 0U;
    UINT_8 data[8];

    switch (pmsg->pgn) {
    case JDL_PGN_RQST:
        jdlGetMsgData(data, pmsg->dIdx, pmsg->dlc, RX_DATA);
        rqstPGN = (UINT_16)data[0];
        rqstPGN |= ((UINT_16)data[1] << 8);
        switch (rqstPGN) {
        case JAPP_PGN_FEDA:
            jappFEDA(pmsg);
            break;
        case JDIAG_PGN_DM1:
            jdiagDM1(pmsg);
            break;
        case JDIAG_PGN_DM2:
            jdiagDM2(pmsg);
            break;
        case JDIAG_PGN_DM3:
            jdiagDM3(pmsg);
            break;
        default:
            if (JDL_OUR_ADDY == pmsg->dst) {
                jdlACK(JDL_NACK, pmsg);
            }
            break;
        }
        break;
    default:
        /* unsupported PGN */
        break;
    }
}
