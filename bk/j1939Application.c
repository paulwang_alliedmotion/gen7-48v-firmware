/*!
 * \file      j1939Application.c
 * \author    Zach Haver
 * \brief     Implementation of the J1939 application layer.
 *
 */

/* $Id: $ */
/* $Name: $ */

/* NOTICE: This file is the property of Globe Motors and contains */
/* CONFIDENTIAL information.  Any use, copying or dissemination of the */
/* information contained in this file is strictly prohibited unless */
/* authorized by Globe Motors in writing. */

/* Copyright 2016, Globe Motors, Inc. */

/*
 * Revision History:
 *
 * $Log: $
 *
 */

#include <string.h> /* for NULL */

#include "fkernel.h"
#include "j1939Application.h"
#include "j1939DataLink.h"
#include "softid.h"

/* private variables */

/* public functions */

/*!
 * \fn    void jappFEDA(const J1939RxMsg_t *pmsg)
 *
 * \brief This function creates and sends the SOFT ID.
 */
void
jappFEDA(const J1939msg_t *pmsg) {
    J1939msg_t feda;
    UINT_8 data[SOFTID_MAX_LENGTH];

    feda.fp = NULL;
    feda.pri = pmsg->pri;
    feda.pgn = JAPP_PGN_FEDA;
    feda.dst = pmsg->src;
    feda.src = JDL_OUR_ADDY;
    feda.dlc = softIdLoadData(data);
    feda.dIdx = jdlAddMsgData(data, feda.dlc, TX_DATA);
    jdlJ1939ToCAN(&feda);
}
