************************************************************************
* File: version.s
* Author: Zach Haver
* Notes: This file will load ecuid information about the revision of the code
*        to a flash section that can be read by a different independent
*        area of the code. (i.e. if this is the application version info
*        can now be read by the boot block, and vice vera). If any new
*        code areas are added in the future they could also have access
*        to the flash areas to get version info about the other areas.
*
*        This info will be loaded into a section titles "version" and
*        then allocated by the linker.
************************************************************************

           .sect   "version"
           .def    _VERSION, _MODEL
_VERSION:  .string "7.0.3"
           .word   0
_MODEL     .string "Allied Motion(Globe Motors) G7 Manufacturing Bootkernel*"
           .word   0
           .end
