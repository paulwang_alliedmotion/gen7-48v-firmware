/*!
 * \file      crc.c
 * \author    Zach Haver
 * \brief     Cyclic Redundancy Check functionality
 */

/* $Id: $ */
/* $Name: $ */

/* NOTICE: This file is the property of Globe Motors and contains */
/* CONFIDENTIAL information.  Any use, copying or dissemination of the */
/* information contained in this file is strictly prohibited unless */
/* authorized by Globe Motors in writing. */

/* Copyright 2016, Globe Motors, Inc. */

/*
 * Revision History:
 *
 * $Log: $
 *
 */

#include "crc.h"
#include "eeprom.h"
#include "misra.h"

/*!
 * \fn     CRCReturn_t crcChkApp(void)
 *
 * \return CRC_VALID - if the known and calculated crc match.
 * \return CRC_INVALID - if the known and calulated crc mis-match.
 * \brief  This function calculates the crc of the application
 *         space and compares it to the known good crc value that
 *         is placed in the eeprom after a successful reflash.
 */
CRCReturn_t
crcChkApp(const UINT_8 *data) {
    UINT_32 crc = crcVerifyBlockFlash(CRC_FLASH_START, CRC_FLASH_LENGTH);
    UINT_32 knownCRC;
    CRCReturn_t ret;

    knownCRC = (((UINT_32)data[3] & 0xffUL) << 8);
    knownCRC |= ((UINT_32)data[2] & 0xffUL);
    knownCRC <<= 8;
    knownCRC |= ((UINT_32)data[1] & 0xffUL);
    knownCRC <<= 8;
    knownCRC |= ((UINT_32)data[0] & 0xffUL);

    if (crc == knownCRC) {
        ret = CRC_VALID;
    } else {
        ret = CRC_INVALID;
    }

    return ret;
}

/*!
 * \fn     UINT_32 crcVerifyBlockFlash(UINT_32 offset, UINT_32 length)
 *
 * \param  offset - memory offset in flash for calculation to start.
 * \param  length - number of flash addresses to includ in calculation.
 * \return The calculated crc of the requested space.
 * \brief  This function will calculate the crc of the passed memory space.
 */
UINT_32
crcVerifyBlockFlash(UINT_32 offset, UINT_32 length) {
    UINT_32 x;
    UINT_32 crc = CRC_INIT;
    const UINT_16 *data;

    /*lint -save -e923 */
    data = (UINT_16 *)offset;
    /*lint -restore */

    for (x = 0UL; x < length; x++) {
        crc = crc32((((UINT_32)data[x] >> 8) & 0xffUL), crc);
        crc = crc32(((UINT_32)data[x] & 0xffUL), crc);
    }
    return ~crc;
}

/*!
 * \fn     UINT_32 crc32(UINT_32 newByte, UINT_32)
 *
 * \param  newByte - next byte to add to the calculation.
 * \param  crc - current crc to add the new byte to.
 * \return new crc value.
 * \brief  This function adds newByte to the crc value that is passed,
 *         and returns the new crc value.
 *         (Note: this function is executed in RAM.)
 */
#pragma CODE_SECTION (crc32, "secureRamFuncs");
UINT_32
crc32(UINT_32 newByte, UINT_32 crc) {
    /*lint -save -e923 */
    const UINT_32 *crcTable = (UINT_32 *)CRC_TABLE_START;
    /*lint -restore */

    return ((crc >> 8) ^ crcTable[(newByte ^ (crc & 0xffUL))]);
}
