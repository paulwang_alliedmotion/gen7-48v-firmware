/*!
 * \file        j1939DataLink.h
 * \author      Zach Haver
 * \brief       J1939 data link layer definitions.
 */

/* $Id: $ */
/* $Name: $ */

/* NOTICE: This file is the property of Globe Motors and contains */
/* CONFIDENTIAL information.  Any use, copying or dissemination of the */
/* information contained in this file is strictly prohibited unless */
/* authorized by Globe Motors in writing. */

/* Copyright 2016, Globe Motors, Inc. */

/*
 * Revision History:
 *
 * $Log: $
 *
 */

#ifndef J1939_DATA_LINK_H
#define J1939_DATA_LINK_H

#include "can.h"
#include "misra.h"

/* Addresses */
/*! \def JDL_OUR_ADDY - The address we will be transmitting from. */
#define JDL_OUR_ADDY        0x13U
/*! \def JDL_SERVICE_TOOL - The address we will be receiving updated from. */
#define JDL_SERVICE_TOOL    0xf9U
/*! \def JDL_GLOBAL_ADDY - The address to be used for global comms. */
#define JDL_GLOBAL_ADDY     0xffU

/*! \enum  JDLTxBuffStates_t
 *  \brief States for the Tx buffer.
 */
typedef enum {
    TX_BUFF_FREE,  /*!< TX_BUFF_FREE - state for a free Tx buffer. */
    TX_BUFF_ACTIVE /*!< TX_BUFF_ACTIVE - state for a busy Tx buffer. */
}JDLTxBuffStates_t;

/*! \def JDL_Q_DATA_SIZE - The number of bytes to be held by the data Q. */
#define JDL_Q_DATA_SIZE     2000U
/*! \def JDL_RX_Q_SIZE - The number of messages allowed in the Rx Q. */
#define JDL_RX_Q_SIZE         64U
/*! \def JDL_TX_Q_SIZE - The number of messages allowed in the Tx Q. */
#define JDL_TX_Q_SIZE         40U
/*! \def JDL_MAX_RX_SIZE - Maximum length for a received message. */
#define JDL_MAX_RX_SIZE     1785U
/*! \def JDL_MAX_TX_SIZE - Maximum length for a transmitted message. */
#define JDL_MAX_TX_SIZE     1025U

/* Data Link Layer PGN definitions */
/*! \def JDL_PGN_TPCM - Transport Protocol Connection Management. */
#define JDL_PGN_TPCM  0xec00U
/*! \def JDL_PGN_TPDT - Transport Protocol Data Transfer. */
#define JDL_PGN_TPDT  0xeb00U
/*! \def JDL_PGN_RQST - Request. */
#define JDL_PGN_RQST  0xea00U
/*! \def JDL_PGN_ACK - Acknowledgement. */
#define JDL_PGN_ACK   0xe800U
/*! \def JDL_PGN_ADCL - Address Claim. */
#define JDL_PGN_ADCL  0xee00U

/* ACK Control bytes */
/*! \def JDL_PACK - ACK positive acknowledge control byte. */
#define JDL_PACK               0U
/*! \def JDL_NACL - ACK negative acknowledge control byte. */
#define JDL_NACK               1U

/* TPCM Control bytes */
/*! \def JDL_TP_CONTROL_RTS - Request To Send control byte. */
#define JDL_TP_CONTROL_RTS    16U
/*! \def JDL_TP_CONTROL_CTS - Clear To Send control byte. */
#define JDL_TP_CONTROL_CTS    17U
/*! \def JDL_TP_CONTROL_EOM - End Of Message control byte. */
#define JDL_TP_CONTROL_EOM    19U
/*! \def JDL_TP_CONTROL_ABORT - Abort control byte. */
#define JDL_TP_CONTROL_ABORT 255U
/*! \def JDL_TP_CONTROL_BAM - Broadcast Announce Message control byte. */
#define JDL_TP_CONTROL_BAM    32U
/*! \def JDL_TP_ABORT_INUSE - in use abort reason byte. */
#define JDL_TP_ABORT_INUSE     1U
/*! \def JDL_TP_ABORT_RESOURCES - unavailable resources abort reason byte. */
#define JDL_TP_ABORT_RESOURCES 2U
/*! \def JDL_TP_ABORT_TIMEOUT - transfer timed out abort reason byte. */
#define JDL_TP_ABORT_TIMEOUT   3U
/*! \def JDL_TP_RX_T2 - reception timeout 2 limit. */
#define JDL_TP_RX_T2        1251U
/*! \def JDL_TP_RX_T3 - reception timeout 3 limit. */
#define JDL_TP_TX_T3        1251U

/*! \enum  JDLTPRXStates_t
 *  \brief States for the Rx TP handler.
 */
typedef enum {
    TPRX_FREE,     /*!< TPRX_FREE - state for a free Rx handler. */
    TPRX_WAIT_DATA /*!< TPRX_WAIT_DATA - state for data Rx wait. */
}JDLTPRXStates_t;

/*! \enum  JDLTPTXStates_t
 *  \brief States for the Tx TP handler.
 */
typedef enum {
    TPTX_FREE,      /*!< TPTX_FREE - state for a free Tx handler. */
    TPTX_WAIT_CTS,  /*!< TPTX_WAIT_CTS - state for CTS wait. */
    TPTX_SEND_DATA, /*!< TPTX_SEND_DATA - state to Tx data. */
    TPTX_WAIT_EOM,  /*!< TPTX_WAIT_EOM - state for EOM wait. */
    TPTX_BAM_DATA,  /*!< TPTX_BAM_DATA - state to Tx BAM data. */
    TPTX_GO
}JDLTPTXStates_t;

typedef enum {
    RX_DATA,
    RX_TP,
    TX_DATA
}JDLDataDir_t;

#define JDL_DATA_RX_SIZE  2048U
#define JDL_DATA_TX_SIZE  1024U

typedef struct {
    UINT_16 rxIdx;
    UINT_16 txIdx;
    UINT_8 rxData[JDL_DATA_RX_SIZE];
    UINT_8 txData[JDL_DATA_TX_SIZE];
} J1939DataHandler_t;

/*! \struct J1939msg_t
 *  \brief  Structure to hold the information on a J1939 message to
 *          be transmitted.
 */
typedef struct {
    UINT_8 pri;          /*!< pri - priority of the message. */
    UINT_16 pgn;         /*!< pgn - parameter group number of the message. */
    UINT_8 dst;          /*!< dst - destination address of the message. */
    UINT_8 src;          /*!< src - source address of the message. */
    UINT_16 dlc;         /*!< dlc - number of bytes in the message. */
    UINT_16 dIdx; /*!< dIdx - start index of msg data in the data queue. */
    void (*fp)(void); /*!< function pointer to execute after Tx. */
} J1939msg_t;

/*! \struct J1939RxMsgQ_t
 *  \brief  Queue structure to hold received J1939 messages for processing.
 */
typedef struct {
    UINT_8 head;                    /*!< head - index to remove elements. */
    UINT_8 tail;                    /*!< tail - index to insert elements. */
    UINT_8 n;                       /*!< n - number of queue elements. */
    J1939msg_t data[JDL_RX_Q_SIZE]; /*!< data - array to hold messages. */
} J1939RxMsgQ_t;

/*! \struct J1939TXQElement_t
 *  \brief  Element for the J1939 CAN Tx queue.
 */
typedef struct {
    CANobj_t obj;     /*!< obj - CAN object to transmit. */
    void (*fp)(void); /*!< function pointer to execute after transmission. */
} J1939TxQElement_t;

/*! \struct J1939TxMsgQ_t
 *  \brief  Queue structure to hold CAN messages to transmit..
 */
typedef struct {
    UINT_8 head;                      /*!< head - index to remove elements. */
    UINT_8 tail;                      /*!< tail - index to insert elements. */
    UINT_8 n;                         /*!< n - number of queue elements. */
    J1939TxQElement_t data[JDL_TX_Q_SIZE]; /*!< data - array to hold CAN obj. */
} J1939TxMsgQ_t;

/*! \struct J1939TPRx_t
 *  \brief  Structure to hold the information needed for an active
 *          transport protocol receive session.
 */
typedef struct {
    JDLTPRXStates_t state; /*!< state of the current Rx session. */
    UINT_16 index;         /*!< data array index to store data. */
    J1939msg_t msg;        /*!< Rx message information. */
    UINT_8 numPckts;       /*!< number of packets for this message. */
    UINT_8 maxPckts;       /*!< max packets for this sequence. */
    UINT_8 pcktsRcv;       /*!< packets received so far. */
    UINT_16 bytesCopied;   /*!< current number of bytes we've received. */
    UINT_16 t1;            /*!< t1 timeout counter. */
    UINT_16 t2;            /*!< t2 timeout counter. */
} J1939TPRx_t;

/*! \struct J1939TPTx_t
 *  \brief  Structure to hold the information needed for an active
 *          transport protocol transmit session.
 */
typedef struct {
    JDLTPTXStates_t state; /*!< state of the current Tx session. */
    J1939msg_t msg;      /*!< Tx message information. */
    UINT_8 numPckts;       /*!< number of packets to transmit. */
    UINT_8 activePckt;     /*!< active packet to transmit. */
    UINT_16 t3;            /*!< t3 timeout counter. */
    UINT_16 t4;            /*!< t4 timeout counter. */
} J1939TPTx_t;

void jdlInit(void);
UINT_16 jdlAddMsgData(const UINT_8 *data, UINT_16 len, JDLDataDir_t d);
void jdlGetMsgData(UINT_8 *data, UINT_16 idx, UINT_16 len, JDLDataDir_t d);
UINT_32 jdlGetRAMBUFandCRC(UINT_16 *data, UINT_16 idx, UINT_16 len);
void jdlCANToJ1939(const CANobj_t *pobj);
void jdlJ1939ToCAN(const J1939msg_t *pmsg);
void jdlTxConfirmation(void);
void jdlTimer(void);
void jdlACK(UINT_16 controlByte, const J1939msg_t *pmsg);
void jdlAddressClaim(void);

#endif /* J1939_DATA_LINK_H */
