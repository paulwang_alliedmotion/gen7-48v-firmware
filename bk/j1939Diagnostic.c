/*!
 * \file      j1939Diagnostics.c
 * \author    Zach Haver
 * \brief     Implementation of the J1939 diagnostics layer.
 *
 */

/* $Id: $ */
/* $Name: $ */

/* NOTICE: This file is the property of Globe Motors and contains */
/* CONFIDENTIAL information.  Any use, copying or dissemination of the */
/* information contained in this file is strictly prohibited unless */
/* authorized by Globe Motors in writing. */

/* Copyright 2016, Globe Motors, Inc. */

/*
 * Revision History:
 *
 * $Log: $
 *
 */

#include <string.h> /* for memcpy */

#include "boot.h"
#include "crc.h"
#include "dtc.h"
#include "eeprom.h"
#include "Flash2803x_API_Library.h"
#include "j1939Diagnostic.h"
#include "j1939DataLink.h"
#include "rand32.h"
#include "secure.h"
#include "sys.h"

/* private function prototypes */
static void jdiagLockSecurity(void);
static void jdiagDM7(const J1939msg_t *pmsg);
static void jdiagDM13(const J1939msg_t *pmsg);
static void jdiagMemoryTimer(void);
static void jdiagDM14Rx(const J1939msg_t *pmsg);
static void jdiagCheckSecurity(void);
static void jdiagMemProcess(void);
static void jdiagTxDM18(void);
static void jdiagRxDM18(const J1939msg_t *pmsg);
static void jdiagTxDM15(UINT_16 status, const J1939msg_t *pmsg, void (*fPass)(void));
static void jdiagDM14Erase(void);
static void jdiagMemEraseCallback(void);
static void jdiagMemEraseExecute(void);
static void jdiagDM14Read(void);
static void jdiagMemEEPROMReadCallback(void);
static void jdiagMemEEPROMReadExecute(void);
static void jdiagMemFlashReadCallback(void);
static void jdiagMemFlashReadExecute(void);
static void jdiagMemReadCompleteCallback(void);
static void jdiagDM14Write(void);
static void jdiagMemFlashCallback(void);
static void jdiagMemEEPROMCallback(void);
static void jdiagDM14Status(void);
static void jdiagDM14Complete(void);
static void jdiagDM14Failed(void);
static void jdiagDM14BootLoad(void);
static void jdiagDM14EDCP(void);
static void jdiagMemEDCPCallback(void);
static void jdiagMemEDCPExecute(void);
static void jdiagRxDM16(const J1939msg_t *pmsg);
static void jdiagMemWriteFlash(const J1939msg_t *pmsg);
static void jdiagMemWriteEEPROM(const J1939msg_t *pmsg);

/* private variables */

/*! \var memAccess - current memory access structure. */
static JDiagMemAccess_t memAccess;
static UINT_16 jdiagDM1Timeout = 0U;
/*! \var jdiagDM13TimeoutCount - counter for DM13 timeout. */
static UINT_16 jdiagDM13TimeoutCount = 0U;

/* public functions */

/*!
 * \fn    void jdiagInit(void)
 *
 * \brief This function will initialize the J1939 Diagnostic layer.
 */
void
jdiagInit(void) {
    jdiagLockSecurity();
    memAccess.beenErased = NOT_ERASED;
    memAccess.beenFlashed = NOT_FLASHED;
}

/*!
 * \fn    void jdiagMessageProcess(const J1939RxMsg_t *pmsg, const UINT_8 *data)
 *
 * \param *pmsg - received message info.
 * \param *data - received message data.
 * \brief This function will process a received diagnostic layer message.
 */
void
jdiagMessageProcess(const J1939msg_t *pmsg) {
    switch(pmsg->pgn) {
    case JDIAG_PGN_DM7:
        jdiagDM7(pmsg);
        break;
    case JDIAG_PGN_DM13:
        jdiagDM13(pmsg);
        break;
    case JDIAG_PGN_DM14:
        jdiagDM14Rx(pmsg);
        break;
    case JDIAG_PGN_DM16:
        jdiagRxDM16(pmsg);
        break;
    case JDIAG_PGN_DM18:
        jdiagRxDM18(pmsg);
        break;
    default:
        /* Unused message discard */
        break;
    }
}

/*!
 * \fn    void jdiagTimer(void)
 *
 * \brief This function provides the timebase for the diagnostic layer.
 */
void
jdiagTimer(void) {
    J1939msg_t msg;

    if (1U < memAccess.securityTime) {
        memAccess.securityTime--;
        if (1U == memAccess.securityTime) {
            jdiagLockSecurity();
        }
    }
    if (1U < jdiagDM13TimeoutCount) {
        jdiagDM13TimeoutCount--;
        if (1U == jdiagDM13TimeoutCount) {
            jdiagDM13TimeoutCount = 0U;
        }
    } else {
        if (JDIAG_DM1_PERIOD <= jdiagDM1Timeout++) {
            msg.pri = 6U;
            msg.src = JDL_GLOBAL_ADDY;
            jdiagDM1(&msg);
            jdiagDM1Timeout = 0U;
        }
    }
    jdiagMemoryTimer();
}

void
jdiagTripDM1(void) {
    jdiagDM1Timeout = JDIAG_DM1_PERIOD;
}

/*!
 * \fn    void jdiagDM1(const J1939RxMsg_t *pmsg)
 *
 * \param *pmsg - pointer to the request message for the DM1.
 * \brief This function will gather the active fault data for the DM1
 *        message and transmit a DM1 message.
 */
void
jdiagDM1(const J1939msg_t *pmsg) {
    J1939msg_t msg;
    UINT_8 data[JDL_MAX_TX_SIZE];

    msg.fp = NULL;
    msg.pri = pmsg->pri;
    msg.pgn = JDIAG_PGN_DM1;
    msg.dst = pmsg->src;
    msg.src = JDL_OUR_ADDY;
    msg.dlc = dtcLoadDataActive(data);
    msg.dIdx = jdlAddMsgData(data, msg.dlc, TX_DATA);
    jdlJ1939ToCAN(&msg);
}

/*!
 * \fn    void jdiagDM2(const J1939RxMsg_t *pmsg)
 *
 * \param *pmsg - pointer to the request message for the DM2.
 * \brief This function will gather the stored fault data for the DM2
 *        message and transmit a DM2 message.
 */
void
jdiagDM2(const J1939msg_t *pmsg) {
    J1939msg_t msg;
    UINT_8 data[JDL_MAX_TX_SIZE];

    msg.fp = NULL;
    msg.pri = pmsg->pri;
    msg.pgn = JDIAG_PGN_DM2;
    msg.dst = pmsg->src;
    msg.src = JDL_OUR_ADDY;
    msg.dlc = dtcLoadDataStored(data);
    msg.dIdx = jdlAddMsgData(data, msg.dlc, TX_DATA);
    jdlJ1939ToCAN(&msg);
}

/*!
 * \fn    void jdiagDM3(const J1939RxMsg_t *pmsg)
 *
 * \param *pmsg - pointer to the request message for the DM3.
 * \brief This function will cause the stored faults to be cleared.
 */
void
jdiagDM3(const J1939msg_t *pmsg) {
    if (JDL_OUR_ADDY == pmsg->dst) {
        dtcClearStored();
        jdlACK(JDL_PACK, pmsg);
    }
}

void
jdiagDM15Reboot(UINT_16 status) {
    J1939msg_t msg;

    msg.pri = 6U;
    msg.src = JDL_SERVICE_TOOL;
    jdiagTxDM15(status, &msg, NULL);
}

/*!
 * \fn    void jdiagLoclSecurity(void)
 *
 * \brief This function will set the memory access state machine back to locked.
 */
static void
jdiagLockSecurity(void) {
    /* reset security and end all operations */
    memAccess.securityState = JDIAG_SECURITY_LOCKED;
    memAccess.securityTime = 0U;
    memAccess.state = MEM_FREE;
    memAccess.opState = MEM_OP_FREE;
}

/*!
 * \fn    void jdiagDM7(const J1939msg_t *pmsg)
 *
 * \param *pmsg - pointer to the DM7 message.
 * \brief This function will execute requested DM7 tests.
 */
static void
jdiagDM7(const J1939msg_t *pmsg) {
    UINT_8 data[8];
    if (8U < pmsg->dlc) {
        jdlACK(JDL_NACK, pmsg);
    } else {
        if (JDL_OUR_ADDY == pmsg->dst) {
            jdlGetMsgData(data, pmsg->dIdx, pmsg->dlc, RX_DATA);
            switch (data[0]) {
            case 0U:
                jdlACK(JDL_NACK, pmsg);
                break;
            default:
                jdlACK(JDL_NACK, pmsg);
                break;
            }
        }
    }
}

/*!
 * \fn    void jdiagDM13(const J1939msg_t *pmsg)
 *
 * \param *pmsg - pointer to the DM13 message.
 * \brief This function will start/stop unsolicited bus traffic.
 */
static void
jdiagDM13(const J1939msg_t *pmsg) {
    UINT_8 data[8];

    if (8U >= pmsg->dlc) {
        jdlGetMsgData(data, pmsg->dIdx, pmsg->dlc, RX_DATA);
        if ((JDL_OUR_ADDY == pmsg->dst) || (JDL_GLOBAL_ADDY == pmsg->dst)) {
            switch (data[0] & 0xc0U) {
            case 0x00U:
                /* stop bus traffic */
                jdiagDM13TimeoutCount = JDIAG_DM13_6_SECONDS;
                break;
            case 0x40U:
                /* start bus traffic */
                jdiagDM13TimeoutCount = 0U;
                break;
            default:
                /* do nothing */
                break;
            }
        }
    }
}

static void
jdiagMemoryTimer(void) {
    switch (memAccess.opState) {
    case MEM_OP_ERASE:
        jdiagMemEraseExecute();
        break;
    case MEM_OP_READ_EEPROM:
        jdiagMemEEPROMReadExecute();
        break;
    case MEM_OP_READ_FLASH:
        jdiagMemFlashReadExecute();
        break;
    case MEM_OP_EDCP:
        jdiagMemEDCPExecute();
        break;
    case MEM_OP_FREE:
    case MEM_OP_ERASE_COMPLETE:
    case MEM_OP_ERASE_FAIL:
    case MEM_OP_WAIT_FLASH:
    case MEM_OP_WAIT_EEPROM:
    case MEM_OP_EDCP_COMPLETE:
    default:
        /* do nothing */
        break;
    }
}

/*!
 * \fn    void jdiagDM14Rx(const J1939msg_t *pmsg)
 *
 * \param *pmsg - pointer to the DM14 message.
 * \brief This function will parse an incoming DM14 message.
 */
static void
jdiagDM14Rx(const J1939msg_t *pmsg) {
    UINT_8 data[8];

    if (8U >= pmsg->dlc) {
        jdlGetMsgData(data, pmsg->dIdx, pmsg->dlc, RX_DATA);
        if ((MEM_FREE == memAccess.state) ||
            (MEM_OP_WAIT_FLASH == memAccess.opState)) {
            memAccess.length = (((UINT_16)data[1] & 0xe0U) << 3);
            memAccess.length |= (UINT_16)data[0];
            memAccess.address = (UINT_32)data[4] << 16;
            memAccess.address |= (((UINT_32)data[3] << 8) & 0xff00UL);
            memAccess.address |= ((UINT_32)data[2] & 0xffUL);
            memAccess.memSpace = data[5];
            memAccess.userLevel = ((UINT_16)data[7] << 8);
            memAccess.userLevel |= ((UINT_16)data[6] & 0xffU);
        }
        memAccess.command = (data[1] >> 1) & 0x07U;
        memAccess.msg = *pmsg;

        if (JDIAG_SECURITY_LOCKED != memAccess.securityState) {
            memAccess.securityTime = JDIAG_SECURITY_TIMEOUT;
        }

        switch (memAccess.command) {
        case JDIAG_DM14_CMD_ERASE:
        case JDIAG_DM14_CMD_READ:
        case JDIAG_DM14_CMD_WRITE:
            /* These operations all require security be unlocked. */
            jdiagCheckSecurity();
            break;
        case JDIAG_DM14_CMD_STATUS:
        case JDIAG_DM14_CMD_COMPLETE:
        case JDIAG_DM14_CMD_FAILED:
        case JDIAG_DM14_CMD_BOOT_LOAD:
        case JDIAG_DM14_CMD_EDCP:
            /* These opertions do not require security */
            jdiagMemProcess();
            break;
        default:
            jdiagLockSecurity();
            jdiagTxDM15(JDIAG_DM15_FAIL_GENERAL, pmsg, NULL);
            break;
        }
    } else {
        jdiagLockSecurity();
        jdiagTxDM15(JDIAG_DM15_FAIL_GENERAL, pmsg, NULL);
    }
}

static void
jdiagCheckSecurity(void) {
    if (JDIAG_SECURITY_LOCKED == memAccess.securityState) {
        /* we need to unlock before doing anything */
        memAccess.state = MEM_SECURITY_WAIT;
        jdiagTxDM18();
    } else {
        /* already unlocked process memory request */
        jdiagMemProcess();
    }
}

static void
jdiagMemProcess(void) {
    switch (memAccess.command) {
    case JDIAG_DM14_CMD_ERASE:
        jdiagDM14Erase();
        break;
    case JDIAG_DM14_CMD_READ:
        jdiagDM14Read();
        break;
    case JDIAG_DM14_CMD_WRITE:
        jdiagDM14Write();
        break;
    case JDIAG_DM14_CMD_STATUS:
        jdiagDM14Status();
        break;
    case JDIAG_DM14_CMD_COMPLETE:
        jdiagDM14Complete();
        break;
    case JDIAG_DM14_CMD_FAILED:
        jdiagDM14Failed();
        break;
    case JDIAG_DM14_CMD_BOOT_LOAD:
        jdiagDM14BootLoad();
        break;
    case JDIAG_DM14_CMD_EDCP:
        jdiagDM14EDCP();
        break;
    default:
        jdiagLockSecurity();
        jdiagTxDM15(JDIAG_DM15_FAIL_GENERAL, &memAccess.msg, NULL);
        break;
    }
}

static void
jdiagTxDM18(void) {
    J1939msg_t dm18;
    UINT_8 data[8U];

    memAccess.seed = (UINT_32)rand32();

    dm18.fp = NULL;
    dm18.pri = memAccess.msg.pri;
    dm18.pgn = JDIAG_PGN_DM18;
    dm18.dst = memAccess.msg.src;
    dm18.src = JDL_OUR_ADDY;
    dm18.dlc = 8U;
    data[0] = 0x40U;
    data[1] = 0x00U;
    data[2] = (UINT_8)memAccess.seed & 0xffU;
    data[3] = (UINT_8)(memAccess.seed >> 8) & 0xffU;
    data[4] = (UINT_8)(memAccess.seed >> 16) & 0xffU;
    data[5] = (UINT_8)(memAccess.seed >> 24) & 0xffU;
    data[6] = 0xffU;
    data[7] = 0xffU;
    dm18.dIdx = jdlAddMsgData(data, dm18.dlc, TX_DATA);
    jdlJ1939ToCAN(&dm18);
}

static void
jdiagRxDM18(const J1939msg_t *pmsg) {
    UINT_8 data[8U];
    UINT_32 rxKey;

    if ((8U == pmsg->dlc) && (MEM_SECURITY_WAIT == memAccess.state)) {
        jdlGetMsgData(data, pmsg->dIdx, pmsg->dlc, RX_DATA);
        rxKey = (UINT_32)data[5];
        rxKey <<= 8;
        rxKey |= (UINT_32)data[4];
        rxKey <<= 8;
        rxKey |= (UINT_32)data[3];
        rxKey <<= 8;
        rxKey |= (UINT_32)data[2];
        if (rxKey == secureSeed2Key(memAccess.seed)) {
            memAccess.securityState = JDIAG_SECURITY_L1;
            memAccess.securityTime = JDIAG_SECURITY_TIMEOUT;
            memAccess.state = MEM_FREE;
            jdiagMemProcess();
        } else {
            jdiagLockSecurity();
            jdiagTxDM15(JDIAG_DM15_FAIL_INVALID_KEY, &memAccess.msg, NULL);
        }
    } else {
        jdiagLockSecurity();
        jdiagTxDM15(JDIAG_DM15_FAIL_SECURITY, &memAccess.msg, NULL);
    }
}

static void
jdiagTxDM15(UINT_16 status, const J1939msg_t *pmsg, void (*fPass)(void)) {
    J1939msg_t dm15;
    UINT_8 data[8];

    dm15.fp = fPass;
    dm15.pri = pmsg->pri;
    dm15.pgn = JDIAG_PGN_DM15;
    dm15.dst = pmsg->src;
    dm15.src = JDL_OUR_ADDY;
    dm15.dlc = 8U;
    data[0] = memAccess.length & 0xffU;
    data[1] = ((memAccess.length >> 3) & 0xe0U) | 0x11U;
    data[2] = (UINT_8)(status & 0xffU);
    data[3] = (UINT_8)(status >> 8);
    data[4] = 0U;
    data[5] = memAccess.memSpace;
    if (JDIAG_SECURITY_LOCKED == memAccess.securityState) {
        data[6] = 1U;
        data[7] = 0U;
    } else {
        data[6] = 0xffU;
        data[7] = 0xffU;
    }

    switch (status) {
    case JDIAG_DM15_PROCEED:
        break;
    case JDIAG_DM15_COMPLETE:
        data[1] |= 0x8U;
        data[2] = (UINT_8)memAccess.address & 0xffU;
        data[3] = (UINT_8)(memAccess.address >> 8) & 0xffU;
        data[4] = (UINT_8)(memAccess.address >> 16) & 0xffU;
        break;
    case JDIAG_DM15_BUSY_GENERIC:
    case JDIAG_DM15_BUSY_ERASE:
    case JDIAG_DM15_BUSY_READ:
    case JDIAG_DM15_BUSY_WRITE:
    case JDIAG_DM15_BUSY_EDCP:
        data[1] |= 0x2U;
        break;
    case JDIAG_DM15_FAIL_GENERAL:
    case JDIAG_DM15_FAIL_ADDRESS:
    case JDIAG_DM15_FAIL_INVALID_KEY:
    case JDIAG_DM15_FAIL_SECURITY:
    default:
        data[1] |= 0xaU;
        break;
    }
    dm15.dIdx = jdlAddMsgData(data, dm15.dlc, TX_DATA);
    jdlJ1939ToCAN(&dm15);
}

static void
jdiagDM14Erase(void) {
    UINT_16 status = JDIAG_DM15_FAIL_GENERAL;
    void (*cb)(void) = NULL;

    if (JDIAG_SECURITY_LOCKED != memAccess.securityState) {
        if (MEM_FREE != memAccess.state) {
            status = JDIAG_DM15_BUSY_GENERIC;
        } else if (JDIAG_MEMSPACE_FLASH != memAccess.memSpace) {
            status = JDIAG_DM15_FAIL_ADDRESS;
        } else {
            status = JDIAG_DM15_BUSY_ERASE;
            cb = jdiagMemEraseCallback;
            memAccess.state = MEM_OPERATING;
        }
    }
    jdiagTxDM15(status, &memAccess.msg, cb);
}

static void
jdiagMemEraseCallback(void) {
    memAccess.opState = MEM_OP_ERASE;
}

static void
jdiagMemEraseExecute(void) {
    UINT_16 status;
    FLASH_ST flashStatus;

    memAccess.beenErased = NOT_ERASED;
    memAccess.beenFlashed = NOT_FLASHED;
    /* corrupt the application CRC */
    (void)eepromWriteWord(EEADDR_APP_CRC_L, 0xffffU);
    /* clear the boot count */
    (void)eepromWriteWord(EEADDR_BOOT_COUNT, 0U);
    /* erase */
    PICCOLO_DINT();
    status = Flash_Erase(JDIAG_FLASH_SECTORS, &flashStatus);
    PICCOLO_EINT();
    if (STATUS_SUCCESS == (SINT_16)status) {
        memAccess.opState = MEM_OP_ERASE_COMPLETE;
        memAccess.beenErased = ERASED;
    } else {
        memAccess.opState = MEM_OP_ERASE_FAIL;
    }
}

static void
jdiagDM14Read(void) {
    UINT_16 status = JDIAG_DM15_FAIL_SECURITY;
    void (*cb)(void) = NULL;
    UINT_32 finalAddress = memAccess.address + memAccess.length;

    if (JDIAG_SECURITY_L1 != memAccess.securityState) {
        status = JDIAG_DM15_FAIL_SECURITY;
    } else if (JDIAG_MAX_READ_LENGTH < memAccess.length) {
        status = JDIAG_DM15_FAIL_GENERAL;
    } else if (MEM_OP_FREE != memAccess.opState) {
        status = JDIAG_DM15_FAIL_GENERAL;
    } else {
        switch (memAccess.memSpace) {
        case JDIAG_MEMSPACE_FLASH:
            if (JDIAG_FLASH_ADDRESS_LIMIT < finalAddress) {
                status = JDIAG_DM15_FAIL_ADDRESS;
            } else {
                memAccess.state = MEM_OPERATING;
                status = JDIAG_DM15_PROCEED;
                cb = jdiagMemFlashReadCallback;
            }
            break;
        case JDIAG_MEMSPACE_EEPROM:
            if (JDIAG_EEPROM_ADDRESS_LIMIT < finalAddress) {
                status = JDIAG_DM15_FAIL_ADDRESS;
            } else {
                memAccess.state = MEM_OPERATING;
                status = JDIAG_DM15_PROCEED;
                cb = jdiagMemEEPROMReadCallback;
            }
            break;
        default:
            status = JDIAG_DM15_FAIL_ADDRESS;
            break;
        }
    }
    jdiagTxDM15(status, &memAccess.msg, cb);
}

static void
jdiagMemEEPROMReadCallback(void) {
    memAccess.opState = MEM_OP_READ_EEPROM;
}

static void
jdiagMemEEPROMReadExecute(void) {
    J1939msg_t dm16;
    UINT_8 data[JDL_MAX_TX_SIZE];
    UINT_8 pageData[32];
    UINT_16 bytes = memAccess.length;
    UINT_16 address = (UINT_16)memAccess.address;
    UINT_16 idx = 0U;
    UINT_8 err = 0U;

    data[idx] = 0U;
    idx++;
    while ((0U != bytes) && (0U == err)) {
        if (32U <= bytes) {
            if (EE_ERR_NONE != eepromReadXBytes(address, pageData, 32U)) {
                err = 1U;
                jdiagTxDM15(JDIAG_DM15_FAIL_DATA, &memAccess.msg, NULL);
            } else {
                memcpy(&data[idx], pageData, 32U);
                idx += 32U;
                address += 32U;
                bytes -= 32U;
            }
        } else {
            if (EE_ERR_NONE != eepromReadXBytes(address, pageData, bytes)) {
                err = 1U;
                jdiagTxDM15(JDIAG_DM15_FAIL_ADDRESS, &memAccess.msg, NULL);
            } else {
                memcpy(&data[idx], pageData, bytes);
                idx += bytes;
                bytes = 0U;
            }
        }
    }
    if (0U == err) {
        dm16.fp = jdiagMemReadCompleteCallback;
        dm16.pri = memAccess.msg.pri;
        dm16.pgn = JDIAG_PGN_DM16;
        dm16.dst = memAccess.msg.src;
        dm16.src = JDL_OUR_ADDY;
        dm16.dlc = idx;
        dm16.dIdx = jdlAddMsgData(data, dm16.dlc, TX_DATA);
        memAccess.opState = MEM_OP_FREE;
        jdlJ1939ToCAN(&dm16);
    }
}

static void
jdiagMemFlashReadCallback(void) {
    memAccess.opState = MEM_OP_READ_FLASH;
}

static void
jdiagMemFlashReadExecute(void) {
    J1939msg_t dm16;
    UINT_8 data[JDL_MAX_TX_SIZE];
    UINT_16 bytes = memAccess.length;
    UINT_32 castAddress = memAccess.address + JDIAG_FLASH_START;
    /*lint -save -e923 */
    const UINT_16 *flash = (UINT_16 *)castAddress;
    /*lint -restore */
    UINT_16 idx = 0U;
    UINT_16 temp;
    UINT_16 x;

    data[idx] = 0U;
    idx++;
    for (x = 0U; x < (bytes >> 1); x++) {
         temp = flash[x];
         data[idx] = (temp & 0xffU);
         idx++;
         data[idx] = (temp >> 8);
         idx++;
    }
    dm16.fp = jdiagMemReadCompleteCallback;
    dm16.pri = memAccess.msg.pri;
    dm16.pgn = JDIAG_PGN_DM16;
    dm16.dst = memAccess.msg.src;
    dm16.src = JDL_OUR_ADDY;
    dm16.dlc = idx;
    dm16.dIdx = jdlAddMsgData(data, dm16.dlc, TX_DATA);
    memAccess.opState = MEM_OP_FREE;
    jdlJ1939ToCAN(&dm16);
}

static void
jdiagMemReadCompleteCallback(void) {
    memAccess.state = MEM_FREE;
    jdiagTxDM15(JDIAG_DM15_COMPLETE, &memAccess.msg, NULL);
}

static void
jdiagDM14Write(void) {
    UINT_16 status = JDIAG_DM15_FAIL_DATA;
    void (*cb)(void) = NULL;
    UINT_32 finalAddress;

    if (JDIAG_MAX_WRITE_LENGTH > memAccess.length) {
        switch (memAccess.memSpace) {
        case JDIAG_MEMSPACE_FLASH:
            finalAddress = memAccess.length + memAccess.address;
            if (JDIAG_SECURITY_LOCKED == memAccess.securityState) {
                status = JDIAG_DM15_FAIL_SECURITY;
            } else if (JDIAG_FLASH_ADDRESS_LIMIT < finalAddress) {
                status = JDIAG_DM15_FAIL_ADDRESS;
            } else if (NOT_ERASED == memAccess.beenErased) {
                status = JDIAG_DM15_FAIL_GENERAL;
            } else if ((MEM_OP_FREE == memAccess.opState) ||
                       (MEM_OP_WAIT_FLASH == memAccess.opState)) {
                memAccess.state = MEM_OPERATING;
                status = JDIAG_DM15_PROCEED;
                cb = jdiagMemFlashCallback;
            } else {
                status = JDIAG_DM15_FAIL_GENERAL;
            }
            break;
        case JDIAG_MEMSPACE_EEPROM:
            finalAddress = memAccess.length + memAccess.address;
            if (JDIAG_SECURITY_L1 != memAccess.securityState) {
                status = JDIAG_DM15_FAIL_SECURITY;
            } else if (JDIAG_EEPROM_ADDRESS_LIMIT < finalAddress) {
                status = JDIAG_DM15_FAIL_ADDRESS;
            } else if ((MEM_OP_FREE == memAccess.opState) ||
                       (MEM_OP_WAIT_EEPROM == memAccess.opState)) {
                memAccess.state = MEM_OPERATING;
                status = JDIAG_DM15_PROCEED;
                cb = jdiagMemEEPROMCallback;
            } else {
                status = JDIAG_DM15_FAIL_GENERAL;
            }
            break;
        default:
            /* do nothing */
            break;
        }
    }
    jdiagTxDM15(status, &memAccess.msg, cb);
}

static void
jdiagMemFlashCallback(void) {
    memAccess.opState = MEM_OP_WAIT_FLASH;
}

static void
jdiagMemEEPROMCallback(void) {
    memAccess.opState = MEM_OP_WAIT_EEPROM;
}

static void
jdiagDM14Status(void) {
    if (((JDIAG_MEMSPACE_BOOT == memAccess.memSpace) &&
        (0x10U == memAccess.address)) ||
        (JDIAG_MEMSPACE_EEPROM == memAccess.memSpace)) {
        /* security request */
        if (JDIAG_SECURITY_LOCKED == memAccess.securityState) {
            memAccess.state = MEM_SECURITY_WAIT;
            jdiagTxDM18();
        } else {
            /* send complete */
            jdiagTxDM15(JDIAG_DM15_COMPLETE, &memAccess.msg, NULL);
        }
    } else {
        /* send status response */
        switch (memAccess.opState) {
        case MEM_OP_FREE:
        case MEM_OP_ERASE_COMPLETE:
            memAccess.state = MEM_FREE;
            memAccess.opState = MEM_OP_FREE;
            jdiagTxDM15(JDIAG_DM15_COMPLETE, &memAccess.msg, NULL);
            break;
        case MEM_OP_ERASE:
            jdiagTxDM15(JDIAG_DM15_BUSY_ERASE, &memAccess.msg, NULL);
            break;
        case MEM_OP_ERASE_FAIL:
            memAccess.state = MEM_FREE;
            jdiagTxDM15(JDIAG_DM15_FAIL_GENERAL, &memAccess.msg, NULL);
            break;
        case MEM_OP_WAIT_FLASH:
        case MEM_OP_WAIT_EEPROM:
            jdiagTxDM15(JDIAG_DM15_BUSY_WRITE, &memAccess.msg, NULL);
            break;
        case MEM_OP_READ_EEPROM:
        case MEM_OP_READ_FLASH:
            jdiagTxDM15(JDIAG_DM15_BUSY_READ, &memAccess.msg, NULL);
            break;
        case MEM_OP_EDCP:
            jdiagTxDM15(JDIAG_DM15_BUSY_EDCP, &memAccess.msg, NULL);
            break;
        case MEM_OP_EDCP_COMPLETE:
            memAccess.state = MEM_FREE;
            memAccess.opState = MEM_OP_FREE;
            memAccess.address = memAccess.crc;
            jdiagTxDM15(JDIAG_DM15_COMPLETE, &memAccess.msg, NULL);
            break;
        default:
            /* do nothing */
            break;
        }
    }
}

static void
jdiagDM14Complete(void) {
    jdiagLockSecurity();
}

static void
jdiagDM14Failed(void) {
    jdiagLockSecurity();
}

static void
jdiagDM14BootLoad(void) {
    UINT_16 status = JDIAG_DM15_BUSY_GENERIC;
    UINT_16 type = BOOT_RESET;

    if (MEM_FREE == memAccess.state) {
        if (JDIAG_MEMSPACE_BOOT == memAccess.memSpace) {
            if (JDIAG_BOOT_ADDRESS == memAccess.address) {
                type = BOOT_REQUESTED;
            }
            if (EE_ERR_NONE == eepromWriteWord(EEADDR_BOOT_OPTIONS, type)) {
                sysReset();
            }
        } else {
            status = JDIAG_DM15_FAIL_ADDRESS;
        }
    }
    jdiagTxDM15(status, &memAccess.msg, NULL);
}

static void
jdiagDM14EDCP(void) {
    UINT_16 status = JDIAG_DM15_FAIL_GENERAL;
    void (*cb)(void) = NULL;

    if ((FLASHED == memAccess.beenFlashed) &&
        (MEM_OP_FREE == memAccess.opState)) {
        if (JDIAG_MEMSPACE_FLASH != memAccess.memSpace) {
            status = JDIAG_DM15_FAIL_ADDRESS;
        } else {
            memAccess.state = MEM_OPERATING;
            status = JDIAG_DM15_BUSY_EDCP;
            cb = jdiagMemEDCPCallback;
        }
    }
    jdiagTxDM15(status, &memAccess.msg, cb);
}

static void
jdiagMemEDCPCallback(void) {
    memAccess.opState = MEM_OP_EDCP;
}

static void
jdiagMemEDCPExecute(void) {
    memAccess.crc = crcVerifyBlockFlash(CRC_FLASH_START, CRC_FLASH_LENGTH);
    (void)eepromWriteDWord(EEADDR_APP_CRC_L, memAccess.crc);
    memAccess.opState = MEM_OP_EDCP_COMPLETE;
}

static void
jdiagRxDM16(const J1939msg_t *pmsg) {
    if ((MEM_OP_WAIT_FLASH == memAccess.opState) ||
        (MEM_OP_WAIT_EEPROM == memAccess.opState)) {
        switch (memAccess.memSpace) {
        case JDIAG_MEMSPACE_FLASH:
            jdiagMemWriteFlash(pmsg);
            break;
        case JDIAG_MEMSPACE_EEPROM:
            jdiagMemWriteEEPROM(pmsg);
            break;
        default:
            jdiagTxDM15(JDIAG_DM15_FAIL_ADDRESS, pmsg, NULL);
            break;
        }
    } else {
        jdiagTxDM15(JDIAG_DM15_FAIL_GENERAL, pmsg, NULL);
    }
}

static void
jdiagMemWriteFlash(const J1939msg_t *pmsg) {
    UINT_16 status;
    UINT_16 ramBuf[JDL_MAX_RX_SIZE >> 1];
    UINT_32 address = ((memAccess.address >> 1) + JDIAG_FLASH_START);
    /*lint -save -e923 */
    UINT_16 *flashAddy = (UINT_16 *)address;
    /*lint -restore */
    UINT_16 dataLength = memAccess.length >> 1;
    FLASH_ST fl_st;

    memAccess.crc = jdlGetRAMBUFandCRC(ramBuf, pmsg->dIdx, pmsg->dlc);

    PICCOLO_DINT();
    status = Flash_Program(flashAddy, ramBuf, (UINT_32)dataLength, &fl_st);
    status |= Flash_Verify(flashAddy, ramBuf, (UINT_32)dataLength, &fl_st);
    PICCOLO_EINT();
    if (STATUS_SUCCESS ==  (SINT_16)status) {
        memAccess.beenFlashed = FLASHED;
        status = JDIAG_DM15_COMPLETE;
    } else {
        status = JDIAG_DM15_FAIL_GENERAL;
    }
    jdiagTxDM15(status, pmsg, NULL);
}

static void
jdiagMemWriteEEPROM(const J1939msg_t *pmsg) {
    UINT_8 data[JDL_MAX_RX_SIZE];
    UINT_16 dataLength = memAccess.length;
    UINT_16 pages;
    UINT_16 words;
    UINT_16 status = JDIAG_DM15_COMPLETE;
    UINT_16 x;
    UINT_16 y;
    UINT_16 word;
    UINT_16 idx = 1U;
    UINT_8 pageBuf[16];
    UINT_16 eeAddy = (UINT_16)memAccess.address & 0x07fffU;

    jdlGetMsgData(data, pmsg->dIdx, pmsg->dlc, RX_DATA);

    if (((dataLength % 16U) == 0U) && (16U <= dataLength)) {
        /* page writes can be used */
        pages = dataLength >> 4;
        words = (dataLength % 16U) >> 1;
    } else {
        /* have to use word writes */
        pages = 0U;
        words = dataLength >> 1;
    }

    for (x = 0U; ((x < pages) && (status != JDIAG_DM15_FAIL_GENERAL)); x++) {
        /* copy data and swap bytes */
        for (y = 0U; y < 8U; y++) {
            word = (y * 2U) + 1U;
            pageBuf[word] = data[idx];
            word--;
            idx++;
            pageBuf[word] = data[idx];
            idx++;
        }
        if (EE_ERR_NONE != eepromWrite16Bytes(eeAddy, pageBuf)) {
            status = JDIAG_DM15_FAIL_GENERAL;
        }
        eeAddy += 16U;
    }
    for (x = 0U; ((x < words) && (status != JDIAG_DM15_FAIL_GENERAL)); x++) {
        word = ((UINT_16)data[idx] & 0xffU);
        idx++;
        word |= ((UINT_16)data[idx] << 8);
        idx++;
        if (EE_ERR_NONE != eepromWriteWord(eeAddy, word)) {
            status = JDIAG_DM15_FAIL_GENERAL;
        }
        eeAddy += 2U;
    }
    jdiagTxDM15(status, pmsg, NULL);
}
