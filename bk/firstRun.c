/*!
 * \file    firstRun.c
 * \author  Zach Haver
 * \brief   Implmentation for manufacturing to set default values and jump to
 *          JTAG programmed app instead of normal procedure.
 */

/* $Id: $ */
/* $Name: $ */

/* NOTICE: This file is the property of Globe Motors and contains */
/* CONFIDENTIAL information.  Any use, copying or dissemination of the */
/* information contained in this file is strictly prohibited unless */
/* authorized by Globe Motors in writing. */

/* Copyright 2017, Globe Motors, Inc. */

/*
 * Revision History:
 *
 * $Log: $
 *
 */

#include "boot.h"
#include "crc.h"
#include "eeprom.h"
#include "firstRun.h"
#include "fkernel.h"
#include "misra.h"

void
firstRun(void) {
    /*lint -save -e923 */
    UINT_16 appStart = *((UINT_16 *) 0x3e8000UL);
    /*lint -restore */
    UINT_32 crc;
    UINT_8 defaultData[16] = {0x2dU, 0x58U, 0x58U, 0x58U, 0x58U, 0x58U, 0x0U, 0x0U, 0x0U, 0x0U, 0x0U, 0x0U, 0x0U, 0x0U, 0x0U, 0x0U };

    /* check first flash address for non-empty value, this can be used as
     * an indicator that the application has also been JTAG flashed */
    if (0xffffU != appStart) {
        /* likely JTAG flashed calc and eeprom save crc for the app */
        (void)eepromWriteDWord(0x20aU, 0x442d5959UL);
        (void)eepromWriteWord(0x20eU, 0x4444U);
        (void)eepromWrite16Bytes(0x210U, defaultData);
        crc = crcVerifyBlockFlash(CRC_BOOT_START, CRC_BOOT_LENGTH);
        (void)eepromWriteDWord(EEADDR_BK_CRC_L, crc);
        crc = crcVerifyBlockFlash(CRC_FLASH_START, CRC_FLASH_LENGTH);
        if (EE_ERR_NONE == eepromWriteDWord(EEADDR_APP_CRC_L, crc)) {
            /* overwrite firtRun */
            (void)eepromWriteWord(EEADDR_FIRSTRUN, 0U);
            BOOT_TO_APP;
        } else {
            fkernelRun(FK_REQUESTED, BOOT_REQUESTED);
        }
    } else {
        fkernelRun(FK_REQUESTED, BOOT_REQUESTED);
    }
}
