/*!
 * \file    sched.c
 * \author  Zach Haver
 * \brief   Task Scheduler
 *
 *  This file contains function that handle the fixed priority
 *  task execution set up in the schedule. Tasks that are READY
 *  will run when there appropriate timeout has been reached.
 *  Until that time the task will be considered RUNNING and not
 *  use any processor time. Otherwise the tasks are considered
 *  SLEEPING when turned off and will not use any processor time.
 *  If no task is ready to run the idle task will be ran which
 *  puts the processor in an idle state until an interrupt is
 *  generated.
 */

/* $Id: $ */
/* $Name: $ */

/* NOTICE: This file is the property of Globe Motors and contains */
/* CONFIDENTIAL information.  Any use, copying or dissemination of the */
/* information contained in this file is strictly prohibited unless */
/* authorized by Globe Motors in writing. */

/* Copyright 2016, Globe Motors, Inc. */

/*
 * Revision History:
 *
 * $Log: $
 *
 */

#include "misra.h"
#include "sched.h"

/* private function declarations */

static void schedIdleTask(void);

/* private variable declarations */
/*! \var process - array of tasks to be executed. */
static task_t process[SCHED_TASK_MAX];
/*! \var processCount - number of tasks in process. */
static UINT_16 processCount = 0U;

/* public functions */

/*!
 * \fn    void schedInit(void)
 *
 * \breif This function fill process with empty tasks so no tasks
 *        exist at start up.
 */
void
schedInit(void) {
    UINT_16 x;

    /* Initialize the task list to safe values */
    for (x = 0U; x < SCHED_TASK_MAX; x++) {
        process[x].tp = schedIdleTask;
        process[x].state = SCHED_TASK_SLEEPING;
        process[x].sleepCtr = 0U;
        process[x].period = 0U;
    }
    processCount = 0U;
}

/*!
 * \fn    void schedExec(void)
 *
 * \brief This function executed tasks that are ready.
 */
void
schedExec(void) {
    UINT_16 x;

    /* Search for a task to execute */
    for (x = 0U; x < processCount; x++) {
        if (SCHED_TASK_READY == process[x].state) {
            process[x].state = SCHED_TASK_RUNNING;
            (*(process[x].tp)) ();
            process[x].state = SCHED_TASK_SLEEPING;
            process[x].sleepCtr = process[x].period;
        }
    }
    /* No tasks are ready execute idle transition */
    if (processCount == x) {
        asm(" IDLE");
    }
}

/*!
 * \fn    void schedTick(void)
 *
 * \brief This function kicks the timebase counter of the scheduler.
 */
void
schedTick(void) {
    UINT_16 x;

    for (x = 0U; x < processCount; x++) {
        switch (process[x].sleepCtr) {
        case 0U:
            /* Do nothing we are already READY */
            break;
        case 1U:
            process[x].sleepCtr--;
            process[x].state = SCHED_TASK_READY;
            break;
        default:
            process[x].sleepCtr--;
            break;
        }
    }
}

/*!
 * \fn    void schedAddTask(void (*tp)(void), UINT_16 period)
 *
 * \param *tp - function pointer to the item to be executed.
 * \param period - timebase of the function pointed to by tp.
 * \brief This function adds to process the passed task.
 */
void
schedAddTask(void (*tp)(void), UINT_16 period) {
    process[processCount].tp = tp;
    process[processCount].state = SCHED_TASK_SLEEPING;
    process[processCount].period = period;
    process[processCount].sleepCtr = period;
    processCount++;
}

/*!
 * \fn    void schedIdleTask(void)
 *
 * \brief Default task that just idles the processor.
 */
static void
schedIdleTask(void) {
    asm(" IDLE");
}
