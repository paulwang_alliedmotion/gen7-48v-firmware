/*!
 * \file    tmr0.h
 * \author  Zach Haver
 * \brief   tmr0 definitions
 *
 * This file contains the tmr0 definitions.
 */

/* $Id: $ */
/* $Name: $ */

/* NOTICE: This file is the property of Globe Motors and contains */
/* CONFIDENTIAL information.  Any use, copying or dissemination of the */
/* information contained in this file is strictly prohibited unless */
/* authorized by Globe Motors in writing. */

/* Copyright 2016, Globe Motors, Inc. */

/*
 * Revision History:
 *
 * $Log: $
 *
 */

#ifndef TMR0_H
#define TMR0_H

void tmr0Init(void);
void tmr0ISR(void);

#endif /* TMR0_H */
