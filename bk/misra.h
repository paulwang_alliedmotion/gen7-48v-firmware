/*!
 * \file      misra.h
 * \author    Zach Haver
 * \brief     misra type definitions
 */

/* $Id: $ */
/* $Name: $ */

/* NOTICE: This file is the property of Globe Motors and contains */
/* CONFIDENTIAL information.  Any use, copying or dissemination of the */
/* information contained in this file is strictly prohibited unless */
/* authorized by Globe Motors in writing. */

/* Copyright 2016, Globe Motors, Inc. */

/*
 * Revision History:
 *
 * $Log: $
 *
 */

#ifndef MISRA_H
#define MISRA_H

/* Limit definitions.  These are specific to the typedefs below. */
#define SINT_16_MAX            32767
#define SINT_16_MIN (-SINT_16_MAX-1)
#define UINT_16_MAX           65535U

#define SINT_32_MAX      2147483647L
#define SINT_32_MIN (-SINT_32_MAX-1L)
#define UINT_32_MAX     4294967295UL

#if defined (UNIT_TESTS)
/* typedefs for gcc on IA32 */
typedef signed char SINT_8;
typedef unsigned char UINT_8;
typedef signed short int SINT_16;
typedef unsigned short int UINT_16;
typedef signed int SINT_32;
typedef unsigned int UINT_32;
typedef float FLOAT_32;
#else
/* typedefs for the TI 320LC/F280xA */
typedef char CHAR_8;
typedef signed char SINT_8;
typedef unsigned char UINT_8;
typedef signed int SINT_16;
typedef unsigned int UINT_16;
typedef signed long SINT_32;
typedef unsigned long UINT_32;
typedef float FLOAT_32;
typedef long double FLOAT_64;

/* typedef interrupt void(*PINT)(void); */
typedef interrupt void(*PINT)(void);
#endif /* UNIT_TETS */
#endif /* MISRA_H */
