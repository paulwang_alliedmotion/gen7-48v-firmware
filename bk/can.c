/*!
 * \file      can.c
 * \author    Zach Haver
 * \brief     eCAN module initialization and functions
 *            for the TI TMS320C28035 Piccolo.
 */

/* $Id: $ */
/* $Name: $ */

/* NOTICE: This file is the property of Globe Motors and contains */
/* CONFIDENTIAL information.  Any use, copying or dissemination of the */
/* information contained in this file is strictly prohibited unless */
/* authorized by Globe Motors in writing. */

/* Copyright 2016, Globe Motors, Inc. */

/*
 * Revision History:
 *
 * $Log: $
 *
 */

#include "can.h"
#include "j1939DataLink.h"
#include "misra.h"
#include "piccolo.h"

/* private function declarations */

/* private variable declarations */
/*!
 * \brief    This is a look up table which contains the register locations
 *           for the eCANA mailbox control and data registers indexed by
 *           mailbox number
 */
static const struct {
    volatile UINT_32 *pId;          /*!< MSGIDn */
    volatile UINT_32 *pLAM;         /*!< LAMn */
    volatile UINT_32 *pCtrl;        /*!< MSGCTRLn */
    volatile UINT_16 *pMBXA;        /*!< MBXnA */
    volatile UINT_16 *pMBXB;        /*!< MBXnB */
    volatile UINT_16 *pMBXC;        /*!< MBXnC */
    volatile UINT_16 *pMBXD;        /*!< MBXnD */
} eCANA[32U] = {
    { ECANA_MSGID00, ECANA_LAM00, ECANA_MSGCTRL00, ECANA_MBX00A,
      ECANA_MBX00B, ECANA_MBX00C, ECANA_MBX00D, },
    { ECANA_MSGID01, ECANA_LAM01, ECANA_MSGCTRL01, ECANA_MBX01A,
      ECANA_MBX01B, ECANA_MBX01C, ECANA_MBX01D, },
    { ECANA_MSGID02, ECANA_LAM02, ECANA_MSGCTRL02, ECANA_MBX02A,
      ECANA_MBX02B, ECANA_MBX02C, ECANA_MBX02D, },
    { ECANA_MSGID03, ECANA_LAM03, ECANA_MSGCTRL03, ECANA_MBX03A,
      ECANA_MBX03B, ECANA_MBX03C, ECANA_MBX03D, },
    { ECANA_MSGID04, ECANA_LAM04, ECANA_MSGCTRL04, ECANA_MBX04A,
      ECANA_MBX04B, ECANA_MBX04C, ECANA_MBX04D, },
    { ECANA_MSGID05, ECANA_LAM05, ECANA_MSGCTRL05, ECANA_MBX05A,
      ECANA_MBX05B, ECANA_MBX05C, ECANA_MBX05D, },
    { ECANA_MSGID06, ECANA_LAM06, ECANA_MSGCTRL06, ECANA_MBX06A,
      ECANA_MBX06B, ECANA_MBX06C, ECANA_MBX06D, },
    { ECANA_MSGID07, ECANA_LAM07, ECANA_MSGCTRL07, ECANA_MBX07A,
      ECANA_MBX07B, ECANA_MBX07C, ECANA_MBX07D, },
    { ECANA_MSGID08, ECANA_LAM08, ECANA_MSGCTRL08, ECANA_MBX08A,
      ECANA_MBX08B, ECANA_MBX08C, ECANA_MBX08D, },
    { ECANA_MSGID09, ECANA_LAM09, ECANA_MSGCTRL09, ECANA_MBX09A,
      ECANA_MBX09B, ECANA_MBX09C, ECANA_MBX09D, },
    { ECANA_MSGID10, ECANA_LAM10, ECANA_MSGCTRL10, ECANA_MBX10A,
      ECANA_MBX10B, ECANA_MBX10C, ECANA_MBX10D, },
    { ECANA_MSGID11, ECANA_LAM11, ECANA_MSGCTRL11, ECANA_MBX11A,
      ECANA_MBX11B, ECANA_MBX11C, ECANA_MBX11D, },
    { ECANA_MSGID12, ECANA_LAM12, ECANA_MSGCTRL12, ECANA_MBX12A,
      ECANA_MBX12B, ECANA_MBX12C, ECANA_MBX12D, },
    { ECANA_MSGID13, ECANA_LAM13, ECANA_MSGCTRL13, ECANA_MBX13A,
      ECANA_MBX13B, ECANA_MBX13C, ECANA_MBX13D, },
    { ECANA_MSGID14, ECANA_LAM14, ECANA_MSGCTRL14, ECANA_MBX14A,
      ECANA_MBX14B, ECANA_MBX14C, ECANA_MBX14D, },
    { ECANA_MSGID15, ECANA_LAM15, ECANA_MSGCTRL15, ECANA_MBX15A,
      ECANA_MBX15B, ECANA_MBX15C, ECANA_MBX15D, },
    { ECANA_MSGID16, ECANA_LAM16, ECANA_MSGCTRL16, ECANA_MBX16A,
      ECANA_MBX16B, ECANA_MBX16C, ECANA_MBX16D, },
    { ECANA_MSGID17, ECANA_LAM17, ECANA_MSGCTRL17, ECANA_MBX17A,
      ECANA_MBX17B, ECANA_MBX17C, ECANA_MBX17D, },
    { ECANA_MSGID18, ECANA_LAM18, ECANA_MSGCTRL18, ECANA_MBX18A,
      ECANA_MBX18B, ECANA_MBX18C, ECANA_MBX18D, },
    { ECANA_MSGID19, ECANA_LAM19, ECANA_MSGCTRL19, ECANA_MBX19A,
      ECANA_MBX19B, ECANA_MBX19C, ECANA_MBX19D, },
    { ECANA_MSGID20, ECANA_LAM20, ECANA_MSGCTRL20, ECANA_MBX20A,
      ECANA_MBX20B, ECANA_MBX20C, ECANA_MBX20D, },
    { ECANA_MSGID21, ECANA_LAM21, ECANA_MSGCTRL21, ECANA_MBX21A,
      ECANA_MBX21B, ECANA_MBX21C, ECANA_MBX21D, },
    { ECANA_MSGID22, ECANA_LAM22, ECANA_MSGCTRL22, ECANA_MBX22A,
      ECANA_MBX22B, ECANA_MBX22C, ECANA_MBX22D, },
    { ECANA_MSGID23, ECANA_LAM23, ECANA_MSGCTRL23, ECANA_MBX23A,
      ECANA_MBX23B, ECANA_MBX23C, ECANA_MBX23D, },
    { ECANA_MSGID24, ECANA_LAM24, ECANA_MSGCTRL24, ECANA_MBX24A,
      ECANA_MBX24B, ECANA_MBX24C, ECANA_MBX24D, },
    { ECANA_MSGID25, ECANA_LAM25, ECANA_MSGCTRL25, ECANA_MBX25A,
      ECANA_MBX25B, ECANA_MBX25C, ECANA_MBX25D, },
    { ECANA_MSGID26, ECANA_LAM26, ECANA_MSGCTRL26, ECANA_MBX26A,
      ECANA_MBX26B, ECANA_MBX26C, ECANA_MBX26D, },
    { ECANA_MSGID27, ECANA_LAM27, ECANA_MSGCTRL27, ECANA_MBX27A,
      ECANA_MBX27B, ECANA_MBX27C, ECANA_MBX27D, },
    { ECANA_MSGID28, ECANA_LAM28, ECANA_MSGCTRL28, ECANA_MBX28A,
      ECANA_MBX28B, ECANA_MBX28C, ECANA_MBX28D, },
    { ECANA_MSGID29, ECANA_LAM29, ECANA_MSGCTRL29, ECANA_MBX29A,
      ECANA_MBX29B, ECANA_MBX29C, ECANA_MBX29D, },
    { ECANA_MSGID30, ECANA_LAM30, ECANA_MSGCTRL30, ECANA_MBX30A,
      ECANA_MBX30B, ECANA_MBX30C, ECANA_MBX30D, },
    { ECANA_MSGID31, ECANA_LAM31, ECANA_MSGCTRL31, ECANA_MBX31A,
      ECANA_MBX31B, ECANA_MBX31C, ECANA_MBX31D, }
};
static UINT_16 callbackBoxId = 5U;
static const volatile UINT_16 *callbackRData = ECANA_MBX05A;

/* public functions */

/*!
 * \fn     void canInit (void)
 * \brief  Initialize the eCAN module.
 *
 */
void
canInit(UINT_16 bitrate) {
    UINT_32 timeout = 0UL;
    UINT_16 x;
    UINT_32 shadow;

    /* Enable EALLOW protected register access */
    PICCOLO_EALLOW();

    /* Enable the eCAN-A module clock */
    *PCLKCR0 |= 0x4000U;

    /* Configure the eCAN-A pins */
    /* ECANA_CANTIOC
     * bits 31-4 - (x) Reserved
     * bit  3    - (1) TXFUNC = CANTX
     * bits 2-0  - (x) Reserved
     */
    *ECANA_CANTIOC = 0x00000008UL;
    /* ECANA_CANRIOC
     * bits 31-4 - (x) Reserved
     * bit  3    - (1) RXFUNC = CANRX
     * bits 2-0  - (x) Reserved
     */
    *ECANA_CANRIOC = 0x00000008UL;

    /* CANMC
     * bits 31-17 (x) Reserved
     * bit  16    (1) SUSP - Free Mode
     * bit  15    (0) MBCC - Timestamp Counter Not Reset
     * bit  14    (0) TCC - Timestamp Counter Unchanged
     * bit  13    (1) SCB - eCAN mode
     * bit  12    (0) CCR - Normal Operation
     * bit  11    (0) PDR - Power Down Mode Not Requested
     * bit  10    (1) DBO - Least Significant Byte First
     * bit  9     (1) WUBA - Wake On Bus Activity
     * bit  8     (0) CDR - Normal Operation
     * bit  7     (1) ABO - Auto Bus On
     * bit  6     (0) STM - Normal Operation
     * bit  5     (0) SRES - No Effect
     * bits 4-0   (x) MBNR - Not Used
     */
    shadow = *ECANA_CANMC;
    shadow |= 0x00012680UL;
    *ECANA_CANMC = shadow;

    /* Initialize all bits of MSGCTRLn registers to zero.
     * TI SPRUGL7 expains. */
    /* MSGCTRLxx
     * bits 31-13 - (0) Reserved
     * bits 12-8  - (0) TPL
     * bits 7-5   - (0) Reserved
     * bit  4     - (0) RTR
     * bits 3-0   - (0) DLC
     */
    for (x = 0U; x < 32U; x++) {
        *(eCANA[x].pCtrl) = 0UL;
    }

    /* Initialize all Transmit Acknowledge Register bits to zero.
     * They should be initialized to zero upon reset, this is just to
     * be sure. */
    *ECANA_CANTA = 0xffffffffUL;

    /* Initialize all Received Message Pending Register bits to zero.
     * They should be initialized to zero upon reset, this is just to
     * be sure. */
    *ECANA_CANRMP = 0xffffffffUL;

    /* Initialize all Interrupt Flag Register bits to zero.  They
     * should be initialized to zero upon reset, this is just to be
     * sure. */
    *ECANA_CANGIF0 = 0xffffffffUL;
    *ECANA_CANGIF1 = 0xffffffffUL;

    /* If we're not in configuration mode, request it. */
    shadow = *ECANA_CANES;
    shadow &= 0x00000010UL;
    if (0UL == shadow) {
        shadow = *ECANA_CANMC;
        shadow |= 0x00001000UL;
        *ECANA_CANMC = shadow;
        timeout = 65535UL;
        shadow = *ECANA_CANES;
        shadow &= 0x00000010UL;
        while ((0UL == shadow) && (0UL < timeout)) {
            timeout--;
            PICCOLO_EALLOW();
            shadow = *ECANA_CANES;
            shadow &= 0x00000010UL;
        }
    }

    /* Set bitrate to 125k, 250k, or 500k */
    switch (bitrate) {
    case CAN_CONF_BITRATE_125:
        /* 125kHz bitrate with 75% sample point */
        *ECANA_CANBTC = CAN_CANBTC_125;
        break;
    case CAN_CONF_BITRATE_250:
        /* 250kHz bitrate with 75% sample point */
        *ECANA_CANBTC = CAN_CANBTC_250;
        break;
    case CAN_CONF_BITRATE_500:
        /* 500kHz bitrate with 75% sample point */
        *ECANA_CANBTC = CAN_CANBTC_500;
        break;
    default:
        /* fail silently on bogus input */
        break;
    }

    /* Leave configuration mode (clear ECANA_CANMC.CCR). */
    shadow = *ECANA_CANMC;
    shadow &= ~0x00001000UL;
    *ECANA_CANMC = shadow;

    /* Verify that we've exited configuration mode (CCR == 0) */
    timeout = 65535UL;
    shadow = *ECANA_CANES;
    shadow &= 0x00000010UL;
    while ((0UL != shadow) && (0UL < timeout)) {
        timeout--;
        PICCOLO_EALLOW();
        shadow = *ECANA_CANES;
        shadow &= 0x00000010UL;
    }

    /* Disable all mailboxes */
    *ECANA_CANME = 0UL;
    *ECANA_CANTRS = 0UL;

    /* Mailbox 0 is configured for Transmit */
    /* Mailboxes 1 - 31 are not used but defaulted to receive */
    *ECANA_CANMD = 0xfffffffeUL;

    /* CANGIM
     * bits 31-18 (x) Reserved
     * bit  17    (0) MTOM - Disabled
     * bit  16    (0) TCOM - Disabled
     * bit  15    (x) Reserved
     * bit  14    (0) AAIM - Disabled
     * bit  13    (0) WDIM - Disabled
     * bit  12    (0) WUIM - Disabled
     * bit  11    (0) RMLIM - Disabled
     * bit  10    (0) BOIM - Disabled
     * bit  9     (0) EPIM - Disabled
     * bit  8     (0) WLIM - Disabled
     * bits 7-3   (x) Reserved
     * bit  2     (0) GIL - Disabled
     * bit  1     (1) I1EN - Enabled
     * bit  0     (0) I0EN - Disabled
     */
    shadow = *ECANA_CANGIM;
    shadow |= 0x00000002UL;
    *ECANA_CANGIM = shadow;

    /* Enable interrupts for mailbox 0 */
    shadow = *ECANA_CANMIM;
    shadow |= 0x00000001UL;
    *ECANA_CANMIM = shadow;

    /* Point these interrupts to line 1 */
    shadow = *ECANA_CANMIL;
    shadow |= 0xffffffffUL;
    *ECANA_CANMIL = shadow;

    /* enable the transmit and receive mailboxes */
    shadow = *ECANA_CANME;
    shadow |= 0x00000001UL;
    *ECANA_CANME = shadow;

    /* Disable EALLOW protected register access.  All registers
     * written to after this point are not EALLOW protected. */
    PICCOLO_EDIS();
}

/*!
 * \fn    void canSetMB(const UINT_32 *ids, const UINT_32 *masks, UINT_8 len)
 *
 * \param *ids - array of acceptables arbitration identifiers.
 * \param *masks - array of masks for the identifiers.
 * \param len - number of elements in the passed arrays.(Not to exceed 31)
 * \brief This function will set the acceptance filters and masks to
 *        receive the desired messages in mailboxes 1 through 31.
 */

void
canSetMB(const CANMBInit_t *init) {
    UINT_32 shadow;
    UINT_8 x;
    UINT_32 mbMask;

    PICCOLO_EALLOW();
    for (x = 1U; x <= init->len; x++) {
        shadow = init->ids[x-1U] | 0xc0000000UL;
        *(eCANA[x].pId) = shadow;
        shadow = init->masks[x-1U];
        *(eCANA[x].pLAM) = shadow;
        mbMask = 1UL << x;
        shadow = *ECANA_CANMIM;
        shadow |= mbMask;
        *ECANA_CANMIM = shadow;
        shadow = *ECANA_CANME;
        shadow |= mbMask;
        *ECANA_CANME = shadow;
    }
    PICCOLO_EDIS();
}

/*!
 * \fn       void canTx (CANobj_t *pobj)
 *
 * \param    *pobj - pointer to the data object to be written
 * \brief    This function transmits a single CAN message
 */
void
canTx(const CANobj_t *pobj) {
    UINT_32 shadow;

    /* disable the mailbox */
    shadow = *ECANA_CANME;
    shadow &= ~0x00000001UL;
    *ECANA_CANME = shadow;
    /* load the CAN object */
    *(eCANA[0].pId) = ((pobj->id & 0x1fffffffUL) | 0x80000000UL);
    /* enable the mailbox */
    shadow = *ECANA_CANME;
    shadow |= 0x00000001UL;
    *ECANA_CANME = shadow;
    shadow = *(eCANA[0].pCtrl);
    shadow &= ~0x0000000fUL;
    shadow |= (UINT_32)pobj->dlc;
    *(eCANA[0].pCtrl) = shadow;
    *(eCANA[0].pMBXA) = (UINT_16)pobj->data[0] | ((UINT_16)pobj->data[1] << 8);
    *(eCANA[0].pMBXB) = (UINT_16)pobj->data[2] | ((UINT_16)pobj->data[3] << 8);
    *(eCANA[0].pMBXC) = (UINT_16)pobj->data[4] | ((UINT_16)pobj->data[5] << 8);
    *(eCANA[0].pMBXD) = (UINT_16)pobj->data[6] | ((UINT_16)pobj->data[7] << 8);
    /* send the message on its way */
    *ECANA_CANTRS = 0x00000001UL;
}

/*!
 * \fn       void canTAHandler (void)
 *
 * \brief    This function is executed upon completion of a CAN tranmission
 *           to notify upper layers that the bus is free to transmit.
 *           (Note: this function is executed from RAM)
 */
#pragma CODE_SECTION(canTAHandler, "secureRamFuncs")
void
canTAHandler (void) {
    UINT_32 shadow;

    /* clear the transmit acknowledge bit */
    shadow = *ECANA_CANTA;
    shadow |= 0x00000001UL;
    *ECANA_CANTA = shadow;

    jdlTxConfirmation();
}

/*!
 * \fn    void canRMHandler(UINT_16 mbId)
 *
 * \param mbId - number of mailbox that triggered the Rx interrupt.
 * \brief This function processes the received CAN message into the
 *        J1939 Rx queue.
 */
void
canRMHandler(UINT_16 mbId) {
    CANobj_t tempMsg;
    UINT_32 mbMask = 1UL << mbId;
    UINT_32 shadow;

    /* load id to temp variable */
    tempMsg.id = (UINT_32)*(eCANA[mbId].pId) & 0x1fffffffUL;
    /* load dlc to temp variable */
    tempMsg.dlc = (UINT_16)(*(eCANA[mbId].pCtrl) & 0x0000000fUL);
    /* load data to temp array */
    tempMsg.data[0] = *(eCANA[mbId].pMBXA) & 0xffU;
    tempMsg.data[1] = (*(eCANA[mbId].pMBXA) >> 8) & 0xffU;
    tempMsg.data[2] = *(eCANA[mbId].pMBXB) & 0xffU;
    tempMsg.data[3] = (*(eCANA[mbId].pMBXB) >> 8) & 0xffU;
    tempMsg.data[4] = *(eCANA[mbId].pMBXC) & 0xffU;
    tempMsg.data[5] = (*(eCANA[mbId].pMBXC) >> 8) & 0xffU;
    tempMsg.data[6] = *(eCANA[mbId].pMBXD) & 0xffU;
    tempMsg.data[7] = (*(eCANA[mbId].pMBXD) >> 8) & 0xffU;
    /* release the mailbox */
    shadow = *ECANA_CANRMP;
    shadow |= mbMask;
    *ECANA_CANRMP = shadow;

    jdlCANToJ1939(&tempMsg);
}

/*!
 * \fn    void canFlashCallBack(void)
 *
 * \brief This function handles can responses while the FlashAPI
 *        routines are being executed.
 *        (Note: This function is executed from RAM)
 */
#pragma CODE_SECTION (canFlashCallBack, "secureRamFuncs")
void
canFlashCallBack(void) {
    UINT_32 shadow;
    UINT_16 data;
    UINT_32 boxMask = 1UL << callbackBoxId;

    /* check for a received message */
    shadow = *ECANA_CANRMP;
    if (boxMask == (shadow & boxMask)) {
        data = *callbackRData;
        data &= 0x0f00U;
        data = data >> 8;
        if (0x07U == data) {
            shadow = *ECANA_CANME;
            shadow &= ~0x00000001UL;
            *ECANA_CANME = shadow;
            *ECANA_MSGID00 = 0x98d8f913UL;
            shadow = *ECANA_CANME;
            shadow |= 0x00000001UL;
            *ECANA_CANME = shadow;
            shadow = *ECANA_MSGCTRL00;
            shadow &= ~0x0000000fUL;
            shadow |= 8UL;
            *ECANA_MSGCTRL00 = shadow;
            *ECANA_MBX00A = 0x1300U;
            *ECANA_MBX00B = 0x0010U;
            *ECANA_MBX00C = 0x0000U;
            *ECANA_MBX00D = 0x0000U;
            PICCOLO_EALLOW ();
            shadow = *ECANA_CANMIM;
            shadow &= ~0x00000001UL;
            *ECANA_CANMIM = shadow;
            PICCOLO_EDIS ();
            *ECANA_CANTRS = 0x00000001UL;
            shadow = *ECANA_CANTA;
            while (0UL == (shadow & 0x00000001UL)) {
                shadow = *ECANA_CANTA;
            }
            shadow |=  0x00000001UL;
            *ECANA_CANTA = shadow;
            PICCOLO_EALLOW ();
            shadow = *ECANA_CANMIM;
            shadow |= 0x00000001UL;
            *ECANA_CANMIM = shadow;
            PICCOLO_EDIS ();
        }
        shadow = *ECANA_CANRMP;
        shadow |= boxMask;
        *ECANA_CANRMP = shadow;
    }
}

void
canSetCallBackBox(UINT_16 boxID) {
    callbackBoxId = boxID;
    callbackRData = eCANA[callbackBoxId].pMBXA;
}
