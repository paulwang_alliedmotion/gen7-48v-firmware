/*!
 * \file    eeprom.c
 * \author  Zach Haver
 * \brief   I2C EEPROM Driver
 *
 */

/* $Id: $ */
/* $Name: $ */

/* NOTICE: This file is the property of Globe Motors and contains */
/* CONFIDENTIAL information.  Any use, copying or dissemination of the */
/* information contained in this file is strictly prohibited unless */
/* authorized by Globe Motors in writing. */

/* Copyright 2016, Globe Motors, Inc. */

/*
 * Revision History:
 *
 * $Log: $
 *
 */

#include "dtc.h"
#include "eeprom.h"
#include "i2c.h"
#include "misra.h"
#include "piccolo.h"

/* private function declarations */
static EEErr_t eepromAddressCheck(UINT_16 address, EEAccess_t access);

/* public functions */
EEErr_t
eepromReadXBytes(UINT_16 addr, UINT_8 *data, UINT_16 len) {
    EEErr_t ret = EE_ERR_ADDR_ILLEGAL;

    if (EE_ERR_NONE == eepromAddressCheck(addr, EE_WORD_ACCESS)) {
        if (I2C_RET_NO_ERROR == i2cRxAndWait(data, addr, len)) {
            ret = EE_ERR_NONE;
        } else {
            dtcSet(DTCHW_EEPROM);
            ret = EE_ERR_I2C_FAIL;
        }
    }
    return ret;
}

/*!
 * \fn     EEErr_t eepromRead32Bytes (UINT_16 addr, UINT_8 *data)
 * \param  addr eeprom address of the data to be read
 * \param  data pointer used to return the read data array
 * \return EE_ERR_NONE if no errors occurred
 * \return EE_ERR_I2C_FAIL if the I2C Read or Write fails
 * \return EE_ERR_ADDR_ILLEGAL if the address is out of range
 * \brief  This function reads 32 bytes of data from the eeprom
 */
EEErr_t
eepromRead32Bytes(UINT_16 addr, UINT_8 *data) {
    EEErr_t ret;

    if (EE_ERR_NONE == eepromAddressCheck(addr, EE_PAGE_ACCESS)) {
        if (I2C_RET_NO_ERROR == i2cRxAndWait(data, addr, 32U)) {
            ret = EE_ERR_NONE;
        } else {
            dtcSet(DTCHW_EEPROM);
            ret = EE_ERR_I2C_FAIL;
        }
    } else {
        ret = EE_ERR_ADDR_ILLEGAL;
    }

    return ret;
}

/*!
 * \fn     EEErr_t eepromRead32Word (UINT_16 addr, UINT_16 *data)
 * \param  addr eeprom address of the data to be read
 * \param  data pointer used to return the read data array
 * \return EE_ERR_NONE if no errors occurred
 * \return EE_ERR_I2C_FAIL if the I2C Read or Write fails
 * \return EE_ERR_ADDR_ILLEGAL if the address is out of range
 * \brief  This function reads 32 words of data from the eeprom
 */
EEErr_t
eepromRead32Words(UINT_16 addr, UINT_16 *data) {
    EEErr_t ret;
    UINT_8 bytes[64] = {0U};
    UINT_16 x;
    UINT_16 word;
    UINT_16 hi, lo;

    if (EE_ERR_NONE == eepromAddressCheck(addr, EE_PAGE_ACCESS)) {
        if (I2C_RET_NO_ERROR == i2cRxAndWait(bytes, addr, 64U)) {
            for (x = 0U; x < 32U; x++) {
                lo = x * 2U;
                hi = lo + 1U;
                word = (UINT_16)bytes[lo] | ((UINT_16)bytes[hi] << 8);
                data[x] = word;
            }
            ret = EE_ERR_NONE;
        } else {
            dtcSet(DTCHW_EEPROM);
            ret = EE_ERR_I2C_FAIL;
        }
    } else {
        ret = EE_ERR_ADDR_ILLEGAL;
    }

    return ret;
}

/*!
 * \fn     EEErr_t eepromWriteWord (UINT_16 addr, UINT_16 data)
 * \param  addr eeprom address of the data to be written
 * \param  data data to be written
 * \return EE_ERR_NONE if no errors occurred
 * \return EE_ERR_I2C_FAIL if the I2C Read or Write fails
 * \return EE_ERR_ADDR_ILLEGAL if the address is out of range
 * \brief  This function writes a word of data to the eeprom
 */
EEErr_t
eepromWriteWord(UINT_16 addr, UINT_16 data) {
    UINT_8 dat[2];
    EEErr_t ret;

    if (EE_ERR_NONE == eepromAddressCheck(addr, EE_WORD_ACCESS)) {
        dat[0] = (UINT_8)data;
        dat[1] = (UINT_8)(data >> 8);
        if (I2C_RET_NO_ERROR == i2cTxAndWait(dat, addr, 2U)) {
            ret = EE_ERR_NONE;
        } else {
            dtcSet(DTCHW_EEPROM);
            ret = EE_ERR_I2C_FAIL;
        }
    } else {
        ret = EE_ERR_ADDR_ILLEGAL;
    }

    return ret;
}

/*!
 * \fn     EEErr_t eepromWriteDWord (UINT_16 addr, UINT_32 data)
 * \param  addr eeprom address of the data to be written
 * \param  data data to be written
 * \return EE_ERR_NONE if no errors occurred
 * \return EE_ERR_I2C_FAIL if the I2C Read or Write fails
 * \return EE_ERR_ADDR_ILLEGAL if the address is out of range
 * \brief  This function writes a double word of data to the eeprom
 */
EEErr_t
eepromWriteDWord(UINT_16 addr, UINT_32 data) {
    UINT_8 dat[4];
    EEErr_t ret;

    if (EE_ERR_NONE == eepromAddressCheck(addr, EE_WORD_ACCESS)) {
        dat[0] = (UINT_8)data;
        dat[1] = (UINT_8)(data >> 8);
        dat[2] = (UINT_8)(data >> 16);
        dat[3] = (UINT_8)(data >> 24);
        if (I2C_RET_NO_ERROR == i2cTxAndWait(dat, addr, 4U)) {
            ret = EE_ERR_NONE;
        } else {
            dtcSet(DTCHW_EEPROM);
            ret = EE_ERR_I2C_FAIL;
        }
    } else {
        ret = EE_ERR_ADDR_ILLEGAL;
    }

    return ret;
}

/*!
 * \fn     EEErr_t eepromWritePage16 (UINT_16 addr, UINT_8 *data)
 * \param  addr eeprom address of the data to be written
 * \param  data data to be written
 * \return EE_ERR_NONE if no errors occurred
 * \return EE_ERR_I2C_FAIL if the I2C Read or Write fails
 * \return EE_ERR_ADDR_ILLEGAL if the address is out of range
 * \brief  This function writes a page (32 bytes) of data to the eeprom
 */
EEErr_t
eepromWrite16Bytes(UINT_16 addr, const UINT_8 *data) {
    EEErr_t ret;

    if (EE_ERR_NONE == eepromAddressCheck(addr, EE_PAGE_ACCESS)) {
        if (I2C_RET_NO_ERROR == i2cTxAndWait(data, addr, 16U)) {
            ret = EE_ERR_NONE;
        } else {
            dtcSet(DTCHW_EEPROM);
            ret = EE_ERR_I2C_FAIL;
        }
    } else {
        ret = EE_ERR_ADDR_ILLEGAL;
    }

    return ret;
}

/*!
 * \fn     EEErr_t eepromAddressCheck(UINT_16 address, EEAccess_t access)
 * \param  address - eeprom address to be checked
 * \param  access - type of EEPROM access
 * \return EE_ERR_NONE if no errors occurred
 * \return EE_ERR_ADDR_ILLEGAL if the address is out of range
 * \brief  This function checks the passed address for the access type
 *         to make sure it is a valid address to access.
 */
static EEErr_t
eepromAddressCheck(UINT_16 address, EEAccess_t access) {
    EEErr_t ret = EE_ERR_NONE;

    switch (access) {
    case EE_WORD_ACCESS:
        if (0U != (address % 2U)) {
            ret = EE_ERR_ADDR_ILLEGAL;
        }
        break;
    case EE_PAGE_ACCESS:
        if (0U != (address % 16U)) {
            ret = EE_ERR_ADDR_ILLEGAL;
        }
        break;
    default:
        ret = EE_ERR_ADDR_ILLEGAL;
        break;
    }

    return ret;
}
