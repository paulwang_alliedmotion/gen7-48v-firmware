/*!
 * \file      boot.h
 * \author    Zach Haver
 * \brief     Bootkernel main and initialization definitions
 */

/* $Id: $*/
/* $Name: $ */

/* NOTICE: This file is the property of Globe Motors and contains */
/* CONFIDENTIAL information.  Any use, copying or dissemination of the */
/* information contained in this file is strictly prohibited unless */
/* authorized by Globe Motors in writing. */

/* Copyright 2016, Globe Motors, Inc. */

/*
 * Revision History:
 *
 * $Log: $
 *
 */

#ifndef BOOT_H
#define BOOT_H

/*! /def BOOT_NONE - flag indicating no special action needs to be taken. */
#define BOOT_NONE             0U
/*! /def BOOT_REQUESTRED - flag indicating the app has requested the boot. */
#define BOOT_REQUESTED        1U
/*! /def BOOT_RESET - flag indicating a reset was requested from the app. */
#define BOOT_RESET            2U
/*! /def BOOT_COUNT_LIMIT - theshold yields the number of power cycles
         to allow reflash attempts before trying the application again */
#define BOOT_COUNT_LIMIT     25U
/*! /def BOOT_COUNT_THRESHOLD - number of failed application loads to allow
         before stopping to allow reflash attempts */
#define BOOT_COUNT_THRESHOLD 21U
/*! /def BOOT_TO_APP - MACRO to branch to the start of the application */
#define BOOT_TO_APP  asm(" LB 3e8000h")

#endif /* BOOT_H */
