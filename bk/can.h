/*!
 * \file        can.h
 * \author      Zach Haver
 * \brief       eCAN module initialization definitions
 *              for the TI TMS320C28035 Piccolo.
 */

/* $Id: $ */
/* $Name: $ */

/* NOTICE: This file is the property of Globe Motors and contains */
/* CONFIDENTIAL information.  Any use, copying or dissemination of the */
/* information contained in this file is strictly prohibited unless */
/* authorized by Globe Motors in writing. */

/* Copyright 2016, Globe Motors, Inc. */

/*
 * Revision History:
 *
 * $Log: $
 *
 */

#ifndef CAN_H
#define CAN_H

#include "misra.h"

/*! \def CAN_CONF_BITRATE_125 - bitrate config switch variable for 125kBPS. */
#define CAN_CONF_BITRATE_125     125U
/*! \def CAN_CONF_BITRATE_250 - bitrate config switch variable for 250kBPS. */
#define CAN_CONF_BITRATE_250     250U
/*! \def CAN_CONF_BITRATE_500 - bitrate config switch variable for 500kBPS. */
#define CAN_CONF_BITRATE_500     500U

/*
 * #define values for the CANBTC register for various bitrates.
 *
 * Based upon a SYSCLCK of 60 MHz.
 *
 * The configuration for the bitrates of 125k 250k and 500k all use a BT
 * value of 12, all use a TSEG1 of 10 and a TSEG2 of 2, for a
 * sample point of 83.3%.
 *
 * Refer to TI document SPRUEU0, TMS320x280x/2801x eCAN Reference
 * Guide, Section 2.12, 3.1.1 and 3.1.2.
 *
 *    bit 31-24 : Reserved, write 0s
 *    bit 23-16 : BRP(reg)
 *    bit 15-10 : Reserved, write 0s
 *    bit 9-8   : SJW = 0, Synchronization Jump Width = (SJW + 1) = 1 TQ
 *    bit 7     : SAM = 0, the CAN module samples only once at the
 *                sampling point
 *    bit 6-3   : TSEG1
 *    bit 2-0   : TSEG2
 */
/*! \def CAN_CANBTC_125 - bit timing register settings for 125kBPS. */
#define CAN_CANBTC_125  ((19UL << 16) | (8UL << 3) | 1UL)
/*! \def CAN_CANBTC_250 - bit timing register settings for 250kBPS. */
#define CAN_CANBTC_250  ((9UL << 16) | (8UL << 3) | 1UL)
/*! \def CAN_CANBTC_500 - bit timing register settings for 500kBPS. */
#define CAN_CANBTC_500   ((4UL << 16) | (8UL << 3) | 1UL)

/*! \struct CANMBInit_t strructure to hold initialization info.
 *          (Note: only 31 message filters and masks can be set.)
 */
typedef struct {
    UINT_32 ids[31];
    UINT_32 masks[31];
    UINT_8 len;
}CANMBInit_t;

/*! \struct CANobj_t structure to hold information on a CAN message. */
typedef struct {
    UINT_32 id;                     /*!< Arbitration Id */
    UINT_16 dlc;                    /*!< Data Length Count */
    UINT_8 data[8];                 /*!< Message Data */
} CANobj_t;

/* public function prototypes */
void canInit(UINT_16 bitrate);
void canSetMB(const CANMBInit_t *init);
void canTx(const CANobj_t *pobj);
void canTAHandler(void);
void canRMHandler(UINT_16 mbId);
void canFlashCallBack(void);
void canSetCallBackBox(UINT_16 boxID);

#endif /* CAN_H */
