/*!
 * \file      fkernel.h
 * \author    Zach Haver
 * \brief     Flash kernel definitions for the TMS320C28035.
 */

/* $Id: $ */
/* $Name: $ */

/* NOTICE: This file is the property of Globe Motors and contains */
/* CONFIDENTIAL information.  Any use, copying or dissemination of the */
/* information contained in this file is strictly prohibited unless */
/* authorized by Globe Motors in writing. */

/* Copyright 2016, Globe Motors, Inc. */

/*
 * Revision History:
 *
 * $Log: $
 *
 */

#ifndef FKERNEL_H
#define FKERNEL_H

#include "misra.h"

/*! \enum  FKReason_t
 *  \brief Reason for entering the flash kernel.
 */
typedef enum {
    FK_REQUESTED,  /*!< FK_REQUESTED - Enter due app to request. */
    FK_EEPROM_FAIL,/*!< FK_EEPROM_FAIL - Enter due to EE failure. */
    FK_BOOT_COUNT, /*!< FK_BOOT_COUNT - Enter due to Boot Count overflow. */
    FK_CRC_APP     /*!< FK_CRC_APP - Enter due to invalid app crc. */
} FKReason_t;

/* function prototypes */
void fkernelRun(FKReason_t reason, UINT_16 bootStatus);

#endif /* FKERNEL_H */
